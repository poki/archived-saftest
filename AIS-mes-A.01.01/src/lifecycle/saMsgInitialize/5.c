/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>

int main(int argc, char *argv[])
{
	SaErrorT 	error;
	SaMsgHandleT	msghandle[5];
	SaMsgCallbacksT	msgcallback = {
		.saMsgQueueOpenCallback = NULL,
		.saMsgQueueGroupTrackCallback = NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = NULL
	};
	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = RELEASE_CODE
	};

	int i, j;

	int ret = SAF_TEST_PASS;

	for ( i = 0; i < 5; i++){
		error = saMsgInitialize(&msghandle[i], &msgcallback, &version);
		if (error != SA_OK){
			printf("  Does not conform the expected behaviors!\n");
			printf("  saMsgInitialize, Return value: %s, should be SA_OK\n"
				,	get_aisAerror_string(error));
			ret = SAF_TEST_FAIL;
			goto out;
		}
		for ( j = 0; j < i; j++){
			if (msghandle[i] == msghandle[j]){
				printf("  Does not conform the expected behaviros!\n");
				printf("  The handle is same\n");
				ret = SAF_TEST_FAIL;
				break;
			}
		}
	}
out:
	for ( j = 0; j <= i; j++)
		saMsgFinalize(&msghandle[j]);
	if (i != 5)
		ret = SAF_TEST_FAIL;

	return ret;
}
