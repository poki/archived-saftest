/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>

int main(int argc, char *argv[])
{
	SaErrorT 	error;
	int ret = SAF_TEST_PASS;
	SaSelectionObjectT selectionobject;

	error = saMsgSelectionObjectGet(NULL, &selectionobject);
	if (error != SA_ERR_BAD_HANDLE){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmSelectionObjectGet, Return value: %s,should be SA_ERR_BAD_HANDLE\n"
		,	get_aisAerror_string(error));
                ret = SAF_TEST_FAIL;
	}
	return ret;
}
