/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void
queue_open_callback(SaInvocationT invocation,
		    const SaMsgQueueHandleT *queuehandle,
		    SaErrorT error);
void
queue_open_callback(SaInvocationT invocation,
		    const SaMsgQueueHandleT *queuehandle,
		    SaErrorT error)
{
	return ;
}

int main(int argc, char *argv[])
{
	SaErrorT	error;
	SaMsgHandleT	msghandle;
	SaMsgCallbacksT msgcallback = {
		.saMsgQueueOpenCallback =
			(SaMsgQueueOpenCallbackT)queue_open_callback,
		.saMsgQueueGroupTrackCallback = NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = NULL
	};
	
	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = RELEASE_CODE
	};

	SaInvocationT invocation;
	SaNameT async_qname;
	SaSelectionObjectT sobj;
	fd_set fset;
	int nfd;
	SaMsgQueueCreationAttributesT cattr;
	SaDispatchFlagsT dFlag = SA_DISPATCH_ALL;

	strcpy(async_qname.value, "async_name");
	async_qname.length = strlen(async_qname.value);

	cattr.creationFlags = 0;
	cattr.retentionTime = 0;
	cattr.size[0] = 1000;
	cattr.size[1] = 1000;
	cattr.size[2] = 1000;
	cattr.size[3] = 1000;

	int ret = SAF_TEST_PASS;

	error = saMsgInitialize(&msghandle, &msgcallback, &version);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saMsgInitialize, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saMsgSelectionObjectGet(&msghandle, &sobj);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saMsgSelectionObjectGet, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	error = saMsgQueueOpenAsync(&msghandle, invocation, &async_qname, &cattr
		,	SA_MSG_QUEUE_PERSISTENT);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saMsgQueueOpenAsync, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	FD_ZERO (&fset);
	FD_SET(sobj, &fset);
	nfd = sobj + 1;
	
	if (select(nfd, &fset, NULL, NULL, NULL) == -1){
		printf("select erro\n");
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	if (!FD_ISSET(sobj, &fset)){
		printf("FD_ISSET error\n");
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	error=saMsgDispatch(&msghandle, dFlag);
	if (error != SA_OK){
		printf("  Does not conform the expectec behaviors!\n");
		printf("  saMsgDispatch, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
		ret = SAF_TEST_FAIL;
	}

final:
	error = saMsgFinalize(&msghandle);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saMsgFinalize, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
	}
out:
		return ret;
}
