/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void error_print(char *api_name, int error, char *should_be)
{
        printf("  Does not conform the expected behaviors!\n");
        printf("  %s, Return value:%s, should be %s\n"
		,	api_name, get_aisAerror_string(error), should_be);
}

int
main(int argc, char * argv[])
{
        SaErrorT        error;
	SaNameT groupname;
	strcpy(groupname.value, "saf_creategroup3");
	groupname.length = strlen(groupname.value);

	int ret = SAF_TEST_PASS;

	error = saMsgQueueGroupCreate(&groupname, SA_MSG_QUEUE_GROUP_ROUND_ROBIN);
	if (error != SA_ERR_INIT){
		error_print("saMsgQueueGroupCreate", error, "SA_ERR_INIT");
		ret = SAF_TEST_FAIL;
	}

	return ret;
}
