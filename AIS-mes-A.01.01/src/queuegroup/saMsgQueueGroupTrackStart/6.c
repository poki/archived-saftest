/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
group_track_callback(const SaNameT *queueGroupName,
			const SaMsgQueueGroupNotificationBufferT *notificationBuffer,
			SaMsgQueueGroupPolicyT queueGroupPolicy,
			SaUint32T numberOfItems,
			SaUint32T numberOfMembers,
			SaErrorT error)
{
	printf("Just a callback for track\n");
	return;
}

void error_print(char *api_name, int error, char *should_be)
{
        printf("  Does not conform the expected behaviors!\n");
        printf("  %s, Return value:%s, should be %s\n"
		,	api_name, get_aisAerror_string(error), should_be);
}

int
main(int argc, char * argv[])
{
        SaErrorT        error;
        SaMsgHandleT    msghandle;
        SaMsgCallbacksT msgcallback = {
                .saMsgQueueOpenCallback = NULL,
                .saMsgQueueGroupTrackCallback =
			(SaMsgQueueGroupTrackCallbackT)group_track_callback,
                .saMsgMessageDeliveredCallback = NULL,
                .saMsgMessageReceivedCallback = NULL
        };
        SaVersionT      version = {
                .major = VERSION_MAJOR,
                .minor = VERSION_MINOR,
                .releaseCode = 'A'
        };
        int ret = SAF_TEST_PASS;

        error = saMsgInitialize(&msghandle, &msgcallback, &version);
        if (error != SA_OK){
		error_print("saMsgInitialize", error, "SA_OK");
                ret = SAF_TEST_UNRESOLVED;
                goto out;
        }

	SaUint32T numberOfItem = 100;
	SaMsgQueueGroupNotificationT nbuf[100];
	SaNameT groupname;
	strcpy(groupname.value, "saf_trackstart6");
	groupname.length = strlen(groupname.value);

	SaUint8T trackFlag = SA_TRACK_CHANGES | SA_TRACK_CHANGES_ONLY;
	error = saMsgQueueGroupCreate(&groupname, SA_MSG_QUEUE_GROUP_ROUND_ROBIN);
	if (error != SA_OK){
		error_print("saMsgQueueGroupCreate", error, "SA_OK");
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	error = saMsgQueueGroupTrackStart(&msghandle, &groupname, trackFlag, &nbuf, numberOfItem);
	if (error != SA_ERR_BAD_FLAGS && error != SA_ERR_INVALID_PARAM){
		error_print("saMsgQueueGroupTrackStart", error
			,	"SA_ERR_BAD_FLAGS or SA_ERR_INVALID_PARAM");
		ret = SAF_TEST_FAIL;
	}

	error = saMsgQueueGroupDelete(&groupname);
	if (error != SA_OK)
		error_print("saMsgQueueGroupDelete", error, "SA_OK");

final:	error = saMsgFinalize(&msghandle);
	if (error != SA_OK)
		error_print("saMsgFinalize", error, "SA_OK");
out:
	return ret;
}
