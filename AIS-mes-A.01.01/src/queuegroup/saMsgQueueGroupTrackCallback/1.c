/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

static int exit_select = 0;
int action_ok = 0;

void error_print(char *api_name, int error, char *should_be)
{
	printf("  Does not conform the expected behaviors!\n");
	printf("  %s, Return value:%s, should be %s\n"
		,	api_name, get_aisAerror_string(error), should_be);
	return;
}

static void
group_track_callback(const SaNameT *queueGroupName,
			const SaMsgQueueGroupNotificationBufferT *notificationBuffer,
			SaMsgQueueGroupPolicyT queueGroupPolicy,
			SaUint32T numberOfItems,
			SaUint32T numberOfMembers,
			SaErrorT error)
{
	if( error != SA_OK){
		error_print("saMsgQueueGroupTrackCallback", error, "SA_OK");
		return;
	}
	exit_select = 1;
	action_ok = 1;
	return;
}

int
main(int argc, char * argv[])
{
        SaErrorT        error;
        SaMsgHandleT    msghandle;
        SaMsgCallbacksT msgcallback = {
                .saMsgQueueOpenCallback = NULL,
                .saMsgQueueGroupTrackCallback
			= (SaMsgQueueGroupTrackCallbackT)group_track_callback,
                .saMsgMessageDeliveredCallback = NULL,
                .saMsgMessageReceivedCallback = NULL
        };
        SaVersionT      version = {
                .major = VERSION_MAJOR,
                .minor = VERSION_MINOR,
                .releaseCode = 'A'
        };
	SaSelectionObjectT selectionobject;
        int ret = SAF_TEST_PASS;

        error = saMsgInitialize(&msghandle, &msgcallback, &version);
        if (error != SA_OK){
		error_print("saMsgInitialize", error, "SA_OK");
                ret = SAF_TEST_UNRESOLVED;
                goto out;
        }

	error = saMsgSelectionObjectGet(&msghandle, &selectionobject);
	if (error != SA_OK){
		error_print("saMsgSelectionObjectGet", error, "SA_OK");
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	SaMsgQueueGroupNotificationT nbuf[100];
	SaUint32T numberOfItem = 100;
	SaNameT groupname;
	strcpy(groupname.value, "saf_trackcb1");
	groupname.length = strlen(groupname.value);

	SaUint8T trackFlag = SA_TRACK_CURRENT;
	error = saMsgQueueGroupCreate(&groupname, SA_MSG_QUEUE_GROUP_ROUND_ROBIN);
	if (error != SA_OK){
		error_print("saMsgQueueGroupCreate", error, "SA_OK");
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	error = saMsgQueueGroupTrackStart(&msghandle, &groupname, trackFlag, &nbuf, numberOfItem);
	if (error != SA_OK){
		error_print("saMsgQueueGroupTrackStart", error, "SA_OK");
		ret = SAF_TEST_FAIL;
	}

	for (;;){
		fd_set rset;
		FD_ZERO(&rset);
		FD_SET(selectionobject, &rset);

		if (select(selectionobject + 1, &rset, NULL, NULL, NULL) == -1){
			if (errno == EINTR)
				continue;
			else{
				printf("select error\n");
				ret = SAF_TEST_UNRESOLVED;
				break;
			}
			if (exit_select){
				break;
			}
		}
		error = saMsgDispatch(&msghandle, SA_DISPATCH_ALL);
		if (error != SA_OK){
			error_print("saMsgDispatch", error, "SA_OK");
			ret = SAF_TEST_UNRESOLVED;
			break;
		}
	}

	if (action_ok != 1)
		ret = SAF_TEST_FAIL;
	
	error = saMsgQueueGroupDelete(&groupname);
	if (error != SA_OK)
		error_print("saMsgQueueGroupDelete", error, "SA_OK");

final:	error = saMsgFinalize(&msghandle);
	if (error != SA_OK)
		error_print("saMsgFinalize", error, "SA_OK");
out:
	return ret;
}
