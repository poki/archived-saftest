/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <string.h>

void error_print(char *api_name, int error, char *should_be)
{
        printf("  Does not conform the expected behaviors!\n");
        printf("  %s, Return value:%s, should be %s\n"
		,	api_name, get_aisAerror_string(error), should_be);
}

static void
msg_recv_cb(const SaMsgQueueHandleT *queueHandle,
		const SaMsgMessageHandleT *messageHandle,
		SaSizeT size)
{
	return;
}

int main(int argc, char *argv[])
{
	SaErrorT 	error;
	SaMsgHandleT	msghandle;
	SaMsgCallbacksT	msgcallback = {
		.saMsgQueueOpenCallback	= NULL,
		.saMsgQueueGroupTrackCallback = NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = msg_recv_cb
	};

	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = RELEASE_CODE
	};

	SaNameT qname;
	strcpy(qname.value, "saf_qopen7");
	qname.length = strlen(qname.value);

	SaSizeT qsize = 1000;
	SaMsgQueueCreationAttributesT attr;
	attr.creationFlags = -1;
	attr.size[0] = qsize;
	attr.size[1] = qsize;
	attr.size[2] = qsize;
	attr.size[3] = qsize;
	attr.retentionTime = 0;

	SaMsgQueueOpenFlagsT flag = SA_MSG_QUEUE_RECEIVE_CALLBACK;
	SaMsgQueueHandleT qhandle;
	int ret = SAF_TEST_PASS;

	error = saMsgInitialize(&msghandle, &msgcallback, &version);
	if (error != SA_OK){
		error_print("saMsgInitialize", error, "SA_OK");
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saMsgQueueOpen(&msghandle, &qname, &attr, flag, &qhandle, SA_TIME_END);
	if(error != SA_ERR_BAD_FLAGS && error != SA_ERR_INVALID_PARAM){
		error_print("saMsgQueueOpen", error, "SA_ERR_BAD_FLAGS");
		ret = SAF_TEST_FAIL;
	}
	
	error =	saMsgFinalize(&msghandle);
	if (error != SA_OK)
		error_print("saMsgQueueFinalize", error, "SA_OK");

out:
	return ret;
}
