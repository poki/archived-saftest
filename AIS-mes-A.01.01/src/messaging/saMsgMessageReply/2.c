/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

static void
msg_recv_cb(const SaMsgQueueHandleT *queueHandle,
		const SaMsgMessageHandleT *messageHandle,
		SaSizeT size)
{
        return;
}

void error_print(char *api_name, int error, char *should_be)
{
	printf("  Does not conform the expected behaviors!\n");
	printf("  %s, Return value:%s, should be %s\n",
		 api_name, get_aisAerror_string(error), should_be);
}

int main(int argc, char *argv[])
{
	SaErrorT 	error;
	SaMsgHandleT	msghandle;
	SaMsgCallbacksT	msgcallback = {
		.saMsgQueueOpenCallback	= NULL,
		.saMsgQueueGroupTrackCallback	= NULL,
		.saMsgMessageDeliveredCallback	= NULL,
		.saMsgMessageReceivedCallback
		=	(SaMsgMessageReceivedCallbackT)msg_recv_cb
	};
	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = RELEASE_CODE
	};
	SaNameT qname;
	strcpy(qname.value, "saf_reply2");
	qname.length = strlen(qname.value);

	int ret = SAF_TEST_PASS;

	error = saMsgInitialize(&msghandle, &msgcallback, &version);
	if (error != SA_OK){
		error_print("saMsgInitialize", error, "SA_OK");
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}
	SaMsgMessageT message_reply;
	SaMsgMessageInfoT replyInfo;
	replyInfo.senderName = qname;

	(char *)message_reply.data = "I send reply message!";
	message_reply.priority = SA_MSG_MESSAGE_LOWEST_PRIORITY;
	message_reply.size = strlen((char *)message_reply.data) + 1;
	error = saMsgMessageReply(&message_reply, &replyInfo,
		SA_MSG_MESSAGE_DELIVERED_ACK, SA_TIME_END);
	if (error != SA_ERR_NOT_EXIST){
		error_print("saMsgMessageReply", error, "SA_ERR_NOT_EXIST");
		ret = SAF_TEST_FAIL;
	}
	
	error = saMsgFinalize(&msghandle);
	if(error != SA_OK)
		error_print("saMsgFinalize", error, "SA_OK");

out:
	return ret;
}
