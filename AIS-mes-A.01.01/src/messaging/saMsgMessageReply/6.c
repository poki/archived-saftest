/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

static void
msg_recv_cb(const SaMsgQueueHandleT *queueHandle,
		const SaMsgMessageHandleT *messageHandle,
		SaSizeT size)
{
        return;
}

void error_print(char *api_name, int error, char *should_be)
{
	printf("  Does not conform the expected behaviors!\n");
	printf("  %s, Return value:%s, should be %s\n",
		 api_name, get_aisAerror_string(error), should_be);
}

int main(int argc, char *argv[])
{
	SaErrorT 	error;
	SaMsgHandleT	msghandle;
	SaMsgCallbacksT	msgcallback = {
		.saMsgQueueOpenCallback	= NULL,
		.saMsgQueueGroupTrackCallback	= NULL,
		.saMsgMessageDeliveredCallback	= NULL,
		.saMsgMessageReceivedCallback
		=	(SaMsgMessageReceivedCallbackT)msg_recv_cb
	};
	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = RELEASE_CODE
	};
	SaNameT qname;
	strcpy(qname.value, "saf_reply6");
	qname.length = strlen(qname.value);

	SaSizeT qsize = 30;
        SaMsgQueueCreationAttributesT attr;
        attr.creationFlags = SA_MSG_QUEUE_PERSISTENT;
        attr.size[0] = qsize;
        attr.size[1] = qsize;
        attr.size[2] = qsize;
        attr.size[3] = qsize;
        attr.retentionTime = 0;

	SaMsgQueueOpenFlagsT openFlag = SA_MSG_QUEUE_RECEIVE_CALLBACK;
	SaMsgQueueHandleT qhandle;

	int ret = SAF_TEST_PASS;

	error = saMsgInitialize(&msghandle, &msgcallback, &version);
	if (error != SA_OK){
		error_print("saMsgInitialize", error, "SA_OK");
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saMsgQueueOpen(&msghandle, &qname, &attr, openFlag, &qhandle, SA_TIME_END);
	if (error != SA_OK){
		error_print("saMsgQueueOpen", error, "SA_OK");
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	pid_t pid;
	if ((pid = fork()) < 0 ){
		printf("fork a process error!\n");
		goto unlink;
	}else if (pid == 0){
		SaMsgMessageT message_send, message_receive;
		/* prepare a message to send */
		(char *)message_send.data = "I send this message to you!";
		message_send.priority = SA_MSG_MESSAGE_LOWEST_PRIORITY;
		message_send.size = strlen((char *) message_send.data) + 1;

		char buff[30];
		bzero(&message_receive, sizeof(SaMsgMessageT));
		bzero(buff, 30);
		message_receive.data = buff;
		message_receive.size = 30;
		SaMsgMessageInfoT recvInfo;
		recvInfo.senderName = qname;

		error = saMsgMessageSendReceive(&qname, &message_send, &message_receive
			, &recvInfo, SA_TIME_END, SA_MSG_MESSAGE_DELIVERED_ACK);
		if (error != SA_OK){
			error_print("saMsgMessageSendReceive", error, "SA_OK");
		}
		exit(0);
	}else{
		sleep(3);
		SaMsgMessageT message_reply;
		SaMsgMessageInfoT replyInfo;
		replyInfo.senderName = qname;

		(char *)message_reply.data = "I send reply message!";
		message_reply.priority = SA_MSG_MESSAGE_LOWEST_PRIORITY;
		message_reply.size = strlen((char *)message_reply.data) + 1;
		error = saMsgMessageReply(&message_reply, &replyInfo,
			SA_MSG_MESSAGE_DELIVERED_ACK, SA_TIME_END);
		if (error != SA_ERR_NO_SPACE){
			error_print("saMsgMessageReply", error, "SA_ERR_NO_SPACE");
			ret = SAF_TEST_FAIL;
		}
	}
	
unlink:
	error = saMsgQueueUnlink(&qname);
	if (error != SA_OK)
		error_print("saMsgQueueUnlink", error, "SA_OK");

final:
	error = saMsgFinalize(&msghandle);
	if(error != SA_OK)
		error_print("saMsgFinalize", error, "SA_OK");

out:
	return ret;
}
