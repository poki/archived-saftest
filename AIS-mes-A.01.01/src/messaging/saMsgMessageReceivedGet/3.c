/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

int ret = SAF_TEST_PASS;
static int exit_select = 0;


void error_print(char *api_name, int error, char *should_be)
{
	printf("  Does not conform the expected behaviors!\n");
	printf("  %s, Return value:%s, should be %s\n", 
		api_name, get_aisAerror_string(error), should_be);
}

static void
msg_recv_cb(const SaMsgQueueHandleT *queueHandle,
		const SaMsgMessageHandleT *messageHandle,
		SaSizeT size)
{
	SaErrorT error;
	exit_select = 1;

        error = saMsgMessageReceivedGet(queueHandle, messageHandle, NULL, NULL);
        if (error != SA_ERR_INVALID_PARAM){
                error_print("saMsgMessageReceivedGet", error, "SA_ERR_INVALID_PARAM");
                ret = SAF_TEST_FAIL;
        }


        return;
}

int main(int argc, char *argv[])
{
	SaErrorT 	error;
	SaMsgHandleT	msghandle;
	SaMsgCallbacksT	msgcallback = {
		.saMsgQueueOpenCallback	= NULL,
		.saMsgQueueGroupTrackCallback	= NULL,
		.saMsgMessageDeliveredCallback	= NULL,
		.saMsgMessageReceivedCallback
		=	(SaMsgMessageReceivedCallbackT)msg_recv_cb
	};
	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = RELEASE_CODE
	};
	SaNameT qname;
	strcpy(qname.value, "saf_receivedget3");
	qname.length = strlen(qname.value);

	SaSizeT qsize = 1000;
        SaMsgQueueCreationAttributesT attr;
        attr.creationFlags = SA_MSG_QUEUE_PERSISTENT;
        attr.size[0] = qsize;
        attr.size[1] = qsize;
        attr.size[2] = qsize;
        attr.size[3] = qsize;
        attr.retentionTime = 0;

	SaMsgQueueOpenFlagsT openFlag = SA_MSG_QUEUE_RECEIVE_CALLBACK;
	SaMsgMessageT message;
	SaMsgQueueHandleT qhandle;
	SaSelectionObjectT selectionobject;

	error = saMsgInitialize(&msghandle, &msgcallback, &version);
	if (error != SA_OK){
		error_print("saMsgInitialize", error, "SA_OK");
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saMsgSelectionObjectGet(&msghandle, &selectionobject);
	if (error != SA_OK){
		error_print("saMsgSelectionObjectGet", error, "SA_OK");
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
		
	error = saMsgQueueOpen(&msghandle, &qname, &attr, openFlag, &qhandle, SA_TIME_END);
	if (error != SA_OK){
		error_print("saMsgQueueOpen", error, "SA_OK");
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

        /* prepare a message to send */
        (char *)message.data = "I send this message to you!";
        message.priority = SA_MSG_MESSAGE_LOWEST_PRIORITY;
        message.size = strlen((char *) message.data) + 1;

        error = saMsgMessageSend(&msghandle, &qname, &message, SA_MSG_MESSAGE_DELIVERED_ACK, SA_TIME_END);
        if (error != SA_OK){
                error_print("saMsgMessageSend", error, "SA_OK");
                ret = SAF_TEST_UNRESOLVED;
		goto unlink;
        }

	for(;;){
		fd_set rset;
		FD_ZERO(&rset);
		FD_SET(selectionobject, &rset);
		if (select(selectionobject + 1, &rset, NULL, NULL, NULL) == -1 ){
			if (errno == EINTR)
				continue;
			else{
				printf("select error\n");
				ret = SAF_TEST_UNRESOLVED;
				break;
			}
			if (exit_select){
				break;
			}
		}
		error = saMsgDispatch(&msghandle, SA_DISPATCH_ALL);
		if (error != SA_OK){
			error_print("saMsgDispatch", error, "SA_OK");
			break;
		}
	}

unlink:
	error = saMsgQueueUnlink(&qname);
	if (error != SA_OK)
		error_print("saMsgQueueUnlink", error, "SA_OK");

final:
	error = saMsgFinalize(&msghandle);
	if(error != SA_OK)
		error_print("saMsgFinalize", error, "SA_OK");

out:
	return ret;
}
