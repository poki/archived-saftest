#!/bin/sh
#
# Copyright (c) 2005, Intel Corporation.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307 USA.
#
# Authors:
#       Yang,Xiaowei  <xiaowei.yang@intel.com>
#	Yu,Ling  <ling.l.yu@intel.com>
#
                                                                                                                            
TOPDIR=.
SRCDIR=$TOPDIR/src
LOGFILE=./log/run_log
# Log for manual case
MLOGFILE=./log/mrun_log
RESULTFILE=./result.txt

usage()
{
        cat << EOF
Usage: $0 [-m] [all|Path]
Summerize the test report the test cases in current directory or specific
directory.
All result could be found at ./result.txt
	-m		Manual test cases will be included. Default is not.
        all             all test cases in current direcoty or subdirectory will be calculated.
        PATH            use this argument, all test cases in the PATH direcoty or its subdirectory will be calculated.

EOF
}

MANUAL=0
if [ "$1" == "-m" ]; then
	MANUAL=1
	shift
fi
	
case "$1" in
  "")
	usage
	exit
	;;
  "--help")
	usage
	exit
	;; 
  "all")
	;;
  *)
	SRCDIR=$1
	;;
esac

> $MLOGFILE

if [ "$MANUAL" == 1 ]; then
	echo -e "Have you run all manual test cases? (y/n) "
	read ANSWER
	if [ ANSWER == "n" ]; then 
		break
	fi
	
	# Ask user each manual test case's run result
	for i in `find $SRCDIR -name '*.test' -path '*/manual/*'`; do
		while :; do
			echo -e "Test case -- $i: Pass or fail? (p/f) \c"
			read ANSWER
			if [ "$ANSWER" = "p" -o "$ANSWER" = "P" ]; then
				echo $i >> $MLOGFILE
				echo PASS >> $MLOGFILE
				break
			fi
			if [ "$ANSWER" = "f" -o "$ANSWER" = "F" ]; then
				echo $i >> $MLOGFILE
				echo FAIL >> $MLOGFILE
				break
			fi
		done
	done

fi

# Generate the statistics
TMPFILE=/tmp/report.$$

cat $LOGFILE $MLOGFILE > $TMPFILE

TOTAL=`grep -c '.test' $TMPFILE`
PASS=`grep -c 'PASS' $TMPFILE`
FAIL=`grep -c 'FAIL' $TMPFILE`
BLOCK=`grep -c 'BLOCK' $TMPFILE`
NOTSUPPORT=`grep -c 'NOTSUPPORT' $TMPFILE`
UNRESOLVED=`grep -c 'UNRESOLVED' $TMPFILE`
UNKNOWN=`grep -c 'UNKNOWN' $TMPFILE`

> $RESULTFILE
echo "Total run test cases: $TOTAL" >> $RESULTFILE
echo "Pass: $PASS" >> $RESULTFILE
echo "Fail: $FAIL" >> $RESULTFILE
echo "Block: $BLOCK" >> $RESULTFILE
echo "Notsupport: $NOTSUPPORT" >> $RESULTFILE
echo "Noresolved: $UNRESOLVED" >> $RESULTFILE
echo "Unknown reason: $UNKNOWN" >> $RESULTFILE
echo "Finish testing" >> $RESULTFILE

rm -f $TMPFILE
