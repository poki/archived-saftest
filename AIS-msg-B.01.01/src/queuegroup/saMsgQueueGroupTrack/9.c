/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgQueueGroupTrack
 * Description:   
 *   Call the api with null notification, test if the message service
 *   allocate memory for the message queue group information.
 *   Return = SA_AIS_OK
 * Line:        P48-34:P48-36
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "saMsg.h"
#include "saf_test.h"

static void msg_queue_group_track_callback(const SaNameT *queueGroupName,
	const SaMsgQueueGroupNotificationBufferT *notificationBuffer,
	SaUint32T numberOfMembers,
	SaAisErrorT error)
{
	return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saMsgQueueGroupTrack";
	SaAisErrorT     expectedReturn = SA_AIS_OK;
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = NULL,
                .saMsgQueueGroupTrackCallback =
				msg_queue_group_track_callback,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = NULL
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

	SaNameT 	queueName = {
		.length = strlen("queueName"),
		.value 	= "queueName"
	};

	SaNameT		queueGroupName = {
		.length = strlen("queue"),
		.value = "queue"
	};

	SaMsgQueueCreationAttributesT	creationAttributes = {
			.creationFlags = 0,
			.size = {10, 10, 10, 10},
			.retentionTime = 0
	};

	SaMsgQueueGroupNotificationT	queueGroupNotification;

	SaMsgQueueGroupNotificationBufferT	queueGroupNotificationBuf ={
			.numberOfItems = 1,
			.notification = NULL,
			.queueGroupPolicy = SA_MSG_QUEUE_GROUP_ROUND_ROBIN
	};

	SaMsgQueueHandleT 	queueHandle;

        int ret = SAF_TEST_PASS;

	memcpy(&(queueGroupNotification.member.queueName), &queueName,
							sizeof(SaNameT));
	queueGroupNotification.change = SA_MSG_QUEUE_GROUP_NO_CHANGE;

        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	msgError = saMsgQueueOpen(msgHandle,
				&queueName,
				&creationAttributes,
				SA_MSG_QUEUE_CREATE | SA_MSG_QUEUE_EMPTY,
				SA_TIME_MAX,
				&queueHandle);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpen, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}

	msgError = saMsgQueueGroupCreate(msgHandle, &queueGroupName,
				SA_MSG_QUEUE_GROUP_ROUND_ROBIN);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueGroupCreate, Return value: %s, should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}

	msgError = saMsgQueueGroupInsert(msgHandle, &queueGroupName,
					&queueName);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueGroupInsert, Return value: %s, should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	msgError = saMsgQueueGroupTrack(msgHandle,&queueGroupName,
					SA_TRACK_CURRENT,
					&queueGroupNotificationBuf);
	if (msgError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n",
			apiName,
			get_error_string(msgError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
		goto final_4;
	}

	if (memcmp(queueGroupNotificationBuf.notification,
	&queueGroupNotification, sizeof(SaMsgQueueGroupNotificationT)) != 0){
		printf(" notificationBuffer is error\n");
		ret = SAF_TEST_FAIL;
		goto final_5;
	}

	if (queueGroupNotificationBuf.numberOfItems != 1){
		printf("numberOfItems is error,should be 1\n");
		ret = SAF_TEST_FAIL;
		goto final_5;
	}

final_5 :
	free(queueGroupNotificationBuf.notification);
final_4 :
	saMsgQueueGroupTrackStop(msgHandle, &queueGroupName);
final_3 :
	saMsgQueueGroupDelete(msgHandle, &queueGroupName);	
final_2 :
	saMsgQueueClose(queueHandle);
final_1 :
	saMsgFinalize(msgHandle);
	return ret;
}

