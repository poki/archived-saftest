/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgQueueGroupTrack
 * Description:   
 *   Call the api with SA_TRACK_CHANGES_ONLY set and other valid params.
 *   Test if the notification callback is invoked each time and the callback
 *   call provides an SaMsgQueueGroupNotificationT structure for all changed
 *   members of the message queue group,when a change occurs
 *   in the membership of the message queue group.
 *   Return = SA_AIS_OK 
 * Line:        P48-11:P48-16
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include "saMsg.h"
#include "saf_test.h"

SaMsgQueueGroupNotificationBufferT notificationBufferGlobal;
SaNameT queueGroupNameGlobal;
SaUint32T numberOfMembersGlobal;
SaAisErrorT errorGlobal;

static void msg_queue_group_track_callback(const SaNameT *queueGroupName,
	const SaMsgQueueGroupNotificationBufferT *notificationBuffer,
	SaUint32T numberOfMembers,
	SaAisErrorT error)
{
	memcpy(&notificationBufferGlobal,notificationBuffer,
		sizeof(SaMsgQueueGroupNotificationBufferT));

	notificationBufferGlobal.notification =
		(SaMsgQueueGroupNotificationT *)
		malloc(sizeof(SaMsgQueueGroupNotificationT) *
			notificationBuffer->numberOfItems);

	memcpy(&queueGroupNameGlobal,queueGroupName,sizeof(SaNameT));

	numberOfMembersGlobal = numberOfMembers;

	memcpy(notificationBufferGlobal.notification,
		notificationBuffer->notification,
		sizeof(SaMsgQueueGroupNotificationT)*
		notificationBuffer->numberOfItems);

	errorGlobal = error;
	return;
}
 
int main(int argc, char *argv[])
{
	char apiName[] = "saMsgQueueGroupTrack";
	SaAisErrorT     expectedReturn = SA_AIS_OK;
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = NULL,
                .saMsgQueueGroupTrackCallback =
				msg_queue_group_track_callback,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = NULL
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

	SaNameT 	queueName = {
		.length = strlen("queueName"),
		.value 	= "queueName"
	};

	SaNameT 	queueNameTmp = {
		.length = strlen("queueNameTmp"),
		.value 	= "queueNameTmp"
	};
	SaNameT		queueGroupName = {
		.length = strlen("queue"),
		.value = "queue"
	};

	SaMsgQueueCreationAttributesT	creationAttributes = {
			.creationFlags = 0,
			.size = {10, 10, 10, 10},
			.retentionTime = 0
	};
	
	SaMsgQueueGroupNotificationT	queueGroupNotification;
	
	SaMsgQueueGroupNotificationBufferT	queueGroupNotificationBuf ={
			.numberOfItems = 1,
			.notification = &queueGroupNotification,
			.queueGroupPolicy = SA_MSG_QUEUE_GROUP_ROUND_ROBIN
	};

	SaMsgQueueHandleT 	queueHandle;
	SaMsgQueueHandleT	queueHandleTmp;

	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
        int ret = SAF_TEST_PASS;

        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			" SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	msgError = saMsgQueueOpen(msgHandle,
				&queueName,
				&creationAttributes,
				SA_MSG_QUEUE_CREATE | SA_MSG_QUEUE_EMPTY,
				SA_TIME_MAX,
				&queueHandle);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpen, Return value: %s, should be "
			" SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}

	msgError = saMsgQueueOpen(msgHandle,
				&queueNameTmp,
				&creationAttributes,
				SA_MSG_QUEUE_CREATE | SA_MSG_QUEUE_EMPTY,
				SA_TIME_MAX,
				&queueHandleTmp);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpen, Return value: %s, should be "
			" SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}

	msgError = saMsgQueueGroupCreate(msgHandle, &queueGroupName,
				SA_MSG_QUEUE_GROUP_ROUND_ROBIN);
				
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueGroupCreate, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	msgError = saMsgQueueGroupInsert(msgHandle, &queueGroupName,
					&queueName);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueGroupInsert, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
	}

	msgError = saMsgSelectionObjectGet(msgHandle, &selObject);
	if (msgError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgSelectionObjectGet, Return value: %s, should be"
			" SA_AIS_OK\n",
			get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }
	msgError = saMsgQueueGroupTrack(msgHandle, &queueGroupName,
					SA_TRACK_CHANGES_ONLY,
					&queueGroupNotificationBuf);
	if (msgError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n",
			apiName,
			get_error_string(msgError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
		goto final_4;
	}

	msgError = saMsgQueueGroupInsert(msgHandle, &queueGroupName,
					&queueNameTmp);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueGroupInsert, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
	}

	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final_5;
        }

	msgError = saMsgDispatch(msgHandle, dispatchFlag);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgDispatch, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_5;
	}

	if (errorGlobal != SA_AIS_OK){
		printf(" error from the callback function should be"
						" SA_AIS_OK\n");
		free(notificationBufferGlobal.notification);
		ret = SAF_TEST_FAIL;
		goto final_5;
	}

	if (memcmp(&queueGroupNameGlobal, &queueGroupName,
					sizeof(SaNameT)) != 0){
		printf(" queueGroupName from the callback function"
							" is error\n");
		free(notificationBufferGlobal.notification);
		ret = SAF_TEST_FAIL;
		goto final_5;
	}
	//check whether the notification only contains the changed queue.
	if (numberOfMembersGlobal != 1){
		printf(" numberOfMembers from the callback function"
							" is error\n");
		ret = SAF_TEST_FAIL;
		free(notificationBufferGlobal.notification);
		goto final_5;
	}

	memcpy(&(queueGroupNotification.member.queueName), &queueNameTmp,
							sizeof(SaNameT));
	queueGroupNotification.change = SA_MSG_QUEUE_GROUP_ADDED;
	if (memcmp(notificationBufferGlobal.notification,
		   &queueGroupNotification,
		   sizeof(SaMsgQueueGroupNotificationT)) != 0){
		printf("notification from callback function is error \n");
		ret = SAF_TEST_FAIL;
	}

	free(notificationBufferGlobal.notification);
final_5 :
	saMsgQueueGroupTrackStop(msgHandle, &queueGroupName);
final_4 :
	saMsgQueueGroupDelete(msgHandle, &queueGroupName);
final_3 :
	saMsgQueueClose(queueHandleTmp);
final_2 :
	saMsgQueueClose(queueHandle);
final_1 :
	saMsgFinalize(msgHandle);
	return ret;
}

