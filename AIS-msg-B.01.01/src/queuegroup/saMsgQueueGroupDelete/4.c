/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgQueueGroupDelete
 * Description:
 *   After the invocation of the api , test if it is no longer possible to
 *   send message to the message queue group
 *   Return = SA_AIS_OK
 * Line:        P46-34:P46-35
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saMsg.h"
#include "saf_test.h"

static void msg_received_callback(SaMsgQueueHandleT msgHandle)
{
	return;
}
 
int main(int argc, char *argv[])
{
	char apiName[] = "saMsgQueueGroupDelete";
	SaAisErrorT     expectedReturn = SA_AIS_OK;
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = NULL,
                .saMsgQueueGroupTrackCallback = NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = msg_received_callback
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

	SaNameT 	queueName = {
		.length = strlen("queueName"),
		.value 	= "queueName"
	};

	SaNameT		queueGroupName = {
		.length = strlen("queue"),
		.value = "queue"
	};

	SaMsgQueueCreationAttributesT	creationAttributes = {
			.creationFlags = 0,
			.size = {10, 10, 10, 10},
			.retentionTime = 0
	};

	SaMsgQueueHandleT 	queueHandle;

	char messageData[10] = "hello";
	SaMsgMessageT	message = {
			.type = 0,
			.version = 0,
			.size = 10,
			.senderName = &queueName,
			.data = messageData,
			.priority = SA_MSG_MESSAGE_HIGHEST_PRIORITY
	};

        int ret = SAF_TEST_PASS;
	
        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	msgError = saMsgQueueOpen(msgHandle,
				&queueName,
				&creationAttributes,
				SA_MSG_QUEUE_CREATE |
					 SA_MSG_QUEUE_RECEIVE_CALLBACK,
				SA_TIME_MAX,
				&queueHandle);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpen, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}

	msgError = saMsgQueueGroupCreate(msgHandle, &queueGroupName,
				SA_MSG_QUEUE_GROUP_ROUND_ROBIN);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueGroupCreate, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}

	msgError = saMsgQueueGroupInsert(msgHandle, &queueGroupName,
					&queueName);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueGroupInsert, Return value: %s, should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	msgError = saMsgQueueGroupRemove(msgHandle, &queueGroupName,
					&queueName);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueGroupRemove, Return value: %s, should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
	}

final_3 :
	msgError = saMsgQueueGroupDelete(msgHandle, &queueGroupName);
	if (msgError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(msgError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_UNRESOLVED;
	}

	if (ret != SAF_TEST_PASS)
		goto final_2;

	msgError = saMsgMessageSend(msgHandle,
				&queueGroupName,
				&message,
				SA_TIME_ONE_SECOND);
	if (msgError != SA_AIS_ERR_NOT_EXIST){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgMessageSend, Return value: %s, should be "
			"SA_AIS_ERR_NOT_EXIST\n",
			get_error_string(msgError));
                ret = SAF_TEST_FAIL;
	}

final_2 :
	saMsgQueueClose(queueHandle);

final_1 :
	saMsgFinalize(msgHandle);
	return ret;
}

