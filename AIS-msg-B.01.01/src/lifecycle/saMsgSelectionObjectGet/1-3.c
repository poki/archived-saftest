/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgSelectionObjectGet
 * Description:   
 *   Get the selectionObject after finalize, which will cause the msgHandle 
 *   invalid
 *   Return = SA_AIS_ERR_BAD_HANDLE
 * Line:        P28-18:P28-20
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saMsg.h"
#include "saf_test.h"

static void msg_queue_open_callback (SaInvocationT invocation,
        SaMsgQueueHandleT msgHandle,
        SaAisErrorT error)
{
        return;
}

static void msg_queue_group_track_callback(const SaNameT *queueGroupName,
	const SaMsgQueueGroupNotificationBufferT *notificationBuffer,
	SaUint32T numberOfMembers,
	SaAisErrorT error)
{
	return;
}


static void msg_deliver_callback (SaInvocationT invocation,
        SaAisErrorT error)
{
        return;
}

static void msg_received_callback(SaMsgQueueHandleT msgHandle)
{
	return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saMsgSelectionObjectGet";
	SaAisErrorT     expectedReturn= SA_AIS_ERR_BAD_HANDLE;
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
                .saMsgQueueOpenCallback  = msg_queue_open_callback,
                .saMsgQueueGroupTrackCallback = 
				msg_queue_group_track_callback,
		.saMsgMessageDeliveredCallback = msg_deliver_callback,
		.saMsgMessageReceivedCallback = msg_received_callback
        };
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaSelectionObjectT 	selObject;
        int ret = SAF_TEST_PASS;

        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
         if (msgError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
        }
	
	msgError = saMsgFinalize(msgHandle);
	  if (msgError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgFinalize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
        }
	
	msgError = saMsgSelectionObjectGet(msgHandle, &selObject);
	if (msgError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(msgError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
	}
final :
	return ret;
}

