/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgSelectionObjectGet
 * Description:   
 *   Call saMsgSelectionObjectGet related to the msgHandle , After finalizing
 *   we call select to detect whether the selectionObject is invaild .
 *   Return = SA_AIS_OK
 * Line:        P28-31:P28-34
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saMsg.h"
#include "saf_test.h"

static void msg_queue_open_callback (SaInvocationT invocation,
        SaMsgQueueHandleT msgHandle,
        SaAisErrorT error)
{
	saMsgQueueClose(msgHandle);
        return;
}

static void msg_queue_group_track_callback(const SaNameT *queueGroupName,
	const SaMsgQueueGroupNotificationBufferT *notificationBuffer,
	SaUint32T numberOfMembers,
	SaAisErrorT error)
{
	return;
}


static void msg_deliver_callback (SaInvocationT invocation,
        SaAisErrorT error)
{
        return;
}

static void msg_received_callback(SaMsgQueueHandleT msgHandle)
{
	return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saMsgSelectionObjectGet";
	SaAisErrorT     expectedReturn = SA_AIS_OK;
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
                .saMsgQueueOpenCallback  = msg_queue_open_callback,
                .saMsgQueueGroupTrackCallback =
				msg_queue_group_track_callback,
		.saMsgMessageDeliveredCallback = msg_deliver_callback,
		.saMsgMessageReceivedCallback = msg_received_callback
        };
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	fd_set readSet;
	int selectRet;
	SaNameT queueName ={
		.value = "queueName",
		.length = strlen("queueName") 
	};
	SaMsgQueueCreationAttributesT	creationAttributes = {
			.creationFlags = SA_MSG_QUEUE_PERSISTENT,
			.size = {10, 10, 10, 10},
			.retentionTime = SA_TIME_MAX
	};
	SaSelectionObjectT 	selObject;
        int ret = SAF_TEST_PASS;
	struct timeval timeout = {
			.tv_sec = 8,
			.tv_usec = 0
	};	

        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
        if (msgError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(msgError));
                ret = SAF_TEST_FAIL;
		return ret;
        }
	
	msgError = saMsgQueueOpenAsync(msgHandle,1,
					&queueName,
					&creationAttributes,
					SA_MSG_QUEUE_CREATE);
	if (msgError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpenAsync, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		saMsgFinalize(msgHandle);
		return ret;
        }
	
	msgError = saMsgSelectionObjectGet(msgHandle, &selObject);
	if (msgError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(msgError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
		saMsgFinalize(msgHandle);
		return ret;
	}
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
	msgError = saMsgFinalize(msgHandle);
	if (msgError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(msgError));
                ret = SAF_TEST_FAIL;
		return ret;
        }
	
        selectRet = select(selObject + 1, &readSet, NULL, NULL, &timeout);
        if (selectRet > 0){
                printf("  select error!\n");
                ret = SAF_TEST_FAIL;
        }

	return ret;
}

