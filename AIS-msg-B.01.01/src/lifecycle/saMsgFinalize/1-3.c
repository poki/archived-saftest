/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgFinalize
 * Description:   
 *   Finalize an uninitialized msgHandle.
 *   Return = SA_AIS_ERR_BAD_HANDLE
 * Line:        P30-32:P30-35
 */

#include <stdio.h>
#include <stdlib.h>
#include "saMsg.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saMsgFinalize";
	SaAisErrorT     expectedReturn= SA_AIS_ERR_BAD_HANDLE;
        SaMsgHandleT   msgHandle;
	SaAisErrorT	msgError;

        int ret = SAF_TEST_PASS;

	msgError = saMsgFinalize(msgHandle);
	if (msgError != expectedReturn){
                printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n",
			apiName,
			get_error_string(msgError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
        }

        return ret;
}

