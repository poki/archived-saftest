/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgFinalize
 * Description:   
 *  Call saMsgSelectionObjectGet related to the msgHandle, After finalizing
 *  we call select to detect whether the selectionObject is invaild.
 *  Same with 4
 *  Return = SA_AIS_OK
 * Line:        P31-7:P31-7
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saMsg.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char sameCase[]="./4.test";

	execl(sameCase, NULL);
       	printf("        Can't find case %s\n", sameCase);
	return SAF_TEST_UNKNOWN; 
}

