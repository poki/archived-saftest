/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgFinalize
 * Description:   
 *  After finalizing, we try to call callbacks related to the msgHandle.
 *  The callback should be removed.
 *  Return = SA_AIS_OK
 * Line:        P31-2:P31-2
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saMsg.h"
#include "saf_test.h"

static void msg_queue_open_callback (SaInvocationT invocation,
        SaMsgQueueHandleT msgHandle,
        SaAisErrorT error)
{
	saMsgQueueClose(msgHandle);
        return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saMsgFinalize";
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = msg_queue_open_callback,
                .saMsgQueueGroupTrackCallback = NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = NULL
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	queueName = {
		.length = strlen("queueName"),
		.value 	= "queueName"
	};
	
	SaMsgQueueCreationAttributesT	creationAttributes = {
			.creationFlags = SA_MSG_QUEUE_PERSISTENT,
			.size = {10, 10, 10, 10},
			.retentionTime = SA_TIME_MAX
	};
	fd_set readSet;
	int selectRet;
	
	struct timeval timeout ={
			.tv_sec = 9,
			.tv_usec = 0
	};
	SaSelectionObjectT 	selObject;
        int ret = SAF_TEST_PASS;
	
        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	msgError = saMsgSelectionObjectGet(msgHandle, &selObject);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgSelectionObjectGet, Return value: %s, "
			"should be SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		saMsgFinalize(msgHandle);
		goto final;
	}
	
	msgError = saMsgQueueOpenAsync(msgHandle, 1,
					&queueName,
					&creationAttributes,
					SA_MSG_QUEUE_CREATE);
	if (msgError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpenAsync, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		saMsgFinalize(msgHandle);
		goto final;
        }
		
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
	msgError = saMsgFinalize(msgHandle);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
               	printf("  %s, Return value: %s, should be "
			"SA_AIS_OK\n",
			apiName,
			get_error_string(msgError));
        	ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
		
	selectRet = select(selObject + 1, &readSet, NULL, NULL, &timeout);
       	if (selectRet > 0){
               	printf("  select error!\n");
               	ret = SAF_TEST_FAIL;
        }
final :
	return ret;
	
}

