/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgDispatch
 * Description:
 *   Dispatch correctly, Set dispatch Flag to SA_DISPATCH_BLOCKING. The 
 *   function completes successfully.
 *   Return = SA_AIS_OK
 * Line:        P29-32:P29-36
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include "saMsg.h"
#include "saf_test.h"

int callbackFlag = 0;
SaAisErrorT dispatchReturn;
sem_t sem_start_dispatch;
sem_t sem_stop_dispatch;

static void msg_queue_open_callback (SaInvocationT invocation,
        SaMsgQueueHandleT msgHandle,
        SaAisErrorT error)
{
	callbackFlag = 1;
	saMsgQueueClose(msgHandle);
        return;
}

static void msg_queue_group_track_callback(const SaNameT *queueGroupName,
	const SaMsgQueueGroupNotificationBufferT *notificationBuffer,
	SaUint32T numberOfMembers,
	SaAisErrorT error)
{
	return;
}

static void msg_deliver_callback (SaInvocationT invocation,
        SaAisErrorT error)
{
        return;
}

static void msg_received_callback(SaMsgQueueHandleT msgHandle)
{
	return;
}

static void *dispatch_thread(void *arg)
{
	callbackFlag = 0;
	sem_post(&sem_start_dispatch);
	dispatchReturn = saMsgDispatch(* ((SaMsgHandleT *) arg),
					SA_DISPATCH_BLOCKING);
	sem_post(&sem_stop_dispatch);
	return NULL;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saMsgDispatch";
	SaAisErrorT     expectedReturn = SA_AIS_OK;
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
                .saMsgQueueOpenCallback  = msg_queue_open_callback,
                .saMsgQueueGroupTrackCallback = 
			msg_queue_group_track_callback,
		.saMsgMessageDeliveredCallback = msg_deliver_callback,
		.saMsgMessageReceivedCallback = msg_received_callback
        };
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	fd_set readSet;
	int selectRet;
	SaNameT queueName ={
		.value = "queueName",
		.length = strlen("queueName") 
	};
	SaMsgQueueCreationAttributesT	creationAttributes = {
			.creationFlags = SA_MSG_QUEUE_PERSISTENT,
			.size = {10, 10, 10, 10},
			.retentionTime = SA_TIME_MAX
	};
	SaSelectionObjectT 	selObject;
        int ret = SAF_TEST_PASS;

	pthread_t thread_id;
	int pthreadError;

        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
        if (msgError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }

	sem_init(&sem_start_dispatch, 0, 0);
	sem_init(&sem_stop_dispatch, 0, 0);
	pthreadError = pthread_create(&thread_id,
					NULL,
					dispatch_thread, 
					(void*)&msgHandle);
	if (pthreadError){
		printf("	invoke \"pthread_create\" has error,"
				" return %s\n", strerror(pthreadError));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	msgError = saMsgSelectionObjectGet(msgHandle, &selObject);
	if (msgError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgSelectionObjectGet, Return value: %s, should be"
			" SA_AIS_OK\n",
			get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
        }
	
	msgError = saMsgQueueOpenAsync(msgHandle, 1,
					&queueName,
					&creationAttributes,
					SA_MSG_QUEUE_CREATE);
	if (msgError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpenAsync, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
        }

	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	msgError = saMsgQueueUnlink(msgHandle, &queueName);
	if (msgError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueUnlink, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
        }

final :
	if (ret != SAF_TEST_UNRESOLVED)
		sem_wait(&sem_start_dispatch);

	msgError = saMsgFinalize(msgHandle);
	if (msgError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgChannelUnlink, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
        }
	
	if (ret != SAF_TEST_PASS)
		return ret;

	sem_wait(&sem_stop_dispatch);
	
	if ( callbackFlag <= 0){
		printf("	invoke %s has error, pending callbacks are "
				"not invoked\n", apiName);
		return SAF_TEST_FAIL;
	}
	
	if ( dispatchReturn != expectedReturn ){
		printf(" Does not conform the expected behaviors!\n");
                printf(" %s, Return value :%s, should be %s\n", 
			apiName,
			get_error_string(dispatchReturn),
			get_error_string(expectedReturn));
		return SAF_TEST_FAIL;
	}
	return ret;
}


