This file defines the coverage for the saMsgQueueStatusGet conformance testing.

Assertion       Status
1-1             YES
1-2		YES
2-1		YES
2-2		YES
2-3		YES
3-1		YES
3-2		YES
4		YES
5		YES	
6		NO
7		NO
8		NO
9		YES
10		YES
11		NO
12		YES

