/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *      Yu, Ling <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgQueueClose
 * Description:   
 *  Test if it cancels all pending callbacks that refer directly or indirectly
 *  to the handle queueHandle, when message queue is closed.
 *  Return = SA_AIS_OK
 * Line:        P37-36:P37-40
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "saMsg.h"
#include "saf_test.h"

static void msg_received_callback(SaMsgQueueHandleT msgHandle)
{
	return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saMsgQueueClose";
	SaAisErrorT     expectedReturn = SA_AIS_OK;
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = NULL,
                .saMsgQueueGroupTrackCallback = NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = msg_received_callback
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	queueName = {
		.length = strlen("queueName"),
		.value 	= "queueName"
	};
	SaMsgQueueCreationAttributesT	creationAttributes = {
			.creationFlags = SA_MSG_QUEUE_PERSISTENT,
			.size = {10, 10, 10, 10},
			.retentionTime = SA_TIME_MAX
	};
	
	SaMsgQueueHandleT 	queueHandle;

	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;

	char messageData[10] = "hello";
	SaMsgMessageT   message = {
		.type = 0,
		.version = 0,
		.size = 10,
		.senderName = &queueName,
		.data = messageData,
		.priority = SA_MSG_MESSAGE_HIGHEST_PRIORITY
	};

	struct timeval tv = {
		.tv_sec = 5,
		.tv_usec = 0
	};

        int ret = SAF_TEST_PASS;
	
        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	msgError = saMsgSelectionObjectGet(msgHandle, &selObject);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	msgError = saMsgQueueOpen(msgHandle,
				&queueName,
				&creationAttributes,
				SA_MSG_QUEUE_CREATE
				|SA_MSG_QUEUE_RECEIVE_CALLBACK,
				SA_TIME_MAX,
				&queueHandle);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpen, Return value: %s, should"
			" be SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	msgError = saMsgMessageSendAsync(msgHandle, 1,
					&queueName,
					&message,
					0);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saMsgMessageSendAsync, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
			
	msgError = saMsgQueueClose(queueHandle);
	if (msgError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be "
			"%s\n",
			apiName,
			get_error_string(msgError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, &tv);
        if (selectRet != 0){
		printf("  Does not conform the expected behaviors!\n");
		printf("  select error! should be 0 , Return value:%d\n",
			selectRet);
		ret = SAF_TEST_FAIL;
	}
	
final_2 :
	saMsgQueueUnlink(msgHandle, &queueName);
final_1 :
	saMsgFinalize(msgHandle);
	return ret;
}


