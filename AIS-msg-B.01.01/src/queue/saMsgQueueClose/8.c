/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgQueueClose
 * Description:   
 *  when a process terminates , test if the message service implicitly
 *  closes all message queues that are open for this process.
 *  Steps:
 *   1. Open a message queue in main process
 *   2. Fork a subprocess and open the same message queue in the subprocess
 *   3. Subprocess ends
 *   4. Close and Unlink the message queue in main process. If the message 
 *      queue has been closed when the subprocess died, the message queue  
 *      should be deleted.
 *   5. open a message queue using the same name as 1. If succeed,
 *     the case fails. Otherwise, the case passes.
 *   8-fork.c is used as its child process
 *  Return = SA_AIS_OK
 * Line:        P37-33:P37-35
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "saMsg.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saMsgQueueClose";
	SaAisErrorT     expectedReturn = SA_AIS_OK;
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = NULL,
                .saMsgQueueGroupTrackCallback = NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = NULL
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

	SaNameT 	queueName = {
		.length = strlen("queueName"),
		.value 	= "queueName"
	};
	SaMsgQueueCreationAttributesT	creationAttributes = {
			.creationFlags = SA_MSG_QUEUE_PERSISTENT,
			.size = {10, 10, 10, 10},
			.retentionTime = SA_TIME_MAX
	};

	SaMsgQueueHandleT 	queueHandle;	
        int ret = SAF_TEST_PASS;

	pid_t pid ;
	int status;

        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	msgError = saMsgQueueOpen(msgHandle,
				&queueName,
				&creationAttributes,
				SA_MSG_QUEUE_CREATE | SA_MSG_QUEUE_EMPTY,
				SA_TIME_MAX,
				&queueHandle);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpen, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	msgError = saMsgQueueClose(queueHandle);
	if (msgError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n",
			apiName,
			get_error_string(msgError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_UNRESOLVED;
		saMsgQueueUnlink(msgHandle,&queueName);
		saMsgFinalize(msgHandle);
		return ret;
	}

	pid = fork();
	if (pid < 0){
		printf(" fork is error\n");
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	if (pid == 0)
		if (execl("./8-fork.test", NULL) == -1){
			printf("execl is error\n");
			ret = SAF_TEST_UNRESOLVED;
			return ret;
		}

	wait(&status);
	if (WIFEXITED(status) <= 0){
		printf("WIFEXITED is error\n");
		ret = SAF_TEST_UNRESOLVED;
	}
	else
		if(ret == SAF_TEST_PASS)
			ret = WEXITSTATUS(status);

	msgError = saMsgQueueUnlink(msgHandle, &queueName);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueUnlink, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		saMsgFinalize(msgHandle);
		return ret;
	}

	creationAttributes.creationFlags = 0;
	msgError = saMsgQueueOpen(msgHandle,
				&queueName,
				&creationAttributes,
				SA_MSG_QUEUE_CREATE | SA_MSG_QUEUE_EMPTY,
				SA_TIME_MAX,
				&queueHandle);
	if (msgError == SA_AIS_ERR_EXIST){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpen, Return value: %s, should not be "
			"SA_AIS_ERR_EXIST\n", get_error_string(msgError));
                ret = SAF_TEST_FAIL;
	}

final :
	saMsgQueueUnlink(msgHandle, &queueName);
	saMsgFinalize(msgHandle);
	return ret;
}



