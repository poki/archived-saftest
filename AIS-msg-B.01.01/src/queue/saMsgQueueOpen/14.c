/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgQueueOpen
 * Description:   
 *   Call the api with valid params, and then test if the return queueHandle 
 *   is valid .
 *   Return = SA_AIS_OK
 * Line:        P33-39:P33-41
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>

#include "saMsg.h"
#include "saf_test.h"
 
int main(int argc, char *argv[])
{
	char apiName[] = "saMsgQueueOpen";
	SaAisErrorT     expectedReturn = SA_AIS_OK;
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = NULL,
                .saMsgQueueGroupTrackCallback = NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = NULL
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	queueName = {
		.length = strlen("queueName"),
		.value 	= "queueName"
	};
	SaMsgQueueCreationAttributesT	creationAttributes = {
			.creationFlags = SA_MSG_QUEUE_PERSISTENT,
			.size = {10, 10, 10, 10},
			.retentionTime = SA_TIME_MAX
	};
	
	SaMsgQueueHandleT 	queueHandle;
		
        int ret = SAF_TEST_PASS;
	
        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	msgError = saMsgQueueOpen(msgHandle,
				&queueName,
				&creationAttributes,
				SA_MSG_QUEUE_CREATE | SA_MSG_QUEUE_EMPTY,
				SA_TIME_MAX,
				&queueHandle);
	if (msgError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be "
			"%s\n",
			apiName,
			get_error_string(msgError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	msgError = saMsgQueueClose(queueHandle);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueClose, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_FAIL;
	}

	saMsgQueueUnlink(msgHandle, &queueName);
	
final :
	saMsgFinalize(msgHandle);
	return ret;
}


