This file defines the coverage for the saMsgQueueOpen conformance testing.

Assertion       Status
1-1             YES
1-2		YES
2-1		YES
2-2		YES
3		YES
4		YES
5		YES
6		YES
7-1		YES
7-2		YES
7-3		YES
8		YES
9		YES
10-1		YES
10-2		YES
10-3		YES
10-4		YES
10-5		YES
10-6		YES
11		YES
12-1		YES
12-2		YES
13		YES
14		YES
15-1		YES
15-2		YES
16		YES
17		YES
18		NO
19		NO
20		NO
21-1		YES
21-2		YES
22		YES
23		YES
24		YES
25		YES
26		NO
27		NO
28		YES
29		YES
30		YES
31		YES


