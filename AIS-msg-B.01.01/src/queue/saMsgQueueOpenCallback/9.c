/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgQueueOpenCallback
 * Description:   
 *  open queue when SA_MSG_QUEUE_CREATE flag is
 *  not set, and the queue, designated by queueName
 *  does not exist.
 *  Return error should be SA_AIS_ERR_NOT_EXIST.
 * Line:        P36-11:P36-12
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>

#include "saMsg.h"
#include "saf_test.h"

SaInvocationT invocationGlobal = 0;
SaAisErrorT errorGlobal = SA_AIS_OK;
static void msg_queue_open_callback (SaInvocationT invocation,
        SaMsgQueueHandleT queueHandle,
        SaAisErrorT error)
{
	invocationGlobal = invocation;
	errorGlobal = error;
	saMsgQueueClose(queueHandle);
        return;
}

static void msg_received_callback(SaMsgQueueHandleT msgHandle)
{
	return;
}
 
int main(int argc, char *argv[])
{
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = msg_queue_open_callback,
                .saMsgQueueGroupTrackCallback =	NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = msg_received_callback
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	queueName = {
		.length = strlen("queueName"),
		.value 	= "queueName"
	};
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ONE;
	
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;	
        int ret = SAF_TEST_PASS;
	
        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	msgError = saMsgSelectionObjectGet(msgHandle, &selObject);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	msgError = saMsgQueueOpenAsync(msgHandle, 1,
					&queueName,
					NULL,
			SA_MSG_QUEUE_RECEIVE_CALLBACK|SA_MSG_QUEUE_EMPTY);
	if (msgError == SA_AIS_ERR_NOT_EXIST)
		goto final_1;
	
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpenAsync, Return value: %s, should"
			" be SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}

	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final_2;
        }
	
	msgError = saMsgDispatch(msgHandle, dispatchFlag);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgDispatch, Return value: %s, should"
			" be SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	if(invocationGlobal != 1){
		printf("invocation should be 1 ,Return :%lld\n",
						invocationGlobal);
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	if (errorGlobal != SA_AIS_ERR_NOT_EXIST){
		printf("error is wrong , should be SA_AIS_ERR_NOT_EXIST.\n");
		ret = SAF_TEST_FAIL;
	}

final_2:
	saMsgQueueUnlink(msgHandle, &queueName);
final_1 :
	saMsgFinalize(msgHandle);
	return ret;
}




