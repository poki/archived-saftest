/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgQueueOpenCallback
 * Description:   
 *  open queue when the queue is already open by another process
 *  Return error should be SA_AIS_ERR_BUSY.
 *  11-fork is used as its child process
 * Line:        P36-16:P36-20
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include "saMsg.h"
#include "saf_test.h"

int sigFlag = 0;
SaInvocationT invocationGlobal = 0;
SaAisErrorT errorGlobal;
static void msg_queue_open_callback (SaInvocationT invocation,
        SaMsgQueueHandleT queueHandle,
        SaAisErrorT error)
{
	invocationGlobal = invocation;
	errorGlobal = error;
	saMsgQueueClose(queueHandle);
        return;
}

void changeFlag(int sig)
{
	sigFlag = sig;
}

int main(int argc, char *argv[])
{
	SaAisErrorT     expectedReturn = SA_AIS_ERR_BUSY;
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = msg_queue_open_callback,
                .saMsgQueueGroupTrackCallback = NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = NULL
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

	SaNameT 	queueName = {
		.length = strlen("queueName"),
		.value 	= "queueName"
	};
	SaMsgQueueCreationAttributesT	creationAttributes = {
			.creationFlags = SA_MSG_QUEUE_PERSISTENT,
			.size = {10, 10, 10, 10},
			.retentionTime = SA_TIME_MAX
	};

	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	pid_t pid;
	int status;	
        int ret = SAF_TEST_PASS;
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	signal(SIGUSR1,changeFlag);
	pid = fork();
	if (pid < 0){
		printf(" fork is error\n");
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	if (pid == 0){
		if (execl("./11-fork.test", NULL) == -1){
			printf("execl is error\n");
			ret = SAF_TEST_UNRESOLVED;
			return ret;		
		}	
	}

	while(sigFlag == 0)
		pause();

	msgError = saMsgSelectionObjectGet(msgHandle, &selObject);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}

	msgError = saMsgQueueOpenAsync(msgHandle, 1,
				&queueName,
				&creationAttributes,
			SA_MSG_QUEUE_CREATE|SA_MSG_QUEUE_EMPTY);
	if (msgError != expectedReturn)
		goto final_1;
	
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpenAsync, Return value: %s, should"
			" be SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}

	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final_1;
        }

	msgError = saMsgDispatch(msgHandle, dispatchFlag);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgDispatch, Return value: %s, should"
			" be SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	if(invocationGlobal != 1){
		printf("invocation should be 1 ,Return :%lld\n",
						invocationGlobal);
		ret = SAF_TEST_UNRESOLVED;
	}
	
	if (errorGlobal != SA_AIS_ERR_BUSY){
		printf("error is wrong , should be SA_AIS_ERR_BUSY.\n");
		ret = SAF_TEST_FAIL;
	}

final_1:
	kill(pid, SIGUSR1);

	wait(&status);
	if (WIFEXITED(status) == 0){
		printf(" the return of child process should be SA_AIS_OK\n");
		ret = SAF_TEST_UNRESOLVED;
	}
	else
		if(ret == SAF_TEST_PASS)
			ret = WEXITSTATUS(status);

	saMsgFinalize(msgHandle);
	return ret;
}

