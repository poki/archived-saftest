/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgQueueUnlink
 * Description:   
 *   create a message queue with SA_MSG_QUEUE_PERSISTENT set, close it
 *   then unlink it.check if the queue is valid.
 *   Return = SA_AIS_OK
 *   Same with 1-1
 * Line:        P40-32:P40-35
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "saMsg.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char same_case[]="./1-1.test";

	execl(same_case, NULL);
       	printf("        Can't find case %s\n", same_case);
	return SAF_TEST_UNKNOWN; 
}
