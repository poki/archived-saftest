/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgMessageSend
 * Description:   
 *   Call the api with NULL destination and other valid params.
 *   Return = SA_AIS_ERR_INVALID_PARAM
 * Line:        P54-38:P54-40
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saMsg.h"
#include "saf_test.h"

static void msg_received_callback(SaMsgQueueHandleT msgHandle)
{
	return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saMsgMessageSend";
	SaAisErrorT     expectedReturn = SA_AIS_ERR_INVALID_PARAM;
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = NULL,
                .saMsgQueueGroupTrackCallback = NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = msg_received_callback
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

	char messageData[10] = "hello";

	SaMsgMessageT	message = {
			.type = 0,
			.version = 0,
			.size = 10,
			.senderName = NULL,
			.data = messageData,
			.priority = SA_MSG_MESSAGE_HIGHEST_PRIORITY
	};

        int ret = SAF_TEST_PASS;
	
        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			" SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	msgError = saMsgMessageSend(msgHandle,
				NULL,
				&message,
				SA_TIME_ONE_SECOND);
	if (msgError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n",
			apiName,
			get_error_string(msgError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
	}

	saMsgFinalize(msgHandle);
	return ret;
}

