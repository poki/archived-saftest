This file defines the coverage for the saMsgMessageReply conformance testing.

Assertion       Status
1-1             YES
1-2		YES
2-1		YES
2-2		YES
2-3		YES
3-1		YES
3-2		YES
4-1		YES
4-2		YES
5		YES
6		YES
7		YES
8		YES
9-1		YES
9-2		NO
9-3		NO
10		YES
11		NO
12		YES
13		NO
14-1		YES
14-2		YES
15-1		YES
15-2		YES
16		NO
17		NO
18-1		YES
18-2		YES
19		YES

