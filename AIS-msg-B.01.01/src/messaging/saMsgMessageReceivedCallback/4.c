/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgMessageReceivedCallbackT
 * Description:
 *   Test if the callback is invoked whenever a message is placed in the
 *   message queue, irrespective of its priority.
 *   Return = NULL
 * Line:        P63-9:P63-11
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saMsg.h"
#include "saf_test.h"

int callbackCount = 0;
static void msg_received_callback(SaMsgQueueHandleT queueHandle)
{
	callbackCount ++;
	return;
}

int main(int argc, char *argv[])
{
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = NULL,
                .saMsgQueueGroupTrackCallback =	NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = msg_received_callback
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	queueName = {
		.length = strlen("queueName"),
		.value 	= "queueName"
	};
	SaMsgQueueCreationAttributesT	creationAttributes = {
			.creationFlags = 0,
			.size = {10, 10, 10, 10},
			.retentionTime = 0
	};

	SaMsgQueueHandleT 	queueHandle;
	SaSelectionObjectT 	selObject;
	char messageData[10] = "hello";
	SaMsgMessageT	message = {
			.type = 0,
			.version = 0,
			.size = 10,
			.senderName = &queueName,
			.data = messageData,
			.priority = SA_MSG_MESSAGE_HIGHEST_PRIORITY
	};

	fd_set readSet;
	int selectRet;

        int ret = SAF_TEST_PASS;

        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	msgError = saMsgQueueOpen(msgHandle, 
				&queueName, 
				&creationAttributes,
			SA_MSG_QUEUE_CREATE | SA_MSG_QUEUE_RECEIVE_CALLBACK,
				SA_TIME_MAX,
				&queueHandle);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpen, Return value: %s,should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}

	msgError = saMsgSelectionObjectGet(msgHandle, &selObject);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgSelectionObjectGet, Return value: %s,should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}

	msgError = saMsgMessageSend(msgHandle,
				&queueName,
				&message,
				SA_TIME_ONE_SECOND);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgMessageSend, Return value: %s,should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}

	message.priority = SA_MSG_MESSAGE_LOWEST_PRIORITY;
	msgError = saMsgMessageSend(msgHandle,
				&queueName,
				&message,
				SA_TIME_ONE_SECOND);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgMessageSend, Return value: %s,should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}

	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
	do{
		selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        	if ((selectRet == -1) || (selectRet == 0)){
                	printf("  select error!\n");
                	ret = SAF_TEST_UNRESOLVED;
			break;
        	}

		msgError = saMsgDispatch(msgHandle,SA_DISPATCH_ALL);
		if (msgError != SA_AIS_OK){
			printf("  Does not conform the expected behaviors!\n");
                	printf("  saMsgDispatch, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(msgError));
                	ret = SAF_TEST_UNRESOLVED;
			break;
		}
	}while(callbackCount != 2);

final_2 :
	saMsgQueueClose(queueHandle);

final_1 :
	saMsgFinalize(msgHandle);
	return ret;
}




