/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgMessageSendReceive
 * Description:   
 *   Call the api with in invalid msgHandle which is uninitialized.
 *   Return = SA_AIS_ERR_BAD_HANDLE
 * Line:        P65-21:P65-24
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saMsg.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saMsgMessageSendReceive";
	SaAisErrorT     expectedReturn = SA_AIS_ERR_BAD_HANDLE;
        SaMsgHandleT   msgHandle;
	SaAisErrorT	msgError;
	
	SaNameT 	queueName = {
		.length = strlen("queueName"),
		.value 	= "queueName"
	};

	char messageData[10] = "hello";
	char replyData[10] = "hello";
	SaMsgMessageT	message = {
			.type = 0,
			.version = 0,
			.size = 10,
			.senderName = &queueName,
			.data = messageData,
			.priority = SA_MSG_MESSAGE_HIGHEST_PRIORITY
	};
	
	SaMsgMessageT	receiveMsg = {
			.type = 0,
			.version = 0,
			.size = 10,
			.senderName = &queueName,
			.data = replyData,
			.priority = SA_MSG_MESSAGE_HIGHEST_PRIORITY
	};
	
        int ret = SAF_TEST_PASS;
	
	msgError = saMsgMessageSendReceive(msgHandle,
				&queueName,
				&message,
				&receiveMsg,
				NULL,
				SA_TIME_MAX);
	if (msgError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n",
			apiName,
			get_error_string(msgError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
	}

	return ret;
}


