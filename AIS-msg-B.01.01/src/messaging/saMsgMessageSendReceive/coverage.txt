This file defines the coverage for the saMsgMessageSendReceive conformance testing.

Assertion       Status
1-1             YES
1-2		YES
2-1		YES
2-2		YES
2-3		YES
2-4		YES
3-1		YES
3-2		YES
3-3		YES
4-1		YES
4-2		YES
5-1		YES
5-2		YES
6		YES
7-1		YES
7-2		YES
8-1		YES
8-2		YES
9-1		YES
9-2		YES
10-1		YES
10-2		YES
11		YES
12		YES
13		YES
14		YES
15-1		NO
15-2		NO
15-3		NO
16		YES
17		NO
18		YES
19		NO
20-1		YES
20-2		YES
21		YES
22		NO
23		NO
24		YES
25		YES
26		YES
27		YES


