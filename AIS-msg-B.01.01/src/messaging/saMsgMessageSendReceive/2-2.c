/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgMessageSendReceive
 * Description:   
 *   Call the api with valid destination which is a pointer to the name
 *   of a unicast message queue group.
 *   Return = SA_AIS_OK
 * Line:        P65-25:P65-28
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include "saMsg.h"
#include "saf_test.h"

SaAisErrorT sendReturn;
sem_t sem_start_send;
sem_t sem_end_send;
SaInvocationT invocationGlobal = 0;
SaAisErrorT errorGlobal;
static void msg_deliver_callback (SaInvocationT invocation,
        SaAisErrorT error)
{
	invocationGlobal = invocation;
	errorGlobal = error;
        return;
}

static void *send_thread(void *arg)
{
	SaNameT		queueGroupName = {
		.length = strlen("queue"),
		.value = "queue"
	};
	char messageData[10] = "hello";
	char receiveData[10];
	SaMsgMessageT	receiveMsg = {
			.type = 0,
			.version = 0,
			.size = 10,
			.senderName = &queueGroupName,
			.data = receiveData,
			.priority = SA_MSG_MESSAGE_HIGHEST_PRIORITY
	};
	SaMsgMessageT	message = {
			.type = 0,
			.version = 0,
			.size = 10,
			.senderName = &queueGroupName,
			.data = messageData,
			.priority = SA_MSG_MESSAGE_HIGHEST_PRIORITY
	};

	sem_post(&sem_start_send);
	sendReturn = saMsgMessageSendReceive(* ((SaMsgHandleT *) arg),
					&queueGroupName,
					&message,
					&receiveMsg,
					NULL,
					SA_TIME_MAX);
	sem_post(&sem_end_send);
	return NULL;
}

int main(int argc, char *argv[])
{
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = NULL,
                .saMsgQueueGroupTrackCallback =	NULL,
		.saMsgMessageDeliveredCallback = msg_deliver_callback,
		.saMsgMessageReceivedCallback = NULL
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

	SaNameT 	queueName = {
		.length = strlen("queueName"),
		.value 	= "queueName"
	};

	SaMsgQueueCreationAttributesT	creationAttributes = {
			.creationFlags = 0,
			.size = {10, 10, 10, 10},
			.retentionTime = 0
	};

	SaMsgQueueHandleT 	queueHandle;
	SaSelectionObjectT 	selObject;

	char replyData[10] = "hello";
	char messageData[10];
	char cmpData[10] = "hello";

	SaMsgMessageT	message = {
			.type = 0,
			.version = 0,
			.size = 10,
			.senderName = &queueName,
			.data = messageData,
			.priority = SA_MSG_MESSAGE_HIGHEST_PRIORITY
	};

	SaMsgMessageT	replyMsg = {
			.type = 0,
			.version = 0,
			.size = 10,
			.senderName = &queueName,
			.data = replyData,
			.priority = SA_MSG_MESSAGE_HIGHEST_PRIORITY
	};

	SaNameT		queueGroupName = {
		.length = strlen("queue"),
		.value = "queue"
	};

	fd_set readSet;
	int selectRet;
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	pthread_t thread_id;
	int pthreadError;

	SaMsgSenderIdT senderId;

        int ret = SAF_TEST_PASS;

	sem_init(&sem_start_send, 0, 0);
	sem_init(&sem_end_send, 0, 0);
        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	msgError = saMsgQueueOpen(msgHandle,
				&queueName,
				&creationAttributes,
			SA_MSG_QUEUE_CREATE | SA_MSG_QUEUE_EMPTY,
				SA_TIME_MAX,
				&queueHandle);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueOpen, Return value: %s,should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}

	msgError = saMsgQueueGroupCreate(msgHandle, &queueGroupName,
				SA_MSG_QUEUE_GROUP_ROUND_ROBIN);
				
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueGroupCreate, Return value: %s, should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}

	msgError = saMsgQueueGroupInsert(msgHandle, &queueGroupName,
					&queueName);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueGroupInsert, Return value: %s, should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	msgError = saMsgSelectionObjectGet(msgHandle, &selObject);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgSelectionObjectGet, Return value: %s,should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	pthreadError = pthread_create(&thread_id,
					NULL,
					send_thread,
					(void*)&msgHandle);
	if (pthreadError){
		printf("	invoke \"pthread_create\" has error,"
				" return %s\n", strerror(pthreadError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	sem_wait(&sem_start_send);
	msgError = saMsgMessageGet(queueHandle, &message, NULL, &senderId,
					SA_TIME_MAX);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgMessageGet, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	if(strncmp(messageData, cmpData, 10) != 0){
		printf(" received message is wrong;\n");
		printf("Should be %s,Return :%s\n",
			cmpData,messageData);
		ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	msgError = saMsgMessageReplyAsync(msgHandle, 1, &replyMsg, &senderId,
					SA_MSG_MESSAGE_DELIVERED_ACK);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgMessageReplyAsync, Return value: %s,should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
	selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
        }

	msgError = saMsgDispatch(msgHandle, dispatchFlag);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgDispatch, Return value: %s, should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	if (errorGlobal != SA_AIS_OK){
		printf(" error from the callback function should be"
						" SA_AIS_OK\n");
		ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	if (invocationGlobal != 1){
		printf("  Does not conform the expected behaviors!\n");
		printf("The invocation should be 1, Return :%lld\n",
			invocationGlobal);
		ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	sem_wait(&sem_end_send);

	if (sendReturn != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf(" the return from the thread should be SA_AIS_OK"
		",Return :%s\n",
		get_error_string(sendReturn));
		ret = SAF_TEST_FAIL;
	}
final_3 :
	saMsgQueueGroupDelete(msgHandle, &queueGroupName);
final_2 :
	saMsgQueueClose(queueHandle);
final_1 :
	saMsgFinalize(msgHandle);
	return ret;
}


