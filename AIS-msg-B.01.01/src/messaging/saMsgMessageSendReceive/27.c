/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saMsgMessageSendReceive
 * Description:   
 *   Test if the api saMsgMessageSendReceive can return
 *   SA_AIS_ERR_QUEUE_NOT_AVAILABLE
 *   Return = SA_AIS_ERR_QUEUE_NOT_AVAILABLE.
 * Line:        P68-13:P68-15
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include "saMsg.h"
#include "saf_test.h"

SaAisErrorT sendReturn;
sem_t sem_end_send;

static void *send_thread(void *arg)
{
	SaNameT		queueGroupName = {
		.length = strlen("queue"),
		.value = "queue"
	};
	char messageData[10] = "hello";
	char receiveData[10];
	SaMsgMessageT	receiveMsg = {
			.type = 0,
			.version = 0,
			.size = 10,
			.senderName = &queueGroupName,
			.data = receiveData,
			.priority = SA_MSG_MESSAGE_HIGHEST_PRIORITY
	};
	SaMsgMessageT	message = {
			.type = 0,
			.version = 0,
			.size = 10,
			.senderName = &queueGroupName,
			.data = messageData,
			.priority = SA_MSG_MESSAGE_HIGHEST_PRIORITY
	};

	sendReturn = saMsgMessageSendReceive(* ((SaMsgHandleT *) arg),
					&queueGroupName,
					&message,
					&receiveMsg,
					NULL,
					SA_TIME_MAX);
	sem_post(&sem_end_send);
	return NULL;
}

int main(int argc, char *argv[])
{
        SaMsgHandleT   msgHandle;
        SaMsgCallbacksT msgCallback = {
        	.saMsgQueueOpenCallback  = NULL,
                .saMsgQueueGroupTrackCallback = NULL,
		.saMsgMessageDeliveredCallback = NULL,
		.saMsgMessageReceivedCallback = NULL
	};
	SaAisErrorT	msgError;
        SaVersionT      msgVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

	SaNameT		queueGroupName = {
		.length = strlen("queue"),
		.value = "queue"
	};

	pthread_t thread_id;
	int pthreadError;

        int ret = SAF_TEST_PASS;

	sem_init(&sem_end_send,0,0);
        msgError = saMsgInitialize(&msgHandle, &msgCallback, &msgVersion);
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	msgError = saMsgQueueGroupCreate(msgHandle,&queueGroupName,
				SA_MSG_QUEUE_GROUP_ROUND_ROBIN);
				
	if (msgError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saMsgQueueGroupCreate, Return value: %s, should be"
			"SA_AIS_OK\n", get_error_string(msgError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}

	pthreadError = pthread_create(&thread_id,
					NULL,
					send_thread,
					(void*)&msgHandle);
	if (pthreadError){
		printf("	invoke \"pthread_create\" has error,"
				" return %s\n", strerror(pthreadError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}

	sem_wait(&sem_end_send);

	if (sendReturn != SA_AIS_ERR_QUEUE_NOT_AVAILABLE){
		printf("  Does not conform the expected behaviors!\n");
		printf(" the return from the thread should be "
		" SA_AIS_ERR_QUEUE_NOT_AVAILABLE,Return :%s\n", 
		get_error_string(sendReturn));
		ret = SAF_TEST_FAIL;
	}
final_2 :
	saMsgQueueGroupDelete(msgHandle,&queueGroupName);

final_1 :
	saMsgFinalize(msgHandle);
	return ret;
}


