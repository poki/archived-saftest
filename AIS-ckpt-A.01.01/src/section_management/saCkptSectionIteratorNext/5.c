/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 */

#include <stdio.h>
#include <string.h>
#include "im_spec.h"
#include "saf_test.h"

/*
 * 5.c
 *
 * function description :
 *	Invoke saCkptSectionIteratorNext when no more section
 *	match section chosen specified.
 *	Thiss api return value should be SA_ERR_NOT_EXIST.
 * returns :
 *      SAF_TEST_PASS:          api test success
 *      SAF_TEST_FAIL:          api test fail
 *      SAF_TEST_BLOCK:         case hangs, this value isn't returned  by case.
 *      SAF_TEST_NOTSUPPORT:    this api has not been supplied
 *      SAF_TEST_UNRESOLVED:    can't invoke successfully because of other
 *                              operation failure
 *      SAF_TEST_UNKNOWN:       can't run test case successfully and can't
 *                              known the reason
 */
void ckpt_open_callback (SaInvocationT invocation,
        const SaCkptCheckpointHandleT *checkpoint_handle,
        SaErrorT error)
{
        return ;
}

void ckpt_sync_callback (SaInvocationT invocation,
        SaErrorT error)
{
        return ;
}

int
main(int argc, char* argv[])
{
	char api_name[] = "saCkptSectionInteratorNext";
	int expected_return = SA_ERR_NOT_EXIST;
	SaErrorT ckpt_error;

	int re = SAF_TEST_UNKNOWN;

	SaCkptHandleT ckpt_handle ;
	
	SaVersionT ckpt_version ;
	ckpt_version.major = VERSION_MAJOR;
	ckpt_version.minor = VERSION_MINOR;
	ckpt_version.releaseCode = RELEASE_CODE ;
	
	SaCkptCallbacksT ckpt_callback ;
	ckpt_callback.saCkptCheckpointOpenCallback = ckpt_open_callback ;
	ckpt_callback.saCkptCheckpointSynchronizeCallback = ckpt_sync_callback;


	ckpt_error = saCkptInitialize ( &ckpt_handle,
					&ckpt_callback,
					&ckpt_version) ;

        if (ckpt_error != SA_OK)
        {
                 printf("	initialization fail, return %s \n",
				get_aisAerror_string(ckpt_error));
	         return SAF_TEST_UNRESOLVED ;
        }

	//open checkpoint
	SaCkptCheckpointHandleT checkpoint_handle;


	SaNameT ckpt_name;
        char name_open[] = "checkpoint";
        ckpt_name.length = sizeof(name_open) ;
        memcpy (ckpt_name.value, name_open, ckpt_name.length);

        SaCkptCheckpointCreationAttributesT ckpt_create_attri ;
        ckpt_create_attri.creationFlags = SA_CKPT_WR_ALL_REPLICAS ;
        ckpt_create_attri.retentionDuration = SA_TIME_END;
        ckpt_create_attri.checkpointSize = 1000 ;
        ckpt_create_attri.maxSectionSize = 100;
        ckpt_create_attri.maxSections = 10;
        ckpt_create_attri.maxSectionIdSize = 10 ;

        ckpt_error = saCkptCheckpointOpen (
                                        &ckpt_name,
                                        &ckpt_create_attri,
                                        SA_CKPT_CHECKPOINT_COLOCATED
                                        |SA_CKPT_CHECKPOINT_WRITE,
                                        SA_TIME_END,
                                        &checkpoint_handle);
        if ( ckpt_error != SA_OK ){
                printf("	open checkpoint has error, return %s\n",
                                        get_aisAerror_string(ckpt_error));
		saCkptFinalize(&ckpt_handle);
                return SAF_TEST_UNRESOLVED;
        }

	char init_data[] = "AIS checkpoint service API test.";

	//create sections
        SaCkptSectionCreationAttributesT section_attri;
	SaCkptSectionIdT section_id = SA_CKPT_GENERATED_SECTION_ID;
        section_attri.expirationTime = SA_TIME_END;
        section_attri.sectionId = &(section_id);

       	ckpt_error = saCkptSectionCreate(&checkpoint_handle,
                                        &section_attri,
                                        &init_data,
                                        sizeof(init_data));
       	if ( ckpt_error != SA_OK ){
               	printf("        create section, return %s\n",
                       		get_aisAerror_string(ckpt_error));
               	re = SAF_TEST_UNRESOLVED;
               	goto out;
	}

	//iterate section with SA_CKPT_SECTIONS_CORRUPTED.
	SaCkptSectionIteratorT section_iterator;
	ckpt_error = saCkptSectionIteratorInitialize(&checkpoint_handle,
					SA_CKPT_SECTIONS_CORRUPTED,
					0,
					&section_iterator);
	if ( ckpt_error != SA_OK){
		printf("	invoke %s, return %s (should be %s)\n", 
			api_name,				
			get_aisAerror_string(ckpt_error),
			get_aisAerror_string(expected_return));
		re = SAF_TEST_FAIL;
		goto out;
	}
	
	SaCkptSectionDescriptorT section_descriptor;
	ckpt_error = saCkptSectionIteratorNext(&section_iterator,
						&section_descriptor);
	if ( ckpt_error != expected_return ){
		printf("	invoke %s, return %s (should be %s)",
				api_name,
				get_aisAerror_string(ckpt_error),
				get_aisAerror_string(expected_return));
		re = SAF_TEST_FAIL;
		goto out;
	}

	ckpt_error = saCkptSectionIteratorFinalize(&section_iterator);
	if ( ckpt_error != SA_OK){
		printf("	finalize section iterator has error, "
			"return %s.", get_aisAerror_string(ckpt_error));
		re = SAF_TEST_FAIL;
		goto out;
	}
	else{
		re = SAF_TEST_PASS;
	}
out:
        saCkptCheckpointClose(&checkpoint_handle);
        saCkptCheckpointUnlink(&ckpt_name);
        saCkptFinalize(&ckpt_handle);
 	return re ;
}
