/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 */

#include <stdio.h>
#include <string.h>
#include "im_spec.h"
#include "saf_test.h"

/*
 * 6.c
 *
 * function description :
 *	Invoke saCkptSectionIteratorInitialize with normal params.
 *	when sectionsChosen is SA_CKPT_SECTIONS_FOREVER,
 *	SA_CKPT_SECTIONS_CORRUPTED or SA_CKPT_SECTIONS_ANY, expiration
 *	time is any value, api should return same value.
 *	This api return value should be SA_OK.
 * returns :
 *      SAF_TEST_PASS:          api test success
 *      SAF_TEST_FAIL:          api test fail
 *      SAF_TEST_BLOCK:         case hangs, this value isn't returned  by case.
 *      SAF_TEST_NOTSUPPORT:    this api has not been supplied
 *      SAF_TEST_UNRESOLVED:    can't invoke successfully because of other
 *                              operation failure
 *      SAF_TEST_UNKNOWN:       can't run test case successfully and can't
 *                              known the reason
 */
void ckpt_open_callback (SaInvocationT invocation,
        const SaCkptCheckpointHandleT *checkpoint_handle,
        SaErrorT error)
{
        return ;
}

void ckpt_sync_callback (SaInvocationT invocation,
        SaErrorT error)
{
        return ;
}

int
main(int argc, char* argv[])
{
	char api_name[] = "saCkptSectionInteratorInitialize";
	int expected_return = SA_OK;
	SaErrorT ckpt_error;

	int re = SAF_TEST_UNKNOWN;

	SaCkptHandleT ckpt_handle ;
	
	SaVersionT ckpt_version ;
	ckpt_version.major = VERSION_MAJOR;
	ckpt_version.minor = VERSION_MINOR;
	ckpt_version.releaseCode = RELEASE_CODE ;
	
	SaCkptCallbacksT ckpt_callback ;
	ckpt_callback.saCkptCheckpointOpenCallback = ckpt_open_callback ;
	ckpt_callback.saCkptCheckpointSynchronizeCallback = ckpt_sync_callback;


	ckpt_error = saCkptInitialize ( &ckpt_handle,
					&ckpt_callback,
					&ckpt_version) ;

        if (ckpt_error != SA_OK)
        {
                 printf("	initialization fail, return %s \n",
				get_aisAerror_string(ckpt_error));
	         return SAF_TEST_UNRESOLVED ;
        }

	//open checkpoint
	SaCkptCheckpointHandleT checkpoint_handle;


	SaNameT ckpt_name;
        char name_open[] = "checkpoint";
        ckpt_name.length = sizeof(name_open) ;
        memcpy (ckpt_name.value, name_open, ckpt_name.length);

        SaCkptCheckpointCreationAttributesT ckpt_create_attri ;
        ckpt_create_attri.creationFlags = SA_CKPT_WR_ALL_REPLICAS ;
        ckpt_create_attri.retentionDuration = SA_TIME_END;
        ckpt_create_attri.checkpointSize = 1000 ;
        ckpt_create_attri.maxSectionSize = 100;
        ckpt_create_attri.maxSections = 10;
        ckpt_create_attri.maxSectionIdSize = 10 ;

        ckpt_error = saCkptCheckpointOpen (
                                        &ckpt_name,
                                        &ckpt_create_attri,
                                        SA_CKPT_CHECKPOINT_COLOCATED
                                        |SA_CKPT_CHECKPOINT_WRITE,
                                        SA_TIME_END,
                                        &checkpoint_handle);
        if ( ckpt_error != SA_OK ){
                printf("	open checkpoint has error, return %s\n",
                                        get_aisAerror_string(ckpt_error));
		saCkptFinalize(&ckpt_handle);
                return SAF_TEST_UNRESOLVED;
        }

	char init_data[] = "AIS checkpoint service API test.";

	//create sections
        SaCkptSectionCreationAttributesT section_attri[4];

	SaTimeT expiration_time = SA_TIME_END / 2;

        section_attri[0].expirationTime = SA_TIME_END;
	section_attri[1].expirationTime = expiration_time;
        section_attri[2].expirationTime = expiration_time - 1;
        section_attri[3].expirationTime = expiration_time + 1;

        int i=0;
	for ( i=0;i<4;i++ ){
                //section_id[i].idLen = sizeof( section_id[i].id );
                SaCkptSectionIdT section_id = SA_CKPT_GENERATED_SECTION_ID;
		section_attri[i].sectionId = &(section_id);

        	ckpt_error = saCkptSectionCreate(&checkpoint_handle,
                                        &section_attri[i],
                                        &init_data,
                                        sizeof(init_data));
        	if ( ckpt_error != SA_OK ){
                	printf("        create section %d, return %s\n", i,
                        		get_aisAerror_string(ckpt_error));
                	re = SAF_TEST_UNRESOLVED;
                	goto out;
		}
	}

	//iterate section with SA_CKPT_SECTIONS_FOREVER
	SaCkptSectionIteratorT section_iterator[5];
	ckpt_error = saCkptSectionIteratorInitialize(&checkpoint_handle,
					SA_CKPT_SECTIONS_FOREVER,
					0,
					&section_iterator[0]);
	if ( ckpt_error != SA_OK){
		printf("	invoke %s, return %s (should be %s)\n", 
			api_name,				
			get_aisAerror_string(ckpt_error),
			get_aisAerror_string(expected_return));
		re = SAF_TEST_FAIL;
		goto out;
	}
	
	SaCkptSectionDescriptorT section_descriptor;
	
	ckpt_error = saCkptSectionIteratorNext(&section_iterator[0],
						&section_descriptor);
	if ( ckpt_error == SA_OK ){
		if ( section_descriptor.expirationTime != SA_TIME_END ){
			printf("	iterator can't get right sectoin."
					"expiration time doesn't match!");
			re = SAF_TEST_FAIL;
			goto out;
		}
		if ( 0 != strcmp( section_descriptor.sectionId.id,
					(*section_attri[0].sectionId).id)){
                        printf("        iterator can't get right sectoin."
                                        "section id  doesn't match!");
                        re = SAF_TEST_FAIL;
                        goto out;
		}
	}
	else{
		printf("	iterator fail, return %s (should be %s)",
				get_aisAerror_string(ckpt_error),
				get_aisAerror_string(SA_OK));
		re = SAF_TEST_FAIL;
		goto out;
	}

	ckpt_error = saCkptSectionIteratorFinalize(&section_iterator[0]);
	if ( ckpt_error != SA_OK){
		printf("	finalize section iterator has error, "
			"return %s.", get_aisAerror_string(ckpt_error));
		re = SAF_TEST_FAIL;
		goto out;
	}



	//iterate section with SA_CKPT_SECTIONS_LEQ_EXPIRATION_TIME
        ckpt_error = saCkptSectionIteratorInitialize(&checkpoint_handle,
                                        SA_CKPT_SECTIONS_LEQ_EXPIRATION_TIME,
                                        expiration_time,
                                        &section_iterator[1]);
        if ( ckpt_error != SA_OK){
                printf("        invoke %s, return %s (should be %s)\n",
                        api_name,
                        get_aisAerror_string(ckpt_error),
                        get_aisAerror_string(expected_return));
                re = SAF_TEST_FAIL;
                goto out;
        }

        while ( (ckpt_error = saCkptSectionIteratorNext(&section_iterator[1],
                 		&section_descriptor)) == SA_OK ){
		if ( section_descriptor.expirationTime == expiration_time ){
	                if (0 != strcmp( section_descriptor.sectionId.id,
                                        (*section_attri[1].sectionId).id)){
 	                        printf("        iterator can't get right"
					" sectoin. section id  doesn't match!");
                        	re = SAF_TEST_FAIL;
                        	goto out;
			}
		}
                else if ( section_descriptor.expirationTime < expiration_time ){
                        if (0 != strcmp( section_descriptor.sectionId.id,
                                        (*section_attri[2].sectionId).id)){
                                printf("        iterator can't get right"                                               " sectoin. section id  doesn't match!");
                                re = SAF_TEST_FAIL;
                                goto out;
                        }
                }
                else{
                        printf("        iterator can't get right sectoin."
                                        "expiration time doesn't match!");
                        re = SAF_TEST_FAIL;
                        goto out;
                }

        }
        ckpt_error = saCkptSectionIteratorFinalize(&section_iterator[1]);
        if ( ckpt_error != SA_OK){
                printf("        finalize section iterator has error, "
                        "return %s.", get_aisAerror_string(ckpt_error));
                re = SAF_TEST_FAIL;
                goto out;
        }


	//iterate section with SA_CKPT_SECTIONS_GEQ_EXPIRATION_TIME
        ckpt_error = saCkptSectionIteratorInitialize(&checkpoint_handle,
                                        SA_CKPT_SECTIONS_GEQ_EXPIRATION_TIME,
                                        expiration_time,
                                        &section_iterator[2]);
        if ( ckpt_error != SA_OK){
                printf("        invoke %s, return %s (should be %s)\n",
                        api_name,
                        get_aisAerror_string(ckpt_error),
                        get_aisAerror_string(expected_return));
                re = SAF_TEST_FAIL;
                goto out;
        }

        while ( (ckpt_error = saCkptSectionIteratorNext(&section_iterator[2],
                                &section_descriptor)) == SA_OK ){
                if ( section_descriptor.expirationTime == expiration_time ){
                        if (0 != strcmp( section_descriptor.sectionId.id,
                                        (*section_attri[1].sectionId).id)){
                                printf("        iterator can't get right"
                                        " sectoin. section id  doesn't match!");
                                re = SAF_TEST_FAIL;
                                goto out;
                        }
                }
                else if ( section_descriptor.expirationTime > expiration_time ){
                        if (0 != strcmp( section_descriptor.sectionId.id,
                                        (*section_attri[3].sectionId).id)){
                                printf("        iterator can't get right"                                               " sectoin. section id  doesn't match!");
                                re = SAF_TEST_FAIL;
                                goto out;
                        }
                }
                else{
                        printf("        iterator can't get right sectoin."
                                        "expiration time doesn't match!");
                        re = SAF_TEST_FAIL;
                        goto out;
                }

        }
        ckpt_error = saCkptSectionIteratorFinalize(&section_iterator[2]);
        if ( ckpt_error != SA_OK){
                printf("        finalize section iterator has error, "
                        "return %s.", get_aisAerror_string(ckpt_error));
                re = SAF_TEST_FAIL;
                goto out;
        }

	//iterate section with SA_CKPT_SECTIONS_CORRUPTED
	ckpt_error = saCkptSectionIteratorInitialize(&checkpoint_handle,
                                        SA_CKPT_SECTIONS_CORRUPTED,
                                        0,
                                        &section_iterator[3]);
        if ( ckpt_error != SA_OK){
                printf("        invoke %s, return %s (should be %s)\n",
                        api_name,
                        get_aisAerror_string(ckpt_error),
                        get_aisAerror_string(expected_return));
                re = SAF_TEST_FAIL;
                goto out;
        }
        ckpt_error = saCkptSectionIteratorNext(&section_iterator[3],
                                                &section_descriptor);
        if ( ckpt_error != SA_ERR_NOT_EXIST ){
		printf("	interater fail, return %s (should be %s)\n",
				get_aisAerror_string(ckpt_error),
				get_aisAerror_string(SA_ERR_NOT_EXIST));
		re = SAF_TEST_FAIL;
		goto out;
	}
        ckpt_error = saCkptSectionIteratorFinalize(&section_iterator[3]);
        if ( ckpt_error != SA_OK){
                printf("        finalize section iterator has error, "
                        "return %s.", get_aisAerror_string(ckpt_error));
                re = SAF_TEST_FAIL;
                goto out;
        }

	//iterate section with SA_CKPT_SECTIONS_ANY 
        ckpt_error = saCkptSectionIteratorInitialize(&checkpoint_handle,
                                        SA_CKPT_SECTIONS_ANY,
                                        0,
                                        &section_iterator[4]);
        if ( ckpt_error != SA_OK){
                printf("        invoke %s, return %s (should be %s)\n",
                        api_name,
                        get_aisAerror_string(ckpt_error),
                        get_aisAerror_string(expected_return));
                re = SAF_TEST_FAIL;
                goto out;
        }
	int section_num =0;
        while ( (ckpt_error = saCkptSectionIteratorNext(&section_iterator[4],
                                &section_descriptor)) == SA_OK ){
		section_num++;
	}
        ckpt_error = saCkptSectionIteratorFinalize(&section_iterator[4]);
        if ( ckpt_error != SA_OK){
                printf("        finalize section iterator has error, "
                        "return %s.", get_aisAerror_string(ckpt_error));
                re = SAF_TEST_FAIL;
                goto out;
        }
	if ( section_num != 4 ){
		printf("	iterator fail, section number isn't match!");
		re = SAF_TEST_FAIL;
		goto out;
	}
	else{
		re = SAF_TEST_PASS;
	}
out:
        saCkptCheckpointClose(&checkpoint_handle);
        saCkptCheckpointUnlink(&ckpt_name);
        saCkptFinalize(&ckpt_handle);
 	return re ;
}
