/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 */

#include <stdio.h>

#include "im_spec.h"
#include "saf_test.h"

/*
 * 1.c
 *
 * function description :
 * 	Invoke saCkptSectionOverwrite before initialization.
 *	This api return value should be SA_ERR_INIT.
 * returns :
 *      SAF_TEST_PASS:          api test success
 *      SAF_TEST_FAIL:          api test fail
 *      SAF_TEST_BLOCK:         case hangs, this value isn't returned  by case.
 *      SAF_TEST_NOTSUPPORT:    this api has not been supplied
 *      SAF_TEST_UNRESOLVED:    can't invoke successfully because of other
 *                              operation failure
 *      SAF_TEST_UNKNOWN:       can't run test case successfully and can't
 *                              known the reason
 */

int
main(int argc, char* argv[])
{
	char api_name[] = "saCkptSectionOverwrite";
	int expected_return = SA_ERR_INIT;
	SaErrorT ckpt_error;

	int re = SAF_TEST_UNKNOWN;
	
	SaCkptCheckpointHandleT checkpoint_handle;
	
	char new_data[] = "AIS checkpoint service API test.";
	
	SaCkptSectionIdT section_id;
	section_id.id = "my_sect";
	section_id.idLen = sizeof(section_id.id);
 
	ckpt_error = saCkptSectionOverwrite(&checkpoint_handle,
					&section_id,
					(void *)new_data,
					sizeof(new_data));
	
	if ( ckpt_error != expected_return){
		printf("	invoke %s, return %s (should be %s)\n", 
			api_name,				
			get_aisAerror_string(ckpt_error),
			get_aisAerror_string(expected_return));
		return  SAF_TEST_FAIL;
	}
	else{
		re = SAF_TEST_PASS;
	}
	return re ;
}
