/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include "im_spec.h"
#include "saf_test.h"

/*
 * 6.c
 *
 * function description :
 *	Synchronize with normal param.
 *	SaCkptCheckpointSynchronizeCallbackT should get error code SA_OK.
 * returns :
 *      SAF_TEST_PASS:          api test success
 *      SAF_TEST_FAIL:          api test fail
 *      SAF_TEST_BLOCK:         case hangs, this value isn't returned  by case.
 *      SAF_TEST_NOTSUPPORT:    this api has not been supplied
 *      SAF_TEST_UNRESOLVED:    can't invoke successfully because of other
 *                              operation failure
 *      SAF_TEST_UNKNOWN:       can't run test case successfully and can't
 *                              known the reason
 */
void ckpt_open_callback (SaInvocationT invocation,
        const SaCkptCheckpointHandleT *checkpointHandle,
        SaErrorT error)
{
        return ;
}

int callback_lock = -1;
SaInvocationT callback_invocation;
SaErrorT callback_error;
void ckpt_sync_callback (SaInvocationT invocation,
        SaErrorT error)
{
        callback_lock = 0;
        callback_invocation = invocation;
        callback_error = error;
        callback_lock = 1;
        return ;
}

int
main(int argc, char* argv[])
{
	char api_name[] = "SaCkptCheckpointSynchronizeCallbackT";
	int expected_return = SA_OK;
	SaErrorT ckpt_error;

	int re = SAF_TEST_UNKNOWN;

        SaCkptHandleT ckpt_handle ;

        SaVersionT ckpt_version ;
        ckpt_version.major = VERSION_MAJOR;
        ckpt_version.minor = VERSION_MINOR;
        ckpt_version.releaseCode = RELEASE_CODE ;

        SaCkptCallbacksT ckpt_callback ;
        ckpt_callback.saCkptCheckpointOpenCallback = ckpt_open_callback ;
        ckpt_callback.saCkptCheckpointSynchronizeCallback = ckpt_sync_callback;


        ckpt_error = saCkptInitialize ( &ckpt_handle,
                                        &ckpt_callback,
                                        &ckpt_version) ;

        if (ckpt_error != SA_OK)
        {
                 printf("       initialization fail, return %s \n",
                                        get_aisAerror_string(ckpt_error));
                 return SAF_TEST_UNRESOLVED ;
        }


        SaCkptCheckpointHandleT checkpoint_handle;

        SaNameT ckpt_name;
        char name_open[] = "checkpoint";
        ckpt_name.length = sizeof(name_open) ;
        memcpy (ckpt_name.value, name_open, ckpt_name.length);

        SaCkptCheckpointCreationAttributesT ckpt_create_attri ;
        ckpt_create_attri.creationFlags = SA_CKPT_WR_ALL_REPLICAS ;
        ckpt_create_attri.retentionDuration = SA_TIME_END;
        ckpt_create_attri.checkpointSize = 1000 ;
        ckpt_create_attri.maxSectionSize = 100;
        ckpt_create_attri.maxSections = 10 ;
        ckpt_create_attri.maxSectionIdSize = 10 ;

        ckpt_error = saCkptCheckpointOpen (
                                        &ckpt_name,
                                        &ckpt_create_attri,
                                        SA_CKPT_CHECKPOINT_COLOCATED
                                        |SA_CKPT_CHECKPOINT_WRITE,
                                        SA_TIME_END,
                                        &checkpoint_handle);
        if ( ckpt_error != SA_OK ){
                printf("        create checkpoint has error, return %s\n",
                                        get_aisAerror_string(ckpt_error));
		saCkptFinalize(&ckpt_handle);
                return SAF_TEST_UNRESOLVED;
        }

	SaInvocationT ckpt_invocation = 1;
	ckpt_error = saCkptCheckpointSynchronizeAsync(&ckpt_handle, 1,
					&checkpoint_handle);
	
	if ( ckpt_error != expected_return){
		printf("	checkpoint sync error, return %s "
				"(should be %s)\n", 
				get_aisAerror_string(ckpt_error),
				get_aisAerror_string(expected_return));
		re = SAF_TEST_UNRESOLVED;
		goto out;
	}
	SaSelectionObjectT ckpt_select_obj;
	if ( saCkptSelectionObjectGet (& ckpt_handle,
						&ckpt_select_obj) != SA_OK){
		
                printf("        get object error\n");
		re = SAF_TEST_UNRESOLVED;
		goto out;
	}
		
	fd_set fs;
	int maxfd ;
	FD_ZERO(&fs);
	FD_SET (ckpt_select_obj, &fs) ;
	maxfd = ckpt_select_obj + 1 ;	
	if ( select (maxfd, &fs, NULL, NULL, NULL ) == -1){
		re = SAF_TEST_UNRESOLVED ;
		goto out;				
	}	

	if ( !FD_ISSET (ckpt_select_obj, & fs)){
		saCkptFinalize (& ckpt_handle) ;
		re = SAF_TEST_UNRESOLVED ;
		goto out;
	}

	ckpt_error = saCkptDispatch(&ckpt_handle,SA_DISPATCH_ALL);
	if(ckpt_error != SA_OK ){
		printf("	dispatch fail, return %s\n",
				get_aisAerror_string(ckpt_error));
		re = SAF_TEST_UNRESOLVED;
		goto out;
	}

	int i=0;
	for(i=0;i<5;i++){
		if(callback_lock!=1)
			sleep(1);
		else
			break;
	}
	if(callback_invocation != ckpt_invocation){
		printf("	call %s has error, invocation value "
						"isn't match!\n",api_name);
		re = SAF_TEST_FAIL;
		goto out;
	}
	if(callback_error != expected_return ){
		printf("	call %s has error, error code value "
						"isn't match!\n",api_name);
		re = SAF_TEST_FAIL;
	}
	else{
		re = SAF_TEST_PASS;
	}
out:
	saCkptCheckpointClose(&checkpoint_handle);
	saCkptCheckpointUnlink(&ckpt_name);
	saCkptFinalize(&ckpt_handle);
	return re ;
}
