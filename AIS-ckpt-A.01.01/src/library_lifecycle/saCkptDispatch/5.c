/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>

#include "im_spec.h"
#include "saf_test.h"

/*
 * 5.c
 *
 * function description :
 *	invoke saCkptDispatch with SA_DISPATCH_ONE, SA_DISPATCH_ALL,
 *	The api invocation returned value should be SA_OK.
 * returns :
 *      SAF_TEST_PASS:          api test success
 *      SAF_TEST_FAIL:          api test fail
 *      SAF_TEST_BLOCK:         case hangs, this value isn't returned  by case.
 *      SAF_TEST_NOTSUPPORT:    this api has not been supplied
 *      SAF_TEST_UNRESOLVED:    can't invoke successfully because of other
 *                              operation failure
 *      SAF_TEST_UNKNOWN:       can't run test case successfully and can't
 *                              known the reason
 */
int callback_flag = 0;
void ckpt_open_callback (SaInvocationT invocation,
        const SaCkptCheckpointHandleT *checkpoint_handle,
        SaErrorT error)
{
	callback_flag++;
        return ;
}

void ckpt_sync_callback (SaInvocationT invocation,
        SaErrorT error)
{
        return ;
}

int
main(int argc, char* argv[])
{
	char api_name[] = "saCkptDispatch";
	SaErrorT expected_return = SA_OK;
	int re = SAF_TEST_UNKNOWN;


        SaCkptCallbacksT ckpt_callback ;
        ckpt_callback.saCkptCheckpointOpenCallback = ckpt_open_callback ;
        ckpt_callback.saCkptCheckpointSynchronizeCallback = ckpt_sync_callback;


 	SaErrorT  ckpt_error;


	SaCkptHandleT ckpt_handle ;

        SaVersionT ckpt_version ;
        ckpt_version.major = VERSION_MAJOR;
        ckpt_version.minor = VERSION_MINOR;
        ckpt_version.releaseCode = RELEASE_CODE ;



        ckpt_error = saCkptInitialize ( &ckpt_handle, 
					&ckpt_callback,
					&ckpt_version) ;

        if (ckpt_error != SA_OK)
        {
                 printf("       initialization fail, return %s "
                        "\n", get_aisAerror_string(ckpt_error));
                 return SAF_TEST_UNRESOLVED ;
        }

	int invocation_num = 3;
	
	SaNameT ckpt_name;
	char name_open[] = "checkpoint";
	ckpt_name.length = sizeof(name_open) ;
	memcpy (ckpt_name.value, name_open, ckpt_name.length);
	

	SaCkptCheckpointCreationAttributesT ckpt_create_attri; 
	ckpt_create_attri.creationFlags = SA_CKPT_WR_ALL_REPLICAS ;
	ckpt_create_attri.retentionDuration = SA_TIME_END;
	ckpt_create_attri.checkpointSize = 1000 ;
	ckpt_create_attri.maxSectionSize = 100;
	ckpt_create_attri.maxSections = 10 ;
	ckpt_create_attri.maxSectionIdSize = 10 ;

	int i;
	for (i=0; i<invocation_num; i++){
		ckpt_error = saCkptCheckpointOpenAsync (&ckpt_handle,
                                           i,
				           &ckpt_name,
				           &ckpt_create_attri , 
				           SA_CKPT_CHECKPOINT_COLOCATED
					   |SA_CKPT_CHECKPOINT_WRITE);
		if( ckpt_error != SA_OK ){
			printf("	open checkpoint fail, return %s\n",
					get_aisAerror_string(ckpt_error));
			saCkptFinalize(&ckpt_handle);
			return SAF_TEST_UNRESOLVED;
		}
	}
	ckpt_error = saCkptDispatch(&ckpt_handle,SA_DISPATCH_ONE);
	if( ckpt_error != expected_return ){
                printf("        invoke %s, return %s (should be %s)\n",
                        api_name,
                        get_aisAerror_string(ckpt_error),
                        get_aisAerror_string(expected_return));
                re = SAF_TEST_FAIL;
                goto out;
	}
	if( callback_flag != 1){
                printf("        callback number error, callback is %d "
			"(should be 1)\n", callback_flag);
                re = SAF_TEST_FAIL;
                goto out;
	}

        ckpt_error = saCkptDispatch(&ckpt_handle,SA_DISPATCH_ALL);
        if( ckpt_error != expected_return ){
                printf("        invoke %s, return %s (should be %s)\n",
                        api_name,
                        get_aisAerror_string(ckpt_error),
                        get_aisAerror_string(expected_return));
                re = SAF_TEST_FAIL;
                goto out;
        }
        if( callback_flag != invocation_num){
                printf("        callback number error, callback is %d "
                        "(should be 3)\n", callback_flag);
                re = SAF_TEST_FAIL;
                goto out;
        }

	else{
		re =SAF_TEST_PASS;
	}
out:
        saCkptCheckpointUnlink(&ckpt_name);
	saCkptFinalize(&ckpt_handle);
	return re;
}
