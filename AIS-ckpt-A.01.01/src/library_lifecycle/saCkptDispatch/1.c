/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 */

#include <stdio.h>
#include <string.h>
#include "im_spec.h"
#include "saf_test.h"

/*
 * function description :
 *      invoke saCkptDispatch before initialization. 
 *	The api should return SA_ERR_INIT.
 * returns :
 *      SAF_TEST_PASS:          api test success
 *      SAF_TEST_FAIL:          api test fail
 *      SAF_TEST_BLOCK:         case hangs, this value isn't returned  by case.
 *      SAF_TEST_NOTSUPPORT:    this api has not been supplied
 *      SAF_TEST_UNRESOLVED:    can't invoke successfully because of other
 *                              operation failure
 *      SAF_TEST_UNKNOWN:       can't run test case successfully and can't
 *                              known the reason
 */

int
main(int argc, char* argv[])
{
        char api_name[] = "saCkptDispatch";
        int expected_return = SA_ERR_INIT;
        SaErrorT ckpt_error;

        int re = SAF_TEST_UNKNOWN;

	SaCkptHandleT ckpt_handle;

	ckpt_error = saCkptDispatch(&ckpt_handle, SA_DISPATCH_ALL);
	if( ckpt_error != expected_return){
                printf("        invoke %s, return %s (should be %s)\n",
                        api_name,
                        get_aisAerror_string(ckpt_error),
                        get_aisAerror_string(expected_return));
                re = SAF_TEST_FAIL;
                goto out;
        }
        else{
                re = SAF_TEST_PASS;
        }
out:
        return re ;
}
