/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>

#include "im_spec.h"
#include "saf_test.h"

/*
 * 4.c
 *
 * function description :
 *	invoke saCkptSelectionObjectGet with null handle.
 *	The api invocation returned value should  be SA_ERR_BAD_HANDLE.
 * returns :
 *      SAF_TEST_PASS:          api test success
 *      SAF_TEST_FAIL:          api test fail
 *      SAF_TEST_BLOCK:         case hangs, this value isn't returned  by case.
 *      SAF_TEST_NOTSUPPORT:    this api has not been supplied
 *      SAF_TEST_UNRESOLVED:    can't invoke successfully because of other
 *                              operation failure
 *      SAF_TEST_UNKNOWN:       can't run test case successfully and can't
 *                              known the reason
 */
void ckpt_open_callback (SaInvocationT invocation,
        const SaCkptCheckpointHandleT *checkpoint_handle,
        SaErrorT error)
{
        return ;
}

void ckpt_sync_callback (SaInvocationT invocation,
        SaErrorT error)
{
        return ;
}

int
main(int argc, char* argv[])
{
	char api_name[] = "saCkptSelectionObjectGet";
	SaErrorT expected_return = SA_ERR_BAD_HANDLE;
	int re = SAF_TEST_UNKNOWN;

	void ckpt_open_callback (SaInvocationT invocation,
		const SaCkptCheckpointHandleT *checkpoint_handle,
		SaErrorT error)
	{
                return ;
        }



        void ckpt_sync_callback (SaInvocationT invocation,
                SaErrorT error)
        {
                return ;
        }

        SaCkptCallbacksT ckpt_callback ;
        ckpt_callback.saCkptCheckpointOpenCallback = ckpt_open_callback ;
        ckpt_callback.saCkptCheckpointSynchronizeCallback = ckpt_sync_callback;


 	SaErrorT  ckpt_error;


	SaCkptHandleT ckpt_handle ;

        SaVersionT ckpt_version ;
        ckpt_version.major = VERSION_MAJOR;
        ckpt_version.minor = VERSION_MINOR;
        ckpt_version.releaseCode = RELEASE_CODE ;



        ckpt_error = saCkptInitialize ( &ckpt_handle, 
					&ckpt_callback,
					&ckpt_version) ;

        if (ckpt_error != SA_OK)
        {
                 printf("       initialization fail, return %s "
                        "\n", get_aisAerror_string(ckpt_error));
                 return SAF_TEST_UNRESOLVED ;
        }

	SaSelectionObjectT ckpt_select_obj;
	ckpt_error = saCkptSelectionObjectGet(NULL, &ckpt_select_obj);
        if( ckpt_error != expected_return ){
                printf("        invoke %s, return %s (should be %s)\n",
                        api_name,
                        get_aisAerror_string(ckpt_error),
                        get_aisAerror_string(expected_return));
                re = SAF_TEST_FAIL;
                goto out;
        }
	else{
		re = SAF_TEST_PASS;
	}
out:
	saCkptFinalize(&ckpt_handle);
	return re;
}
