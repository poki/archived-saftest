#!/bin/sh
#
# Copyright (c) 2004, Intel Corporation.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307 USA.
#
# Authors:
#       Mi,Jun  <jun.mi@intel.com>
#	Yu,Ling <ling.l.yu@intel.com>
#	Wang, Jing <jing.j.wang@intel.com>
#	Ye, Bo <bo.ye@intel.com>
#	2005-5.17	1)Modify the TIMEOUT from 10 to 120
#			2)Show the test case running status on the fly
#       Li, Qun <qun.li@intel.com>
#       2005/09/22      Add ipmi connection test interface.
#       2005/09/26      Add ignore list interface
#
topdir=`pwd`
srcdir=$topdir/src
abat_log=$topdir/log/abat_log
abatlist=$topdir/abatlist
run_log=$topdir/log/run_log
tmp_log=$topdir/log/tmp_log
error_log=$topdir/log/error_log

TIMEOUT_EXE=$topdir/../t0
TIMEOUT=120

$TIMEOUT_EXE 0 >/dev/null 2>&1
TIMEOUT_RET=$?

# name of script file to check ipmi connection
# shall be relative path rather than absolute
conn_check_file=conn_check.sh

# name of file to be ignored when test
ignorelist=$topdir/ignorelist
tmp_list=$topdir/.tmp_list
if [ -r $ignorelist ]; then
        grep '^[^#].*$' $ignorelist > $tmp_list
        sed -i 's@\(^.*$\)@'$topdir'/\1@g' $tmp_list
fi

usage()
{
	cat << EOF
Usage: $0 [all|Path]
Run the tests for in current directory or specific directory.
All log information could be found at ./log directory.

	all		all test cases in current direcoty or subdirectory will
			be run.
	PATH		use this argument, all test cases in the PATH direcoty
			or its subdirectory will be run. If the process was 
			interrupted, it can continue the test. Cases have been
			tested will not be run again.
	--abat		only one or several typical test cases from each API will
			be run.
	PATH --all	similiar to PATH. But if the process was interrupted, it
			will not continue the test. All cases will be run again.
	--abat --all	similiar to --abat. But if the process was interrupted, it
			will not continue the test. All cases will be run again. 

EOF
}

abat_test()
{
    run_log=$abat_log

    if [ -d log ]; then
	if [ "$1" = "--all" ]; then
	    rm -f $run_log
	    rm -f $error_log
	else
    	     if [ -f $run_log ];then
		touch $run_log
             fi

       	    # Delete the last line in the logfile if it is a case.
            # Because test of this case has not been finished.
	    lastfile=`tail -1 $run_log 2>/dev/null | grep test 2>/dev/null`
	    if [ $? = 0 ];then
		linenum=`wc $run_log | awk '{print $1}'`
		linenum=`echo $linenum-1 | bc`
		cp $run_log $tmp_log
		head -$linenum $tmp_log > $run_log
	    fi
	fi
    else
	mkdir log
    fi

    for file in `cat $abatlist`
    do
	if [ "$1" != "--all" ];then
	    grep $file $run_log >/dev/null 2>&1
	
	# if it has been tested, then skip it.
	    if [ $? -eq 0 ];then
		continue
	    fi
	fi

        # if it is in ignore-list
        if [ -r $ignorelist ]; then
                fullpathname="$topdir/$file"
                status=`gawk -v str=$fullpathname '
                        BEGIN {cnt=0}
                            {if (index(str, $0)) {++cnt; exit}}
                        END {print cnt}
                ' $tmp_list`
                if [ $status -eq 1 ]; then
                        continue
                fi
        fi

        # check ipmi connection status if we have proper utility
        if [ -x "$topdir/$conn_check_file" ]; then
                sh "$topdir/$conn_check_file"
                if [ $? -ne 0 ]; then
                        exit -1 # ipmi connection down
                fi
        fi

	chmod 777 $file
	echo $file >> $run_log
	echo $file >> $error_log
	$TIMEOUT_EXE $TIMEOUT $file >$tmp_log 2>&1 
	out=$?
	check_result $out $file
    done
    rm -f $tmp_log $tmp_list
}

run_test()
{
    if [ -d log ]; then
	if [ "$2" = "--all" ];then
	     `rm -f $run_log`
	     `rm -f $error_log`
	else
    	    if [ -f $run_log ];then
		touch $run_log
            fi
            # Delete the last line in the logfile if it is a case.
            # Because test of this case has not been finished.
	    lastfile=`tail -1 $run_log 2>/dev/null | grep test 2>/dev/null`
	    if [ $? -eq 0 ];then
		linenum=`wc $run_log | awk '{print $1}'`
		linenum=`echo $linenum-1 | bc`
		cp $run_log $tmp_log
		head -$linenum $tmp_log > $run_log
	    fi
	fi
    else
	mkdir log
    fi

    if [ -d $1 ]; then	
	for i in `find $1 -name *.test ! -path */manual/*`
	do
		dir=${i%/*}
		file=$i
		if [ "$1" != "--all" ];then
		     grep $file $run_log >/dev/null 2>&1
		
		# if it has been tested, then skip it.
		     if [ $? -eq 0 ];then
			continue
		     fi
		fi

                # if it is in ignore-list
                if [ -r $ignorelist ]; then
                        status=`gawk '{if (index(str, $0)) {print;exit}}' str=$file $tmp_list`
                        if [ -n "$status" ]; then
                                continue
                        fi
                fi

                # check ipmi connection status if we have proper utility
                if [ -x "$topdir/$conn_check_file" ]; then
                        sh "$topdir/$conn_check_file"
                        if [ $? -ne 0 ]; then
                                exit -1 # ipmi connection down
                        fi
                fi

		cd $dir
		chmod 777 $file
		echo $file >> $run_log
		echo $file >> $error_log
		$TIMEOUT_EXE $TIMEOUT $file >$tmp_log 2>&1 
		out=$?
		check_result $out $file
	done
    else
	echo "Could not find $1"
    fi
    rm -f $tmp_log $tmp_list
}

check_result()
{
	result=$1
	case "$result" in
		0)	echo "PASS" >> $run_log
			echo "$2 PASS"
			;;
		1)	echo "FAIL" >> $run_log
			echo "$2 FAIL"
			cat $tmp_log >> $error_log 2>&1
			;;
		$TIMEOUT_RET)	echo "BLOCK" >> $run_log
			echo "$2 BLOCK"
			cat $tmp_log >> $error_log 2>&1
			;;
		3)	echo "NOTSUPPORT" >>$run_log
			echo "$2 NOTSUPPORT"
			cat $tmp_log >> $error_log 2>&1
			;;
		4)	echo "UNRESOLVED" >> $run_log
			echo "$2 UNRESOLVED"
			cat $tmp_log >>$error_log 2>&1
			;; 
               139)    	echo "UNKNOWN" >> $run_log 2>&1
			echo "$2 Segmentation fault"
			echo "Segmentation fault" >$tmp_log
			;;	
		*)	echo "UNKNOWN" >> $run_log 2>&1
			echo "$2 UNKNOWN"
			cat $tmp_log >> $error_log 2>&1
			;;
	esac
}

case "$1" in
	"")
		usage
		;;
	"--help")
		usage
		;;
	"all")
		run_test $srcdir
		;;
	"--abat")
		abat_test $2
		;;
	*)
		run_test `pwd`/$1 $2
		;;
esac
