/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void
callback_event_deliver(const SaEvtChannelHandleT *channelHandle,
			SaEvtSubscriptionIdT sub_id,
			const SaEvtEventHandleT event_handle,
			const SaSizeT eventDataSize)
{
	return;
}
int main(int argc, char *argv[])
{
	SaErrorT 	error;
	SaEvtHandleT	evthandle;
	SaEvtCallbacksT	evtcallback = {
		.saEvtEventDeliverCallback
		=	(SaEvtEventDeliverCallbackT)callback_event_deliver
	};
	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = RELEASE_CODE
	};
	int ret = SAF_TEST_PASS;

	error = saEvtInitialize(&evthandle, &evtcallback, &version);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtInitialize, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	SaEvtChannelHandleT channel_handle;
	SaNameT channel_name;
	strcpy(channel_name.value, "unsubscribe1");
	channel_name.length = strlen(channel_name.value);
	SaEvtChannelOpenFlagsT openFlag
		= SA_EVT_CHANNEL_CREATE|SA_EVT_CHANNEL_PUBLISHER|SA_EVT_CHANNEL_SUBSCRIBER;
	
	error = saEvtChannelOpen(&evthandle, &channel_name, openFlag, &channel_handle);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtChannelOpen, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	SaEvtEventFilterArrayT filter_array;
	filter_array.filters = malloc(sizeof(SaEvtEventFilterT));
	filter_array.filters[0].filterType = SA_EVT_EXACT_FILTER;
	filter_array.filters[0].filter.patternSize = 6;
	filter_array.filters[0].filter.pattern = (SaUint8T *)malloc(6);
	memcpy(filter_array.filters[0].filter.pattern, "abcxyz", 6);

	error = saEvtEventSubscribe(&channel_handle, &filter_array, 1);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtEventSubscribe, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	error = saEvtEventUnsubscribe(&channel_handle, 1);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtEventUnscribe, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
		ret = SAF_TEST_FAIL;
	}
	
	error = saEvtChannelClose(&channel_handle);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtChannelClose, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
	}

final:
	error =	saEvtFinalize(&evthandle);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtFinalize, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
	}
out:
	return ret;
}
