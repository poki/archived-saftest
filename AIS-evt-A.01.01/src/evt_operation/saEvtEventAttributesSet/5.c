/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 * 	Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

SaInvocationT evt_invocation;
SaEvtChannelHandleT channel_handle;

static void
callback_event_deliver(const SaEvtChannelHandleT *channelHandle,
			SaEvtSubscriptionIdT sub_id,
			const SaEvtEventHandleT event_handle,
			const SaSizeT eventDataSize)
{
	SaSizeT data_size = 40;
	void *event_data = malloc(sizeof(data_size));

	saEvtEventDataGet(&event_handle, event_data, &data_size);
	printf("the data size == %d\n", data_size);
	printf("%s\n", (char *)event_data);
	saEvtEventFree(&event_handle);
}

int main(int argc, char *argv[])
{
	SaErrorT 	error;
	SaEvtHandleT	evthandle;
	SaEvtCallbacksT	evtcallback = {
		.saEvtEventDeliverCallback
		=	(SaEvtEventDeliverCallbackT)callback_event_deliver
	};
	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = RELEASE_CODE
	};
	int ret = SAF_TEST_PASS;

	error = saEvtInitialize(&evthandle, &evtcallback, &version);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtInitialize, Return value : %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
		goto out;
	}

	SaNameT channel_name;
	strcpy(channel_name.value, "attributeset5");
	channel_name.length = strlen(channel_name.value);
	SaEvtChannelOpenFlagsT openFlag
		= SA_EVT_CHANNEL_CREATE|SA_EVT_CHANNEL_PUBLISHER|SA_EVT_CHANNEL_SUBSCRIBER;
	SaEvtChannelHandleT channel_handle;	

	error = saEvtChannelOpen(&evthandle, &channel_name, openFlag, &channel_handle);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtChannelOpen, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	SaEvtEventHandleT event_hd;
	error = saEvtEventAllocate(&channel_handle, &event_hd);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtEventAllocate, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto close;
	}

	SaEvtEventPatternArrayT pattern_array;
	SaNameT publisher_name;
	strcpy(publisher_name.value, "attributeset_p5");
	publisher_name.length = strlen(publisher_name.value);

	pattern_array.patternsNumber = 1;
	pattern_array.patterns = (SaEvtEventPatternT *)malloc(sizeof(SaEvtEventPatternT));
	pattern_array.patterns[0].patternSize = 6;
	pattern_array.patterns[0].pattern = (SaUint8T *)malloc(6);
	memcpy(pattern_array.patterns[0].pattern, "abcxyz", 6);

	error = saEvtEventAttributesSet(&event_hd, &pattern_array, 1, -1, &publisher_name);
	if (error != SA_ERR_INVALID_PARAM){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtEventAttributesSet, Return value: %s,\
			 should be SA_ERR_INVALID_PARAM\n",get_aisAerror_string(error));
		ret = SAF_TEST_FAIL;
	}

	error = saEvtEventFree(&event_hd);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtEventFree, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
	}

close:
	error = saEvtChannelClose(&channel_handle);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtChannelClose, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
	}

final:
	error = saEvtFinalize(&evthandle);
	if(error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtFinalize, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
	}
out:
	return ret;
}
