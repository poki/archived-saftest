/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 * 	Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>

SaInvocationT evt_invocation;
SaEvtChannelHandleT channel_handle;
SaErrorT dispatch_ret = SA_OK;
sem_t sem;

static void
callback_event_deliver(const SaEvtChannelHandleT *channelHandle,
			SaEvtSubscriptionIdT sub_id,
			const SaEvtEventHandleT event_handle,
			const SaSizeT eventDataSize)
{
	SaSizeT data_size = 40;
	void *event_data = malloc(sizeof(data_size));

	saEvtEventDataGet(&event_handle, event_data, &data_size);
	printf("the data size == %d\n", data_size);
	printf("%s\n", (char *)event_data);
	saEvtEventFree(&event_handle);
}

static void *dispatch_thread(void *arg)
{
	SaEvtHandleT *handle = (SaEvtHandleT*)arg;
	SaErrorT err;

	err = saEvtDispatch(handle, SA_DISPATCH_BLOCKING);
	dispatch_ret = err;
	sem_post(&sem);
	return NULL;
}

int main(int argc, char *argv[])
{
	SaErrorT 	error;
	SaEvtHandleT	evthandle;
	SaEvtCallbacksT	evtcallback = {
		.saEvtEventDeliverCallback
		=	(SaEvtEventDeliverCallbackT)callback_event_deliver
	};
	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = RELEASE_CODE
	};
	pthread_t thread_id;
	int ret = SAF_TEST_PASS;

	error = saEvtInitialize(&evthandle, &evtcallback, &version);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtInitialize, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	sem_init(&sem, 0, 0);
	error = pthread_create(&thread_id, NULL, dispatch_thread, (void *)evthandle);
	if (error != SA_OK){
		printf("  Function \"pthread_create\" Work abnormally!\n");
		printf("  Return value: %s\n", strerror(error));
		ret = SAF_TEST_UNRESOLVED;
	}

	error = saEvtFinalize(&evthandle);
	if(error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtFinalize, Return value: %s, should be SA_OK\n"
		,	get_aisAerror_string(error));
	}

	if (ret == SAF_TEST_PASS){
		sem_wait(&sem);
		if(dispatch_ret != SA_OK){
			printf("  Does not conform the expected behaviors!\n");
			printf("  saEvtDispatch, Return value: %s, should be SA_OK\n"
			,	get_aisAerror_string(dispatch_ret));
			ret = SAF_TEST_FAIL;
		}
	}
out:
	return ret;
}
