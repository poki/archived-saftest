#!/bin/bash
#
# Copyright (c) 2004, Intel Corporation.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307 USA.
#
# Authors:
#      Yang,Xiaowei  <xiaowei.yang@intel.com>
#
# Reorder the test cases per the line order
# $1: test case dir

for i in *.c; do
	if ! grep -q $i coverage.txt; then
		echo "There are test cases not listed in coverage.txt ..."
		echo "Correct it first! "
		exit
	fi
done

pwd=`pwd`

for i in `find $1 -name assertion.xml`
do

dir=${i%/*}
cd $dir

sed -n -e "s#.*id=\"\(.*\)\".*line=\"P\(.*\):.*#\2  \1#p" assertion.xml \
| sort -n -t "-" -k 2 \
| sort -s -n -t "-" -k 1 > aaa 

awk '
BEGIN { 
	FS = "-"
	i = 0
	s = "" 
}
{
	if (s != $1$2)
     	{
		s = $1$2
		i++
	}
	if ($3 != "") {
		printf "%s-%s-%s %d-%s\n", $1, $2, $3, i, $3
	} else {
		printf "%s-%s %d\n", $1, $2, i
	} 
}' aaa > bbb

mkdir -p tmp
[ -d manual ] && mkdir -p m_tmp

head -1 assertion.xml > assertion.xml.new

#if grep -q 'Assertion' coverage.txt; then
#	sed -n -e "1,/Assertion/p" coverage.txt > coverage.txt.new
#fi
echo -e "Assertion\t\tStatus" > coverage.txt.new

while read line old new
do
	[ -f $old.c ] && mv $old.c tmp/$new.c
	[ -f manual/$old.c ] && mv manual/$old.c m_tmp/$new.c
	
	sed -n -e "/\"$old\"/,/<\/assertion>/p" assertion.xml | sed -e "s/\"$old\"/\"$new\"/" \
		-e "s/Same with $old/Same with $new/" >> assertion.xml.new
	sed -n -e "/^$old[[:space:]]/p" coverage.txt | sed "s/^$old[[:space:]]/$new	/" >> coverage.txt.new
done < bbb

rm -if *.c
mv tmp/*.c .
rm -rf tmp

rm -rf manual
[ -d m_tmp ] && mv m_tmp manual

while read line old new
do
	for i in `find . -name '*.c'`;
	do
		sed -e "s/Same with $old/Same with $new/" -e "s/$old\.test/$new\.test/" $i > $i.new
		mv -f $i.new $i
	done
	
done < bbb

tail -1 assertion.xml >> assertion.xml.new
mv -f assertion.xml.new assertion.xml
mv -f coverage.txt.new coverage.txt

rm -f aaa bbb

cd $pwd

done
