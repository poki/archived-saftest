#! /usr/bin/python
#
# This script removes comments for functions such as "Test_Resource" etc.
# Author:       Qun Li <qun.li@intel.com>
#
import sys, os, re

if __name__ == '__main__':
        if len(sys.argv) != 2:
                print "usage: " + sys.argv[0] + " file"
                sys.exit(-1)

        old_file = open(sys.argv[1], 'r')
        new_file = open(sys.argv[1] + ".new", 'w')
        file_buf = old_file.readlines()
        index    = 0

        # firstly, dump pre-defines to new file
        while not re.compile('^#').match(file_buf[index]):
                new_file.write(file_buf[index])
                index = index + 1

        while re.compile('^#').match(file_buf[index]):
                new_file.write(file_buf[index])
                index = index + 1

        # process comments in file
        while index < len(file_buf):
                # match the beginning of comments
                if not re.compile('\ *\/(\*)+').match(file_buf[index]):
                        new_file.write(file_buf[index])
                        index = index + 1
                        continue

                reminder = index;

                # match the end of comments                
                while not re.compile('.*(\*)+\/').match(file_buf[index]):
                        index = index + 1
                
                if re.compile('\s*int\s+main\s*(.+)').match(file_buf[index + 1]):
                        while reminder <= index:
                                new_file.write(file_buf[reminder])
                                reminder = reminder + 1
                
                index = index + 1

        old_file.close()
        new_file.close()
        os.remove(old_file.name)
        os.rename(new_file.name, old_file.name)
