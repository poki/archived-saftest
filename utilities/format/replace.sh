#!/bin/bash

filename="$1"
#prename=`echo $1|cut -d"." -f1` 
backname="$1""bak.c"
i=1

if [ $# -ne 1 ];then
	printf "usage: replace.sh filename"
	printf "error. param num must be 1\n"
	exit
fi

linenum=`cat $filename|wc -l`

if [ -f $backname ];then
	rm -f $backname
fi
touch $backname

while [ $i -le $linenum ];do
	curline=`awk "{ if(NR==$i) print }" $filename`
	nextline=
	next2line=
	matchline=0
	echo $curline|grep "if *(.*!=.*SA_.*)" >/dev/null
	if [ $? -eq 0 ];then
		nexti=$(( $i + 1 ))
		next2i=$(( $i + 2 ))
		nextline=`awk "{ if(NR==$nexti) print }" $filename`
		echo $nextline|grep "{">/dev/null
		if [ $? -eq 0 ];then #match {
			next2line=`awk "{ if(NR==$next2i) print }" $filename`
			echo $next2line|grep "printf(">/dev/null
			if [ $? -eq 0 ];then #match printf
			       	matchline=$i
				#find the function name before if statement
				tmpline=$matchline
				while [ $tmpline -gt 0 ]
				do
				  tmpline=$(( $tmpline - 1 ))
  				  funcline=`awk "{ if(NR==$tmpline) print}" $filename`
				  echo $funcline|grep "= *saHpi.*(">/dev/null
				  if  [ $? -eq 0 ];then
                		      break;
	  			  fi
  				done
				funcname=`echo $funcline|cut -d"=" -f2|cut -d"(" -f1|sed 's/ //'`
				#get "status"
				printf "$curline\n"
				statusname=`echo $curline|cut -d"(" -f2|cut -d"!" -f1|sed 's/ //'`
				printf "$statusname\n"
				#find expected return value
				expectreturn=`echo $curline|cut -d"=" -f2|cut -d")" -f1|sed 's/ //'`
				#the error value is status
				alignspaces=`echo "$next2line"|cut -d"p" -f1`
				echo "$curline">>$backname
				echo "$nextline">>$backname
				echo "$alignspaces""e_print($funcname,">>$backname
				echo "$alignspaces""        $expectreturn,">>$backname 
				echo "$alignspaces""        $statusname);">>$backname
				#find the end line that is not printf after next2line
				endi=$next2i
				while [ $endi -lt $linenum ];do
				  endi=$(( $endi + 1 ))
				  endline=`awk "{ if(NR==$endi) print }" $filename`
				  echo $endline|grep "printf(">/dev/null
				  if [ $? -eq 0 ];then
					continue
				  else
					break;
				  fi
				done
				i=$(( $endi - 1 ))
			fi #match printf
		fi #match {
	fi #match if(status
	if [ $matchline -eq 0 ];then 
		echo "$curline">>$backname
	fi
	i=$(( $i + 1 ))
done
if [ -f $backname -a -f $filename ];then 
	diff $filename $backname
	cp $backname $filename
	rm $backname
fi
