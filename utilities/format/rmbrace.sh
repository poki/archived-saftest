#!/bin/bash
#
# Author: Daming Liang <daming.liang@intel.com>
#
#using global filename, i , linenum
function findmatchline (){
while [ $i -le $linenum ];do
	match=0
	curline=`awk "{ if (NR==$i) print }" $filename`
	countif=`echo $curline|grep "if *(.*)" -c`
	countelse=`echo $curline|grep "^[[:space:]]*else[^{]*$" -c`
	if [ $countif -eq 1 -o $countelse -eq 1 ];then
	        nexti=$(( $i + 1 ))	
		next1line=`awk "{ if(NR==$nexti) print }" $filename`
		echo $next1line|grep "^[[:space:]]*{[[:space:]]*$" >/dev/null
		if [ $? -eq 0 ];then
			next2i=$(( $i + 2 ))
			next2line=`awk "{ if(NR==$next2i) print }" $filename`
			next3i=$(( $i + 3 ))
			next3line=`awk "{ if(NR==$next3i) print }" $filename`
			echo $next3line|grep "^[[:space:]]*}[[:space:]]*$"  >/dev/null
			if [ $? -eq 0 ];then
				match=1			     #now match all lines 
				echo "$curline">>$backname   #if()
				echo "$next2line">>$backname #xxx
				i=$(( $i + 3 ))
			fi
		fi
	fi
	if [ $match -eq 0 ];then
		echo "$curline">>$backname	
	fi
	i=$(( $i + 1 ))
done
}

filename=$1
backname="$1back"
if [ -f $backname ];then
	rm -rf $backname
fi
touch $backname
linenum=`wc -l $filename|cut -d" " -f1`
i=1
findmatchline 
if [ -f $filename -a -f $backname ];then
	diff $filename $backname
fi
mv $backname $filename

