#! /usr/bin/python
#
# This scripts gruarantee that no continues black lines in source code
# Author:       Qun Li <qun.li@intel.com>
import sys, os, re

if __name__ == '__main__':
        if len(sys.argv) != 2:
                print "usage: " + sys.argv[0] + " file"
                sys.exit(-1)

        old_file = open(sys.argv[1], 'r')
        new_file = open(sys.argv[1] + ".new", 'w')
        file_buf = old_file.readlines()
        index    = 0
        length   = len(file_buf)

        # process comments in file
        while index < length:
                new_file.write(file_buf[index])
                flag = 1

                while re.compile('^\s*$').match(file_buf[index]):
                        index = index + 1;
                        flag  = 0
                        if index >= length: break

                if index < length and flag == 0: new_file.write(file_buf[index])
                index = index + 1

        old_file.close()
        new_file.close()
        os.remove(old_file.name)
        os.rename(new_file.name, old_file.name)
