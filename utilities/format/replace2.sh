#!/bin/bash
#
# Author:  Daming Liang <daming.liang@intel.com>
#
filename="$1"
backname="$1""bak.c"
i=1

if [ $# -ne 1 ];then
	printf "usage: $0 filename\n"
	exit
fi

linenum=`cat $filename|wc -l`

if [ -f $backname ];then
	rm -f $backname
fi
touch $backname

while [ $i -le $linenum ];do
	curline=`awk "{ if(NR==$i) print }" $filename`
	nextline=
	nextnextline=
	matchline=0
	echo $curline|grep "if *(.*!=.*{" >/dev/null
	if [ $? -eq 0 ];then
		nexti=$(( $i + 1 ))
		nextline=`awk "{ if(NR==$nexti) print }" $filename`
		echo $nextline|grep "printf(">/dev/null
		if [ $? -eq 0 ];then #match printf(
			       	matchline=$i
				#find the function name before if statement
				tmpline=$matchline
				while [ $tmpline -gt 0 ]
				do
				  tmpline=$(( $tmpline - 1 ))
  				  funcline=`awk "{ if(NR==$tmpline) print}" $filename`
				  echo $funcline|grep "= *saHpi.*(">/dev/null
				  if  [ $? -eq 0 ];then
                		      break;
	  			  fi
  				done
				funcname=`echo $funcline|cut -d"=" -f2|cut -d"(" -f1|sed 's/ //'`
				#find statusname
				statusname=`echo $curline|cut -d"(" -f2|cut -d"!" -f1|sed 's/ //'`	
				#find expected return value
				expectreturn=`echo $curline|cut -d"=" -f2|cut -d")" -f1|sed 's/ //'`
				#the error value is status
				alignspaces=`echo "$nextline"|cut -d"p" -f1`
				echo "$curline">>$backname
				echo "$alignspaces""e_print($funcname,">>$backname
				echo "$alignspaces""        $expectreturn,">>$backname 
				echo "$alignspaces""        $statusname);">>$backname
				#find the end line that is not printf after nextnextline
				endi=$nexti
				while [ $endi -le $linenum ];do
				  endi=$(( $endi + 1 ))
				  endline=`awk "{ if(NR==$endi) print }" $filename`
				  echo $endline|grep "printf(">/dev/null
				  if [ $? -eq 0 ];then
					continue
				  else
					break;
				  fi
				done
				i=$(( $endi - 1 ))
		fi #match printf(
	fi #match if(status  {
	if [ $matchline -eq 0 ];then 
		echo "$curline">>$backname
	fi
	i=$(( $i + 1 ))
done
if [ -f $filename -a -f $backname ];then
	diff $filename $backname
	cp $backname $filename
	rm $backname
fi
