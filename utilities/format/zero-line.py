#! /usr/bin/python
#
# This scripts guarantee that there's no blank line between block comments and function
# Author:       Qun Li <qun.li@intel.com>
import sys, os, re

if __name__ == '__main__':
        if len(sys.argv) != 2:
                print "usage: " + sys.argv[0] + " file"
                sys.exit(-1)

        old_file = open(sys.argv[1], 'r')
        new_file = open(sys.argv[1] + ".new", 'w')
        file_buf = old_file.readlines()
        index    = 0
        length   = len(file_buf)

        # process comments in file
        while index < length:
                new_file.write(file_buf[index])

                if re.compile('.*(\*)+\/').match(file_buf[index]):
                        if re.compile('^\s*$').match(file_buf[index + 1]):
                                index = index + 1
                index = index + 1

        old_file.close()
        new_file.close()
        os.remove(old_file.name)
        os.rename(new_file.name, old_file.name)
