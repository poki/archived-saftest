#!/bin/bash
#
# Author:  Daming Liang <daming.liang@intel.com>
#
if [ $# -ne 1 ];then
	printf "Usage: allrmbrace dir/file\n"
fi
name=$1
if [ -d $name ];then
	cp -r $name /tmp/$name
	list=`find $name -name "*.c"`
	for i in $list 
	do
		printf "$i\n"
		rmbrace.sh $i
	done
else
	printf "$name\n"
	rmbrace.sh $name
fi
