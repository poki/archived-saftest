#!/bin/bash
#
# Copyright (c) 2005, Intel Corporation.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307 USA.
#
# Authors:
#       Yang,Xiaowei  <xiaowei.yang@intel.com>
#
# assertion.xml <-- test case header
                                                                                                                             
# For test
#set -x

# Extract description from .c and append them to new assertion file
# $1: test case file name
#
extract_from_c() {
	if grep -q "manual" <<< $1 ; then
		ID=${1##*/}
		ID=${ID%.c}
		MANUAL=" manual=\"yes\""
	else
                ID=${1%.c}
                MANUAL=""
	fi

	LINE=`sed -n '/Line:/s/[^:]*:[[:space:]]*//p' $TCFILE`

	echo "  <assertion id=\"$ID\" line=\"$LINE\"$MANUAL>" >> $AFILE_TMP
	sed -n -e '/Description:/,/Line:/p' $1 | sed -e '1d;$d' -e 's/ \*//' >> $AFILE_TMP
	echo "  </assertion>" >> $AFILE_TMP
}

# Handle the description of test cases we need impl.
# $1: test case id
#
do_yes_way() {
	if [ -f $1.c ]; then
		TCFILE=$1.c
		MANUAL=""
	elif [ -f manual/$1.c ]; then
		TCFILE=manual/$1.c
		MANUAL=" manual=\"yes\""
	else
		echo "ERROR: Test case $1.c is labeled \"Yes\" in coverage.txt but it doesn't exist!"
		echo "ERROR: Pls. create it first or modify coverage.txt!"
		exit 2
	fi

	extract_from_c $TCFILE
}

# Handle the description of test cases we needn't impl.
# $1: test case id
#
do_no_way() {
	if ! grep -q "id=\"$1\"" $AFILE ; then
                echo "ERROR: Test case $1 is labeled \"No\" in coverage.txt"
                echo "ERROR: and we can't find description for it in assertion.xml"
                echo "ERROR: Pls. populate assertion.xml or modify coverage.txt!"
                exit 3
	else
		sed -n -e '/id='"\"$1\""'/,/<\/assertion>/p' $AFILE >> $AFILE_TMP
	fi
}


# Assertion file name
AFILE=./assertion.xml

if [ ! -f $AFILE ]; then
	> $AFILE
fi

# Temp assertion file name
AFILE_TMP=/tmp/assertion.xml.tmp
 
TCFILE=`ls -1 *.c | head -1`
SPEC=`sed -n '/Spec:/s/[^:]*:[[:space:]]*//p' $TCFILE`
FUNCTION=`sed -n '/Function:/s/[^:]*:[[:space:]]*//p' $TCFILE`
echo "<assertions spec=\"$SPEC\" function=\"$FUNCTION\">" > $AFILE_TMP

# Coverage file name
CFILE=./coverage.txt

if [ ! -f $CFILE ]; then
	echo "ERROR: coverage.txt doesn't exist!"
	exit 1
fi
                                                                                                     
while read LINE; do
	if grep -q -i "yes[[:space:]]*$"  <<< "$LINE"; then
		do_yes_way `awk '{ print $1 }' <<< "$LINE"`
	elif grep -q -i "no[[:space:]]*$"  <<< "$LINE"; then
                do_no_way `awk '{ print $1 }' <<< "$LINE"`
	fi
done < $CFILE

echo "</assertions>" >> $AFILE_TMP

mv -f $AFILE $AFILE.bak
mv -f $AFILE_TMP $AFILE
