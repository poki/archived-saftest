#!/bin/bash
#
# Copyright (c) 2005, Intel Corporation.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307 USA.
#
# Authors:
#       Yang,Xiaowei  <xiaowei.yang@intel.com>
#
# assertion.xml <-- test case header
                                                                                                                            
                                                                                                                            
# For test
#set -x

# Modify test case desciption section according to assertion.xml
# $1
#
modify_desc() {
	# Split the copyright/license/author section
	tac $1 | sed -n -e '/@.*com/,$p' | tac > $TCFILE_TMP
	
	# Add description section
	echo " *" >> $TCFILE_TMP
	echo " * Spec:        $SPEC" >> $TCFILE_TMP
	echo " * Function:    $FUNCTION" >> $TCFILE_TMP
	echo " * Description:   " >> $TCFILE_TMP
	
	sed -n -e "/id=\"$ID\"/,/<\/assertion>/p" $AFILE | sed -e '1d;$d' -e 's/^/ */' >> $TCFILE_TMP
	
	echo " * Line:        $LN" >> $TCFILE_TMP
	echo -e " */\n" >> $TCFILE_TMP
	
	# Append code section
	sed -n -e '/#include/,$p' $1 >> $TCFILE_TMP
	
	# Replace old case with new one
	mv -f $TCFILE_TMP $1
}

# Create test case file containing copyright/license/desciption section
# $1
#
create_tc() {
	cat > $1 <<eof
/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      ...add your name here...
 *
 * Spec:        $SPEC
 * Function:    $FUNCTION
 * Description:
eof
	sed -n -e "/id=\"$ID\"/,/<\/assertion>/p" $AFILE | sed -e '1d;$d' -e 's/^/ */' >> $1
	echo " * Line:        $LN" >> $1

        # For test
        echo -e " */\n\n#include <...>\n$1" >> $1
}

# Assertion file name
AFILE=./assertion.xml
                                                                                                                            
if [ ! -f $AFILE ]; then
        echo "ERROR: assertion.xml doesn't exist!"
        exit 1
fi

# Coverage file name
CFILE=./coverage.txt
                                                                                                                            
if [ ! -f $CFILE ]; then
        echo "ERROR: coverage.txt doesn't exist!"
        exit 2
fi

# Test case temp file
TCFILE_TMP=/tmp/tc.tmp

SPEC=`sed -n 's@.*spec="\([^\"]*\)".*@\1@p' $AFILE`
FUNCTION=`sed -n 's@.*function="\([^\"]*\)".*@\1@p' $AFILE`

while read LINE; do
	if grep -q -i "yes"  <<< "$LINE"; then
		ID=`awk '{ print $1 }' <<< "$LINE"`
		if grep -q "id=\"$ID\".*manual" $AFILE ; then
			TCFILE=manual/$ID.c
			mkdir -p manual
		elif grep -q "id=\"$ID\"" $AFILE ; then
			TCFILE=$ID.c
		else
	                echo "ERROR: We can't find description for test case $ID in $AFILE!"
        	        echo "ERROR: Pls. populate $AFILE or modify coverage.txt!\n"
	          	exit 3
		fi

		LN=`grep "id=\"$ID\"" $AFILE  | sed -n 's@.*line="\([^\"]*\)".*@\1@p'`
		
		if [ -f $TCFILE ]; then
			modify_desc $TCFILE
		else
			create_tc $TCFILE
		fi
	fi
done < $CFILE
