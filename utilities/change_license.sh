#!/bin/bash
#
# Copyright (c) 2004, Intel Corporation.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307 USA.
#
# Authors:
#      Yang,Xiaowei  <xiaowei.yang@intel.com>
#

# Modify all C source code's head to use identical license

# For test 
#set -x

# Where to store generated files
TOPDIR=`pwd`/develop/saf_test/HPI-A.01.01
# Original source code except '*' is replace by ' '
TMPFILE=/tmp/change_license.$$
# Source code with Copyright/License head re-generated
TMPFILE2=/tmp/change_license2.$$
                                                                                                                            
# Whether this source file belongs to IBM
copyright_ibm() {
	COPYRIGHT=`grep "Copyright.*IBM" $1` || return 1
	COPYRIGHT=`sed 's/^[[:space:]]*//' <<<$COPYRIGHT`
	COPYRIGHT2="Copyright (c) 2005, Intel Corporation"
	return 0
}

# Whether this source file belongs to Intel
copyright_intel() {
	COPYRIGHT=`grep "Copyright.*Intel" $1` || return 1
	COPYRIGHT=`awk '{ print $3 }' <<<$COPYRIGHT`
	COPYRIGHT="Copyright (c) $COPYRIGHT-2005, Intel Corporation"
	return 0
}

# Start here
for i in `find $TOPDIR -name '*.[hc]'`; do
	tr "*" " " <$i >$TMPFILE

	# Generate Copyright
	COPYRIGHT2=""
	copyright_ibm $TMPFILE || copyright_intel $TMPFILE || \
		{ echo "ERR: $i -- Unknown Copyright!"; continue; }
#	echo $COPYRIGHT

	# Write Copyright to a temp file
	cat >$TMPFILE2 <<eof
/*
 * $COPYRIGHT
 * $COPYRIGHT2
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
eof
	# Locate where "Auther:" begins
	if ! AUTHOR_LN=`grep -n "Author" $i`; then
		echo "ERR: $i -- No author found!"
		continue
	fi
	AUTHOR_LN=`awk -F: '{ print $1 }' <<<$AUTHOR_LN`
	
	# Cat the remaining lines to TMPFILE2
	sed -n $AUTHOR_LN',$p' <$i >>$TMPFILE2

	# Replace origial one
	mv $TMPFILE2 $i
done
