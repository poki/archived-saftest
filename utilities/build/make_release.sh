#!/bin/sh
#
# Copyright (c) 2005, Intel Corporation.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307 USA.
#
# Authors:
#      Xiong, Crystal  <crystal.xiong@intel.com>
#
# The script is used to make releases for saftest. 
# If you want to create release for SAF A.01.01 tests, please use:
#         ./make_release A_01_01 X_X_X
# If you want to create release for HPI-B.01.01 tests, please use:
#         ./make_release HPI-B_01_01 X_X_X
# If you want to create release for AIS-B.01.01 tests, please use:
#         ./make_release AIS-B_01_01 X_X_X
# X_X_X is the version number you want to use.

# The working directory  
BUILD_ROOT=../../

# The build direcotry
BUILD_DIR=/tmp/saftest

DATE=`date +%Y%m%d` 
# DATE for saftest_0.2.0
# DATE=041129
# DATE for saftest_0.3.0
#DATE=041215
# DATE for saftest_0.4.0
#DATE=041231 
# DATE for saftest_1.0.0
#DATE=051114
# DATE for saftest_1.0.1
#DATE=20050205
# DATE for saftest_AIS-B.01.01_0.1.0
#DATE=20050203

export CVS_RSH=ssh
# Please specify the SF account you used to access CVS.
export CVSROOT=:ext:lingyu@saftest.cvs.sourceforge.net:/cvsroot/saftest

usage()
{
	cat <<EOF
*************************************************
The script is used to make releases for saftest. 
If you want to create release for SAF A.01.01 tests, please use:
         ./make_release A_01_01 X_X_X
If you want to create release for HPI-B.01.01 tests, please use:
         ./make_release HPI-B_01_01 X_X_X
If you want to create release for AIS-B.01.01 tests, please use:
         ./make_release AIS-B_01_01 X_X_X
X_X_X is the version number you want to use.
**************************************************

Usage: $0 [A_01_01|HPI-B_01_01|AIS-B_01_01] version
	A_01_01		make build for A.01.01 tests, saftest_X.X.X
	HPI-B_01_01     make build for HPI B.01.01 test,
	                release name is saftest_HPI-B.01.01_X.X.X
	AIS-B_01_01     make build for AIS B.01.01 test,
	                release name is saftest_AIS-B.01.01_X.X.X
	version         please use X.X.X as version, such as 0_1_0
EOF
}

add_tag()
{
	echo $SPEC $VERSION
	cd $BUILD_ROOT
	# Add tag to the release files, this section may need to
	# be refined, since there are some duplicate code :)
	if [ $SPEC == "A_01_01" ]; then
		tag=Release__$VERSION"__"saftest"__"$DATE
		echo "Add tag $tag to:"
		echo "` ls *A.01.01 -d` and other common files"
		cvs tag $tag *A.01.01 AUTHORS ChangeLog COPYING doc \
		             include Makefile utilities README \
			     report.sh run_tests.sh
	else 
		tag="Release__$SPEC"_"$VERSION"__"saftest"__$DATE
		echo "Add tag $tag to:"
		if [ $SPEC == "HPI-B_01_01" ]; then
			echo "` ls HPI*B.01.01 -d` and other common files"
			cvs tag $tag HPI*B.01.01 AUTHORS ChangeLog COPYING doc \
			             include Makefile utilities README \
			       	     report.sh run_tests.sh t0.c
		else 
			echo "` ls AIS*B.01.01 -d` and other common files"
			cvs tag $tag AIS-clm-B.01.01 AIS-evt-B.01.01 \
			             AIS-ckpt-B.01.01 AIS-lock-B.01.01 \
				     AIS-msg-B.01.01 AIS-amf-B.01.01\
				     AUTHORS ChangeLog \
				     COPYING doc \
			             include Makefile utilities README \
			       	     report.sh run_tests.sh t0.c
		fi
	fi
}

get_code()
{
	rm -rf $BUILD_DIR
	if [ ! -e $BUILD_DIR ]; then
		mkdir $BUILD_DIR
	fi
	cd $BUILD_DIR/../ 
	echo $BUILD_DIR
	if [ $SPEC == "A_01_01" ]; then
		tag=Release__$VERSION"__"saftest"__"$DATE
		cvs co -P -r $tag saftest 
	else 
		tag=Release__$SPEC"_"$VERSION"__"saftest"__"$DATE
		cvs co -P -r $tag saftest 
	fi
	# Since the empty directory is not checked out,
	# we need to create log directory in each test suite, 
	# to make sure all the test can be compiled.
	cd $BUILD_DIR
	# *.01.01 is for current test suite directory. 
	# It may need to be changed if there are SAF *.02.02 spec 
	# released.  
	for i in `ls *.01.01 -d`
	do
		cd $i 
		echo $i
		mkdir log
		cd ..
	done
}

make_tarball()
{
	cd $BUILD_DIR
	# Remove all the CVS file
	echo "Remove all the CVS file..."
	rm -rf `find ./ -name CVS`
	cd ../
	chmod +x `find ./ -name find-test`
	# make the tarball 
	tar -czvf saftest"_"$SPEC"_"$VERSION.tar.gz saftest
}

post_tarball()
{
	echo "Then You need to post your tarball to SF :)"
	cd $BUILD_DIR/../
	echo "The tarball can be found at `pwd`"
	# Then access the upload.sf.net and post the tarball to the incoming
	# directory.
}

VERSION=$2
SPEC=$1
case "$SPEC" in
	"")
	usage
	;;      
	"--help")
	usage
	;;
	"A_01_01"|"HPI-B_01_01"|"AIS-B_01_01")
	add_tag 
	sleep 2
	get_code
	make_tarball
#	post_tarball
	;;
	*)
	usage
	;;
esac


