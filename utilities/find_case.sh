#!/bin/bash
#
# Copyright (c) 2004, Intel Corporation.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307 USA.
#
# Authors:
#      Yang,Xiaowei  <xiaowei.yang@intel.com>
#
# Find automatic/manual test cases

case "$1" in
"-m") 
	MANUAL='-path */manual/*'
	shift
	;;
"-a")	AUTO='! -path */manual/*'
	shift
	;;
esac

if [ "$1" == "" ]; then
	cat << EOF
Usage: $0 [-m|-a] path
-m	Find only manual test cases
-a	Find only automatic test cases
EOF
exit
fi

#find $1 -name "*.c" "$MANUAL" "$AUTO"
find $1 -name "*.c" $MANUAL $AUTO
