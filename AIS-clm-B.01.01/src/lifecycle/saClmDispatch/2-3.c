/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmDispatch
 * Description:   
 *   Dispatch correctly, Set dispatch Flag to SA_DISPATCH_BLOCKING. The 
 *   function completes successfully.
 *   Return value should be SA_AIS_OK
 * Line:        P23-1:P23-5
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include "saAis.h"
#include "saClm.h"
#include "saf_test.h"


SaAisErrorT dispatch_ret = SA_AIS_OK;
sem_t sem_start_dispatch;
sem_t sem_stop_dispatch;

static void getnode_callback(SaInvocationT invocation,
                                SaClmClusterNodeT *clusterNode,
                                SaAisErrorT error)
{
        if(error != SA_AIS_OK){
                printf("GetNode Callback failed [%d]\n", error);
        }
        return;
}

static void *dispatch_thread(void *arg)
{
        SaClmHandleT *handle=(SaClmHandleT*)arg;
        SaAisErrorT err;
	sem_post(&sem_start_dispatch);
        err = saClmDispatch(*handle, SA_DISPATCH_BLOCKING);
        dispatch_ret = err;
        sem_post(&sem_stop_dispatch);
        return NULL;
}

int main(int argc, char *argv[])
{
        SaAisErrorT        error;
        SaClmHandleT    clmhandle;
        SaClmCallbacksT clmcallback = {
                .saClmClusterNodeGetCallback
                =       (SaClmClusterNodeGetCallbackT)getnode_callback,
                .saClmClusterTrackCallback      = NULL
        };
        SaVersionT      version = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE 
        };
        SaInvocationT invocation;
        SaClmNodeIdT nodeId = SA_CLM_LOCAL_NODE_ID;
        pthread_t thread_id;
	int thread_ret;

        int ret = SAF_TEST_PASS;

        error = saClmInitialize(&clmhandle, &clmcallback, &version);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
        sem_init(&sem_start_dispatch, 0, 0);
	sem_init(&sem_stop_dispatch, 0, 0);
        thread_ret = pthread_create(&thread_id, NULL, dispatch_thread, 
			(void*)&clmhandle);
        if (thread_ret != 0){
                printf("  Function \"pthread_create\" work abnormally!\n");
                printf("  Return value: %s\n", strerror(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }
	
	sem_wait(&sem_start_dispatch);
        error = saClmClusterNodeGetAsync(clmhandle, invocation, nodeId);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmClusterNodeGetAsync, Return value: %s, "
			"should be SA_AIS_OK", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

final:  error = saClmFinalize(clmhandle);
        if(error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmFinalize, Return value: %s, should be SA_OK\n", 
			get_error_string(error));
        }
	if (ret == SAF_TEST_PASS){
                sem_wait(&sem_stop_dispatch);
                if (dispatch_ret != SA_AIS_OK){
                        printf("  Does not conform the expected behaviors!\n");
                        printf("  saClmDispatch, Return value: %s, should be "
			  "SA_AIS_OK\n", get_error_string(dispatch_ret));
			ret = SAF_TEST_FAIL;
                }
        }


        return ret;
}


