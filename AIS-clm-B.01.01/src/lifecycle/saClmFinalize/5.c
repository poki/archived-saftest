/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmFinalize
 * Description:   
 *  After finalizing, we call saClmSelectionObjectGet related to the clmHandle
 *  Return value should be SA_AIS_ERR_BAD_HANDLE.
 * Line:        P24-15:P24-15
 */

#include <stdio.h>
#include <stdlib.h>
#include "saAis.h"
#include "saClm.h"
#include "saf_test.h"

static void getnode_callback(SaInvocationT invocation,
                                SaClmClusterNodeT *clusterNode,
                                SaAisErrorT error)
{
        if(error != SA_AIS_OK){
                printf("GetNode Callback failed [%d]\n", error);
        }
        return;
}

static void track_callback(SaClmClusterNotificationBufferT *notificationBuffer,
                SaUint32T numberOfMembers,
                SaAisErrorT error)
{
        if(error != SA_AIS_OK){
                printf("Track Callback failed [%d]\n", error);
        }
        return;
}

int main(int argc, char *argv[])
{
        SaAisErrorT     error;
        SaClmHandleT    clmhandle;
        SaClmCallbacksT clmcallback = {
                .saClmClusterNodeGetCallback  = 
			(SaClmClusterNodeGetCallbackT)getnode_callback,
                .saClmClusterTrackCallback = 
			(SaClmClusterTrackCallbackT)track_callback,
        };
        SaVersionT      clmversion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	SaSelectionObjectT 	selobject;

        int ret = SAF_TEST_PASS;

        error = saClmInitialize(&clmhandle, &clmcallback, &clmversion);
        if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	error = saClmFinalize(clmhandle);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmFinalize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
        }

	error = saClmSelectionObjectGet(clmhandle, &selobject);
	if (error != SA_AIS_ERR_BAD_HANDLE) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saClmSelectionObjectGet, Return value: %s, should be"
			"SA_AIS_ERR_BAD_HANDLE\n", get_error_string(error));
                ret = SAF_TEST_FAIL;
	}

	return ret;
}
	
