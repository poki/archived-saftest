/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmSelectionObjectGet
 * Description:   
 * Test whether the API saClmSelectionObjectGet can return the value
 * SA_AIS_ERR_BAD_HANDLE,Get object after finalize, which will cause
 * the clmHandle unvalid
 * Same with 1-3 
 * Line:        P22-15:P22-17
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "saClm.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char same_case[]="./1-3.test";

	execl(same_case, NULL);
       	printf("        Can't find case %s\n", same_case);
	return SAF_TEST_UNKNOWN; 
}

