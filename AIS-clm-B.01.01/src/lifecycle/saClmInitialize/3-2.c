/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *        Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmInitialize
 * Description:   
 *   Initialize the membership service with a NULL version code.
 *   Return value should be SA_AIS_ERR_INVALID_PARAM
 * Line:        P19-24:P19-29
 */

#include <stdio.h>
#include <stdlib.h>
#include "saClm.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT clmcallback = {
		.saClmClusterNodeGetCallback  = NULL,
		.saClmClusterTrackCallback = NULL,
	};

	int ret = SAF_TEST_PASS;

	error = saClmInitialize(&clmhandle, &clmcallback, NULL);
	if (error != SA_AIS_ERR_INVALID_PARAM) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize, Return value: %s, should be "
			"SA_AIS_ERR_INVALID_PARAM\n", get_error_string(error));
		ret = SAF_TEST_FAIL;
		return ret;
	} 

	return ret;

}
