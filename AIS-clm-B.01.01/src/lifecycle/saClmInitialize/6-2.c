/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *        Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmInitialize
 * Description:   
 *   Initialize the membership service with a version code, whose release 
 *   code is bhigher than current one, and could not be supported now. 
 *   Return value should be SA_AIS_ERR_VERSION.
 * Line:        P20-6:P20-29
 */

#include <stdio.h>
#include <stdlib.h>
#include "saClm.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT clmcallback = {
		.saClmClusterNodeGetCallback  = NULL,
		.saClmClusterTrackCallback = NULL,
	};
	SaVersionT	clmversion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = 'C' 
	};

	int ret = SAF_TEST_PASS;

	error = saClmInitialize(&clmhandle, &clmcallback, &clmversion);
	if (error != SA_AIS_ERR_VERSION) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize: Return value: %s, should be "
			"SA_AIS_ERR_VERSION\n", get_error_string(error));
		ret = SAF_TEST_FAIL;
		return ret;
	} 

	if (clmversion.releaseCode != 'B') {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize, Return realase code %c, should "
			"be 'B'\n", clmversion.releaseCode);
                ret = SAF_TEST_FAIL;
                return ret;

	}
	return ret;

}
