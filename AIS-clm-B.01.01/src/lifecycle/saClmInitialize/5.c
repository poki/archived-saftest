/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *        Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmInitialize
 * Description:   
 *   Initialize the membership service with a version code. Suppose the 
 *   returned major version number is bigger or equal with the required 
 *   majorversion number
 *   Return value should be SA_AIS_OK.
 * Line:        P19-38:P20-5
 */

#include <stdio.h>
#include <stdlib.h>
#include "saClm.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT clmcallback = {
		.saClmClusterNodeGetCallback  = NULL,
		.saClmClusterTrackCallback = NULL,
	};
	SaUint8T 	major_version, minor_version;
	SaVersionT	clmversion; 

	major_version = AIS_B_VERSION_MAJOR;
	minor_version = AIS_B_VERSION_MINOR;
	clmversion.majorVersion = major_version;
	clmversion.minorVersion = minor_version; 
	clmversion.releaseCode = AIS_B_RELEASE_CODE;

	int ret = SAF_TEST_PASS;

	error = saClmInitialize(&clmhandle, &clmcallback, &clmversion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_FAIL;
		return ret;
	}
	if (clmversion.majorVersion < major_version) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmInitialize's major version code: %d\n", 
			clmversion.majorVersion);
		ret = SAF_TEST_FAIL;
		return ret;
	}
	if (clmversion.minorVersion < minor_version) {
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize's minor version code: %d\n",
                        clmversion.minorVersion);
                ret = SAF_TEST_FAIL;
                return ret;
        }

	error = saClmFinalize(clmhandle);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmFinalize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
        }
 	

	return ret;
}
