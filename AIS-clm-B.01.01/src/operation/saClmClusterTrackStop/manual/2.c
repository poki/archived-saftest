/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterTrackStop
 * Description:   
 *     Call saClmClusterTrack, then use this function stops
 *     any further notifications through the handle clmHandle. Test if callback
 *     isn't in use through select. 
 * Line:        P30-16:P30-17
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "saClm.h"
#include "saf_test.h"

static void clm_track_callback(
                        SaClmClusterNotificationBufferT *notificationBuffer,
                        SaUint32T numberOfMembers,
                        SaAisErrorT error)
{
	return;
}

int main(int argc, char *argv[])
{
	SaAisErrorT 	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT	clmcallback = {
		.saClmClusterNodeGetCallback
		=	NULL,
		.saClmClusterTrackCallback
		=	(SaClmClusterTrackCallbackT)clm_track_callback
	};
	SaVersionT	version = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE
	};
	
	int ret = SAF_TEST_PASS;

        SaUint8T trackFlag = SA_TRACK_CHANGES;
        SaClmClusterNotificationBufferT buf;
        SaClmClusterNotificationT notification[100];
        buf.notification = notification;

        fd_set readSet;
        int selectRet;

        struct timeval tv = {
                .tv_sec = 5,
                .tv_usec = 0
        };

	SaSelectionObjectT      selObject;

        error = saClmInitialize(&clmhandle, &clmcallback, &version);
        if (error != SA_AIS_OK){
                printf("  saClmInitialize, Return value: %s, should be "
                                "SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto out;
        }

        error = saClmSelectionObjectGet(clmhandle, &selObject);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmSelectionObjectGet, Return value: %s,should"
                        " be SA_AIS_OK\n",get_error_string(error) );
                ret = SAF_TEST_UNRESOLVED;
                goto final;

        }

	error = saClmClusterTrack(clmhandle, trackFlag, &buf);
	if (error != SA_AIS_OK){
		printf("  saClmClusterTrack, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	error = saClmClusterTrackStop(clmhandle);
	if (error != SA_AIS_OK){
		printf("  saClmClusterTrackStop, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

        //user make membership change
        printf("please change cluster membership, and enter any key:");
        char tempchar;
        scanf("%c", &tempchar);

        FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, &tv);
        if (selectRet != 0){
                printf("  Does not conform the expected behaviors!\n");
                printf("  select error! should be 0 , Return value:%d\n",
                        selectRet);
                ret = SAF_TEST_FAIL;
        }

final:
	saClmFinalize(clmhandle);
out:
	return ret;
}
