/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu, Ling  <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterTrackStop
 * Description:   
 *     saClmClusterTrackStop a saClmHandle which has not been started at all.
 *     Return value should be SA_AIS_ERR_NOT_EXIST
 * Line:        P30-38:P30-42
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "saClm.h"
#include "saf_test.h"

static void track_callback(
                        SaClmClusterNotificationBufferT *notificationBuffer,
                        SaUint32T numberOfMembers,
                        SaAisErrorT error)
{
	return;
}
int main(int argc, char *argv[])
{
	SaAisErrorT 	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT	clmcallback = {
		.saClmClusterNodeGetCallback
		=	NULL,
		.saClmClusterTrackCallback
		=	(SaClmClusterTrackCallbackT)track_callback 
	};
	SaVersionT	version = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE
	};
	
	int ret = SAF_TEST_PASS;

	error = saClmInitialize(&clmhandle, &clmcallback, &version);
	if (error != SA_AIS_OK){
		printf("  saClmInitialize, Return value: %s, should be "
				"SA_AIS_OK", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saClmClusterTrackStop(clmhandle);
	if (error != SA_AIS_ERR_NOT_EXIST){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmClusterTrackStop, Return value: %s, should be "
			"SA_AIS_ERR_NOT_EXIST\n", get_error_string(error));
		ret = SAF_TEST_FAIL;
	}
	
	saClmFinalize(clmhandle);
out:
	return ret;
}
