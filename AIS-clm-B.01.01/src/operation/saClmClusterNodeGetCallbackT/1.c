/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    SaClmClusterNodeGetCallbackT
 * Description:   
 *     saClmClusterNodeGetAsync is called with a invocation.
 *     Check if the invocation match the value in Callback.
 * Line:        P34-14:P34-16
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "saClm.h"
#include "saf_test.h"

SaInvocationT callback_invocation = -1;
static void
getnode_callback(SaInvocationT invocation, 
		SaClmClusterNodeT *clusterNode,
		SaAisErrorT error)
{
	callback_invocation = invocation;
	return;
}

int main(int argc, char *argv[])
{
	SaAisErrorT 	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT	clmcallback = {
		.saClmClusterNodeGetCallback
		=	(SaClmClusterNodeGetCallbackT)getnode_callback,
		.saClmClusterTrackCallback	= NULL
	};
	SaVersionT	version = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE
	};

	int ret = SAF_TEST_UNKNOWN;

	error = saClmInitialize(&clmhandle, &clmcallback, &version);
	if (error != SA_AIS_OK){
		printf("  saClmInitialize, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

        SaInvocationT invocation = 1;
        SaClmNodeIdT nodeId = SA_CLM_LOCAL_NODE_ID;

	error = saClmClusterNodeGetAsync(clmhandle, invocation, nodeId);
	if (error != SA_AIS_OK){
		printf("  saClmClusterNodeGetAsync, Return value: %s, should be"				" SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

        SaSelectionObjectT selectionObject;

        error = saClmSelectionObjectGet(clmhandle, &selectionObject);
        if (error != SA_AIS_OK){
                printf("  saClmSelectionObjectGet Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

        fd_set rset;
        FD_ZERO(&rset);
        FD_SET(selectionObject, &rset);

        if(select(selectionObject + 1, &rset, NULL, NULL, NULL) == -1){
		printf("select error\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }
	if(!FD_ISSET(selectionObject, &rset)){
                printf("fd set error\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
	}
        
	error = saClmDispatch(clmhandle, SA_DISPATCH_ALL);
        if (error != SA_AIS_OK){
                printf("saClmDispath, Return value: %s, should be SA_AIS_OK\n",
						get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	if(callback_invocation == -1){
                printf("callback isn't called.\n");
                ret = SAF_TEST_UNRESOLVED;
	}
	else if(callback_invocation != invocation){
		printf("invocation in callback can't match the initial "
			"invocation.\n");
		ret = SAF_TEST_FAIL;
	}
	else
		ret = SAF_TEST_PASS;

final:
	saClmFinalize(clmhandle);
out:
	return ret;
}
