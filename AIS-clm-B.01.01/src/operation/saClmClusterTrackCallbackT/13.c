/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    SaClmClusterTrackCallbackT
 * Description:   
 *   Two processes request tracking with the same flags set. For a 
 *   particular view number, check if the Cluster Membership Service shall 
 *   provide the same information about the cluster membership when it invokes
 *   the saClmClusterTrackCallback() function of those processes.
 *   13-fork.c will be the child process.
 * Line:        P29-17:P29-20
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "saClm.h"
#include "saf_test.h"

int ret = SAF_TEST_UNKNOWN;
static void clm_track_callback(
			SaClmClusterNotificationBufferT *notificationBuffer,
			SaUint32T numberOfMembers,
			SaAisErrorT error)
{
	int i=0;
	for (i=0;i<numberOfMembers;i++){
		if ((*notificationBuffer).notification[i].clusterChange
			!= SA_CLM_NODE_NO_CHANGE){
			printf("  notification change status error!");
			ret = SAF_TEST_FAIL;
			break;
		}
	}
	if (i>=numberOfMembers)
		ret = SAF_TEST_PASS;
	return;
}

int main(int argc, char *argv[])
{

	SaAisErrorT 	error;
	SaClmHandleT	clm_handle;
	SaClmCallbacksT	clm_callback = {
		.saClmClusterNodeGetCallback	= NULL,
		.saClmClusterTrackCallback
		=	(SaClmClusterTrackCallbackT)clm_track_callback
	};
	SaVersionT	clm_version = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE
	};

        SaUint8T trackFlag = SA_TRACK_CURRENT | SA_TRACK_CHANGES;

	pid_t pid;
	int child_return;

        if ((pid = fork()) < 0 ){
                printf("fork a process error!\n");
                goto out;
	}
	if(pid == 0){
		execl("./13-fork.test", NULL);
	        printf("	fork fails \n");
	        goto out;
	}
	error = saClmInitialize(&clm_handle, &clm_callback, &clm_version);
	if (error != SA_AIS_OK){
		printf("  can't initalize, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saClmClusterTrack(clm_handle, trackFlag, NULL);
	if (error != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmClusterTrack, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

        SaSelectionObjectT selectionObject;

        error = saClmSelectionObjectGet(clm_handle, &selectionObject);
        if (error != SA_AIS_OK){
                printf("  saClmSelectionObjectGet, Return value: %s, should be "
                                "SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

        fd_set rset;
        FD_ZERO(&rset);
        FD_SET(selectionObject, &rset);

        if(select(selectionObject + 1, &rset, NULL, NULL, NULL) == -1){
                printf("select error\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }
        if(!FD_ISSET(selectionObject, &rset)){
                printf("fd set error\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }
	
	error = saClmDispatch(clm_handle, SA_DISPATCH_ALL);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmDispatch, Return value: %s, should be "
                                "SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }	
        if (pid != wait(&child_return)){
		printf("wait error!");
		ret = SAF_TEST_UNRESOLVED;
                goto final;
	}
		
        if (!WIFEXITED(child_return) ||
		(WEXITSTATUS(child_return) != SAF_TEST_PASS)){
		printf("child failed\n");
                ret = SAF_TEST_FAIL;
                goto final;
        }

final:
	saClmFinalize(clm_handle);
out:
	return ret;
}
