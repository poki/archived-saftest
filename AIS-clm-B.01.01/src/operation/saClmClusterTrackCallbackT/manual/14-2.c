/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    SaClmClusterTrackCallbackT
 * Description:   
 *     Call saClmClusterTrack() with the SA_TRACK_CHANGES or
 *     SA_TRACK_CHANGES_ONLY is set.
 *     Check if the value of the numberOfItems attribute in the 
 *     notificationBuffer parameter is greater than the value of the
 *     numberOfMembers parameter.
 * Line:        P29-22:P29-27
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "saClm.h"
#include "saf_test.h"

int ret = SAF_TEST_UNKNOWN;
static void clm_track_callback(
			SaClmClusterNotificationBufferT *notificationBuffer,
			SaUint32T numberOfMembers,
			SaAisErrorT error)
{
	if ((*notificationBuffer).numberOfItems <= numberOfMembers){		
		printf("  notification change status error!");
			ret = SAF_TEST_FAIL;
	}
	else
		ret = SAF_TEST_PASS;
	return;
}

int main(int argc, char *argv[])
{

	SaAisErrorT 	error;
	SaClmHandleT	clm_handle;
	SaClmCallbacksT	clm_callback = {
		.saClmClusterNodeGetCallback	= NULL,
		.saClmClusterTrackCallback
		=	(SaClmClusterTrackCallbackT)clm_track_callback
	};
	SaVersionT	clm_version = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE
	};

	error = saClmInitialize(&clm_handle, &clm_callback, &clm_version);
	if (error != SA_AIS_OK){
		printf("  can't initalize, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

        SaUint8T trackFlag = SA_TRACK_CHANGES;
        SaClmClusterNotificationBufferT buf;
	SaClmClusterNotificationT notification[100];
	buf.notification = notification;

	error = saClmClusterTrack(clm_handle, trackFlag, &buf);
	if (error != SA_AIS_OK){
		printf("  saClmClusterTrack, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	//user need to change membership manually to let one node leave the
	//cluster.
	char tempchar;
	printf("please make a node leave the cluster and type enter:\n");
	scanf("%c", &tempchar);

        SaSelectionObjectT selectionObject;

        error = saClmSelectionObjectGet(clm_handle, &selectionObject);
        if (error != SA_AIS_OK){
                printf("  saClmSelectionObjectGet, Return value: %s, should be "
                                "SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

        fd_set rset;
        FD_ZERO(&rset);
        FD_SET(selectionObject, &rset);

        if(select(selectionObject + 1, &rset, NULL, NULL, NULL) == -1){
                printf("select error\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }
        if(!FD_ISSET(selectionObject, &rset)){
                printf("fd set error\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	error = saClmDispatch(clm_handle, SA_DISPATCH_ALL);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmDispatch, Return value: %s, should be "
                                "SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }	
        
final:
	saClmFinalize(clm_handle);
out:
	return ret;
}
