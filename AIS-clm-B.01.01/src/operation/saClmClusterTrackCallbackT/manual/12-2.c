/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    SaClmClusterTrackCallbackT
 * Description:   
 *     Call saClmClusterTrack() with flag SA_TRACK_CHANGES and non-null
 *     notificationBuffer pointer parameters, let a node leave the cluster and
 *     then saClmDispatch(),
 *     check if the callback function returns information about the cluster
 *     nodes in the notificationBuffer parameter.
 * Line:        P29-11:P29-16
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "saClm.h"
#include "saf_test.h"

SaClmNodeIdT nodeId;
char nodeName[100];
int ret = SAF_TEST_UNKNOWN;
static void clm_track_callback(
			SaClmClusterNotificationBufferT *notificationBuffer,
			SaUint32T numberOfMembers,
			SaAisErrorT error)
{
	if(numberOfMembers<=1){
		printf("	the number of buffer isn't correct!\n");
		ret = SAF_TEST_FAIL;
		return;
	}
	int i=0;
	for(i=0; i<numberOfMembers; i++){
		if( (*notificationBuffer).notification[i].clusterNode.nodeId
		== nodeId ){
			if(!strcmp((*notificationBuffer).notification[i]
			.clusterNode.nodeName.value, nodeName)
			&&((*notificationBuffer).notification[i].clusterChange
			== SA_CLM_NODE_LEFT)){
				ret = SAF_TEST_PASS;
				return;
			}
			else{
				printf("the node information isn't correct.\n");
				ret = SAF_TEST_FAIL;
				return;
			}
		}		
	}
	printf("	can't find the node.\n");
	ret = SAF_TEST_FAIL;
	return;
}

int main(int argc, char *argv[])
{

	SaAisErrorT 	error;
	SaClmHandleT	clm_handle;
	SaClmCallbacksT	clm_callback = {
		.saClmClusterNodeGetCallback	= NULL,
		.saClmClusterTrackCallback
		=	(SaClmClusterTrackCallbackT)clm_track_callback
	};
	SaVersionT	clm_version = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE
	};

	error = saClmInitialize(&clm_handle, &clm_callback, &clm_version);
	if (error != SA_AIS_OK){
		printf("  can't initalize, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

        SaUint8T trackFlag = SA_TRACK_CHANGES;
        SaClmClusterNotificationBufferT buf;
	SaClmClusterNotificationT notification[100];
	buf.notification = notification;

	error = saClmClusterTrack(clm_handle, trackFlag, &buf);
	if (error != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmClusterTrack, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

        //let a node leave the cluster and enter one nodeId and its nodeName.
        printf("enter one node id which leave current membership:");
        scanf("%ld",&nodeId);
        printf("\nenter the node name of this node id:");
        scanf("%s",nodeName);

	
        SaSelectionObjectT selectionObject;

        error = saClmSelectionObjectGet(clm_handle, &selectionObject);
        if (error != SA_AIS_OK){
                printf("  saClmSelectionObjectGet, Return value: %s, should be "
                                "SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

        fd_set rset;
        FD_ZERO(&rset);
        FD_SET(selectionObject, &rset);

        if(select(selectionObject + 1, &rset, NULL, NULL, NULL) == -1){
                printf("select error\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }
        if(!FD_ISSET(selectionObject, &rset)){
                printf("fd set error\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	error = saClmDispatch(clm_handle, SA_DISPATCH_ALL);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmDispatch, Return value: %s, should be "
                                "SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }	
        
final:
	saClmFinalize(clm_handle);
out:
	return ret;
}
