/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterNodeGet
 * Description:   
 *     saClmClusterNodeGet is called with the constant SA_CLM_LOCAL_NODE_ID is
 *     used as nodeId, Check if the returned information is about the cluster
 *     node that hosts the invoking process.
 * Line:        P31-39:P31-40
 */

#include <stdio.h>
#include <string.h>

#include "saClm.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	SaAisErrorT 	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT	clmcallback = {
		.saClmClusterNodeGetCallback	= NULL,
		.saClmClusterTrackCallback	= NULL
	};
	SaVersionT	version = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE
	};

	int ret = SAF_TEST_PASS;

	error = saClmInitialize(&clmhandle, &clmcallback, &version);
	if (error != SA_AIS_OK){
		printf("  saClmInitialize, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

        SaClmClusterNodeT clusterNode;
	char nodeName[100];
        SaClmNodeIdT nodeId = SA_CLM_LOCAL_NODE_ID;

        //enter local node nodeName in current cluster.
        printf("enter the node name of the local node:");
        scanf("%s",nodeName);

	error = saClmClusterNodeGet(	clmhandle,
					nodeId, 
					(SaTimeT)1000000000*10, 
					&clusterNode);
	if (error != SA_AIS_OK){
		printf("  saClmClusterNodeGet, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

        if(!clusterNode.member){
		printf("this node isn't the member of this cluster.\n");
		ret = SAF_TEST_FAIL;
	}
	else if(strcmp(clusterNode.nodeName.value, nodeName)){
                printf("this name isn't the name of this node id.\n");
                ret = SAF_TEST_FAIL;
	}
final:
	error = saClmFinalize(clmhandle);
	if(error != SA_AIS_OK){
		printf("  saClmFinalize, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
	}
out:
	return ret;
}
