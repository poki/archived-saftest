/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterNodeGetAsync
 * Description:   
 *     saClmClusterNodeGetAsync is called with all valid parameters.
 *     Return should be SA_AIS_OK. Check if the clusterNode information is 
 *     correct.
 * Line:        P33-9:P33-10
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "saClm.h"
#include "saf_test.h"

char nodeName[100];
int callback_flag = -1;
static void
getnode_callback(SaInvocationT invocation, 
		SaClmClusterNodeT *clusterNode,
		SaAisErrorT error)
{
	callback_flag = 0;
	if(((*clusterNode).member)
	&&(!strcmp((*clusterNode).nodeName.value, nodeName)))
		callback_flag = 1;
	return;
}

int main(int argc, char *argv[])
{
	SaAisErrorT 	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT	clmcallback = {
		.saClmClusterNodeGetCallback
		=	(SaClmClusterNodeGetCallbackT)getnode_callback,
		.saClmClusterTrackCallback	= NULL
	};
	SaVersionT	version = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE
	};

	int ret = SAF_TEST_UNKNOWN;

	error = saClmInitialize(&clmhandle, &clmcallback, &version);
	if (error != SA_AIS_OK){
		printf("  saClmInitialize, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

        SaInvocationT invocation = 1;
	SaClmNodeIdT nodeId;

	//enter one nodeId and its nodeName in current cluster.
	printf("enter one node id in current cluster:");
	scanf("%ld",&nodeId);
	printf("\nenter the node name of this node id:");
	scanf("%s",nodeName);


	error = saClmClusterNodeGetAsync(clmhandle, invocation, nodeId);
	if (error != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmClusterNodeGetAsync, Return value: %s, should be"				" SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

        SaSelectionObjectT selectionObject;

        error = saClmSelectionObjectGet(clmhandle, &selectionObject);
        if (error != SA_AIS_OK){
                printf("  saClmSelectionObjectGet, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

        fd_set rset;
        FD_ZERO(&rset);
        FD_SET(selectionObject, &rset);

        if(select(selectionObject + 1, &rset, NULL, NULL, NULL) == -1){
                printf("select error\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }
        if(!FD_ISSET(selectionObject, &rset)){
                printf("fd set error\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
	}

	error = saClmDispatch(clmhandle, SA_DISPATCH_ALL);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmDispatch, Return value: %s, should be "
			" SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
        }

	if(callback_flag == -1){
		printf("callback can't be called.");
		ret = SAF_TEST_UNRESOLVED;
	}
	else if(callback_flag == 0){
                printf("node information in callback is incorrect.\n");
                ret = SAF_TEST_FAIL;
	}
	else
		ret = SAF_TEST_PASS;

final:
	saClmFinalize(clmhandle);
out:
	return ret;
}
