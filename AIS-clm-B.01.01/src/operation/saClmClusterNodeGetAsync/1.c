/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterNodeGetAsync
 * Description:   
 *     saClmClusterNodeGetAsync is called with valid clmHandle.
 *     Return should be SA_AIS_OK.
 * Line:        P32-39:P32-40
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "saClm.h"
#include "saf_test.h"

static void
getnode_callback(SaInvocationT invocation, 
		SaClmClusterNodeT *clusterNode,
		SaAisErrorT error)
{
	return;
}

int main(int argc, char *argv[])
{
	SaAisErrorT 	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT	clmcallback = {
		.saClmClusterNodeGetCallback
		=	(SaClmClusterNodeGetCallbackT)getnode_callback,
		.saClmClusterTrackCallback	= NULL
	};
	SaVersionT	version = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE
	};
	SaInvocationT invocation;
	SaClmNodeIdT nodeId = SA_CLM_LOCAL_NODE_ID;

	int ret = SAF_TEST_UNKNOWN;

	error = saClmInitialize(&clmhandle, &clmcallback, &version);
	if (error != SA_AIS_OK){
		printf("  saClmInitialize, Return value: %s, should be "
				"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saClmClusterNodeGetAsync(clmhandle, invocation, nodeId);
	if (error != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmClusterNodeGetAsync, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_FAIL;
	}
	else
		ret = SAF_TEST_PASS;

	saClmFinalize(clmhandle);
out:
	return ret;
}
