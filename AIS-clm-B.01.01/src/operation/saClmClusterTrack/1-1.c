/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *        Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterTrack
 * Description:   
 *   saClmClusterTrack correctly, Set a valid clmHandle and valid track 
 *   Flag as SA_TRACK_CURRENT. The function completes successfully.
 *   Return value should be SA_AIS_OK
 * Line:        P25-16:P25-18
 */

#include <stdio.h>
#include <stdlib.h>
#include "saAis.h"
#include "saClm.h"
#include "saf_test.h"

static void track_callback(SaClmClusterNotificationBufferT *nbuf,
        			SaUint32T nitem,
			        SaAisErrorT error)
{
	return;
}


int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT clmcallback = {
		.saClmClusterNodeGetCallback  = NULL,
		.saClmClusterTrackCallback = 
			(SaClmClusterTrackCallbackT)track_callback, 
	};
	SaVersionT	clmversion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE 
	};
	SaUint8T trackFlags = SA_TRACK_CURRENT;
	SaClmClusterNotificationBufferT nbuf = {
		.notification = NULL
	};

	int ret = SAF_TEST_PASS;

	error = saClmInitialize(&clmhandle, &clmcallback, &clmversion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	error = saClmClusterTrack(clmhandle, trackFlags, &nbuf);
	if (error != SA_AIS_OK) {
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmClusterTrack, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_FAIL;
	}
	
	if (nbuf.notification != NULL)
		free(nbuf.notification);

	 saClmFinalize(clmhandle);

	return ret;
}
