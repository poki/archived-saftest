/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *        Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterTrack
 * Description:   
 *   First call saClmClusterTrack using the flag of SA_TRACK_CHANGES_ONLY, 
 *   then call it using SA_TRACK_CHANGES. Test whether a new callbacks is used.
 *   Return value should be SA_AIS_OK
 * Line:        P27-1:P27-5
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>
#include "saAis.h"
#include "saClm.h"
#include "saf_test.h"

#define SLEEP_TIME	4

int ret = SAF_TEST_FAIL;
int changed = 0;

static void track_callback(SaClmClusterNotificationBufferT *nbuf,
		SaUint32T nitem,
		SaAisErrorT error)
{
	int i, success;
	success = 0;
	if (changed == 1) {
		for (i=0; i<nitem; i++){
                	if ((*nbuf).notification[i].clusterChange == 
					SA_CLM_NODE_NO_CHANGE){
				success = 1;
       	                 break;
                	}
        	}
	}
	if (success == 0) {
		 printf("  notification change status error!");
	} else {	
		ret = SAF_TEST_PASS;
	}
		
}


int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT clmcallback = {
		.saClmClusterNodeGetCallback  = NULL,
		.saClmClusterTrackCallback = 
			(SaClmClusterTrackCallbackT)track_callback,
	};
	SaVersionT	clmversion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE 
	};
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ONE;
        SaSelectionObjectT fd;
        int select_ret;
        fd_set rset;

	SaUint8T trackFlags = SA_TRACK_CURRENT | SA_TRACK_CHANGES_ONLY;
	SaUint8T newTrackFlags = SA_TRACK_CURRENT | SA_TRACK_CHANGES;


	error = saClmInitialize(&clmhandle, &clmcallback, &clmversion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
        error = saClmSelectionObjectGet(clmhandle, &fd);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmSelectionObjectGet, Return value: %s, should "
			"be SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	error = saClmClusterTrack(clmhandle, trackFlags, NULL);
	if (error != SA_AIS_OK) {
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmClusterTrack, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
 	sleep(SLEEP_TIME);	/* sleep for a while and then invoke a new track*/
	changed = 1;
  	error = saClmClusterTrack(clmhandle, newTrackFlags, NULL);
        if (error != SA_AIS_OK) {
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmClusterTrack, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_FAIL;
		goto final;
        }

        FD_ZERO(&rset);
        FD_SET(fd, &rset);
        select_ret = select(fd + 1, &rset, NULL, NULL, NULL);
        if ((select_ret == -1) || (select_ret == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	error = saClmDispatch(clmhandle, dispatchFlag);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmDispatch, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
        }


final:
	saClmFinalize(clmhandle);

	return ret;
}
