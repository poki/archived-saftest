/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *        Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterTrack
 * Description:   
 *   call saClmClusterTrack repeatedly with callbacks for the same value 
 *   of clmHandle.
 *   Return value should be SA_AIS_OK
 * Line:        P26-40:P26-43
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include "saAis.h"
#include "saClm.h"
#include "saf_test.h"
#define CALL_TIMES 10

int ret = SAF_TEST_PASS;

static void
track_callback(SaClmClusterNotificationBufferT *nbuf,
	SaUint32T nitem,
	SaAisErrorT error)
{
	ret = SAF_TEST_PASS;
}


int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT clmcallback = {
		.saClmClusterNodeGetCallback  = NULL,
		.saClmClusterTrackCallback = 
				(SaClmClusterTrackCallbackT)track_callback,
	};
	SaVersionT	clmversion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE 
	};
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
        SaSelectionObjectT fd;
        int select_ret;
	int count;
        fd_set rset;

	SaUint8T trackFlags = SA_TRACK_CURRENT;


	error = saClmInitialize(&clmhandle, &clmcallback, &clmversion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
        error = saClmSelectionObjectGet(clmhandle, &fd);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmSelectionObjectGet, Return value: %s, should "
			"be SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	count = 0;
	while ((count <= CALL_TIMES) && (ret != SAF_TEST_FAIL)) {
		error = saClmClusterTrack(clmhandle, trackFlags, NULL);
		if (error != SA_AIS_OK) {
       	         	printf("  Does not conform the expected behaviors!\n");
                	printf("  saClmClusterTrack, Return value: %s, "
			  "should be SA_AIS_OK\n", get_error_string(error));
                	ret = SAF_TEST_FAIL;
		}
		count ++;
	}

	ret = SAF_TEST_FAIL;
        FD_ZERO(&rset);
        FD_SET(fd, &rset);
        select_ret = select(fd + 1, &rset, NULL, NULL, NULL);
        if ((select_ret == -1) || (select_ret == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	error = saClmDispatch(clmhandle, dispatchFlag);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmDispatch, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
        }

	if (count > 0)
		saClmClusterTrackStop(clmhandle);
final:
	saClmFinalize(clmhandle);

	return ret;
}
