/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterTrack
 * Description:   
 *   Test whether this API saClmClusterTrack can return a value of 
 *   SA_AIS_ERR_BAD_FLAGS
 *   Same with 2
 * Line:        P27-39:P27-40
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "saClm.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char same_case[]="./2.test";

	execl(same_case, NULL);
       	printf("        Can't find case %s\n", same_case);
	return SAF_TEST_UNKNOWN; 
}

