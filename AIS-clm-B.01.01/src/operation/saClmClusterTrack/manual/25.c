/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *        Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterTrack
 * Description:   
 *  Test the view number implemant
 *  If the application has implemented the view number, then a change
 *  may cause the view number change
 * Line:        P26-16:P26-20
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <string.h>
#include "saClm.h"
#include "saf_test.h"
#define MAX_NODE_NAME 128

int ret = SAF_TEST_UNKNOWN;
int membership_changed = 0;
SaUint64T current_view_number = 0;

static void
track_callback(SaClmClusterNotificationBufferT *nbuf,
	SaUint32T nitem,
	SaAisErrorT error)
{
	if (!membership_changed) {
		current_view_number = (*nbuf).viewNumber;
	} else {
		if (current_view_number == 0) {
			ret = SAF_TEST_UNRESOLVED;
		} else {
			 if (current_view_number == (*nbuf).viewNumber) 
				ret = SAF_TEST_FAIL;
			else 
				ret = SAF_TEST_PASS;
		}
	}
        return;
}


int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT clmcallback = {
		.saClmClusterNodeGetCallback  = NULL,
		.saClmClusterTrackCallback = 
				(SaClmClusterTrackCallbackT)track_callback,
	};
	SaVersionT	clmversion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE 
	};
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ONE;
        SaSelectionObjectT fd;
        fd_set rset;
	SaClmClusterNotificationBufferT nbuf= {
		.notification = NULL
	};
        int select_ret;
	char yes;

	SaUint8T trackFlags = SA_TRACK_CURRENT;


	printf("Do this implementation use viewNumber to describ current"
		 " cluster membership?\n");
	scanf("%c", &yes);
	if ((yes != 'Y') || (yes != 'y'))  {
		ret = SAF_TEST_PASS;
		return ret;
	}
		
	error = saClmInitialize(&clmhandle, &clmcallback, &clmversion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
        error = saClmSelectionObjectGet(clmhandle, &fd);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmSelectionObjectGet, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	error = saClmClusterTrack(clmhandle, trackFlags, &nbuf);
	if (error != SA_AIS_OK) {
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmClusterTrack, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_FAIL;
	}

	FD_ZERO(&rset);
        FD_SET(fd, &rset);
        select_ret = select(fd + 1, &rset, NULL, NULL, NULL);
        if ((select_ret == -1) || (select_ret == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

        error = saClmDispatch(clmhandle, dispatchFlag);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmDispatch, Return value: %s, should be "
                        "SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	/* call saClmClusterTrack again */
	trackFlags = SA_TRACK_CHANGES_ONLY;
        error = saClmClusterTrack(clmhandle, trackFlags, &nbuf);
        if (error != SA_AIS_OK) {
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmClusterTrack, Return value: %s, should be "
                        "SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_FAIL;
        }

	/* change the current membership */
	printf(" Please change the cluster membership or an attribute of a"
		 "cluster node\n"); 
	printf("  Press any key to continue...\n");
        getchar();
	membership_changed = 1;


        FD_ZERO(&rset);
        FD_SET(fd, &rset);
        select_ret = select(fd + 1, &rset, NULL, NULL, NULL);
        if ((select_ret == -1) || (select_ret == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	error = saClmDispatch(clmhandle, dispatchFlag);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmDispatch, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
        }


final:
	saClmFinalize(clmhandle);

	return ret;
}
