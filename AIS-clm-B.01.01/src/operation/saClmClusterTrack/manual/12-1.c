/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *        Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterTrack
 * Description:
 *  Call saClmClusterTrack with flag of SA_TRACK_CHANGES, and then call
 *  saClmClusterTrackStop. Change the cluster member and test whether the
 *  callback still enable.
 * Line:       P27-6:P27-9 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>
#include "saClm.h"
#include "saf_test.h"

#define TIMEOUT_TIME	100

int ret = SAF_TEST_PASS;
int	track_stopped = 0;

static void track_callback(SaClmClusterNotificationBufferT *nbuf,
		SaUint32T nitem,
		SaAisErrorT error)
{
	if (track_stopped) {
		ret = SAF_TEST_FAIL;
	}
}


int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT clmcallback = {
		.saClmClusterNodeGetCallback  = NULL,
		.saClmClusterTrackCallback = 
				(SaClmClusterTrackCallbackT)track_callback,
	};
	SaVersionT	clmversion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE 
	};
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ONE;
        SaSelectionObjectT fd;
        fd_set rset;
        int select_ret;
	char membership_change;
	struct timeval  timeout;

	SaUint8T trackFlags = SA_TRACK_CHANGES;

	error = saClmInitialize(&clmhandle, &clmcallback, &clmversion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
        error = saClmSelectionObjectGet(clmhandle, &fd);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmSelectionObjectGet, Return value: %s, should "
				"be SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	error = saClmClusterTrack(clmhandle, trackFlags, NULL);
	if (error != SA_AIS_OK) {
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmClusterTrack, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
	}

	track_stopped = 1;
  	error = saClmClusterTrackStop(clmhandle);
        if (error != SA_AIS_OK) {
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmClusterTrackStop, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }

	printf("  Please change the cluster membership or an attribute of a "
                        "cluster node\n");
        printf("  Press any key to continue...\n");
        membership_change = getchar();

        FD_ZERO(&rset);
        FD_SET(fd, &rset);
	timeout.tv_sec = TIMEOUT_TIME; 
	timeout.tv_usec = 0; 
        select_ret = select(fd + 1, &rset, NULL, NULL, &timeout);
        if (select_ret == -1 ){	
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	error = saClmDispatch(clmhandle, dispatchFlag);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmDispatch, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
        }

final:
	saClmFinalize(clmhandle);

	return ret;
}
