/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *        Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterTrack
 * Description:   
 *  Call saClmClusterTrack correctly, set a valid clmHandle and valid track
 *  Flag as SA_TRACK_CURRENT and set notificationBuffer as NULL. The
 *  specified left node should not be returned in callback correctly.
 *  Return value should be SA_AIS_OK.
 * Line:        P25-23:P25-27
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <string.h>
#include "saClm.h"
#include "saf_test.h"
#define MAX_NODE_NAME 128

int ret = SAF_TEST_UNKNOWN;
SaClmNodeIdT nodeId;
char node_name[MAX_NODE_NAME];

static void
track_callback(SaClmClusterNotificationBufferT *nbuf,
			SaUint32T nitem, SaAisErrorT error)
{

        int i = 0;
        for(i = 0; i < nitem; i++){
                if( (*nbuf).notification[i].clusterNode.nodeId == nodeId ){
                        if(!strcmp((*nbuf).notification[i]
                               .clusterNode.nodeName.value, node_name)) {
                                ret = SAF_TEST_FAIL;
                                printf("      the node should not exist"
					" in callback function\n");
                                return;
                        }
                }
        }
        ret = SAF_TEST_PASS;
        return;

}


int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT clmcallback = {
		.saClmClusterNodeGetCallback  = NULL,
		.saClmClusterTrackCallback = 
			(SaClmClusterTrackCallbackT)track_callback,
	};
	SaVersionT	clmversion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE 
	};
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ONE;
        SaSelectionObjectT fd;
        fd_set rset;
        int select_ret;

	SaUint8T trackFlags = SA_TRACK_CURRENT;


	error = saClmInitialize(&clmhandle, &clmcallback, &clmversion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
        error = saClmSelectionObjectGet(clmhandle, &fd);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmSelectionObjectGet, Return value: %s, should "
			"be SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	error = saClmClusterTrack(clmhandle, trackFlags, NULL);
	if (error != SA_AIS_OK) {
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmClusterTrack, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_FAIL;
		goto final;
	}

	printf("Please make a node to leave the current membership");
        printf("Press any key to continue...\n");
	getchar();
	printf("enter the node ID who has left current  membership:");
        scanf("%ld",&nodeId);
        printf("\nEnter the node name of this node id:");
        scanf("%s",node_name);	

        FD_ZERO(&rset);
        FD_SET(fd, &rset);
        select_ret = select(fd + 1, &rset, NULL, NULL, NULL);
        if ((select_ret == -1) || (select_ret == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

	error = saClmDispatch(clmhandle, dispatchFlag);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmDispatch, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
        }


final:
	error = saClmFinalize(clmhandle);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmFinalize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
        }

	return ret;
}
