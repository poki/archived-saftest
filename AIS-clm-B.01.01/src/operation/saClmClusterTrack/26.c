/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *        Yu, Ping  <ping.y.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saClmClusterTrack
 * Description:   
 *  Use numberOfItems to specify the buffer size pointed by notification
 *  Test whether the return value is less than or equal with input one
 * Line:       P26-21:P26-27 
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include "saClm.h"
#include "saf_test.h"
#define MAX_NOTIFICATION 128

int buffer_size = 0;
int ret = SAF_TEST_UNKNOWN;

static void track_callback(SaClmClusterNotificationBufferT *nbuf,
        			SaUint32T nitem,
			        SaAisErrorT error)
{
	if ((error == SA_AIS_OK) || (error == SA_AIS_ERR_NO_SPACE)) {
		if (nitem > buffer_size) {
   		  /* the return value should less than the orignal size */
			ret = SAF_TEST_FAIL; 
		} else {
			ret = SAF_TEST_PASS;
		}
	}
}


int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT clmcallback = {
		.saClmClusterNodeGetCallback  = NULL,
		.saClmClusterTrackCallback = 
			(SaClmClusterTrackCallbackT)track_callback, 
	};
	SaVersionT	clmversion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE 
	};
	SaUint8T trackFlags = SA_TRACK_CURRENT;
	SaClmClusterNotificationBufferT nbuf;
	SaClmClusterNotificationT notification[MAX_NOTIFICATION];
	SaSelectionObjectT fd;

	nbuf.notification = notification;
	nbuf.numberOfItems = MAX_NOTIFICATION; /* the buffer size */
	buffer_size = nbuf.numberOfItems;

	error = saClmInitialize(&clmhandle, &clmcallback, &clmversion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saClmInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	error = saClmSelectionObjectGet(clmhandle, &fd);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmSelectionObjectGet, Return value: %s, should be "
                        "SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }
	
	error = saClmClusterTrack(clmhandle, trackFlags, &nbuf);
	if (error != SA_AIS_OK) {
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmClusterTrack, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_FAIL;
	}

	if (nbuf.numberOfItems >= buffer_size) {
		/* the return value should be less than the orignal size */
		ret = SAF_TEST_FAIL; 
	} else {
		ret = SAF_TEST_PASS;
	}

final:
	error = saClmFinalize(clmhandle);
        if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmFinalize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
        }

	return ret;
}
