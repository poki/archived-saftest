#!/bin/sh
#
# Copyright (c) 2004 by Intel Corp.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307 USA.
#
# Authors:
#       Yu,Ling  <ling.l.yu@intel.com>
#

topdir=.
report_tests="all"

RESULT_TXT=result.txt
RESULT_XML=result.xml

MRUN_LOG=./log/mrun_log

usage()
{
	cat <<EOF
Usage: $0 [-m] [all|SPEC]
Summerize the test report for the SAF-Test conformance test cases for SAF SPECs.The results will be at ./result.txt and ./result.xml.
  -m		Manual test cases will be included. Default is not.
  
  all		All test cases will be calculated, including:
		- AIS-A.01.01 conformance test cases
		- AIS-B.01.01 conformance test cases
		- HPI-A.01.01 conformance test cases
		- HPI-B.01.01 conformance test cases
  SPEC		Summerize the test report for the test cases for specific SAF
		Specification.
		SPEC can be:
	AIS-A.01.01		for whole SAF AIS A.01.01
	AIS-amf-A.01.01		for SAF AIS A.01.01 Availability Management
				Framework
        AIS-ckpt-A.01.01	for SAF AIS A.01.01 Checkpoint Service
        AIS-evt-A.01.01       	for SAF AIS A.01.01 Event Service
        AIS-mem-A.01.01         for SAF AIS A.01.01 Cluster Membership Service
        AIS-mes-A.01.01         for SAF AIS A.01.01 Message Service
        AIS-lock-A.01.01        for SAF AIS A.01.01 Lock Service
        AIS-B.01.01             for whole SAF AIS B.01.01
        AIS-amf-B.01.01         for SAF AIS B.01.01 Availability Management
				Framework
        AIS-ckpt-B.01.01        for SAF AIS B.01.01 Checkpoint Service
        AIS-evt-B.01.01         for SAF AIS B.01.01 Event Service
        AIS-clm-B.01.01         for SAF AIS B.01.01 Custer Membership Service
        AIS-msg-B.01.01         for SAF AIS B.01.01 Message Service
        AIS-lck-B.01.01		for SAF AIS B.01.01 Lock Service
        HPI-A.01.01		for whole SAF HIP A.01.01
	HPI-B.01.01             for whole SAF HIP B.01.01

EOF
}

result_header()
{
	echo "SAF-Test Result" >> $RESULT_TXT 2>&1
	echo "<?xml version=\"1.0\"?>" >> $RESULT_XML 2>&1
	echo "<Report Title=\"SAF-Test\">" >> $RESULT_XML 2>&1
}
report_testsuites()
{
	echo "calculate test cases in $topdir/$1"
	if [ -d $topdir/$1 ]; then	
		echo "Spec Name= $1" >> $RESULT_TXT
		echo "<Spec Name=\"$1\">" >> $RESULT_XML
		echo "<Data>" >> $RESULT_XML
		run_prog="./report.sh $MANUAL all"
		cd $topdir/$1/
		$run_prog 2>&1
		cd -
		cat $topdir/$1/result.txt >> $RESULT_TXT
		cat $topdir/$1/result.txt >> $RESULT_XML
		cat $topdir/$1/log/mrun_log >> "$MRUN_LOG-$1"
		echo "</Data>" >> $RESULT_XML
		echo "</Spec>" >> $RESULT_XML
	else
		echo "Can't find  $topdir/$1"
	fi
}

if [ "$1" == "-m" ]; then
	MANUAL="-m"
	shift
fi

case "$1" in
   "")
	usage
	;;
   "--help")
        usage
        ;;
   "all")
	cat /dev/null > $RESULT_TXT
	cat /dev/null > $RESULT_XML
	result_header
	report_testsuites "AIS-amf-A.01.01"
	report_testsuites "AIS-ckpt-A.01.01"
	report_testsuites "AIS-evt-A.01.01"
	report_testsuites "AIS-mem-A.01.01"
	report_testsuites "AIS-mes-A.01.01"
	report_testsuites "AIS-lock-A.01.01"
        report_testsuites "AIS-amf-B.01.01"
        report_testsuites "AIS-ckpt-B.01.01"
        report_testsuites "AIS-evt-B.01.01"
        report_testsuites "AIS-clm-B.01.01"
        report_testsuites "AIS-msg-B.01.01"
        report_testsuites "AIS-lck-B.01.01"
	report_testsuites "HPI-A.01.01"
	report_testsuites "HPI-B.01.01"
        echo "</Report>" >> $RESULT_XML
	;;
   "AIS-A.01.01")
        cat /dev/null > $RESULT_TXT
        cat /dev/null > $RESULT_XML
        result_header
        report_testsuites "AIS-amf-A.01.01"
        report_testsuites "AIS-ckpt-A.01.01"
        report_testsuites "AIS-evt-A.01.01"
        report_testsuites "AIS-mem-A.01.01"
        report_testsuites "AIS-mes-A.01.01"
        report_testsuites "AIS-lock-A.01.01"
	echo "</Report>" >> $RESULT_XML
	;;
   "AIS-B.01.01")
        cat /dev/null > $RESULT_TXT
        cat /dev/null > $RESULT_XML
        result_header
        report_testsuites "AIS-amf-B.01.01"
        report_testsuites "AIS-ckpt-B.01.01"
        report_testsuites "AIS-evt-B.01.01"
        report_testsuites "AIS-clm-B.01.01"
        report_testsuites "AIS-msg-B.01.01"
        report_testsuites "AIS-lck-B.01.01"
        echo "</Report>" >> $RESULT_XML
	;;
   *) 
        cat /dev/null > $RESULT_TXT
        cat /dev/null > $RESULT_XML
        result_header
        report_testsuites $1
        echo "</Report>" >> $RESULT_XML
        ;;
esac
