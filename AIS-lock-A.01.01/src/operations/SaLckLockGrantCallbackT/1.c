/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Liu,Li  <li.l.liu@intel.com>
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "im_spec.h"
#include "saf_test.h"

int a = 5;
SaInvocationT arg = (SaInvocationT)&a;
int lock_granted = 0;

char name[] = "TestLockName";

/* ================================================================== */
static void lock_grant_callback(SaInvocationT invocation,
		const SaLckResourceIdT *resourceId,
		const SaLckLockIdT *lockId,
		SaLckLockModeT lockMode,
		SaLckLockStatusT lockStatus,
		SaErrorT error)
{
	lock_granted = 1;
	return;
}

int
main(int argc, char * argv[])
{
	SaErrorT err;
	SaSelectionObjectT selection_obj;
	SaNameT lock_name;
	SaVersionT version;
	SaLckHandleT lck_handle;
	SaLckResourceIdT res_handle;
	SaLckCallbacksT lck_callbacks;
	SaLckLockModeT lock_mode = SA_LCK_EX_LOCK_MODE;
	SaLckLockFlagsT lock_flag;
	SaLckLockIdT lock_id;
	int timeout = 2000;
	int ret;
	fd_set fset;
	int nfd;

	version.major = VERSION_MAJOR;
	version.minor = VERSION_MINOR;
	version.releaseCode = 'A';
	
	lck_callbacks.saLckLockGrantCallback = lock_grant_callback;
	lck_callbacks.saLckLockWaiterCallback = NULL;
	lck_callbacks.saLckResourceUnlockCallback = NULL;

	err = saLckInitialize(&lck_handle, &lck_callbacks, &version);
	if (err != SA_OK) {
		printf("  Function \"saLckInitialize\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out1;

	}
	lock_name.length = strlen(name) + 1;
	strncpy(lock_name.value, name, lock_name.length);
	err = saLckResourceOpen(&lck_handle, &lock_name, &res_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckResourceOpen\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out2;
	}
	
	
	err = SaLckResourceLockAsync(&lck_handle,
			arg,
			&res_handle,
			&lock_id,
			lock_mode,
			lock_flag,
			timeout
			);
	if (err != SA_OK) {
		printf("  Function \"SaLckResourceLockAsync\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out3;
	}

	err = saLckSelectionObjectGet(&lck_handle, &selection_obj);
	if (err != SA_OK) {
		printf("  Function \"saLckSelectionObjectGet\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out4;
	}

	FD_ZERO(&fset);
	FD_SET(selection_obj, &fset);
	nfd = selection_obj + 1;
	if (select(nfd, &fset, NULL, NULL, NULL) == -1) {
		printf("  Function \"select\" works abnormally!\n");
		ret = SAF_TEST_UNRESOLVED;
		goto out4;

	}
	if(!FD_ISSET(selection_obj, &fset)) {
		printf("  Function \"select\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out4;
	} 
				
	err = saLckDispatch(&lck_handle, SA_DISPATCH_ALL);
	if (err != SA_OK) {
		printf("  Function \"saLckDispatch\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
	}

	if (lock_granted <= 0) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Pending callbacks are not called!\n");
		ret = SAF_TEST_FAIL;
	}

out4:
	err = saLckResourceUnlock(&lck_handle, &lock_id, timeout);	
	if (err != SA_OK) {
		printf("  Function \"saLckResourceUnlock\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
	}
out3:
	err = saLckResourceClose(&lck_handle, &res_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckResourceClose\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
	}
out2:
	err = saLckFinalize(&lck_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckFinalize\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
	}
out1:
	return ret;
}
