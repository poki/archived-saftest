/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Liu,Li  <li.l.liu@intel.com>
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "im_spec.h"
#include "saf_test.h"

int a = 5;
SaInvocationT arg = (SaInvocationT)&a;
char name[] = "TestLockName";

/* ================================================================== */
static void lock_unlock_callback(SaInvocationT invocation,
				const SaLckResourceIdT *resourceId,
				const SaLckLockIdT *lockId,
				SaLckLockStatusT lockStatus,
				SaErrorT error)
{
	return;
}

int
main(int argc, char * argv[])
{
	SaErrorT err;
	SaNameT lock_name;
	SaTimeT timeout = 5000;
	SaLckResourceIdT res_handle;
	SaLckLockStatusT lock_status;
	SaVersionT version;
	SaLckLockIdT lock_id;
	SaLckHandleT lck_handle;
	SaLckLockModeT lock_mode = SA_LCK_EX_LOCK_MODE;
	SaLckLockFlagsT lock_flag = 0;
	SaLckCallbacksT lck_callbacks;
	int ret = SAF_TEST_PASS;

	version.major = VERSION_MAJOR;
	version.minor = VERSION_MINOR;
	version.releaseCode = 'A';
	
	lck_callbacks.saLckLockGrantCallback = NULL;
	lck_callbacks.saLckLockWaiterCallback = NULL;
	lck_callbacks.saLckResourceUnlockCallback = lock_unlock_callback;

	err = saLckInitialize(&lck_handle, &lck_callbacks, &version);
	if (err != SA_OK) {
		printf("  Function \"saLckInitialize\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out1;
	}

	lock_name.length = strlen(name)+1;
	strncpy(lock_name.value, name, lock_name.length);
	err = saLckResourceOpen(&lck_handle, &lock_name, &res_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckResourceOpen\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out2;
	}

	err = saLckResourceLock(&lck_handle,
			arg,
			&res_handle,
			&lock_id,
			lock_mode,
			lock_flag,
			timeout,
			&lock_status);
	if (err != SA_OK) {
		printf("  Function \"saLckResourceLock\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out3;
	}
	
	err = saLckResourceUnlockAsync(&lck_handle, arg, &lock_id);
	if (err != SA_OK) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_FAIL;
	}
out3:
	err = saLckResourceClose(&lck_handle,&res_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckResourceClose\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
	}
out2:
	err = saLckFinalize(&lck_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckFinalize\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
	}
out1:
	return ret;
}
