/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Liu,Li  <li.l.liu@intel.com>
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "im_spec.h"
#include "saf_test.h"

static void lock_grant_callback(SaInvocationT invocation,
		const SaLckResourceIdT *resourceId,
		const SaLckLockIdT *lockId,
		SaLckLockModeT lockMode,
		SaLckLockStatusT lockStatus,
		SaErrorT error)
{
	return;
}

static void lock_waiter_callback(SaInvocationT invocation,
			  const SaLckResourceIdT *resourceId,
			  const SaLckLockIdT *lockId,
			  SaLckLockModeT modeHeld,
			  SaLckLockModeT modeRequested
			  )
{
	return;
}

static void lock_unlock_callback(SaInvocationT invocation,
		const SaLckResourceIdT *resourceId,
		const SaLckLockIdT *lockId,
		SaLckLockStatusT lockStatus,
		SaErrorT error)
{
	return;
}

int
main(int argc, char * argv[])
{
	SaErrorT err;
	SaVersionT version;
	SaLckHandleT lck_handle;
	SaLckCallbacksT lck_callbacks;
	int ret = SAF_TEST_PASS;

	version.major = VERSION_MAJOR;
	version.minor = VERSION_MINOR;
	version.releaseCode = 'B';
	
	lck_callbacks.saLckLockGrantCallback = lock_grant_callback;
	lck_callbacks.saLckLockWaiterCallback = lock_waiter_callback;
	lck_callbacks.saLckResourceUnlockCallback = lock_unlock_callback;

	err = saLckInitialize(&lck_handle, &lck_callbacks, &version);
	if (err != SA_ERR_VERSION) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_FAIL;
	} 
	return ret;
}
