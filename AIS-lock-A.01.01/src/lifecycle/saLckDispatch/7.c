/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Liu,Li  <li.l.liu@intel.com>
 *
 */

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <wait.h>

#include "im_spec.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	pid_t pid;
	int stat, ret = SAF_TEST_PASS;

	pid = fork();
	if (pid < 0) {
		printf("  Function \"fork\" works abnormally!\n");
		ret = SAF_TEST_UNRESOLVED;
		goto out1;
	} else if (pid == 0) {	
		execl("./core.test", "core.test", "one", "callback", NULL);
	} else {
		wait(&stat);
		ret = WEXITSTATUS(stat);
	}
out1:
	return ret;
}


