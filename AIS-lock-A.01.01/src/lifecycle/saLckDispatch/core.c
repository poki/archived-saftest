/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Liu,Li  <li.l.liu@intel.com>
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "im_spec.h"
#include "saf_test.h"

int a = 5;
SaInvocationT arg = (SaInvocationT)&a;

char name[] = "TestLockName";
int lock_granted = 0;
int unlock_granted = 0;


/* ================================================================== */
static void lock_grant_callback(SaInvocationT invocation,
		const SaLckResourceIdT *resourceId,
		const SaLckLockIdT *lock_id,
		SaLckLockModeT lock_mode,
		SaLckLockStatusT lockStatus,
		SaErrorT error)
{
	lock_granted = 1;
}

static void lock_waiter_callback(SaInvocationT invocation,
			  const SaLckResourceIdT *resourceId,
			  const SaLckLockIdT *lock_id,
			  SaLckLockModeT modeHeld,
			  SaLckLockModeT modeRequested
			  )
{
	return;
}

static void lock_unlock_callback(SaInvocationT invocation,
		const SaLckResourceIdT *resourceId,
		const SaLckLockIdT *lock_id,
		SaLckLockStatusT lockStatus,
		SaErrorT error)
{
	unlock_granted = 1;
}

int
main(int argc, char * argv[])
{
	SaErrorT err;
	SaSelectionObjectT selection_obj;
	SaNameT lock_name;
	SaVersionT version;
	SaLckHandleT lck_handle;
	SaLckResourceIdT res_handle;
	SaLckCallbacksT lck_callbacks;
	SaLckLockIdT lock_id;
	SaLckLockModeT lock_mode = SA_LCK_EX_LOCK_MODE;
	SaLckLockFlagsT lock_flag = 0;
	SaDispatchFlagsT flag;
	int timeout = 2000;
	int ret;
	fd_set fset;
	int nfd;
	int test_return = 1; 

	if (argc != 3) {
		return SAF_TEST_UNRESOLVED;
	}

	if (!strcmp(argv[1], "one")){
		flag = SA_DISPATCH_ONE;	
	} else if (!strcmp(argv[1], "all")) {
		flag = SA_DISPATCH_ALL;
	}  else 
		return SAF_TEST_UNRESOLVED;

	if (!strcmp(argv[2], "return")) {
		test_return = 1;	/* Only test the return value*/
	} else if(!strcmp(argv[2], "callback"))
		test_return = 0;	/*Only test whether the pending callbacks are called*/
	else
		return SAF_TEST_UNRESOLVED;
			
	version.major = VERSION_MAJOR;
	version.minor = VERSION_MINOR;
	version.releaseCode = 'A';
	
	lck_callbacks.saLckLockGrantCallback = lock_grant_callback;
	lck_callbacks.saLckLockWaiterCallback = lock_waiter_callback;
	lck_callbacks.saLckResourceUnlockCallback = lock_unlock_callback;

	err = saLckInitialize(&lck_handle, &lck_callbacks, &version);
	if (err != SA_OK) {
		printf("  Function \"saLckInitialize\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out1;

	}
	lock_name.length = strlen(name) + 1;
	strncpy(lock_name.value, name, lock_name.length);
	err = saLckResourceOpen(&lck_handle, &lock_name, &res_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckResourceOpen\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out2;
	}
	
	err = SaLckResourceLockAsync(&lck_handle,
			arg,
			&res_handle,
			&lock_id,
			lock_mode,
			lock_flag,
			timeout
			);
	if (err != SA_OK) {
		printf("  Function \"SaLckResourceLockAsync\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out3;
	}

	err = saLckResourceUnlockAsync(&lck_handle, arg, &lock_id);
	if (err != SA_OK) {
		printf("  Function \"SaLckResourceUnlockAsync\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out3;
	}
	
	err = saLckSelectionObjectGet(&lck_handle, &selection_obj);
	if (err != SA_OK) {
		printf("  Function \"saLckSelectionObjectGet\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out3;
	}

	FD_ZERO(&fset);
	FD_SET(selection_obj, &fset);
	nfd = selection_obj + 1;
	if (select(nfd, &fset, NULL, NULL, NULL) == -1) {
		printf("  Function \"select\" works abnormally!\n");
		ret = SAF_TEST_UNRESOLVED;
		goto out3;

	}
	if(!FD_ISSET(selection_obj, &fset)) {
		printf("  Function \"select\" works abnormally!\n");
		ret = SAF_TEST_UNRESOLVED;
		goto out3;
	} 
				
	err = saLckDispatch(&lck_handle, flag);
	if (err != SA_OK) {
		if (test_return) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Return value: %s\n", get_aisAerror_string(err));
			ret = SAF_TEST_FAIL;
		} else {
			printf("  Function \"select\" works abnormally!\n");
			printf("  Return value: %s\n", get_aisAerror_string(err));
			ret = SAF_TEST_UNRESOLVED;
		}
		goto out3;
	}

	if (!test_return) {	
		if (flag == SA_DISPATCH_ONE) {
			if ((lock_granted && unlock_granted) || (!lock_granted && !unlock_granted)) {
				printf("  Does not conform the expected behaviors!\n");
				printf("  Pending callbacks are not called\n");
				ret = SAF_TEST_FAIL;
			} 
		} else if (flag == SA_DISPATCH_ALL) {
			if (!lock_granted || !unlock_granted) {
				printf("  Does not conform the expected behaviors!\n");
				printf("  Pending callbacks are not called\n");
				ret = SAF_TEST_FAIL;
			}
		}
	}

out3:
	err = saLckResourceClose(&lck_handle, &res_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckResourceClose\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
	}
out2:
	err = saLckFinalize(&lck_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckFinalize\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
	}
out1:
	return ret;
}
