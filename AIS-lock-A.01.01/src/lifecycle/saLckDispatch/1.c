/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Liu,Li  <li.l.liu@intel.com>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#include "im_spec.h"
#include "saf_test.h"

int a = 5;
SaInvocationT arg = (SaInvocationT)&a;
char name[] = "TestLockName";
int lock_granted = 0;
SaErrorT dispatch_ret = SA_OK;

sem_t sem;

/* ================================================================== */
static void lock_grant_callback(SaInvocationT invocation,
		const SaLckResourceIdT *resourceId,
		const SaLckLockIdT *lock_id,
		SaLckLockModeT lock_mode,
		SaLckLockStatusT lockStatus,
		SaErrorT error)
{
	lock_granted = 1;
}

static void *dispatch_thread(void *arg)
{
	SaLckHandleT *lck_handle = (SaLckHandleT*)arg;
	SaErrorT err;

	lock_granted = 0;
	err = saLckDispatch(lck_handle, SA_DISPATCH_BLOCKING);
	dispatch_ret = err;
	sem_post(&sem);
	return NULL;
}

int
main(int argc, char * argv[])
{
	SaErrorT err;
	SaNameT lock_name;
	SaVersionT version;
	SaLckHandleT lck_handle;
	SaLckResourceIdT res_handle;
	SaLckCallbacksT lck_callbacks;
	SaLckLockIdT lock_id;
	SaLckLockModeT lock_mode = SA_LCK_EX_LOCK_MODE;
	SaLckLockFlagsT lock_flag = 0;
	int timeout = 2000;
	int ret;
	pthread_t thread_id;

	version.major = VERSION_MAJOR;
	version.minor = VERSION_MINOR;
	version.releaseCode = 'A';
	
	lck_callbacks.saLckLockGrantCallback = lock_grant_callback;
	lck_callbacks.saLckLockWaiterCallback = NULL;
	lck_callbacks.saLckResourceUnlockCallback = NULL;

	err = saLckInitialize(&lck_handle, &lck_callbacks, &version);
	if (err != SA_OK) {
		printf("  Function \"saLckInitialize\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out1;
	}
	
	sem_init(&sem, 0, 0);
	err = pthread_create(&thread_id, NULL, dispatch_thread, (void*)&lck_handle);
	if (err) {
		printf("  Function \"pthread_create\" works abnormally!\n");
		printf("  Return value: %s\n", strerror(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out2;
	}
	
	lock_name.length = strlen(name) + 1;
	strncpy(lock_name.value, name, lock_name.length);
	err = saLckResourceOpen(&lck_handle, &lock_name, &res_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckResourceOpen\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out2;
	}
	
	err = SaLckResourceLockAsync(&lck_handle,
			arg,
			&res_handle,
			&lock_id,
			lock_mode,
			lock_flag,
			timeout
			);
	if (err != SA_OK) {
		printf("  Function \"SaLckResourceLockAsync\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
		goto out3;
	}

	err = saLckResourceUnlock(&lck_handle, &lock_id, timeout);	
	if (err != SA_OK) {
		printf("  Function \"saLckResourceUnlock\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
	}

out3:
	err = saLckResourceClose(&lck_handle, &res_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckResourceClose\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
	}
out2:
	err = saLckFinalize(&lck_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckFinalize\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_UNRESOLVED;
	}
	
	if (ret == SAF_TEST_PASS) {
		sem_wait(&sem);
		if ((dispatch_ret != SA_OK) || (lock_granted <= 0)) {
			printf("  Does not conform the expected behaviors!\n");
			if (dispatch_ret != SA_OK)
				printf("  Return value: %s\n", get_aisAerror_string(dispatch_ret));
			if (lock_granted <= 0) 
				printf("  Pending callbacks not invoked!\n");
			ret = SAF_TEST_FAIL;
		}
	}
	
out1:
	return ret;
}


