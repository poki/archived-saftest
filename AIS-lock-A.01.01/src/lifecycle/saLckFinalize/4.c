/*
 *  Copyright (c) 2004 by Intel Corp.
 *  
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License,
 *  version 2, as published by the Free Software Foundation.
 *  
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc, 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 *  Authors:
 *  	Liu Li  <li.l.liu@intel.com>
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "im_spec.h"
#include "saf_test.h"

int
main(int argc, char * argv[])
{
	SaErrorT err;
	SaVersionT version;
	SaLckHandleT lck_handle;
	int ret = SAF_TEST_PASS;

	version.major = VERSION_MAJOR;
	version.minor = VERSION_MINOR;
	version.releaseCode = 'A';
	
	err = saLckInitialize(&lck_handle, NULL, &version);
	if (err != SA_OK) {
		printf("  Function \"saLckInitialize\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_FAIL;
		goto out;
	}

	err = saLckFinalize(NULL);
	if (err != SA_ERR_BAD_HANDLE) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		ret = SAF_TEST_FAIL;
		goto out;	
	}

	err = saLckFinalize(&lck_handle);
	if (err != SA_OK) {
		printf("  Function \"saLckFinalize\" works abnormally!\n");
		printf("  Return value: %s\n", get_aisAerror_string(err));
		goto out;
	}
out:
	return ret;
}
