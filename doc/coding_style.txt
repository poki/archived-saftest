C Coding standards:
==================
General: 
  All code will compile without any warnings on Linux, using the compile 
  option of -Wall

(1)C dialect
-------------
  Nowadays, there are some different dialect C languages. To keep compiler 
  compatibility, we use gcc 3.0 or above to be the compiler and code 

(2)Code lay-out
---------------
    - Use tab to be indents and the tab size should be 8 

    - restrict one line has less than 79 characters.

    - user header file should used """ to separate and system header file 
   should use "<" and ">" to separate.
	#include <stdio.h>
        #include <stdlib.h>
        #include "myheader.h"
    
    - system header file should be placed before user header file 

    - No line should end in white space. 

    - Function definition style: outermost curly braces in column 1, blank 
   line after local variable declarations.

	static int function(vartype1 *var1, vartype2 *var2)
	{
		int local_var1;
		int local_var2;

		/* the code begins here */
		return 1;
	}
   
    - Function implementation style: all local variable should be defined 
   before any statement.
              

    - Code structure: one space between keywords like 'if', 'for' and
   the following left paren
      
	if (mro != NULL){
		...
	}
	else { 	
		...
	} 

     - The return statement should *not* get redundant parentheses:

	return ret_value; /* correct */
	return(ret_value); /* incorrect */

   
    - SUGGESTION: Function and macro call style: foo(a, b, c) -- 
      one space after each comma.

    - SUGGESTION: Always put spaces around assignment, Boolean and comparison
      operators.  In expressions using a lot of operators, add spaces
      around the outermost (lowest-priority) operators.

    - SUGGESTION: Comments go before the code they describe.

    - SUGGESTION: All functions and global variables should be declared static
      unless they are to be part of a published interface

    - SUGGESTION: use global variables as little as possible


(3)Naming conventions
-------------------
   - The variable's name is clear enough, so that reader can get the meaning 
  for the variable. Never to use single character to be variable except cycle 
  variable
   - SUGGESTION: to use lower case characters and underscore to connect with 
  different words
   Example: cycle_number, person_count

(4)SAFtest Specified Style
------------------------
    - Copyright & License
      Preferred Copyright format is:
         Copyright (c) year, copyright_owner
      For example:
         Copyright (c) 2004, Intel Corporation.
      If you need to follow other Copyright format, that's fine too. 

    The license declaration should be followed. The example is the following:

    This program is free software; you can redistribute it and/or modify it
    under the terms and conditions of the GNU General Public License,
    version 2, as published by the Free Software Foundation.
 
    This program is distributed in the hope it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.
 
    You should have received a copy of the GNU General Public License along with
    this program; if not, write to the Free Software Foundation, Inc., 59 Temple
    Place - Suite 330, Boston, MA 02111-1307 USA.
           
    - Author
      author's name should be listed directly below the copyright declaration.
    Author section format is the following:
      Authors:
		last_name, first_name <eamil_address>
      For example:
		Yu, Ping <ping.y.yu@intel.com>

    - Case Description
      The case description section includes Spec, Function, Description, and 
   Line information.
      For example:	
	Spec:        AIS-B.01.01
 	Function:    saClmDispatch
 	Description:
	  Dispatch correctly, Set dispatch Flag to SA_DISPATCH_ALL. The function
 	  completes successfully.
 	  Return value should be SA_AIS_OK
          Line:        P23-1:P23-5


(5)Others
-------
    - Security is importants in design and in code.  
	Avoid strcpy, strcat etc.  use strncpy, strncat etc.
	Design interfaces to pass size of buffers for return
		strings, or malloc space for return results.


