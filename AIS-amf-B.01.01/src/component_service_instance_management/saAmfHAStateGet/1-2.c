/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saAmfHAStateGet
 * Description:
 *  Call the api with invalid amfHandle which is uninitialized. 
 *  Return = SA_AIS_ERR_BAD_HANDLE
 * Line:        P177-34:P177-35
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saAmf.h"
#include "saf_test.h"
#define COMPONENT_1 "comp_1"
#define CSI_1 "csi_1"
int main(int argc, char *argv[])
{
	char apiName[] = "saAmfHAStateGet";
	SaAisErrorT     expectedReturn= SA_AIS_ERR_BAD_HANDLE;
        SaAmfHandleT   amfHandle;

	SaAisErrorT	amfError;
	SaNameT compName = {
		.value = COMPONENT_1,
		.length = strlen(COMPONENT_1)
	};

	SaNameT csiName ={
		.value = CSI_1,
		.length = strlen(CSI_1)
	};
	
	SaAmfHAStateT haState;
        int ret = SAF_TEST_PASS;
	
	amfError = saAmfHAStateGet(amfHandle,&compName,&csiName,&haState);
	if (amfError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(amfError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
	}

	return ret;
}


