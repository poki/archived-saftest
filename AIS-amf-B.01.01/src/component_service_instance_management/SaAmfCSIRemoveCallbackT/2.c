/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saAmfCSIRemoveCallbackT
 * Description:
 *  Invoke the api with valid compName.
 *  Return = NULL 
 * Line:        P179-16:P179-20
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saAmf.h"
#include "saf_test.h"
#define COMPONENT_1 "comp_1"

SaAmfCSIDescriptorT	csiDescriptorGlobal;
SaAmfHandleT   amfHandle;
SaAisErrorT    error;
SaInvocationT invocationGlobal;
SaNameT 	compNameGlobal;
	
static void amf_health_check_callback (SaInvocationT invocation,
        SaNameT		*compName,
        SaAmfHealthcheckKeyT *healthcheckKey)
{
        return;
}

static void amf_component_terminate_callback(SaInvocationT invocation,
        SaNameT		*compName)
{
	return;
}

static void amf_csi_set_callback (SaInvocationT invocation,
	SaNameT 	*compName,
	SaAmfHAStateT	haState,
        SaAmfCSIDescriptorT	csiDescriptor)
{
	memcpy(&csiDescriptorGlobal,&csiDescriptor,
			sizeof(SaAmfCSIDescriptorT));
	error = saAmfResponse(amfHandle,invocation,SA_AIS_OK);
        return;
}

static void amf_csi_remove_callback(SaInvocationT invocation,
	SaNameT 	*compName,
	SaNameT 	*csiName,
        SaAmfCSIFlagsT	csiFlags)
{
	memcpy(&compNameGlobal,csiName,sizeof(SaNameT));
	error = saAmfResponse(amfHandle,invocation,SA_AIS_OK);
	return;
}

static void amf_protectiongroup_track_callback(SaNameT	*csiName,
	SaAmfProtectionGroupNotificationBufferT	*notificationBuffer,
	SaUint32T	numberOfMembers,
	SaAisErrorT	error)
{
	return;
}

static void amf_proxiedcomponent_instantiate_callback(SaInvocationT invocation,
	SaNameT 	*proxiedCompName)
{
	return;
}

static void amf_proxiedcomponent_cleanup_callback(SaInvocationT invocation,
	SaNameT 	*proxiedCompName)
{
	return;
}

int main(int argc, char *argv[])
{
        SaAmfCallbacksT amfCallback = {
               .saAmfHealthcheckCallback  = 
	       	(SaAmfHealthcheckCallbackT ) amf_health_check_callback,
                .saAmfComponentTerminateCallback = 
	(SaAmfComponentTerminateCallbackT) amf_component_terminate_callback,
		.saAmfCSISetCallback = 
		(SaAmfCSISetCallbackT)	amf_csi_set_callback,
		.saAmfCSIRemoveCallback = 
		(SaAmfCSIRemoveCallbackT) amf_csi_remove_callback,
		.saAmfProtectionGroupTrackCallback = 
	(SaAmfProtectionGroupTrackCallbackT) amf_protectiongroup_track_callback,
		.saAmfProxiedComponentInstantiateCallback = 
		(SaAmfProxiedComponentInstantiateCallbackT)	
				amf_proxiedcomponent_instantiate_callback,
		.saAmfProxiedComponentCleanupCallback = 
			(SaAmfProxiedComponentCleanupCallbackT)
				amf_proxiedcomponent_cleanup_callback
        };
	SaAisErrorT	amfError;
        SaVersionT      amfVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaSelectionObjectT 	selObject;
	fd_set readSet;
	int selectRet;
	SaNameT compName = {
		.value = COMPONENT_1,
		.length = strlen(COMPONENT_1)
	};
	SaAisErrorT aisError;
	SaAmfHAStateT haState;
        int ret = SAF_TEST_PASS;

        amfError = saAmfInitialize(&amfHandle, &amfCallback, &amfVersion);
        if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	amfError = saAmfSelectionObjectGet(amfHandle, &selObject);
	if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfInitialize, Return value: %s, should be"
			" saAmfSelectionObjectGet\n",
			get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
        amfError = saAmfComponentNameGet(amfHandle,&compName);
        if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("saAmfComponentNameGet, Return value: %s, should be"
                        " SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
                goto final_1;
        }
	
	amfError = saAmfComponentRegister(amfHandle,&compName,NULL);
	if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("saAmfComponentRegister, Return value: %s, should be"
                        " SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
                goto final_1;
        }
	
	/* ACTION: accoring to P198 6.1 Administrative Shutdown of a service
	instance operational scenarios in the spec AIS-B.01.01
	*/
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	// ACTION: amf should invoke saAmfCSISetCallbackT firstly
	amfError = saAmfDispatch(amfHandle, SA_DISPATCH_ALL);
	if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfDispatch, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	//check if the return of saAmfResponse is SA_AIS_OK 
	if (error != SA_AIS_OK){
		printf("  Does not conform the expected behaviros\n");
		printf("  saAmfResponse Return valude: %s,should be"
			" be SA_AIS_OK\n",get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	//the component tell amf that it is ok.
	amfError = saAmfCSIQuiescingComplete(amfHandle,invocationGlobal,
								aisError);
	if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfCSIQuiescingComplete, Return value: %s,"
			" should be SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}	
	
	//the component check its status.
	amfError = saAmfHAStateGet(amfHandle,&compName,
		&(csiDescriptorGlobal.csiName),&haState);
	if (amfError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfHAStateGet, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_FAIL;
	}
	
	// ACTION: wait for amf invoking RemoveCallbackT
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	
	// ACTION: amf should invoke saAmfCSIRemoveCallbackT 
	amfError = saAmfDispatch(amfHandle, SA_DISPATCH_ALL);
	if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfDispatch, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	//check if the return of saAmfResponse is SA_AIS_OK 
	if (error != SA_AIS_OK){
		printf("  Does not conform the expected behaviros\n");
		printf("  saAmfResponse Return valude: %s,should be"
			" be SA_AIS_OK\n",get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
	}
	
	/* check the compName from callback function is the same with
	   the one obtained from saAmfNameGet
	*/
	if (memcmp(&compNameGlobal,&compName,sizeof(SaNameT))) {
		printf("  Does not conform the expected behaviors\n");
		printf("  compName does not match \n");
		ret = SAF_TEST_FAIL;
	}
	
final_2 :	
	saAmfComponentUnregister(amfHandle, &compName,NULL);
final_1 :
	saAmfFinalize(amfHandle);
	return ret;
}
