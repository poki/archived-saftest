/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saAmfDispatch
 * Description:   
 *   Dispatch correctly, Set dispatch Flag to SA_DISPATCH_BLOCKING. The 
 *   function completes successfully.
 *   Return = SA_AIS_OK
 * Line:        P158-32:P158-36
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include "saAmf.h"
#include "saf_test.h"

#define COMPONENT_1 "comp_1"
#define KEY_1 "saftest_key_1"
int callbackFlag = 0;
SaAisErrorT dispatchReturn;
sem_t sem_start_dispatch;
sem_t sem_stop_dispatch;

static void amf_health_check_callback (SaInvocationT invocation,
        SaNameT		*compName,
        SaAmfHealthcheckKeyT *healthcheckKey)
{
	callbackFlag ++;
        return;
}

static void amf_component_terminate_callback(SaInvocationT invocation,
        SaNameT		*compName)
{
	return;
}

static void amf_csi_set_callback (SaInvocationT invocation,
	SaNameT 	*compName,
	SaAmfHAStateT	haState,
        SaAmfCSIDescriptorT	csiDescriptor)
{
        return;
}

static void amf_csi_remove_callback(SaInvocationT invocation,
	SaNameT 	*compName,
	SaNameT 	*csiName,
        SaAmfCSIFlagsT	csiFlags)
{
	return;
}

static void amf_protectiongroup_track_callback(SaNameT	*csiName,
	SaAmfProtectionGroupNotificationBufferT	*notificationBuffer,
	SaUint32T	numberOfMembers,
	SaAisErrorT	error)
{
	return;
}

static void amf_proxiedcomponent_instantiate_callback(SaInvocationT invocation,
	SaNameT 	*proxiedCompName)
{
	return;
}

static void amf_proxiedcomponent_cleanup_callback(SaInvocationT invocation,
	SaNameT 	*proxiedCompName)
{
	return;
}

static void *dispatch_thread(void *arg)
{
	sem_post(&sem_start_dispatch);
	dispatchReturn = saAmfDispatch(* ((SaAmfHandleT *) arg),
					SA_DISPATCH_BLOCKING);
	sem_post(&sem_stop_dispatch);
	return NULL;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saAmfDispatch";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaAmfHandleT   amfHandle;
        SaAmfCallbacksT amfCallback = {
               .saAmfHealthcheckCallback  = 
	       	(SaAmfHealthcheckCallbackT ) amf_health_check_callback,
                .saAmfComponentTerminateCallback = 
	(SaAmfComponentTerminateCallbackT) amf_component_terminate_callback,
		.saAmfCSISetCallback = 
	(SaAmfCSISetCallbackT)	amf_csi_set_callback,
		.saAmfCSIRemoveCallback = 
	(SaAmfCSIRemoveCallbackT) amf_csi_remove_callback,
		.saAmfProtectionGroupTrackCallback = 
	(SaAmfProtectionGroupTrackCallbackT) 
			amf_protectiongroup_track_callback,
		.saAmfProxiedComponentInstantiateCallback = 
		(SaAmfProxiedComponentInstantiateCallbackT)	
				amf_proxiedcomponent_instantiate_callback,
		.saAmfProxiedComponentCleanupCallback = 
			(SaAmfProxiedComponentCleanupCallbackT)
				amf_proxiedcomponent_cleanup_callback
        };
	SaAisErrorT	amfError;
        SaVersionT      amfVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	fd_set readSet;
	int selectRet;
	SaNameT compName = {
		.value = COMPONENT_1,
		.length = strlen(COMPONENT_1)
	};
	
	SaAmfHealthcheckKeyT	healthcheckKey = {
			.key = KEY_1,
			.keyLen = strlen(KEY_1)
	};
	SaSelectionObjectT 	selObject;
        int ret = SAF_TEST_PASS;
	
	pthread_t thread_id;
	int pthreadError;

        amfError = saAmfInitialize(&amfHandle, &amfCallback, &amfVersion);
        if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }

        amfError = saAmfComponentNameGet(amfHandle,&compName);
        if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("saAmfComponentNameGet, Return value: %s, should be"
                        " SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
                goto final_1;
        }
	
	amfError = saAmfComponentRegister(amfHandle,&compName,NULL);
	if (amfError != SA_AIS_OK) {
		printf(" Does not conform the expected behaviors!\n");
		printf(" saAmfComponentRegister, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(amfError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_1;	
	}
	
	sem_init(&sem_start_dispatch, 0, 0);
	sem_init(&sem_stop_dispatch, 0, 0);
	pthreadError = pthread_create(&thread_id,
					NULL, 
					dispatch_thread, 
					(void*)&amfHandle);
	if (pthreadError){
		printf("	invoke \"pthread_create\" has error,"
				" return %s\n", strerror(pthreadError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	amfError = saAmfSelectionObjectGet(amfHandle, &selObject);
	if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfSelectionObjectGet, Return value: %s, should be"
			" SA_AIS_OK\n",
			get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	
	amfError = saAmfHealthcheckStart(amfHandle,&compName,&healthcheckKey,
					SA_AMF_HEALTHCHECK_AMF_INVOKED,
					SA_AMF_NO_RECOMMENDATION);
	if (amfError != SA_AIS_OK) {
		printf(" Does not conform the expected behaviors!\n");
		printf(" saAmfHealthcheckStart, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(amfError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;	
	}
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final_3;
        }
	
	sem_wait(&sem_start_dispatch);
final_3 :	
	amfError = saAmfHealthcheckStop(amfHandle,&compName,&healthcheckKey);
	if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfHealthcheckStop, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
        }
	
final_2 :
	amfError = saAmfComponentUnregister(amfHandle, &compName,NULL);
	if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfComponentUnregister, Return value: %s,should"
			" be SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
        }
		
final_1 :

	amfError = saAmfFinalize(amfHandle);
	if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfChannelUnlink, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
        }
	
	if (ret != SAF_TEST_PASS)
		return ret;

	sem_wait(&sem_stop_dispatch);
	
	if ( callbackFlag <= 0){
		printf("	invoke %s has error, pending callbacks are "
				"not invoked\n", apiName);
		return SAF_TEST_FAIL;
	}
	
	if ( dispatchReturn != expectedReturn ){
		printf(" Does not conform the expected behaviors!\n");
                printf(" %s, Return value :%s, should be %s\n", 
			apiName,
			get_error_string(dispatchReturn),
			get_error_string(expectedReturn));
		return SAF_TEST_FAIL;
	}
	return ret;
}


