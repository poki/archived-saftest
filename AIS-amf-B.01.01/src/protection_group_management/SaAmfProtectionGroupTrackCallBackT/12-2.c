/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, Xuelian <xuelian.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saAmfProtectionGroupTrackCallback
 * Description:   
 *   when a CSI have recently left the Protection group and 
 *   SA_TRACK_CHANGES_ONLY is set ,the notificationBuffer contains information
 *   about the current members of the Protection group and also about
 *   CSI that have recently left the Protection group,and the 
 *   value of the numberOfItems attribute in the notificationBuffer parameter
 *   should be greater than the value the numberOfMembers parameter.
 *   Return = NULL
 *   Same with 11-2
 * Line:        P190-14:P190-19
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "saAmf.h"
#include "saf_test.h"
#define CURRENT_DIR "/tmp/exec_test_log"

int main(int argc, char *argv[])
{
	char sameCase[100];
	char *p;
	FILE * fp;

	fp = fopen(CURRENT_DIR, "r");
	fscanf(fp, "%s", sameCase);
	fclose(fp);

	p=rindex(sameCase, '/');
	*(p+1)='\0';
	strcat(sameCase, "11-2.test");

	execl(sameCase, NULL);
	printf("        Can't find case %s\n", sameCase);
	return SAF_TEST_UNKNOWN;
}

