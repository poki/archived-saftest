/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, Xuelian <xuelian.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saAmfProtectionGroupTrack
 * Description:   
 *   Test whether this API saAmfInitialize can return a value of
 *   SA_AIS_ERR_BAD_HANDLE
 *   Return = SA_AIS_ERR_BAD_HANDLE
 *   Same with test case 1-3
 * Line:        P188-19:P188-20
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "saAmf.h"
#include "saf_test.h"
#define CURRENT_DIR "/tmp/exec_test_log"

int main(int argc, char *argv[])
{
	char sameCase[100];
	char *p;
	FILE * fp;

	fp = fopen(CURRENT_DIR, "r");
	fscanf(fp, "%s", sameCase);
	fclose(fp);

	p=rindex(sameCase, '/');
	*(p+1)='\0';
	strcat(sameCase, "1-3.test");

	execl(sameCase, NULL);
	printf("        Can't find case %s\n", sameCase);
	return SAF_TEST_UNKNOWN;
}

