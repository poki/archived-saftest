/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, Xuelian <xuelian.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saAmfProtectionGroupTrack
 * Description:   
 *   call saAmfProtectionGroupTrack repeatedly with callbacks for the same value
 *   of amfHandle.
 *   Return value should be SA_AIS_OK
 * Line:        P187-39:P187-41
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saAmf.h"
#include "saf_test.h"
#define CALL_TIMES 10
#define COMPONENT_1 "comp_1"
int ret = SAF_TEST_PASS;
SaNameT 	csiNameGloble;
static void amf_health_check_callback (SaInvocationT invocation,
        SaNameT		*compName,
        SaAmfHealthcheckKeyT *healthcheckKey)
{
        return;
}

static void amf_component_terminate_callback(SaInvocationT invocation,
        SaNameT		*compName)
{
	return;
}

static void amf_csi_set_callback (SaInvocationT invocation,
	SaNameT 	*compName,
	SaAmfHAStateT	haState,
        SaAmfCSIDescriptorT	csiDescriptor)
{
	csiNameGloble = csiDescriptor.csiName;
        return;
}

static void amf_csi_remove_callback(SaInvocationT invocation,
	SaNameT 	*compName,
	SaNameT 	*csiName,
        SaAmfCSIFlagsT	csiFlags)
{
	return;
}

static void amf_protectiongroup_track_callback(SaNameT	*csiName,
	SaAmfProtectionGroupNotificationBufferT	*notificationBuffer,
	SaUint32T	numberOfMembers,
	SaAisErrorT	error)
{
   	ret = SAF_TEST_PASS;
}

static void amf_proxiedcomponent_instantiate_callback(SaInvocationT invocation,
	SaNameT 	*proxiedCompName)
{
	return;
}

static void amf_proxiedcomponent_cleanup_callback(SaInvocationT invocation,
	SaNameT 	*proxiedCompName)
{
	return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saAmfProtectionGroupTrack";
	SaAisErrorT     expectedReturn = SA_AIS_OK;
        SaAmfHandleT    amfHandle;
	SaUint8T trackFlags = SA_TRACK_CURRENT;
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ONE;
	SaAmfCallbacksT amfCallback = {
               .saAmfHealthcheckCallback  = 
	       	(SaAmfHealthcheckCallbackT ) amf_health_check_callback,
                .saAmfComponentTerminateCallback = 
	(SaAmfComponentTerminateCallbackT) amf_component_terminate_callback,
		.saAmfCSISetCallback = 
		(SaAmfCSISetCallbackT)	amf_csi_set_callback,
		.saAmfCSIRemoveCallback = 
		(SaAmfCSIRemoveCallbackT) amf_csi_remove_callback,
		.saAmfProtectionGroupTrackCallback = 
	(SaAmfProtectionGroupTrackCallbackT) 
				amf_protectiongroup_track_callback,
		.saAmfProxiedComponentInstantiateCallback = 
		(SaAmfProxiedComponentInstantiateCallbackT)	
				amf_proxiedcomponent_instantiate_callback,
		.saAmfProxiedComponentCleanupCallback = 
			(SaAmfProxiedComponentCleanupCallbackT)
				amf_proxiedcomponent_cleanup_callback
        };
	SaAisErrorT	amfError;
        SaVersionT      amfVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	SaSelectionObjectT fd;
        fd_set rset;
        int select_ret;
	int count;	
	SaNameT compName = {
		.value = COMPONENT_1,
		.length = strlen(COMPONENT_1)
	};

	amfError = saAmfInitialize(&amfHandle, &amfCallback, &amfVersion);
        if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }

        amfError = saAmfComponentNameGet(amfHandle, &compName);
        if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("saAmfComponentNameGet, Return value: %s, should be"
                        " SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
                goto final_1;
        }
	amfError = saAmfComponentRegister(amfHandle, &compName, NULL);
        if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfComponentRegister, Return value: %s, should"
                        " be SA_AIS_OK\n",get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
                goto final_1;
        }
	
	amfError = saAmfSelectionObjectGet(amfHandle, &fd);
        if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saClmSelectionObjectGet, Return value: %s, should "
                        "be SA_AIS_OK\n", get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
                goto final_2;
        }

	// ACTION : AMF need take amf_csi_set_callback action to make a CSI
	FD_ZERO(&rset);
        FD_SET(fd, &rset);
        select_ret = select(fd + 1, &rset, NULL, NULL, NULL);
        if ((select_ret == -1) || (select_ret == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final_2;
        }
        amfError = saAmfDispatch(amfHandle, dispatchFlag);
        if (amfError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saAmfDispatch, Return value: %s, should be "
                        "SA_AIS_OK\n", get_error_string(amfError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	count = 0;
        while ((count < CALL_TIMES) && (ret != SAF_TEST_FAIL)) {

		amfError = saAmfProtectionGroupTrack(amfHandle, &csiNameGloble,
   			trackFlags, NULL);
		if (amfError != expectedReturn){
			printf("  Does not conform the expected behaviors!\n");
                	printf("  %s, Return value: %s, should be %s\n", 
				apiName,
				get_error_string(amfError),
				get_error_string(expectedReturn));
               		 ret = SAF_TEST_FAIL;
		}
		count++;
	}

	if(count == 0)
		goto final_2;

	saAmfProtectionGroupTrackStop(amfHandle, &csiNameGloble);	
final_2:
	saAmfComponentUnregister(amfHandle, &compName, NULL);
final_1:
	saAmfFinalize(amfHandle);
	return ret;
}
