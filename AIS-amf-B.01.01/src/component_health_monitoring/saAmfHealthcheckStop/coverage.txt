This file defines the coverage for the saAmfHealthcheckStop conformance testing.

Assertion	Status
1-1		Yes
1-2		Yes
1-3		Yes
2-1		Yes
2-2		Yes
2-3		Yes
3-1		Yes
3-2		Yes
3-3		Yes
4		Yes
5		Yes
6		No
7		No
8		No
9-1		Yes
9-2		Yes
10		Yes
11		No
12		No
13-1		Yes
13-2		Yes
