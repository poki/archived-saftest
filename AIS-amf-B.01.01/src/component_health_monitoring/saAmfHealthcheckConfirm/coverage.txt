This file defines the coverage for the saAmfHealthcheckConfirm conformance testing.

Assertion	Status
1-1		Yes
1-2		Yes
1-3		Yes
2-1		Yes
2-2		Yes
2-3		Yes
3-1		Yes
3-2		Yes
4-1		Yes
4-2		Yes
5		Yes
6		Yes
7		No
8		No
9		No
10-1		Yes
10-2		Yes
11		Yes
12		No
13		No
14-1		Yes
14-2		Yes
