#!/bin/sh
#
# Copyright (c) 2004, Intel Corporation.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307 USA.
#
# Authors:
#       Mi,Jun  <jun.mi@intel.com>
#	Yu,Ling <ling.l.yu@intel.com>
#

. ./AMF_START_STOP
WAIT_TIME=10

topdir=`pwd`
srcdir=$topdir/src
run_log=$topdir/log/run_log
error_log=$topdir/log/error_log

exec_test_log=/tmp/exec_test_log
tmp_log=/tmp/tmp_log

usage()
{
	cat << EOF
Usage: $0 [all|Path]
Run the tests for in current directory or specific directory.
All log information could be found at ./log directory.

	all		all test cases in current direcoty or subdirectory will
			be run.
	PATH		use this argument, all test cases in the PATH direcoty
			or its subdirectory will be run.

EOF
}

run_test()
{
    if [ -d log ]; then
	`rm -f $run_log`
	`rm -f $error_log`
    else
	`mkdir ./log`
    fi
    if [ -d $1 ]; then	
	for i in `find $1 -name *.test ! -path */manual/*`
	do
		file=$i
		chmod 777 $file
		echo $file >> $run_log
		echo "Now, We are running the case:" >> $error_log
		echo $file >> $error_log

		cat /dev/null >$tmp_log
		echo $i >$exec_test_log
		
		# Start AMF
		$AMF_START_CMD
		echo $?
		if [ $? != 0 ];then
			echo "Starting AMF failed!!!"
			exit 1
		fi
		echo "Starting AMF succeeded."
		# AMF will call ./exec_test
		sleep $WAIT_TIME
		# Stop AMF
		$AMF_STOP_CMD
		if [ $? != 0 ];then
			echo "Stopping AMF failed!!!"
			exit 1
		fi
		echo "Stopping AMF succeeded."

		out=$(cat $tmp_log)
		check_result $out
	done
    else
	echo "Could not find $1"
    fi
    `rm -f $tmp_log`
}

check_result()
{
	result=$1
	case "$result" in
		0)	echo "PASS" >> $run_log
			;;
		1)	echo "FAIL" >> $run_log
			cat $tmp_log >> $error_log 2>&1
			;;
		2)	echo "BLOCK" >> $run_log
                        echo "Timeout" >$tmp_log
			cat $tmp_log >> $error_log 2>&1
			;;
		3)	echo "NOTSUPPORT" >>$run_log
			cat $tmp_log >> $error_log 2>&1
			;;
		4)	echo "UNRESOLVED" >> $run_log
			cat $tmp_log >>$error_log 2>&1
			;; 
		139)	echo "UNKNOWN" >> $run_log 2>&1
                        echo "Segmentation fault" >$tmp_log
			cat $tmp_log >> $error_log 2>&1
			;;
                *)      echo "UNKNOWN" >> $run_log 2>&1
                        cat $tmp_log >> $error_log 2>&1
                        ;;

	esac
}

case "$1" in
	"")
		usage
		;;
	"--help")
		usage
		;;
	"all")
		run_test $srcdir
		;;
	*)
		run_test `pwd`/$1
		;;
esac
