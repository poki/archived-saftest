/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Yu, Ling <ling.l.yu@intel.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define EXEC_TEST_LOG "/tmp/exec_test_log"
#define TMP_LOG "/tmp/tmp_log"

int main (int argc, char * argv[])
{
	int ret, status;
	pid_t pid;
	
	//read test case binary path from EXEC_TEST_LOG
	char binary_path[1024];
	FILE *fp;
	fp=fopen(EXEC_TEST_LOG,"r");
	fscanf(fp,"%s",binary_path);
	fclose(fp);
	
	//make the test case default return code is BLOCK	
	fp=fopen(TMP_LOG,"w");
	fprintf(fp,"2\n");
	fclose(fp);	
	
	pid = fork();
	if ( pid < 0 ){
		printf("fork fails!");
		return 2;
	}
	if ( pid == 0 ){
		/* Execute the command */
		if (execvp(binary_path, NULL)== -1)
		{
			/* Application was not launched */
			printf("Unable to run test case %s\n", binary_path);
			return 2;
		}
	}
	wait(&status);
	if (WIFEXITED(status) == 0){
		printf("WIFEXITED is error\n");
		return 1;
	}else{
		ret = WEXITSTATUS(status);
	}
	
	//output the return code to TMP_LOG
	fp=fopen(TMP_LOG,"w");
	fprintf(fp,"%d\n",ret);
	fclose(fp);	
	
	return 1;
}
		
