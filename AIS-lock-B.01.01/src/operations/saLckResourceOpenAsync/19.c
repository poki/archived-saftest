/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, Xuelian <xuelian.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckResourceOpenAsync
 * Description:   
 *  Test if API saLckResourceOpenAsync can return SA_AIS_ERR_NOT_EXIST
 *  Return = SA_AIS_ERR_NOT_EXIST
 * Line:        P31-21:P31-22
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saLck.h"
#include "saf_test.h"

SaInvocationT invocationGlobal = 0;
SaAisErrorT global_lockError;
static void lck_resource_open_callback(SaInvocationT invocation,
	SaLckResourceHandleT lockResourceHandle,
	SaAisErrorT error)
{
	global_lockError = error;
	return;
}

static void lck_grant_callback(SaInvocationT invocation,
	SaLckLockStatusT lockStatus,
	SaAisErrorT error)
{
	return;
}

static void lck_waiter_callback(SaLckWaiterSignalT waiterSignal,
	SaLckLockIdT	lockId,
	SaLckLockModeT	modeHeld,
	SaLckLockModeT	modeRequested)
{
	return;
}

static void lck_resource_unlock_callback(SaInvocationT invocation,
	SaAisErrorT	error)
{
	return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saLckResourceLockAsync";
	SaAisErrorT     expectedReturn = SA_AIS_ERR_NOT_EXIST;
	SaLckHandleT   lckHandle;
        SaLckCallbacksT lckCallback = {
                .saLckResourceOpenCallback  = lck_resource_open_callback,
                .saLckLockGrantCallback = lck_grant_callback,
		.saLckLockWaiterCallback = lck_waiter_callback,
		.saLckResourceUnlockCallback = lck_resource_unlock_callback
        };
	SaAisErrorT	lckError;
        SaVersionT      lckVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

        int ret = SAF_TEST_PASS;
	
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	
	SaNameT	lockResourceName = {
		.value = "resource",
		.length = strlen("resource")
	};
	
	SaLckResourceHandleT lockResourceHandle;
		
        lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
        if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	lckError = saLckSelectionObjectGet(lckHandle, &selObject);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
	lckError = saLckResourceOpenAsync(lckHandle, 1, &lockResourceName,
					0x0);
	/*
	 * Taken care of in the library
	 */
	if (lckError == expectedReturn) {
		goto final_1;
	}

	/*
	 * Taken care of in the callback
	 */
	if (lckError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be SA_AIS_OK\n", 
			apiName,
			get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final_2;
        }
	
	lckError = saLckDispatch(lckHandle, dispatchFlag);
	if (lckError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saLckDispatch, Return value: %s,should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	if (global_lockError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
		printf("errorcode, Return value :%s,should be %s\n",
		get_error_string(global_lockError),
		get_error_string(expectedReturn));
		ret = SAF_TEST_FAIL;
	}
	
final_2 :
	saLckResourceClose(lockResourceHandle);
final_1 :
	saLckFinalize(lckHandle);
	
        return ret;
}




