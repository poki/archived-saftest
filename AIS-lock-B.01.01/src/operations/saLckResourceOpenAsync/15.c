/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckResourceOpenAsync
 * Description:
 *  Test if API saLckResourceOpenAsync can return SA_AIS_ERR_INIT,when 
 *  the previous initialization with saLckInitialize was incomplete, since
 *  the saLckResourceOpenCallback function is missing.
 *  Return = SA_AIS_ERR_INIT.
 * Line:        P31-11:P31-14
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "saLck.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saLckResourceOpenAsync";
	SaAisErrorT     expectedReturn = SA_AIS_ERR_INIT;
        SaLckHandleT   lckHandle;
        SaLckCallbacksT lckCallback = {
                .saLckResourceOpenCallback  = NULL,
                .saLckLockGrantCallback = NULL,
		.saLckLockWaiterCallback = NULL,
		.saLckResourceUnlockCallback = NULL
        };
	SaAisErrorT	lckError;
        SaVersionT      lckVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

        int ret = SAF_TEST_PASS;

	SaNameT	lockResourceName = {
		.value = "resource",
		.length = strlen("resource")
	};

        lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
        if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }

	lckError = saLckResourceOpenAsync(lckHandle, 1, &lockResourceName,
					SA_LCK_RESOURCE_CREATE);
	if (lckError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(lckError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
	}

	saLckFinalize(lckHandle);
	
        return ret;
}



