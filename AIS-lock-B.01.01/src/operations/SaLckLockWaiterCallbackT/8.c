/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    SaLckLockWaiterCallbackT
 * Description:   
 *  When a single process that holds multiple PR locks against a single lock
 *  resource and it requests a EX lock, check if each PR lock has this
 *  callback executed against each different lockId.
 *  Return = NULL
 * Line:        P43-18:P43-20
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saLck.h"
#include "saf_test.h"

SaLckWaiterSignalT waiterSignalGlobal = 0;
int callbackCount = 0;
SaLckLockIdT	lockIdGlobal[2];

static void lck_resource_open_callback(SaInvocationT invocation,
	SaLckResourceHandleT lockResourceHandle,
	SaAisErrorT error)
{
	return;
}

static void lck_grant_callback(SaInvocationT invocation,
	SaLckLockStatusT lockStatus,
	SaAisErrorT error)
{
	return;
}

static void lck_waiter_callback(SaLckWaiterSignalT waiterSignal,
	SaLckLockIdT	lockId,
	SaLckLockModeT	modeHeld,
	SaLckLockModeT	modeRequested)
{
	waiterSignalGlobal = waiterSignal;
	lockIdGlobal[callbackCount] = lockId;
	callbackCount ++;
	return;
}

static void lck_resource_unlock_callback(SaInvocationT invocation,
	SaAisErrorT	error)
{
	return;
}

int main(int argc, char *argv[])
{
        SaLckHandleT   lckHandle;
        SaLckCallbacksT lckCallback = {
                .saLckResourceOpenCallback  = lck_resource_open_callback,
                .saLckLockGrantCallback = lck_grant_callback,
		.saLckLockWaiterCallback = lck_waiter_callback,
		.saLckResourceUnlockCallback = lck_resource_unlock_callback
        };
	SaAisErrorT	lckError;
        SaVersionT      lckVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

        int ret = SAF_TEST_PASS;
	SaLckLockIdT	lockId[3];
	SaLckLockStatusT	lockStatus;
	
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	
	SaNameT	lockResourceName = {
		.value = "resource",
		.length = strlen("resource")
	};
	
	SaLckResourceHandleT lockResourceHandle;
		
        lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
        if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	lckError = saLckSelectionObjectGet(lckHandle, &selObject);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
	lckError = saLckResourceOpen(lckHandle,&lockResourceName,
				SA_LCK_RESOURCE_CREATE,
				SA_TIME_MAX,
				&lockResourceHandle);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceOpen, Return value: %s, "
			"should be SA_AIS_OK\n", get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
	lckError = saLckResourceLock(lockResourceHandle,
					lockId,
					SA_LCK_PR_LOCK_MODE,
					SA_LCK_LOCK_NO_QUEUE,
					1,
					SA_TIME_MAX,
					&lockStatus);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  First saLckResourceLock, Return value: %s, "
			"should be SA_AIS_OK\n", get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	
	if (lockStatus != SA_LCK_LOCK_GRANTED){
		printf("  the lock is not obtained");
		printf("  lockStatus should be SA_LCK_LOCK_GRANTED\n");
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	lckError = saLckResourceLock(lockResourceHandle,
					lockId + 1,
					SA_LCK_PR_LOCK_MODE,
					SA_LCK_LOCK_NO_QUEUE,
					1,
					SA_TIME_MAX,
					&lockStatus);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  Second saLckResourceLock, Return value: %s, "
			"should be SA_AIS_OK\n", get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
        }
	
	if (lockStatus != SA_LCK_LOCK_GRANTED){
		printf("  the lock is not obtained");
		printf("  lockStatus should be SA_LCK_LOCK_GRANTED\n");
		ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}
	
	lckError = saLckResourceLock(lockResourceHandle,
					lockId + 2,
					SA_LCK_EX_LOCK_MODE,
					0,
					2,
					SA_TIME_ONE_SECOND,
					&lockStatus);
	if (lckError != SA_AIS_ERR_TIMEOUT){
                printf("  Does not conform the expected behaviors!\n");
                printf("  Third saLckResourceLock, Return value: %s, "
			"should be SA_AIS_ERR_TIMEOUT\n",
		       	get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
	do{
        	selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        	if (selectRet == 0 || selectRet == -1){
                	printf("  select error!\n");
                	ret = SAF_TEST_UNRESOLVED;
			goto final_4;
        	}
	
		lckError = saLckDispatch(lckHandle, dispatchFlag);
		if (lckError != SA_AIS_OK){
                	printf("  Does not conform the expected behaviors!\n");
                	printf("  saLckDispatch, Return value: %s, "
				"should be SA_AIS_OK\n",
				 get_error_string(lckError));
                	ret = SAF_TEST_UNRESOLVED;
			goto final_4;
        	}
	}while(callbackCount != 2);

	if (waiterSignalGlobal != 2){
		printf("waiterSignal is not correct ,Return:%llu"
		"should be 2\n",waiterSignalGlobal);
		ret = SAF_TEST_FAIL;
		goto final_4;
	}
	
	if(lockIdGlobal[0] != lockId[0] && lockIdGlobal[0] != lockId[1]){
		printf(" lockIds from callback do not match the ones from"
		" lock function \n");
		ret = SAF_TEST_FAIL;
		goto final_4;
	}
	
	if(lockIdGlobal[1] != lockId[0] && lockIdGlobal[1] != lockId[1]){
		printf(" lockIds from callback do not match the ones from"
		" lock function \n");
		ret = SAF_TEST_FAIL;
	}
	
final_4 :
	saLckResourceUnlock(lockId[1], SA_TIME_MAX);	
final_3 :
	saLckResourceUnlock(lockId[0], SA_TIME_MAX);
	
final_2 :
	saLckResourceClose(lockResourceHandle);

final_1 :
	saLckFinalize(lckHandle);
	
        return ret;
}



