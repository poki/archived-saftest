This file defines the coverage for the saLckResourceOpen conformance
testing.

Assertion       Status
1-1             YES
1-2		YES
2-1             YES
2-2		YES
3-1             YES
3-2		YES
4               YES
5-1             YES
5-2		YES 
6               YES
7		NO
8		YES
9		YES
10		YES
11		YES
12		NO
13		NO
14		NO
15-1		YES
15-2		YES
16		YES
17		NO
18		NO
19		YES
20		YES



