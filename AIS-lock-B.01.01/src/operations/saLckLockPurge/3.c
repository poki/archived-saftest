/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckLockPurge
 * Description:   
 *  Call the api when other processes hold the lock in shared mode, test if
 *  they continue to do 
 *  Return = SA_AIS_OK
 * Line:        P49-11:P49-15
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include "saLck.h"
#include "saf_test.h"

int sigFlag = 0;

void changeFlag(int sig)
{
	sigFlag ++;
}

static void lck_resource_open_callback(SaInvocationT invocation,
	SaLckResourceHandleT lockResourceHandle,
	SaAisErrorT error)
{
	return;
}

static void lck_grant_callback(SaInvocationT invocation,
	SaLckLockStatusT lockStatus,
	SaAisErrorT error)
{
	return;
}

static void lck_waiter_callback(SaLckWaiterSignalT waiterSignal,
	SaLckLockIdT	lockId,
	SaLckLockModeT	modeHeld,
	SaLckLockModeT	modeRequested)
{
	return;
}

static void lck_resource_unlock_callback(SaInvocationT invocation,
	SaAisErrorT	error)
{
	return;
}

int main(int argc, char *argv[])
{
        SaLckHandleT   lckHandle;
        SaLckCallbacksT lckCallback = {
                .saLckResourceOpenCallback  = lck_resource_open_callback,
                .saLckLockGrantCallback = lck_grant_callback,
		.saLckLockWaiterCallback = lck_waiter_callback,
		.saLckResourceUnlockCallback = lck_resource_unlock_callback
        };
	SaAisErrorT	lckError;
        SaVersionT      lckVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	int status;
	pid_t pid;
        int ret = SAF_TEST_PASS;
	SaLckLockIdT	lockId;
	SaLckLockStatusT	lockStatus;
	
	SaNameT	lockResourceName = {
		.value = "resource",
		.length = strlen("resource")
	};
	
	SaLckResourceHandleT lockResourceHandle;
	SaLckOptionsT lckOptions = 0;
	
        lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
        if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	lckError = saLckOptionCheck(lckHandle, &lckOptions);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckOptionCheck, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
	if (!(lckOptions & SA_LCK_OPT_ORPHAN_LOCKS)){
		printf("The Lock Service doesn't support orphan locks");
		goto final_1;
	}
	
	signal(SIGUSR1,changeFlag);
	pid = fork();
	if (pid < 0){
		printf(" fork is error\n");
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	if (pid == 0)
		if (execl("./3-fork.test", NULL) == -1){
			printf("  execl error\n");
			ret = SAF_TEST_UNRESOLVED;
			return ret;
		}	
	
	// wait until the child process is ready
	while (sigFlag == 0)
		pause();

	lckError = saLckResourceOpen(lckHandle, &lockResourceName,
				SA_LCK_RESOURCE_CREATE,
				SA_TIME_MAX,
				&lockResourceHandle);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceOpen, Return value: %s, "
			"should be SA_AIS_OK\n", get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
	lckError = saLckResourceLock(lockResourceHandle,
					&lockId,
					SA_LCK_PR_LOCK_MODE,
					SA_LCK_LOCK_ORPHAN,
					1,
					SA_TIME_MAX,
					&lockStatus);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceLock, Return value: %s, "
			"should be SA_AIS_OK\n", get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
        }
	
	if (lockStatus != SA_LCK_LOCK_GRANTED){
		printf("  the lock is not obtained");
		printf("  lockStatus should be SA_LCK_LOCK_GRANTED\n");
		ret = SAF_TEST_UNRESOLVED;
	}
	
	lckError = saLckResourceClose(lockResourceHandle);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceClose, Return value: %s, "
			"should be SA_AIS_OK\n", get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
        }
	
	if (ret == SAF_TEST_UNRESOLVED)
		goto final_1;

	lckError = saLckResourceOpen(lckHandle, &lockResourceName,
				SA_LCK_RESOURCE_CREATE,
				SA_TIME_MAX,
				&lockResourceHandle);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceOpen, Return value: %s, "
			"should be SA_AIS_OK\n", get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
	lckError = saLckLockPurge(lockResourceHandle);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckLockPurge, Return value: %s, "
			"should be SA_AIS_OK\n", get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	
	lckError = saLckResourceLock(lockResourceHandle,
					&lockId,
					SA_LCK_EX_LOCK_MODE,
					SA_LCK_LOCK_NO_QUEUE,
					1,
					SA_TIME_MAX,
					&lockStatus);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceLock, Return value: %s, "
			"should be SA_AIS_OK\n", get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	
	if (lockStatus != SA_LCK_LOCK_NOT_QUEUED){
		printf(" purging  the lock is fail\n");
		printf(" after purging ,lock again,the result should be "
		"SA_LCK_LOCK_NOT_QUEUED\n");
		ret = SAF_TEST_FAIL;
	}
	
	kill(pid,SIGUSR1);
	wait(&status);
	if (WIFEXITED(status) == 0){
		printf("WIFEXITED error!\n");
		ret = SAF_TEST_UNRESOLVED;
	}
	else
		if(ret == SAF_TEST_PASS)
			ret = WEXITSTATUS(status);
	
final_2:
	saLckResourceClose(lockResourceHandle);	
	kill(pid,SIGUSR1);
final_1 :
	saLckFinalize(lckHandle);
        return ret;
}


