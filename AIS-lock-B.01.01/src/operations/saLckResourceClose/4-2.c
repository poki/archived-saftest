/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckResourceClose
 * Description:
 *  Call the api to test if other processes' locks are unaffected,and
 *  any pending requests from other processes remain and may be processed
 *  by the lock service when they hold locks in EX mode.
 *  Test steps:
 *    1. open a resource to lock.
 *    2. lock the resource in EX mode with asynclock
 *    3. fork a child process, in the child process, close the resource,
 *     and then send a signal SIGUSR1 to father process, and
 *     then pause.
 *    4. father process dispatch the pending lock to test if the lock is 
 *     still valid.
 *    5. unlock the same resource, child process go on and return.
 *    6. father wait for child process' terminal.
 *  Return = SA_AIS_OK 
 *  4-2-fork.c is the child process
 * Line:        P34-8:P34-10
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <sys/wait.h>
#include "saLck.h"
#include "saf_test.h"

int sigFlag = 0;
SaLckLockStatusT lockStatusGlobal;
void changeFlag(int sig)
{
	sigFlag ++;
}
static void lck_grant_callback(SaInvocationT invocation,
	SaLckLockStatusT lockStatus,
	SaAisErrorT error)
{
	lockStatusGlobal = lockStatus;
	return;
}
int main(int argc, char *argv[])
{
        SaLckHandleT   lckHandle;
        SaLckCallbacksT lckCallback = {
                .saLckResourceOpenCallback  = NULL,
                .saLckLockGrantCallback = lck_grant_callback,
		.saLckLockWaiterCallback = NULL,
		.saLckResourceUnlockCallback = NULL
        };
	SaAisErrorT	lckError;
        SaVersionT      lckVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

        int ret = SAF_TEST_PASS;
	
	pid_t pid;
	int status;
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;

	SaNameT	lockResourceName = {
		.value = "resource",
		.length = strlen("resource")
	};
	
	SaLckLockIdT	lockId;
	SaLckResourceHandleT lockResourceHandle;

        lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
        if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }

	lckError = saLckResourceOpen(lckHandle,&lockResourceName,
				SA_LCK_RESOURCE_CREATE,
				SA_TIME_MAX,
				&lockResourceHandle);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceOpen, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	lckError = saLckSelectionObjectGet(lckHandle, &selObject);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	lckError = saLckResourceLockAsync(lockResourceHandle, 1,
					&lockId,
					SA_LCK_EX_LOCK_MODE,
					SA_LCK_LOCK_NO_QUEUE,
					1);
	if (lckError != SA_AIS_OK){
                	printf("  Does not conform the expected behaviors!\n");
                	printf("  saLckResourceLockAsync, Return value: %s, "
			"should be SA_AIS_OK\n",get_error_string(lckError));
                	ret = SAF_TEST_UNRESOLVED;
			goto final_1;
        }
	signal(SIGUSR1,changeFlag);
	
	pid = fork();
	if (pid < 0){
		printf(" fork is error\n");
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	if (pid == 0){
		if (execl("./4-2-fork.test", NULL) == -1){
			ret = SAF_TEST_UNRESOLVED;
			return ret;
		}
	}

	while(sigFlag == 0)
		pause();

	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if (selectRet == 0 || selectRet == -1){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	
	lckError = saLckDispatch(lckHandle, dispatchFlag);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckDispatch, Return value: %s, "
			"should be SA_AIS_OK\n", get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }

	if (lockStatusGlobal != SA_LCK_LOCK_GRANTED){
		printf("  the lock is not obtained");
		printf("  lockStatus should be SA_LCK_LOCK_GRANTED\n");
		ret = SAF_TEST_FAIL;
		goto final_2;
	}
	saLckResourceUnlock(lockId, SA_TIME_MAX);
	kill(pid,SIGUSR1);

	wait(&status);
	if (WIFEXITED(status) == 0){
		printf("  WIFEXITED is error!\n");
		ret = SAF_TEST_UNRESOLVED;
	}
	else
		if(ret == SAF_TEST_PASS)
			ret = WEXITSTATUS(status);

final_2 :
	saLckResourceClose(lockResourceHandle);
final_1 :
	saLckFinalize(lckHandle);
        return ret;
}



