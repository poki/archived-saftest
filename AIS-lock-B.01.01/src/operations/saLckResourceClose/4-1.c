/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckResourceClose
 * Description:
 *  Call the api to test if other processes' locks are unaffected,and
 *  any pending requests from other processes remain and may be processed
 *  by the lock service when they hold locks in PR mode.
 *  Test steps:
 *    1. open a resource to lock.
 *    2. lock the resource in PR mode
 *    3. fork a child process, in the child process, close the resource,
 *     and then send a signal SIGUSR1 to father process,and
 *     then pause.
 *    4. father process unlock the same resource and test if the lock is s
 *	still valid, then send a signal to child process.
 *    5. child process go on and return.
 *    6. father wait for child process' terminal.
 *  Return = SA_AIS_OK 
 *  4-1-fork.c is the child process
 * Line:        P34-8:P34-10
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <sys/wait.h>
#include "saLck.h"
#include "saf_test.h"

int sigFlag = 0;
void changeFlag(int sig)
{
	sigFlag ++;
}

int main(int argc, char *argv[])
{
        SaLckHandleT   lckHandle;
        SaLckCallbacksT lckCallback = {
                .saLckResourceOpenCallback  = NULL,
                .saLckLockGrantCallback = NULL,
		.saLckLockWaiterCallback = NULL,
		.saLckResourceUnlockCallback = NULL
        };
	SaAisErrorT	lckError;
        SaVersionT      lckVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

        int ret = SAF_TEST_PASS;
	
	pid_t pid;
	int status;
	
	SaNameT	lockResourceName = {
		.value = "resource",
		.length = strlen("resource")
	};
	
	SaLckLockIdT	lockId;
	SaLckLockStatusT	lockStatus;
	SaLckResourceHandleT lockResourceHandle;
		
        lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
        if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	lckError = saLckResourceOpen(lckHandle,&lockResourceName,
				SA_LCK_RESOURCE_CREATE,
				SA_TIME_MAX,
				&lockResourceHandle);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceOpen, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
		
	lckError = saLckResourceLock(lockResourceHandle,
			&lockId,
			SA_LCK_PR_LOCK_MODE,
			SA_LCK_LOCK_NO_QUEUE,
			1,
			SA_TIME_MAX,
			&lockStatus);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceLock, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }

	if (lockStatus != SA_LCK_LOCK_GRANTED){
		printf(" lockStatus is wrong, should be SA_LCK_LOCK_GRANTED\n");
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}

	pid = fork();
	if (pid < 0){
		printf(" fork is error\n");
		ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	if (pid == 0){
		if (execl("./4-1-fork.test", NULL) == -1){
			ret = SAF_TEST_UNRESOLVED;
			return ret;
		}
	}
	while(sigFlag == 0)
		pause();

	lckError = saLckResourceUnlock(lockId, SA_TIME_MAX);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceUnlock, Return value: %s, should"
		" be SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_FAIL;
		goto final_1;
        }

	kill(pid, SIGUSR1);
	wait(&status);
	if (WIFEXITED(status) == 0){
		printf("WIFEXITED error!\n");
		ret = SAF_TEST_UNRESOLVED;
	}
	else
		if(ret == SAF_TEST_PASS)
			ret = WEXITSTATUS(status);

final_2 :
	saLckResourceClose(lockResourceHandle);
final_1 :
	saLckFinalize(lckHandle);
        return ret;
}

