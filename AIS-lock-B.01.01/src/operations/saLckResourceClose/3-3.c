/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, Xuelian <xuelian.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckResourceClose
 * Description:   
 *  Call the api and test if the lock is dropped, when the process
 *  has a PR mode lock against the lock resource.
 *  Return = SA_AIS_OK
 * Line:        P34-5:P34-7
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <sys/wait.h>
#include "saLck.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saLckResourceClose";
        SaLckHandleT   lckHandle;
        SaLckCallbacksT lckCallback = {
                .saLckResourceOpenCallback  = NULL,
                .saLckLockGrantCallback = NULL,
		.saLckLockWaiterCallback = NULL,
		.saLckResourceUnlockCallback = NULL
        };
	SaAisErrorT	lckError;
        SaVersionT      lckVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

        int ret = SAF_TEST_PASS;
	SaLckLockIdT	lockId[2];
	SaLckLockStatusT	lockStatus;
	
	SaNameT	lockResourceName = {
		.value = "resource",
		.length = strlen("resource")
	};
	
	SaLckResourceHandleT lockResourceHandle;
	
        lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
        if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }

	lckError = saLckResourceOpen(lckHandle,&lockResourceName,
				SA_LCK_RESOURCE_CREATE,
				SA_TIME_MAX,
				&lockResourceHandle);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceOpen, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
	lckError = saLckResourceLock(lockResourceHandle,
			&lockId[0],
			SA_LCK_PR_LOCK_MODE,
			SA_LCK_LOCK_NO_QUEUE,
			1,
			SA_TIME_MAX,
			&lockStatus);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceLock, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		saLckResourceClose(lockResourceHandle);
		goto final_1;
        }
	if (lockStatus != SA_LCK_LOCK_GRANTED){
		printf(" lockStatus is wrong, should be SA_LCK_LOCK_GRANTED\n");
		ret = SAF_TEST_UNRESOLVED;
		saLckResourceClose(lockResourceHandle);
		goto final_1;
	}

	lckError = saLckResourceClose(lockResourceHandle);
	if (lckError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be SA_AIS_OK\n",
			apiName,
			get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	lckError = saLckResourceOpen(lckHandle,&lockResourceName,
				SA_LCK_RESOURCE_CREATE,
				SA_TIME_MAX,
				&lockResourceHandle);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceOpen, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
	lckError = saLckResourceLock(lockResourceHandle,
			&lockId[1],
			SA_LCK_EX_LOCK_MODE,
			SA_LCK_LOCK_NO_QUEUE,
			1,
			SA_TIME_MAX,
			&lockStatus);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceLock, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_FAIL;
		goto final_3;
        }

	if (lockStatus != SA_LCK_LOCK_GRANTED){
		printf(" lockStatus is wrong, should be SA_LCK_LOCK_GRANTED\n");
		ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}

	saLckResourceUnlock(lockId[1], SA_TIME_MAX);
final_3:
	saLckResourceClose(lockResourceHandle);
final_2:
	saLckResourceUnlock(lockId[0], SA_TIME_MAX);
final_1:
	saLckFinalize(lckHandle);

        return ret;
}
