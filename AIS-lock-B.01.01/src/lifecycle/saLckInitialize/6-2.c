/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckInitialize
 * Description:   
 *   Initialize the lock service with a version code, whose release
 *   code is bigger than current one, and could not be supported now.
 *   Return = SA_AIS_ERR_VERSION
 * Line:        P22-24:P23-20
 */

#include <stdio.h>
#include <stdlib.h>
#include "saLck.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saLckInitialize";
	SaAisErrorT     expectedReturn = SA_AIS_ERR_VERSION;
        SaLckHandleT   lckHandle;
        SaLckCallbacksT lckCallback = {
                .saLckResourceOpenCallback  = NULL,
                .saLckLockGrantCallback = NULL,
		.saLckLockWaiterCallback = NULL,
		.saLckResourceUnlockCallback = NULL
        };
	SaAisErrorT	lckError;
        SaVersionT      lckVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = 'C'
        };

        int ret = SAF_TEST_PASS;

        lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
        if (lckError != expectedReturn){
                printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(lckError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_UNRESOLVED;
        }
	else {
		if (lckVersion.releaseCode != AIS_B_RELEASE_CODE)
		{
			printf("  Does not conform the expected behaviors!\n");
                	printf("  %s's release code: %c, should be %c\n", 
				apiName,
				lckVersion.releaseCode,
				AIS_B_RELEASE_CODE);
                	ret = SAF_TEST_FAIL;
			goto final;
		}
		
		if (lckVersion.majorVersion != AIS_B_VERSION_MAJOR){
                        printf("  Does not conform the expected behaviors!\n");
                        printf("  %s's major version code:%d,should be %d\n",
                                apiName,
                                lckVersion.majorVersion,
                                AIS_B_VERSION_MAJOR);
                        ret = SAF_TEST_UNRESOLVED;
                        goto final;
                }
		
		 if (lckVersion.minorVersion != AIS_B_VERSION_MINOR){
                        printf("  Does not conform the expected behaviors!\n");
                        printf("  %s's minor version code:%d,should be %d\n",
                                apiName,
                                lckVersion.minorVersion,
                                AIS_B_VERSION_MINOR);
                        ret = SAF_TEST_UNRESOLVED;
                }
final :
		saLckFinalize(lckHandle);
	}
        return ret;
}




