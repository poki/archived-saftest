/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckInitialize
 * Description:   
 *   Initialize the lock service with a version code. Suppose the 
 *   returned major version number is  equal to the required
 *   majorversion number
 *   Return = SA_AIS_OK
 * Line:        P22-13:P22-23
 */

#include <stdio.h>
#include <stdlib.h>
#include "saLck.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saLckInitialize";
	SaAisErrorT     expectedReturn = SA_AIS_OK;
        SaLckHandleT   lckHandle;
        SaLckCallbacksT lckCallback = {
                .saLckResourceOpenCallback  = NULL,
                .saLckLockGrantCallback = NULL,
		.saLckLockWaiterCallback = NULL,
		.saLckResourceUnlockCallback = NULL
        };
	SaAisErrorT	lckError;
	SaUint8T 	majorVersion = AIS_B_VERSION_MAJOR;
	SaUint8T	minorVersion = AIS_B_VERSION_MINOR;
	
        SaVersionT      lckVersion = {
                .majorVersion = majorVersion,
                .minorVersion = minorVersion,
                .releaseCode = AIS_B_RELEASE_CODE
        };

        int ret = SAF_TEST_PASS;
	
        lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
        if (lckError != expectedReturn){
                printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(lckError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_UNRESOLVED;
        }
	else {
		if (lckVersion.majorVersion != majorVersion){
                        printf("  Does not conform the expected behaviors!\n");
                        printf("  %s's major version code:%d,should be %d\n",
                                apiName,
                                lckVersion.majorVersion,
                                majorVersion);
                        ret = SAF_TEST_FAIL;
			goto final;
                }

                if (lckVersion.releaseCode != AIS_B_RELEASE_CODE){
                        printf("  Does not conform the expected behaviors!\n");
                        printf("  %s's release code:%d,should be %d\n",
                                apiName,
                                lckVersion.releaseCode,
                                AIS_B_RELEASE_CODE);
                        ret = SAF_TEST_UNRESOLVED;
			goto final;
                }

                if (lckVersion.minorVersion != AIS_B_VERSION_MINOR){
                        printf("  Does not conform the expected behaviors!\n");
                        printf("  %s's minor version code:%d,should be %d\n",
                                apiName,
                                lckVersion.minorVersion,
				AIS_B_VERSION_MINOR);
			 ret = SAF_TEST_UNRESOLVED;
		}
final :
		saLckFinalize(lckHandle);
	}
	return ret;
}



