/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, AnLi <an.li.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckOptionCheck
 * Description:   
 *  Test saLckOptionCheck will not get lckOptions with invalid parameter.
 *  Return = SA_AIS_ERR_BAD_HANDLE
 *  Same with 1-1.
 * Line:        P25-32:P25-35
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "saLck.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char same_case[]="./1-1.test";

	execl(same_case, NULL);
       	printf("        Can't find case %s\n", same_case);
	return SAF_TEST_UNKNOWN; 
}
