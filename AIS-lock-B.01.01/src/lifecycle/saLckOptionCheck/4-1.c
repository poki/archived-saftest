/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, AnLi <an.li.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckOptionCheck
 * Description:   
 *  Test if application attempts to use SA_LCK_LOCK_ORPHAN, which is not 
 *  supported, it will return error.
 *  Steps:
 *  1. get lckOptions by this api
 *  2. if lckOptions is SA_LCK_OPT_ORPHAN_LOCKS, return pass
 *  3. if lckOptions is not SA_LCK_OPT_ORPHAN_LOCKS, use
 *     SA_LCK_LOCK_ORPHAN as lockFlag to lock the resource.
 *  4. If the lock action of step 3 succeeds, case fails. Otherwise, case
 *     passes.
 *  Return = SA_AIS_OK 
 * Line:        P25-41:P26-3
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saLck.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	SaLckHandleT   lckHandle;
	SaLckCallbacksT lckCallback = {
		.saLckResourceOpenCallback  = NULL,
		.saLckLockGrantCallback = NULL,
		.saLckLockWaiterCallback = NULL,
		.saLckResourceUnlockCallback = NULL
	};
	SaLckOptionsT lckOptions;
	
	SaAisErrorT	lckError;
	SaVersionT  lckVersion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE
	};

	SaLckLockIdT	lockId;
	SaLckLockStatusT	lockStatus;
	
	SaNameT	lockResourceName = {
		.value = "resource",
		.length = strlen("resource")
	};
	
	SaLckResourceHandleT lockResourceHandle;

	int ret = SAF_TEST_PASS;
	
	lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
	if (lckError != SA_AIS_OK ){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saLckInitialize, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(lckError));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	lckError = saLckOptionCheck(lckHandle, &lckOptions);
	if (lckError != SA_AIS_OK ) {
		printf(" Does not conform the expected behaviors!\n");
		printf(" saLckOptionCheck, Return value: %s, should be"
			 " SA_AIS_OK\n", get_error_string(lckError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	if (!(lckOptions & SA_LCK_OPT_ORPHAN_LOCKS)){
		lckError = saLckResourceOpen(lckHandle, &lockResourceName,
					SA_LCK_RESOURCE_CREATE,
					SA_TIME_MAX,
					&lockResourceHandle);
		if (lckError != SA_AIS_OK){
       		         printf("  Does not conform the expected behaviors!\n");
       		         printf("  saLckResourceOpen, Return value: %s, "
				"should be SA_AIS_OK\n",
				get_error_string(lckError));
     		         ret = SAF_TEST_UNRESOLVED;
		   	goto final_1;
       		 }
	
		lckError = saLckResourceLock(lockResourceHandle,
					&lockId,
					SA_LCK_PR_LOCK_MODE,
					SA_LCK_LOCK_ORPHAN,
					1,
					SA_TIME_MAX,
					&lockStatus);
		if (lckError != SA_AIS_ERR_NOT_SUPPORTED){
			printf("  Does not conform the expected behaviors!\n");
			printf("  saLckResourceLock, Return value: %s, "
				" should be SA_AIS_ERR_NOT_SUPPORTED\n",
				get_error_string(lckError));
 			ret = SAF_TEST_FAIL;
			
		}
		saLckResourceUnlock(lockId,SA_TIME_MAX);
		saLckResourceClose(lockResourceHandle);
	}
	
final_1:
	saLckFinalize(lckHandle);
	return ret;
}
