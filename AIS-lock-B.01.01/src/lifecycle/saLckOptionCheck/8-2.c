/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, AnLi <an.li.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckOptionCheck
 * Description:   
 *  Test saLckOptionsCheck returns SA_AIS_ERR_BAD_HANDLE when lckHandle 
 *  is finalized.
 *  Return = SA_AIS_ERR_BAD_HANDLE
 * Line:        P26-15:P26-16
 */

#include <stdio.h>
#include <stdlib.h>
#include "saLck.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	SaLckHandleT   lckHandle;
	SaLckCallbacksT lckCallback = {
		.saLckResourceOpenCallback  = NULL,
		.saLckLockGrantCallback = NULL,
		.saLckLockWaiterCallback = NULL,
		.saLckResourceUnlockCallback = NULL
	};
	SaLckOptionsT lckOptions;
	
	SaAisErrorT	lckError;
	SaVersionT  lckVersion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE
	};
	int ret = SAF_TEST_PASS;
	
	lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
	if (lckError != SA_AIS_OK ){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saLckInitialize, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(lckError));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	lckError = saLckFinalize(lckHandle);
	if (lckError != SA_AIS_OK ){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saLckFinalize, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(lckError));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	lckError = saLckOptionCheck(lckHandle, &lckOptions);
	if (lckError != SA_AIS_ERR_BAD_HANDLE ) {
		printf(" Does not conform the expected behaviors!\n");
		printf(" saLckOptionCheck, Return value: %s, should be"
			 " SA_AIS_ERR_BAD_HANDLE\n", 
			get_error_string(lckError));
		ret = SAF_TEST_FAIL;
	}
	
	return ret;
}

