/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, AnLi <an.li.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckOptionCheck
 * Description:   
 *  Test saLckOptionCheck without initialized lckHandle.
 *  Return = SA_AIS_ERR_BAD_HANDLE
 * Line:        P25-29:P25-31
 */

#include <stdio.h>
#include <stdlib.h>
#include "saLck.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	SaLckHandleT   lckHandle;
	SaLckOptionsT lckOptions;
	
	SaAisErrorT	lckError;

	int ret = SAF_TEST_PASS;

	lckError = saLckOptionCheck(lckHandle, &lckOptions);
	if (lckError != SA_AIS_ERR_BAD_HANDLE) {
		printf(" Does not conform the expected behaviors!\n");
		printf(" saLckOptionCheck, Return value: %s, should be"
			 " SA_AIS_ERR_BAD_HANDLE\n",
			get_error_string(lckError));
		ret = SAF_TEST_FAIL;
	}
	return ret;
}


