/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckDispatch
 * Description:
 *   Dispatch correctly, Set dispatch Flag to SA_DISPATCH_BLOCKING. The 
 *   function completes successfully.
 *   Return = SA_AIS_OK
 * Line:        P26-38:P26-42
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include "saLck.h"
#include "saf_test.h"

int callbackFlag = 0;
SaAisErrorT dispatchReturn;
sem_t sem_start_dispatch;
sem_t sem_end_dispatch;

static void lck_resource_open_callback(SaInvocationT invocation,
	SaLckResourceHandleT lockResourceHandle,
	SaAisErrorT error)
{
	callbackFlag ++;
	saLckResourceClose(lockResourceHandle);
	return;
}

static void lck_grant_callback(SaInvocationT invocation,
	SaLckLockStatusT lockStatus,
	SaAisErrorT error)
{
	return;
}

static void lck_waiter_callback(SaLckWaiterSignalT waiterSignal,
	SaLckLockIdT	lockId,
	SaLckLockModeT	modeHeld,
	SaLckLockModeT	modeRequested)
{
	return;
}

static void lck_resource_unlock_callback(SaInvocationT invocation,
	SaAisErrorT	error)
{
	return;
}

static void *dispatch_thread(void *arg)
{
	callbackFlag = 0;
	sem_post(&sem_start_dispatch);
	dispatchReturn = saLckDispatch(* ((SaLckHandleT *) arg),
					SA_DISPATCH_BLOCKING);
	sem_post(&sem_end_dispatch);
	return NULL;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saLckDispatch";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaLckHandleT   lckHandle;
        SaLckCallbacksT lckCallback = {
                .saLckResourceOpenCallback  = lck_resource_open_callback,
                .saLckLockGrantCallback = lck_grant_callback,
		.saLckLockWaiterCallback = lck_waiter_callback,
		.saLckResourceUnlockCallback = lck_resource_unlock_callback
        };
	SaAisErrorT	lckError;
        SaVersionT      lckVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

        int ret = SAF_TEST_PASS;
	SaSelectionObjectT 	selObject;
	
	fd_set readSet;
	int selectRet;
	struct timeval timeout	= {
			.tv_sec = 9,
			.tv_usec = 0
	};

	SaNameT	lockResourceName = {
		.value = "resource",
		.length = strlen("resource")
	};

	pthread_t thread_id;
	int pthreadError;

        lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
         if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }

	sem_init(&sem_start_dispatch, 0, 0);
	sem_init(&sem_end_dispatch, 0, 0);
	pthreadError = pthread_create(&thread_id,
					NULL,
					dispatch_thread,
					(void*)&lckHandle);
	if (pthreadError){
		printf("	invoke \"pthread_create\" has error,"
				" return %s\n", strerror(pthreadError));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	lckError = saLckSelectionObjectGet(lckHandle, &selObject);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
        }
	
	lckError = saLckResourceOpenAsync(lckHandle,
					1,
					&lockResourceName,
					SA_LCK_RESOURCE_CREATE);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceOpenAsync, Return value: %s, should "
			" be SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
        }
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, &timeout);
        if (selectRet == 0 || selectRet == -1){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
        }

final :
	if (ret != SAF_TEST_UNRESOLVED)
		sem_wait(&sem_start_dispatch);

	lckError = saLckFinalize(lckHandle);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckChannelUnlink, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
        }

	if (ret != SAF_TEST_PASS)
		return ret;

	sem_wait(&sem_end_dispatch);

	if ( callbackFlag <= 0){
		printf("	invoke %s has error, pending callbacks are "
				"not invoked\n", apiName);
		return SAF_TEST_FAIL;
	}
	
	if ( dispatchReturn != expectedReturn ){
		printf(" Does not conform the expected behaviors!\n");
                printf(" %s, Return value :%s, should be %s\n",
			apiName,
			get_error_string(dispatchReturn),
			get_error_string(expectedReturn));
		return SAF_TEST_FAIL;
	}

	return ret;
}


