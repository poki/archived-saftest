/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, Qingfeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckSelectionObjectGet
 * Description:   
 *   Get the selectionObject without initialize
 *   Return = SA_AIS_ERR_BAD_HANDLE
 * Line:        P24-15:P24-20
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saLck.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saLckSelectionObjectGet";
	SaAisErrorT     expectedReturn = SA_AIS_ERR_BAD_HANDLE;
        SaLckHandleT   lckHandle;
	SaAisErrorT	lckError;
	SaSelectionObjectT 	selObject;
        int ret = SAF_TEST_PASS;

	lckError = saLckSelectionObjectGet(lckHandle, &selObject);
	if (lckError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(lckError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
	}
	
	return ret;
}

