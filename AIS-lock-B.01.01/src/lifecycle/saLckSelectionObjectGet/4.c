/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <Qingfeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saLckSelectionObjectGet
 * Description:   
 *   Call saLckSelectionObjectGet related to the lckHandle , After finalizing
 *   we call select to detect whether the selectionObject is invaild .the
 *   returned value should be -1.
 *   Return = SA_AIS_OK
 * Line:        P24-33:P24-35
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saLck.h"
#include "saf_test.h"

static void lck_resource_open_callback(SaInvocationT invocation,
	SaLckResourceHandleT lockResourceHandle,
	SaAisErrorT error)
{
	saLckResourceClose(lockResourceHandle);
	return;
}

static void lck_grant_callback(SaInvocationT invocation,
	SaLckLockStatusT lockStatus,
	SaAisErrorT error)
{
	return;
}

static void lck_waiter_callback(SaLckWaiterSignalT waiterSignal,
	SaLckLockIdT	lockId,
	SaLckLockModeT	modeHeld,
	SaLckLockModeT	modeRequested)
{
	return;
}

static void lck_resource_unlock_callback(SaInvocationT invocation,
	SaAisErrorT	error)
{
	return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saLckSelectionObjectGet";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaLckHandleT   lckHandle;
        SaLckCallbacksT lckCallback = {
                .saLckResourceOpenCallback  = lck_resource_open_callback,
                .saLckLockGrantCallback = lck_grant_callback,
		.saLckLockWaiterCallback = lck_waiter_callback,
		.saLckResourceUnlockCallback = lck_resource_unlock_callback
        };
	SaAisErrorT	lckError;
        SaVersionT      lckVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

        int ret = SAF_TEST_PASS;
	SaSelectionObjectT 	selObject;
	
	fd_set readSet;
	int selectRet;
	struct timeval timeout	= {
			.tv_sec = 9,
			.tv_usec = 0
	};
	
	SaNameT	lockResourceName = {
		.value = "resource",
		.length = strlen("resource")
	};
	
        lckError = saLckInitialize(&lckHandle, &lckCallback, &lckVersion);
         if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	lckError = saLckSelectionObjectGet(lckHandle, &selObject);
	if (lckError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(lckError),
			get_error_string(expectedReturn));
		saLckFinalize(lckHandle);
                ret = SAF_TEST_UNRESOLVED;
	}
	
	lckError = saLckResourceOpenAsync(lckHandle,
					1,
					&lockResourceName,
					SA_LCK_RESOURCE_CREATE);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckResourceOpenAsync, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		saLckFinalize(lckHandle);
		return ret;
        }
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
	lckError = saLckFinalize(lckHandle);
	if (lckError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saLckFinalize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(lckError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
        selectRet = select(selObject + 1, &readSet, NULL, NULL, &timeout);
        if (selectRet > 0){
                printf("  select error!\n");
                ret = SAF_TEST_FAIL;
        }

        return ret;
}



