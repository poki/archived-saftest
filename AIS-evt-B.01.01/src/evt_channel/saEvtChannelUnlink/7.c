/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtChannelUnlink
 * Description:   
 *	create a channel and fork a sub-process,
 *    	sub-process close the channel and unlink it; 
 *    	the main process still can access the channel. after sub-process 
 *	unlink the channel.
 *	Return = SA_AIS_OK	
 * Line:        P36-33:P36-37
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include "saEvt.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
        SaEvtHandleT    evtHandle;
	SaAisErrorT	evtError;
	SaTimeT 	timeout = SA_TIME_MAX;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};
	SaEvtChannelHandleT	channelHandle;
        int ret = SAF_TEST_PASS;
	
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = NULL,
                .saEvtEventDeliverCallback = NULL
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	pid_t pid;
	int status;
	
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtChannelOpen(evtHandle,&channelName,
				SA_EVT_CHANNEL_CREATE|
				SA_EVT_CHANNEL_PUBLISHER,
				timeout,
				&channelHandle);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpen, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	pid = fork();
	if (pid < 0){
		printf(" fork is error\n");
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	if (pid == 0)
		if (execl("./7-fork.test", NULL) == -1){
			printf(" execl fails.\n");
			ret = SAF_TEST_UNRESOLVED;
			return ret;		
		}
		
	wait(&status);
	if (WIFEXITED(status) == 0)
		ret = SAF_TEST_UNRESOLVED;
	else
		if(ret == SAF_TEST_PASS)
			ret = WEXITSTATUS(status);
	
	if (ret != SAF_TEST_PASS)
		goto final;
		
	// check if the checkpoint has been deleted
	evtError = saEvtChannelClose(channelHandle);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelClose, Return value: %s, should "
			" be SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_FAIL;
	}

	
final :	
	if (ret != SAF_TEST_PASS)
		saEvtChannelUnlink(evtHandle, &channelName);
		
	saEvtFinalize(evtHandle);
	return ret;
}


