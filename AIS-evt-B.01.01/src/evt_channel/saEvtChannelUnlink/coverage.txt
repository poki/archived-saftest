This file defines the coverage for the saEvtChannelUnlink conformance testing.

Assertion       Status
1-1		YES
1-2		YES
1-3		YES
2-1		YES
2-2		YES
2-3		YES
3		YES
4		YES
5		YES
6		YES
7		YES
8		YES
9		YES
10		YES
11		NO
12		NO
13		NO
14		YES
15		YES
16		YES
