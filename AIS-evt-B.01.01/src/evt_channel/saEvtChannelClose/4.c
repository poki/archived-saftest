/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, AnLi <an.li.wang@intel.com> 
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtChannelClose
 * Description:
 *   Call saEvtChannelClose after saEvtChannelUnlink. The channelHandle
 *   should be deleted. 
 *   Return = SA_AIS_OK
 * Line:        P34-39:P34-42
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <unistd.h>
#include "saAis.h"
#include "saEvt.h"
#include "saf_test.h"
#define TIMEOUT_VALUE SA_TIME_MAX

static void 
callback_channel_open(SaInvocationT invocation,
		      SaEvtChannelHandleT channelHandle,
		      SaAisErrorT error)

{
	return;
}

static void 
callback_event_deliver(SaEvtSubscriptionIdT sub_id,
			SaEvtEventHandleT event_handle,
			SaSizeT eventDataSize)

{
	return; 
}

int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaEvtHandleT	evtHandle;

	SaEvtCallbacksT evtCallback = {
		.saEvtChannelOpenCallback
		= (SaEvtChannelOpenCallbackT)callback_channel_open,
		.saEvtEventDeliverCallback
		= (SaEvtEventDeliverCallbackT)callback_event_deliver
	};

	SaVersionT	evtVersion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE 
	};
	SaNameT 	channelName = {
		.length = strlen("channel"),
		.value = "channel"
	};
	
	SaTimeT	saTimeout = TIMEOUT_VALUE;
	SaEvtChannelOpenFlagsT channelOpenFlags = 
					SA_EVT_CHANNEL_CREATE;
	SaEvtChannelHandleT channelHandle;
	

	int ret = SAF_TEST_PASS;
	
	pid_t pid ;
	int status;
	
	error = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	error = saEvtChannelOpen(evtHandle,
	            	&channelName,        
                        channelOpenFlags,
				saTimeout,
				&channelHandle
			);
       
	if (error != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtChannelOpen, Return value: %s, should be"
		     "  SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		saEvtFinalize(evtHandle);
		return ret;
	}               

	pid = fork();
	if (pid < 0){
		printf(" fork is error\n");
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	if (pid == 0)
		if (execl("./4-fork.test", NULL) == -1){
			printf("  execl fails.\n");
			ret = SAF_TEST_UNRESOLVED;
			return ret;		
		}
		
	wait(&status);
	if (WIFEXITED(status) == 0)
		ret = SAF_TEST_UNRESOLVED;
	else
		if(ret == SAF_TEST_PASS)
			ret = WEXITSTATUS(status);
	
   
	error = saEvtChannelClose(channelHandle);
	if (error != SA_AIS_OK) {
        	printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtChannelClose, Return value: %s, should be"
			"  SA_AIS_OK\n", get_error_string(error));
		ret =  SAF_TEST_UNRESOLVED;
	}
	
	if (ret != SAF_TEST_PASS){
		saEvtChannelUnlink(evtHandle,&channelName);
		goto final;
	}
		
//After close channel in main process, the channel will be deleted
//automatically because child process has unlinked the channel.
//So here ChannelUnlink should return not_exist 
	error =  saEvtChannelUnlink(evtHandle,
                                 &channelName
                                  );
	if (error != SA_AIS_ERR_NOT_EXIST) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtChannelUnlink, Return value: %s, should be"
			" SA_AIS_ERR_NOT_EXIST\n", get_error_string(error));
		ret = SAF_TEST_FAIL;
	}
final:
	saEvtFinalize(evtHandle);
	return ret;

}
