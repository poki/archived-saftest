/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, AnLi <an.li.wang@intel.com>
 *      Yu, Ling <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtChannelClose
 * Description:
 *   Test saEvtChannelClose will close event channel opened by an earlier
 *   invocation of the saEvtChannelOpenAsync(). 
 *   Return = SA_AIS_OK
 * Line:        P34-35:P34-37
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "saAis.h"
#include "saEvt.h"
#include "saf_test.h"
#define TIMEOUT_VALUE SA_TIME_MAX

int ret = SAF_TEST_PASS;

static void
callback_channel_open(SaInvocationT invocation,
		      SaEvtChannelHandleT channelHandle,
		      SaAisErrorT error)
{
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  error in SaEvtChannelOpenCallbackT is: %s, should be "
			" SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
                return;
	}
	if (SA_AIS_OK != saEvtChannelClose(channelHandle)) {
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelClose, Return value: %s, should be "
                        " SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_FAIL;
        }
	return;
}

static void
callback_event_deliver(SaEvtSubscriptionIdT sub_id,
			SaEvtEventHandleT event_handle,
			SaSizeT eventDataSize)

{
	return;
}

int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaEvtHandleT	evtHandle;

	SaEvtCallbacksT evtCallback = {
		.saEvtChannelOpenCallback
		= (SaEvtChannelOpenCallbackT)callback_channel_open,
		.saEvtEventDeliverCallback
		= (SaEvtEventDeliverCallbackT)callback_event_deliver
	};

	SaVersionT	evtVersion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE 
	};
	SaNameT 	channelName = {
		.length = strlen("channelName"),
		.value = "channelName"
	};
	
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ONE;
	
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;

	error = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtInitialize, Return value: %s, should be "
			" SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	error = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (error != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtSelectionObjectGet, Return value: %s,should be"
			" SA_AIS_OK\n",get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
	
		saEvtFinalize(evtHandle);
		return ret;
	}

	error = saEvtChannelOpenAsync(evtHandle, 1,
						&channelName,
						SA_EVT_CHANNEL_CREATE);

	if (error != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtChannelOpenAsync, Return value: %s, should be "
			" SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		saEvtFinalize(evtHandle);
		return ret;
	}
	FD_ZERO(&readSet);
	FD_SET(selObject, &readSet);
	selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
	if ((selectRet == -1) || (selectRet == 0)){
		printf("  select error!\n");
		ret = SAF_TEST_UNRESOLVED;
		saEvtChannelUnlink(evtHandle, &channelName);
		saEvtFinalize(evtHandle);
		return ret;
	}
	
	error = saEvtDispatch(evtHandle, dispatchFlag);
	if (error != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtDispatch, Return value: %s,should be"
			" SA_AIS_OK\n",get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
	}
	
	saEvtChannelUnlink(evtHandle, &channelName);
	saEvtFinalize(evtHandle);
	return ret;
}

