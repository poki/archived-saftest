/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *     Wang, AnLi <an.li.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtChannelClose
 * Description:
 *   Test if events allocated for a process are all freed when an event
 *   channel closes.  
 *   Steps:
 *   1. Initialize an evtHanle
 *   2. Open a channel
 *   3. Allocate event to the channel
 *   4. Close the channel. The event should be freed. 
 *   5. Call the event by eventhandle, should return bad handle. 
 *   6. Unlink the channel and finalize the evtHandle.
 *   Return = SA_AIS_OK  
 * Line:        P35-1:P35-3
 */



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "saAis.h"
#include "saEvt.h"
#include "saf_test.h"
#define TIMEOUT_VALUE SA_TIME_MAX

int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaEvtHandleT	evtHandle;

	SaEvtCallbacksT evtCallback = {
		.saEvtChannelOpenCallback
		= NULL,
		.saEvtEventDeliverCallback
		= NULL
	};

	SaVersionT	evtVersion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE 
	};
	SaNameT 	channelName = {
		.length = strlen("channelName"),
		.value = "channelName"
	};
	
	SaTimeT	saTimeout = TIMEOUT_VALUE;
	SaEvtChannelOpenFlagsT channelOpenFlags = 
					SA_EVT_CHANNEL_CREATE;
	SaEvtChannelHandleT channelHandle;
	SaEvtEventHandleT eventHandle;

	int ret = SAF_TEST_PASS;

	error = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	error = saEvtChannelOpen(evtHandle,
					&channelName,
					channelOpenFlags,
					saTimeout,
					&channelHandle
					);
	if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpen, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		saEvtFinalize(evtHandle);
		return ret;
        }
      	
	error =  saEvtEventAllocate(channelHandle,
                                    &eventHandle );
      
	if (error != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvetEventAllocate, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;

		saEvtChannelClose(channelHandle);
		saEvtChannelUnlink(evtHandle, &channelName);
		saEvtFinalize(evtHandle);	
	
		return ret;
        }


	error = saEvtChannelClose(channelHandle);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtChannelClose, Return value: %s,"
			" should be SA_AIS_OK\n", 
			get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;

		saEvtChannelUnlink(evtHandle, &channelName);
		saEvtFinalize(evtHandle);
		return ret;
	}
     
//After close the channel in this process, the eventhandle allocated by 
//saEvtEventAllocate() will be released automatically.
//So when call saEvtEventFree, BAD_HANDLE should be returned.
	error = saEvtEventFree(eventHandle); 

	if (error != SA_AIS_ERR_BAD_HANDLE) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtEventFree, Return value: %s,"
			" should be SA_AIS_ERR_BAD_HANDLE\n", 
			get_error_string(error));
                ret = SAF_TEST_FAIL;
	}                

	saEvtChannelUnlink(evtHandle, &channelName);
	saEvtFinalize(evtHandle);
	return ret;

}

