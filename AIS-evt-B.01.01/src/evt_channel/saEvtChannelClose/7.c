/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, AnLi <an.li.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtChannelClose
 * Description:
 *   Test if event channels are all closed when a process terminates.
 *   Steps: 
 *   1. Open a event channel in main process
 *   2. Fork a subprocess and open the same event channel in the subprocess
 *   3. Subprocess ends
 *   4. Close the event channel in main process. If the event 
 *      channel has been closed when the subprocess died, the unlink here 
 *      should succeed. 
 *   5. Re-create a event channel using the same name as 1. If succeed, 
 *      The case passes.
 *   Return = SA_AIS_ERR_BAD_HANDLE
 *   7-fork.c is used as its child process
 * Line:        P35-7:P35-8
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <unistd.h>
#include "saAis.h"
#include "saEvt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE SA_TIME_MAX
int main(int argc, char *argv[])  
{
	SaAisErrorT	error; 
	SaEvtHandleT	evtHandle;
	
	SaEvtCallbacksT evtCallback = {
		.saEvtChannelOpenCallback
		= NULL ,
		.saEvtEventDeliverCallback
		= NULL
	};

	SaVersionT	evtVersion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE 
	};
	
	SaNameT 	channelName = {
		.length = strlen("channelName"),
		.value = "channelName"
	};
	
	SaTimeT	saTimeout = TIMEOUT_VALUE;

	SaEvtChannelOpenFlagsT channelOpenFlags = SA_EVT_CHANNEL_CREATE;
	SaEvtChannelHandleT channelHandle;
	
	int ret = SAF_TEST_PASS;

	error = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	error = saEvtChannelOpen(evtHandle,
				&channelName,
				channelOpenFlags,
				saTimeout,
				&channelHandle
				);
	if (error != SA_AIS_OK) {
		printf(" Does not conform the expected behaviors!\n");
		printf(" saEvtChannelOpen, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		saEvtFinalize(evtHandle);
		return ret;
	}

	pid_t pid ;
	int status;

	pid = fork();
	if (pid < 0){
		printf(" fork is error\n");
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	if (pid == 0)
		if (execl("./7-fork.test", NULL) == -1){
			printf(" execl fails.\n");
			ret = SAF_TEST_UNRESOLVED;
			return ret;
		}
		
	wait(&status);
	if (WIFEXITED(status) == 0){
		printf(" Child process exits abnormally");
		ret = SAF_TEST_UNRESOLVED;
	}
	else
		if(ret == SAF_TEST_PASS)
			ret = WEXITSTATUS(status);

	error = saEvtChannelClose(channelHandle);
	if (error != SA_AIS_OK){
		printf(" Does not conform the expected behaviors!\n");
		printf(" SaEvtChannelClose: Return value: %s,"
			" should be SA_AIS_OK\n",
			 get_error_string(error));
		ret = SAF_TEST_FAIL;
		saEvtChannelUnlink(evtHandle, &channelName);
		saEvtFinalize(evtHandle);
		return  ret;
	}
	
	error = saEvtChannelOpen(evtHandle, &channelName,
				 SA_EVT_CHANNEL_CREATE,
				 saTimeout,
				 &channelHandle
				);
	if (error != SA_AIS_OK) {
		printf(" Does not conform the expected behaviors!\n");
		printf(" SaEvtChannelOpen: Return value : %s,"
			" should be SA_AIS_OK\n",
			get_error_string(error));
		ret = SAF_TEST_FAIL;
		saEvtFinalize(evtHandle);
		return ret;
	}

	saEvtChannelClose(channelHandle);
final:
	saEvtChannelUnlink(evtHandle, &channelName);
	saEvtFinalize(evtHandle);
	return ret;
}

