/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtChannelOpenAsync
 * Description:
 *   Open a channel multiple times in the same process.
 *   Steps:
 *    1.initialize a evthandle.
 *    2.use the evthandle to open a channel.
 *    3.open the channel again and again.
 *   Return = SA_AIS_OK
 * Line:        P31-34:P31-35
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

SaAisErrorT 	openError;
static void evt_channel_open_callback (SaInvocationT invocation,
        SaEvtChannelHandleT channelHandle,
        SaAisErrorT error)
{
	openError = error;
	saEvtChannelClose(channelHandle);
        return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saEvtChannelOpenAsync";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaEvtHandleT    evtHandle;
	SaAisErrorT	evtError;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};
 
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	
	int ret = SAF_TEST_PASS;
	int i;
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = evt_channel_open_callback,
                .saEvtEventDeliverCallback = NULL
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtSelectionObjectGet, Return value: %s,should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	for (i = 0; i < 3; i++){
		evtError = saEvtChannelOpenAsync(evtHandle,i,
					&channelName,
					SA_EVT_CHANNEL_CREATE |
					SA_EVT_CHANNEL_PUBLISHER);
		if (evtError != expectedReturn){
			printf("  Does not conform the expected behaviors!\n");
                	printf("  %s, Return value: %s, should be %s\n", 
				apiName,
				get_error_string(evtError),
				get_error_string(expectedReturn));
                	ret = SAF_TEST_FAIL;
			if (!i)
				goto final_1;
			else
				break;
		}
	}
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final_2;
        }
	
	evtError = saEvtDispatch(evtHandle, dispatchFlag);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtDispatch, Return value: %s,should be"
			" SA_AIS_OK\n",get_error_string(evtError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	if (openError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("errorcode,Return value :%s,should be SA_AIS_OK\n",
			get_error_string(openError));
		ret = SAF_TEST_FAIL;
	}
	
final_2:
	saEvtChannelUnlink(evtHandle, &channelName);
final_1 :
	saEvtFinalize(evtHandle);
	return ret;
}



