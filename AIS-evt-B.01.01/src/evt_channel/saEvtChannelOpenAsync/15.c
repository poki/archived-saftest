/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtChannelOpenAsync
 * Description:   
 *  Test if a process can receive an event more than once on a particular event 
 *  channel handle.
 *  Steps:
 *    1.initialize a evthandle.
 *    2.use the evthandle to open a channel with SA_EVT_CHANNEL_SUBSCRIBER |
 *     SA_EVT_CHANNEL_PUBLISHER| SA_EVT_CHANNEL_CREATE.
 *    3.allocate a event handle for the channel.
 *    4.publish a event using the event handle multi-times.
 *    5.get the event after dispatching .Test if it gets the event more than
 *    once.
 *  Return = SA_AIS_OK. 
 * Line:        P31-35:P31-36
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

SaEvtChannelHandleT channelHandleGlobal;
int callbackFlag = 0;
int ret = SAF_TEST_PASS;
static void evt_deliver_callback (SaEvtSubscriptionIdT subscriptionId,
        SaEvtEventHandleT eventHandle,
	SaSizeT eventDataSize)
{
	callbackFlag ++;
	saEvtEventFree(eventHandle);
        return;
}

static void evt_channel_open_callback (SaInvocationT invocation,
        SaEvtChannelHandleT channelHandle,
        SaAisErrorT error)
{
	if (error != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("errorcode,Return value :%s,should be SA_AIS_OK\n",
			get_error_string(error));
		ret = SAF_TEST_FAIL;
		return;
	}
	channelHandleGlobal = channelHandle;
        return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saEvtChannelOpenAsync";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaEvtHandleT    evtHandle;
	SaAisErrorT	evtError;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};
 
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	
	int i;
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = evt_channel_open_callback,
                .saEvtEventDeliverCallback = evt_deliver_callback
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	char evtdata[] = "evtdata";
	SaEvtEventIdT  eventId;
	SaEvtEventHandleT evtEventHandle;
	char pattern[20] = "pattern";
	SaEvtEventPatternT evtPattern = {
				.allocatedSize = 20,
				.patternSize = 20,
				.pattern =  pattern
	};
	SaEvtEventPatternArrayT	evtPatternArray ={
			.allocatedNumber = 1,
			.patternsNumber = 1,
			.patterns = &evtPattern
	};
	
	SaEvtEventPriorityT	priority = SA_EVT_HIGHEST_PRIORITY;
	SaTimeT		retentionTime = SA_TIME_MAX;
	SaNameT		publisherName = {
				.value = "publish",
				.length = strlen("publish")
	};
		
	SaEvtEventFilterT evtFilter = {
				.filterType = SA_EVT_PASS_ALL_FILTER,
				.filter = evtPattern
	};
	
	SaEvtEventFilterArrayT	filter = {
				.filtersNumber = 1,
				.filters = &evtFilter
	};
	
	SaEvtSubscriptionIdT 	subscriptionId;
	
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtSelectionObjectGet, Return value: %s,should "
			" be SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
	evtError = saEvtChannelOpenAsync(evtHandle, 1,
					&channelName,
					SA_EVT_CHANNEL_CREATE |
					SA_EVT_CHANNEL_PUBLISHER|
					SA_EVT_CHANNEL_SUBSCRIBER);
	if (evtError != expectedReturn){
		printf("Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(evtError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final_1;
        }
	
	evtError = saEvtDispatch(evtHandle, dispatchFlag);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtDispatch, Return value: %s,should be"
			" SA_AIS_OK\n",get_error_string(evtError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	if (ret == SAF_TEST_FAIL)
		goto final_1;

	evtError = saEvtEventAllocate(channelHandleGlobal,&evtEventHandle);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAllocate, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	
	evtError = saEvtEventAttributesSet(evtEventHandle,
					&evtPatternArray,
					priority,
					retentionTime,
					&publisherName);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAttributesSet, Return value: %s, should "
			" be SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
        }
	
	evtError = saEvtEventSubscribe(
			channelHandleGlobal, &filter,subscriptionId);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventSubscribe, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }
	
	evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n",
			get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }
	
	for (i = 0;i < 10 ;i++){
		evtError = saEvtEventPublish(evtEventHandle,evtdata,
				strlen(evtdata),&eventId);
		if (evtError != SA_AIS_OK){
                	printf("Does not conform the expected behaviors!\n");
                	printf("  saEvtEventPublish, Return value: %s, should"
				" be SA_AIS_OK\n",get_error_string(evtError));
                	ret = SAF_TEST_UNRESOLVED;
			goto final_4;
        	}
	}
	do {
		FD_ZERO(&readSet);
        	FD_SET(selObject, &readSet);
        	selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        	if ((selectRet == -1) || (selectRet == 0)){
        		printf("  select error!\n");
                	ret = SAF_TEST_UNRESOLVED;
			goto final_4;
        	}

		evtError = saEvtDispatch(evtHandle, dispatchFlag);
		if (evtError != SA_AIS_OK){
                	printf("  Does not conform the expected behaviors!\n");
                	printf("  saEvtDispatch, Return value: %s, should be"
				" SA_AIS_OK\n",get_error_string(evtError));
                	ret = SAF_TEST_UNRESOLVED;
			goto final_4;
        	}
	} while (callbackFlag != 10);
final_4:
	saEvtEventUnsubscribe(channelHandleGlobal,subscriptionId);
final_3:
	saEvtEventFree(evtEventHandle);
final_2:
	saEvtChannelClose(channelHandleGlobal);
	saEvtChannelUnlink(evtHandle, &channelName);
final_1 :
	saEvtFinalize(evtHandle);
	return ret;
}



