/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtChannelOpenAsync
 * Description:
 *  Test if saEvtChannelOpenAsync can return SA_AIS_ERR_NOT_EXIST,when
 *  open a non-existed channel without SA_EVT_CHANNEL_CREATE flags set.
 *  Return = SA_AIS_ERR_NOT_EXIST
 * Line:        P32-24:P32-26
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"
static SaAisErrorT errorGloble;

static void evt_channel_open_callback (SaInvocationT invocation,
        SaEvtChannelHandleT channelHandle,
        SaAisErrorT error)
{
	saEvtChannelClose(channelHandle);
	errorGloble = error;
        return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saEvtChannelOpenAsync";
	SaAisErrorT     expectedReturn = SA_AIS_ERR_NOT_EXIST;
        SaEvtHandleT    evtHandle;
	SaAisErrorT	evtError;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ONE;
	
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	int ret = SAF_TEST_PASS;
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = evt_channel_open_callback,
                .saEvtEventDeliverCallback = NULL
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }

	evtError = saEvtChannelOpenAsync(evtHandle, 1,
					&channelName,
					SA_EVT_CHANNEL_PUBLISHER);
	if (evtError != expectedReturn){
		if (evtError != SA_AIS_OK){
			printf("  Does not conform the expected behaviors!\n");
                	printf("  %s, Return value: %s, should be %s\n", 
				apiName,
				get_error_string(evtError),
				get_error_string(expectedReturn));
                	ret = SAF_TEST_FAIL;
			goto final_1;
		}
		evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
		if (evtError != SA_AIS_OK){
                	printf("  Does not conform the expected behaviors!\n");
                	printf("  saEvtSelectionObjectGet, Return value:"
				" %s,should be SA_AIS_OK\n",
				get_error_string(evtError));
                	ret = SAF_TEST_UNRESOLVED;
			goto final_2;
        	}

		FD_ZERO(&readSet);
        	FD_SET(selObject, &readSet);
        	selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        	if ((selectRet == -1) || (selectRet == 0)){
                	printf("  select error!\n");
                	ret = SAF_TEST_UNRESOLVED;
                	goto final_2;
        	}
	
		evtError = saEvtDispatch(evtHandle, dispatchFlag);
		if (evtError != SA_AIS_OK){
			printf("  Does not conform the expected behaviors!\n");
        		printf("  saEvtDispatch, Return value: %s,should be"
				" SA_AIS_OK\n",get_error_string(errorGloble));
                	ret = SAF_TEST_UNRESOLVED;
			goto final_2;
		}
		if (errorGloble != expectedReturn){
			printf("  Does not conform the expected behaviors!\n");
                	printf("  %s, Return value: %s, should be %s\n", 
				apiName,
				get_error_string(errorGloble),
				get_error_string(expectedReturn));
                	ret = SAF_TEST_FAIL;
		}
	}
final_2:
	saEvtChannelUnlink(evtHandle, &channelName);
final_1:
	saEvtFinalize(evtHandle);
	return ret;
}