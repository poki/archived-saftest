/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtChannelOpenCallbackT
 * Description:
 *		Test if the saEvtChannelOpenCallbackT can get
 *		code SA_AIS_ERR_BAD_FLAGS.
 * Line:        P33-41:P33-41
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

SaInvocationT	callInvocation = 0;
SaAisErrorT 	openError;

static void evt_channel_open_callback (SaInvocationT invocation,
        SaEvtChannelHandleT channelHandle,
        SaAisErrorT error)
{
	callInvocation = invocation;
	openError = error;
	saEvtChannelClose(channelHandle);
        return;
}

int main(int argc, char *argv[])
{
        SaEvtHandleT    evtHandle;
	SaAisErrorT	evtError;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};
 
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ONE;
	
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	
	int ret = SAF_TEST_PASS;
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = evt_channel_open_callback,
                .saEvtEventDeliverCallback = NULL
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtSelectionObjectGet, Return value: %s,should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
        }

	evtError = saEvtChannelOpenAsync(evtHandle,1,
					&channelName,
					0xf0);

	/*
	 * Taken care of in the library
	 */
	if (evtError == SA_AIS_ERR_BAD_FLAGS) {
		goto final;
	}

	/*
	 * Taken care of in the callback
	 */
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpenAsync, Return value: %s, "
			"should be SA_AIS_OK\n", get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }
	
	evtError = saEvtDispatch(evtHandle, dispatchFlag);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtDispatch, Return value: %s,should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	if (callInvocation != 1){
		printf("  Does not conform the expected behaviors!\n");
		printf("invocation ,Return value:%lld,should be 1",
				callInvocation);
		ret = SAF_TEST_UNRESOLVED;
		goto final; 
	}
	
	if (openError != SA_AIS_ERR_BAD_FLAGS){
		printf("  Does not conform the expected behaviors!\n");
		printf("errorcode,Return value :%s,should be"
		" SA_AIS_ERR_BAD_FLAGS\n",get_error_string(openError));
		ret = SAF_TEST_FAIL;
	}

final :
	saEvtFinalize(evtHandle);
	return ret;
}




