 /*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 * Spec:        AIS-B.01.01
 * Function:    saEvtChannelOpen
 * Description:   
 *  Test if when a process opens an event channel multiple times,it is 
 *  possible to receive the same event multiple times.
 *   Steps:
 *   1.initialize a evthandle.
 *   2.use the evthandle to open a channel with SA_EVT_CHANNEL_SUBSCRIBER |
 *    SA_EVT_CHANNEL_PUBLISHER| SA_EVT_CHANNEL_CREATE once more.
 *   3.allocate a event handle for the channel.
 *   4.publish a event through the event handle.
 *   5.get the event after dispatching .Test if it gets the event more than
 *   once.
 *  Return = SA_AIS_OK.
 * Line:        P31-35:P31-36
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

int callbackFlag = 0;
static void evt_deliver_callback (SaEvtSubscriptionIdT subscriptionId,
        SaEvtEventHandleT eventHandle,
	SaSizeT eventDataSize)
{
	callbackFlag ++;
        return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saEvtChannelOpen";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaEvtHandleT    evtHandle;
	SaAisErrorT	evtError;
	SaTimeT 	timeout = SA_TIME_MAX;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};
	SaEvtChannelHandleT	channelHandle[2];
        int ret = SAF_TEST_PASS;
	
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = NULL,
                .saEvtEventDeliverCallback = evt_deliver_callback
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	char evtdata[] = "evtdata";
	SaEvtEventIdT  eventId;
	SaEvtEventHandleT evtEventHandle;
	char pattern[20] = "pattern";
	SaEvtEventPatternT evtPattern = {
				.allocatedSize = 20,
				.patternSize = 20,
				.pattern =  pattern
	};
	SaEvtEventPatternArrayT	evtPatternArray ={
			.allocatedNumber = 1,
			.patternsNumber = 1,
			.patterns = &evtPattern
	};
	
	SaEvtEventPriorityT	priority = SA_EVT_HIGHEST_PRIORITY;
	SaTimeT		retentionTime = SA_TIME_MAX;
	SaNameT		publisherName = {
				.value = "publish",
				.length = strlen("publish")
	};
		
	SaEvtEventFilterT evtFilter = {
				.filterType = SA_EVT_PASS_ALL_FILTER,
				.filter = evtPattern
	};
	
	SaEvtEventFilterArrayT	filter = { 
				.filtersNumber = 1,
				.filters = &evtFilter
	};
	
	SaEvtSubscriptionIdT 	subscriptionId = 1;
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtChannelOpen(evtHandle,&channelName,
				SA_EVT_CHANNEL_CREATE|
				SA_EVT_CHANNEL_SUBSCRIBER|
				SA_EVT_CHANNEL_PUBLISHER,
				timeout,
				channelHandle);
	if (evtError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(evtError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	evtError = saEvtChannelOpen(evtHandle,&channelName,
				SA_EVT_CHANNEL_SUBSCRIBER|
				SA_EVT_CHANNEL_PUBLISHER,
				timeout,
				channelHandle + 1);
	if (evtError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(evtError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	evtError = saEvtEventAllocate(channelHandle[0],&evtEventHandle);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAllocate, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
        }
	
	evtError = saEvtEventAttributesSet(evtEventHandle,
					&evtPatternArray,
					priority,
					retentionTime,
					&publisherName);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAttributesSet, Return value: %s, should "
			" be SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }
	
	evtError = saEvtEventSubscribe(channelHandle[0],&filter,
						subscriptionId);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventSubscribe, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }
	
	evtError = saEvtEventSubscribe(channelHandle[1],&filter,
						subscriptionId+1);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventSubscribe, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_5;
        }
	
	evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n",
			get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_6;
        }
	
	evtError = saEvtEventPublish(evtEventHandle, evtdata,
				strlen(evtdata),&eventId);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventPublish, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_6;
        }
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
		goto final_6;
        }

	evtError = saEvtDispatch(evtHandle, dispatchFlag);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtDispatch, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_6;
        }
	
	if (callbackFlag != 2){
		printf("Does not conform the expected behaviors!\n");
		printf(" event should be gotten more than one ,but Return" 
		" :%d times\n",callbackFlag);
		ret = SAF_TEST_FAIL;
	}
	
final_6:
	saEvtEventUnsubscribe(channelHandle[1],subscriptionId + 1);	
final_5:	
	saEvtEventUnsubscribe(channelHandle[0],subscriptionId);
final_4:
	saEvtEventFree(evtEventHandle);
final_3:
	saEvtChannelClose(channelHandle[1]);
final_2:
	saEvtChannelClose(channelHandle[0]);
	saEvtChannelUnlink(evtHandle,&channelName);
final_1 :	
	saEvtFinalize(evtHandle);
	return ret;
}
