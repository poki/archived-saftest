/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, Xuelian <xuelian.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtEventUnsubscribe
 * Description:
 *   Test if a process that wishes to modify a subscription without
 *   losing any events must establish the new subscription before
 *   removing the existing subscription.
 *   	1.create a subscription
 *   	2.create a new subscrition
        3.initiate evtent attribute and publish
 *   	4.remove the old subsrciption
 *	5.dispatch all
 *   	6.check if the new subscription accept the event published in step 4
 *   then the event will not lost
 *   Return = SA_AIS_OK
 * Line:        P53-13;P53-17
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

SaEvtEventHandleT eventHandleGlobal;
static void evt_deliver_callback (SaEvtSubscriptionIdT subscriptionId,
        SaEvtEventHandleT eventHandle,
	SaSizeT eventDataSize)
{
	eventHandleGlobal = eventHandle;
        return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saEvtEventUnsubscribe";
	SaEvtHandleT    evtHandle;
	SaAisErrorT	evtError;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};

	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	SaEvtChannelHandleT	channelHandle;
	int ret = SAF_TEST_PASS;
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = NULL,
                .saEvtEventDeliverCallback = evt_deliver_callback
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	char evtdata[] = "evtdata";
	SaEvtEventHandleT evtEventHandle;
	unsigned char fitpattern[20] = "pattern";
	unsigned char filterpattern[20] = "pattern";
	SaEvtEventPatternT fitevtPattern = {
				.allocatedSize = 20,
				.patternSize = 20,
				.pattern =  fitpattern
	};
	
	SaEvtEventPatternT filterPattern = {
				.allocatedSize = 20,
				.patternSize = 20,
				.pattern =  filterpattern
	};
	SaEvtEventPatternArrayT	fitevtPatternArray = {
			.allocatedNumber = 1,
			.patternsNumber = 1,
			.patterns = &fitevtPattern
	};
	SaEvtEventPatternArrayT	evtPatternArrayGet = {
			.allocatedNumber = 0,
			.patternsNumber = 0,
			.patterns = NULL
	};

	SaEvtEventPriorityT	priority = SA_EVT_HIGHEST_PRIORITY;
	SaTimeT		retentionTime = SA_TIME_MAX;
	SaTimeT		publishTime;
	SaNameT		publisherName = {
				.value = "publish",
				.length = strlen("publish")
	};
	SaEvtEventIdT	eventId;
	struct timeval tv = {
			.tv_sec = 5,
			.tv_usec = 0
	};
	SaEvtEventFilterT evtFilter = {
				.filterType = SA_EVT_SUFFIX_FILTER,
				.filter = filterPattern
	};
	
	SaEvtEventFilterArrayT	filter = {
				.filtersNumber = 1,
				.filters = &evtFilter
	};
	
	SaEvtSubscriptionIdT 	subscriptionId = 2;
	
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtChannelOpen(evtHandle,&channelName,
				SA_EVT_CHANNEL_CREATE|
				SA_EVT_CHANNEL_PUBLISHER|
				SA_EVT_CHANNEL_SUBSCRIBER,
				SA_TIME_MAX,
				&channelHandle);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpen, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	evtError = saEvtEventAllocate(channelHandle,&evtEventHandle);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAllocate, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	
	//create a subscription
	evtError = saEvtEventSubscribe(channelHandle,&filter,subscriptionId);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventSubscribe, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
        	goto final_3;
	}
	
	// change filter and create a new subscribe 
	evtFilter.filterType = SA_EVT_PASS_ALL_FILTER;
	
	subscriptionId ++;
	
	evtError = saEvtEventSubscribe(channelHandle,&filter,subscriptionId);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventSubscribe, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		saEvtEventUnsubscribe(channelHandle,subscriptionId-1);
		goto final_4;
	}
	//initiate evtent attribute and publish
	evtError = saEvtEventAttributesSet(evtEventHandle,
					&fitevtPatternArray,
					priority,
					retentionTime,
					&publisherName);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAttributesSet, Return value: %s, should "
			" be SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
        }
	evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n",
			get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }
	
	evtError = saEvtEventPublish(evtEventHandle,evtdata,
				strlen(evtdata),&eventId);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventPublish, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }
	// remove old filter using saEvtEventUnsubscribe 	
	evtError = saEvtEventUnsubscribe(channelHandle,subscriptionId-1);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventUnsubscribe, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_FAIL;
		goto final_4;
	}
	//dispatch all and check if the new subscription accept the event
	// published in step 4
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, &tv);
        /*this event fits the filter, so it will be received
	and the evt_deliver_callback will run. the selectRet should not
	be 0*/
        if(selectRet == -1){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }
	else if(selectRet == 0){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventSubscribe, when SA_EVT_SUFFIX_FILTER"
			" didn't accept fit value\n");
                ret = SAF_TEST_FAIL;
		goto final_4;
	}

	evtError = saEvtDispatch(evtHandle, dispatchFlag);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtDispatch, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }
	
	evtError = saEvtEventAttributesGet(eventHandleGlobal,
					&evtPatternArrayGet,
					&priority,
					&retentionTime,
					&publisherName,
					&publishTime,
					&eventId);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be SA_AIS_OK\n",
			apiName, get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_5;
	}
	if (evtPatternArrayGet.patternsNumber != 1 ){
		printf(" patternArray is not the same as initialize value");
		printf(" patternsNumber should be 1,Returned value :%lld",
			evtPatternArrayGet.patternsNumber);
		ret = SAF_TEST_FAIL;
		goto final_5;
	}

	if ((strcmp(evtPatternArrayGet.patterns->pattern,
		     fitevtPatternArray.patterns->pattern) !=
			 0)||evtPatternArrayGet.patterns->patternSize
		!= 20){
		printf("patternArray is not the same as the fit pattern\n");
		ret = SAF_TEST_FAIL;
	}

final_5 :
	saEvtEventFree(eventHandleGlobal);
final_4 :
	saEvtEventUnsubscribe(channelHandle,subscriptionId);
final_3 :
	saEvtEventFree(evtEventHandle);
final_2 :
	saEvtChannelClose(channelHandle);
	saEvtChannelUnlink(evtHandle, &channelName);
final_1 :
	saEvtFinalize(evtHandle);
	return ret;
}
 
