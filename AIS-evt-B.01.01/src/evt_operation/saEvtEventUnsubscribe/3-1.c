/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtEventUnsubscribe
 * Description:
 *   Call the api with valid params before the publish.Test if the process 
 *   cannot receive events after the api is called.
 *   Return = SA_AIS_OK
 * Line:        P53-6:P53-10
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>
#include "saEvt.h"
#include "saf_test.h"

static void evt_deliver_callback (SaEvtSubscriptionIdT subscriptionId,
        SaEvtEventHandleT eventHandle,
	SaSizeT eventDataSize)
{
	saEvtEventFree(eventHandle);
        return;
}

int main(int argc, char *argv[])
{
        SaEvtHandleT    evtHandle;
	SaAisErrorT	evtError;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	}; 
	
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	SaEvtChannelHandleT	channelHandle;
	int ret = SAF_TEST_PASS;
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = NULL,
                .saEvtEventDeliverCallback = evt_deliver_callback
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	char evtdata[] = "evtdata";
	SaEvtEventHandleT evtEventHandle;
	char pattern[20] = "pattern";
	SaEvtEventPatternT evtPattern = {
				.allocatedSize = 20,
				.patternSize = 20,
				.pattern =  pattern
	};
	SaEvtEventPatternArrayT	evtPatternArray ={
			.allocatedNumber = 1,
			.patternsNumber = 1,
			.patterns = &evtPattern
	};
	
	SaEvtEventPriorityT	priority = SA_EVT_HIGHEST_PRIORITY;
	SaTimeT		retentionTime = SA_TIME_MAX;
	SaNameT		publisherName = {
				.value = "publish",
				.length = strlen("publish")
	};
	SaEvtEventIdT	eventId;
		
	SaEvtEventFilterT evtFilter = {
				.filterType = SA_EVT_PASS_ALL_FILTER,
				.filter = evtPattern
	};
	
	SaEvtEventFilterArrayT	filter = { 
				.filtersNumber = 1,
				.filters = &evtFilter
	};
	
	SaEvtSubscriptionIdT 	subscriptionId = 2;
	
	struct timeval timeout = {
		.tv_sec = 9,
		.tv_usec = 0
	};
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtChannelOpen(evtHandle,&channelName,
				SA_EVT_CHANNEL_CREATE|
				SA_EVT_CHANNEL_PUBLISHER|
				SA_EVT_CHANNEL_SUBSCRIBER,
				SA_TIME_MAX,
				&channelHandle);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpen, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	
	evtError = saEvtEventAllocate(channelHandle,&evtEventHandle);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAllocate, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	
	evtError = saEvtEventAttributesSet(evtEventHandle,
					&evtPatternArray,
					priority,
					retentionTime,
					&publisherName);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAttributesSet, Return value: %s, should "
			" be SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
        }
	
	evtError = saEvtEventSubscribe(channelHandle,&filter,subscriptionId);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventSubscribe, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
        }
	
	evtError = saEvtEventUnsubscribe(channelHandle,subscriptionId);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventUnsubscribe, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
        }
	
	evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n",
			get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
        }
	
	evtError = saEvtEventPublish(evtEventHandle,evtdata,
				strlen(evtdata),&eventId);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventPublish, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
        }
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, &timeout);
        if (selectRet != 0){
                printf("  select error!\n");
                ret = SAF_TEST_FAIL;
        }
	
	
final_3 :
	saEvtEventFree(evtEventHandle);	
final_2 :
	saEvtChannelClose(channelHandle);	
	saEvtChannelUnlink(evtHandle, &channelName);
final_1 :
	saEvtFinalize(evtHandle);
	return ret;
}



