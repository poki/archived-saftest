/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtEventAttributesGet
 * Description:
 *   Invoke this API function on the event allocated by saEvtEventAllocate.
 *   Test if the attributes have the initial values set by the event service.
 *   Return = SA_AIS_OK.
 * Line:        P43-36:P43-38
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saEvtEventAttributesGet";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaEvtHandleT    evtHandle;
	SaAisErrorT	evtError;
	SaTimeT 	timeout = SA_TIME_MAX;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};
	SaEvtChannelHandleT	channelHandle;
        int ret = SAF_TEST_PASS;
	
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = NULL,
                .saEvtEventDeliverCallback = NULL
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaEvtEventHandleT evtEventHandle;
	SaEvtEventIdT  eventId;
	SaEvtEventPatternArrayT	patternArray;
	SaEvtEventPriorityT	priority;
	SaTimeT 	retentionTime;
	SaNameT 		publisherName;
	SaTimeT		publishTime;
	
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtChannelOpen(evtHandle,&channelName,
				SA_EVT_CHANNEL_CREATE|
				SA_EVT_CHANNEL_PUBLISHER,
				timeout,
				&channelHandle);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpen, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	evtError = saEvtEventAllocate(channelHandle,&evtEventHandle);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAllocate, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	
	// get the information from eventhandle
	publishTime = 0;
	evtError = saEvtEventAttributesGet(evtEventHandle,
					&patternArray,
					&priority,
					&retentionTime,
					&publisherName,
					&publishTime,
					&eventId);
	if (evtError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(evtError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
		goto final_3;
	}
	
	if (patternArray.patternsNumber != 0){
		printf("patternArray is not same as default values\n");
		ret = SAF_TEST_FAIL;
		goto final_3;
	}
	
	if (priority != SA_EVT_LOWEST_PRIORITY){
		printf("priority is not same as default values,Return:%d"
		"  should be %d\n",priority,SA_EVT_LOWEST_PRIORITY);
		ret = SAF_TEST_FAIL;
		goto final_3;
	}
	
	if (retentionTime != 0){
		printf("retentionTime is not same as default values,Return:"
		"%lld,should be 0 \n",retentionTime);
		ret = SAF_TEST_FAIL;
		goto final_3;
	}
	
	if (publishTime != SA_TIME_UNKNOWN){
		printf("publishTime is not same as default values,Return:"
		"%lld,should be %lld \n",retentionTime,SA_TIME_UNKNOWN);
		ret = SAF_TEST_FAIL;
		goto final_3;
	}
	
	if (publisherName.length != 0){
		printf("publisherName is not same as default values,should "
		"be a emtpy,but returned length:%d\n",publisherName.length);
		ret = SAF_TEST_FAIL;
		goto final_3;
	}
	
	if (eventId != SA_EVT_EVENTID_NONE){
		printf("eventId is not same as default values,Return:"
		"%lld,should be %d \n",eventId,SA_EVT_EVENTID_NONE);
		ret = SAF_TEST_FAIL;
	}
	
final_3 :	
	saEvtEventFree(evtEventHandle);
	
final_2 :
	saEvtChannelClose(channelHandle);
	saEvtChannelUnlink(evtHandle,&channelName);
final_1 :
	saEvtFinalize(evtHandle);	
	return ret;
}

