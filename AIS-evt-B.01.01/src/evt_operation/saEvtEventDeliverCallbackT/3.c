/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtEventDeliverCallbackT
 * Description:
 *   saEvtEventDeliverCallbackT is called with a eventDataSize.
 *   Test if the eventDataSize match the ones we published .
 * Line:        P47-21:P47-22
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

SaEvtEventHandleT eventHandleGlobal;
SaSizeT eventDataSizeGobal;
static void evt_deliver_callback (SaEvtSubscriptionIdT subscriptionId,
        SaEvtEventHandleT eventHandle,
	SaSizeT eventDataSize)
{
	eventDataSizeGobal = eventDataSize;
	eventHandleGlobal = eventHandle;
        return;
}

int main(int argc, char *argv[])
{
        SaEvtHandleT    evtHandle;
	SaAisErrorT	evtError;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};
 
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	SaEvtChannelHandleT	channelHandle;
	int ret = SAF_TEST_PASS;
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = NULL,
                .saEvtEventDeliverCallback = evt_deliver_callback
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	char evtdata[] = "evtdata";
	SaEvtEventHandleT evtEventHandle;
	char patternInit[20] = "pattern";
	char pattern[20] = "pattern";
	SaEvtEventPatternT evtPattern = {
				.allocatedSize = 20,
				.patternSize = 20,
				.pattern =  pattern
	};
	SaEvtEventPatternArrayT	evtPatternArray ={
			.allocatedNumber = 1,
			.patternsNumber = 1,
			.patterns = &evtPattern
	};
	
	SaEvtEventPriorityT	priority = SA_EVT_HIGHEST_PRIORITY;
	SaTimeT		retentionTime = SA_TIME_MAX;
	SaNameT		publisherName = {
				.value = "publish",
				.length = strlen("publish")
	};
	SaEvtEventIdT	eventId;
	SaTimeT		publishTime;
		
	SaEvtEventFilterT evtFilter = {
				.filterType = SA_EVT_PASS_ALL_FILTER,
				.filter = evtPattern
	};
	
	SaEvtEventFilterArrayT	filter = { 
				.filtersNumber = 1,
				.filters = &evtFilter
	};
	
	SaEvtSubscriptionIdT 	subscriptionId = 2;
	
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtChannelOpen(evtHandle,&channelName,
				SA_EVT_CHANNEL_CREATE|
				SA_EVT_CHANNEL_PUBLISHER|
				SA_EVT_CHANNEL_SUBSCRIBER,
				SA_TIME_MAX,
				&channelHandle);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpen, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	
	evtError = saEvtEventAllocate(channelHandle,&evtEventHandle);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAllocate, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	
	evtError = saEvtEventAttributesSet(evtEventHandle,
					&evtPatternArray,
					priority,
					retentionTime,
					&publisherName);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAttributesSet, Return value: %s, should "
			" be SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
        }
	
	evtError = saEvtEventSubscribe(channelHandle,&filter,subscriptionId);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventSubscribe, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
        }
	
	evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n",
			get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }
	
	evtError = saEvtEventPublish(evtEventHandle,evtdata,
				strlen(evtdata),&eventId);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventPublish, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }

	evtError = saEvtDispatch(evtHandle, dispatchFlag);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtDispatch, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_4;
        }

	if (eventDataSizeGobal != strlen(evtdata)){
		printf(" Does not conform the expected behaviors!\n");
		printf(" eventDataSize does not match the value we set\n");
		ret = SAF_TEST_FAIL;
		goto final_4;
	}
	
	evtError = saEvtEventAttributesGet(eventHandleGlobal,
					&evtPatternArray,
					&priority,
					&retentionTime,
					&publisherName,
					&publishTime,
					&eventId);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAttributesGet, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_5;
        }
	
	
	if (evtPatternArray.patternsNumber != 1 ){
		printf(" patternArray is not the same as initialize value");
		printf(" patternsNumber should be 1,Returned value :%lld",
			evtPatternArray.patternsNumber);
		ret = SAF_TEST_FAIL;
		goto final_5;	  
	}
	
	if ((strncmp(evtPatternArray.patterns->pattern,patternInit,
	strlen("pattern")) != 0)||evtPatternArray.patterns->patternSize 
		!= 20){
		printf("patternArray is not the same as initialize value");
		printf("patterns is not the same as initialize value");
		ret = SAF_TEST_FAIL;  
	}
	
final_5 :
	saEvtEventFree(eventHandleGlobal);
final_4 :	
	saEvtEventUnsubscribe(channelHandle,subscriptionId);
final_3 :
	saEvtEventFree(evtEventHandle);	
final_2 :
	saEvtChannelClose(channelHandle);	
	saEvtChannelUnlink(evtHandle, &channelName);
final_1 :
	saEvtFinalize(evtHandle);
	return ret;
}







