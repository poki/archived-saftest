/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtEventAttributesSet
 * Description:
 *   Test the api with null publisherName.
 *   Test the corresponding publisherName in attributes are not changed.
 *   Return = SA_AIS_OK 
 * Line:        P40-43:P41-1
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saEvtEventAttributesSet";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
	SaAisErrorT	evtError;
	SaEvtHandleT    evtHandle;
	SaEvtEventHandleT evtEventHandle;
        SaTimeT 	timeout = SA_TIME_MAX;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};
	SaEvtChannelHandleT	channelHandle;
        int ret = SAF_TEST_PASS;
	
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = NULL,
                .saEvtEventDeliverCallback = NULL
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	char patternInit[20] = "pattern";
	char pattern[20] = "pattern";
	SaEvtEventPatternT evtPattern = {
				.allocatedSize = 20,
				.patternSize = 20,
				.pattern =  pattern
	};
	SaEvtEventPatternArrayT	evtPatternArray ={
			.allocatedNumber = 1,
			.patternsNumber = 1,
			.patterns = &evtPattern
	};
	
	SaEvtEventPriorityT	priority = SA_EVT_HIGHEST_PRIORITY;
	SaTimeT		retentionTime = SA_TIME_MAX;
	SaNameT		publisherName = {
				.value = "publish",
				.length = strlen("publish")
	};	
	char publisherInit[] = "publish";
	SaTimeT		publishTime;
	SaEvtEventIdT	eventId;
	
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtChannelOpen(evtHandle,&channelName,
				SA_EVT_CHANNEL_CREATE|
				SA_EVT_CHANNEL_PUBLISHER,
				timeout,
				&channelHandle);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpen, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	evtError = saEvtEventAllocate(channelHandle,&evtEventHandle);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAllocate, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	evtError = saEvtEventAttributesSet(evtEventHandle,
					&evtPatternArray,
					priority,
					retentionTime,
					&publisherName);
	if (evtError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(evtError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}
	
	// test if the attribute is set rightly.
	evtError = saEvtEventAttributesGet(evtEventHandle,
					&evtPatternArray,
					&priority,
					&retentionTime,
					&publisherName,
					&publishTime,
					&eventId);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAttributesGet, Return value: %s, should "
			" be SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}
	
	if (strncmp(publisherName.value,publisherInit,strlen("publish"))!= 0){
		printf(" publishName is not the same as initialize value\n");
		printf(" Returned value is %s,should be publish\n",
			publisherName.value);
		ret = SAF_TEST_FAIL;
		goto final_3;
	}				 
	
	if (publisherName.length != strlen("publish")){
		printf(" publisherName length is not the same as initialize"
			" value\n");
		printf(" Returned value is %d ,should be %d \n",
			publisherName.length,(int)strlen("publish"));
		ret = SAF_TEST_FAIL;
		goto final_3;	
	}
	
	if (evtPatternArray.patternsNumber != 1 ){
		printf(" patternArray is not the same as initialize value");
		printf(" patternsNumber should be 1,Returned value :%lld",
			evtPatternArray.patternsNumber);
		ret = SAF_TEST_FAIL;
		goto final_3;	  
	}
	
	if ((strncmp(evtPatternArray.patterns->pattern,patternInit,
	strlen("pattern")) != 0)||evtPatternArray.patterns->patternSize 
		!= 20){
		printf("patternArray is not the same as initialize value");
		printf("patterns is not the same as initialize value");
		ret = SAF_TEST_FAIL;  
		goto final_3;  
	}
	
	// set publisherName and patterArray null and test if the api don't
	// change the publisherName.
	evtError = saEvtEventAttributesSet(evtEventHandle,
					&evtPatternArray,
					priority,
					retentionTime,
					NULL);
	if (evtError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(evtError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}
	
	evtError = saEvtEventAttributesGet(evtEventHandle,
					&evtPatternArray,
					&priority,
					&retentionTime,
					&publisherName,
					&publishTime,
					&eventId);
	if (evtError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtEventAttributesGet, Return value: %s, should "
			" be SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_3;
	}
	
	if (strncmp(publisherName.value,publisherInit,strlen("publish"))!= 0){
		printf(" publishName is not the same as initialize value\n");
		printf(" Returned value is %s,should be publish\n",
			publisherName.value);
		ret = SAF_TEST_FAIL;
		goto final_3;
	}				 
	
	if (publisherName.length != strlen("publish")){
		printf(" publisherName length is not the same as initialize"
			" value\n");
		printf(" Returned value is %d ,should be %d \n",
			publisherName.length,(int)strlen("publish"));
		ret = SAF_TEST_FAIL;
	}
	
final_3 :
	saEvtEventFree(evtEventHandle);
	
final_2 :
	saEvtChannelClose(channelHandle);
	saEvtChannelUnlink(evtHandle,&channelName);
final_1 :
	saEvtFinalize(evtHandle);	
	return ret;
}
