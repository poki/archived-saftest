/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *      Yu, Ling <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtEventAttributesSet
 * Description:
 *   Test if saEvtEventAttributesSet can return SA_AIS_ERR_BAD_HANDLE
 *   when the evtHandle that was passed to the saEvtChannelOpenAsync() functions
 *   has already been finalized.
 *   Return = SA_AIS_ERR_BAD_HANDLE
 * Line:        P41-21:P41-22
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

SaEvtChannelHandleT channelHandleGlobal;
static void evt_channel_open_callback (SaInvocationT invocation,
		        SaEvtChannelHandleT channelHandle,
			SaAisErrorT error)
{
	channelHandleGlobal = channelHandle;
        return;
}

int main(int argc, char *argv[])
{
        SaEvtHandleT    evtHandle;
	SaAisErrorT	evtError;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};
 
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	SaEvtChannelHandleT	channelHandle;
	int ret = SAF_TEST_PASS;
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = evt_channel_open_callback,
                .saEvtEventDeliverCallback = NULL
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaEvtEventHandleT evtEventHandle;
	char pattern[20] = "pattern";
	SaEvtEventPatternT evtPattern = {
				.allocatedSize = 20,
				.patternSize = 20,
				.pattern =  pattern
	};
	SaEvtEventPatternArrayT	evtPatternArray ={
			.allocatedNumber = 1,
			.patternsNumber = 1,
			.patterns = &evtPattern
	};
	
	SaEvtEventPriorityT	priority = SA_EVT_HIGHEST_PRIORITY;
	SaTimeT		retentionTime = SA_TIME_MAX;
	SaNameT		publisherName = {
				.value = "publish",
				.length = strlen("publish")
	};
		
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtChannelOpenAsync(evtHandle, 1, &channelName,
				SA_EVT_CHANNEL_CREATE|
				SA_EVT_CHANNEL_PUBLISHER|
				SA_EVT_CHANNEL_SUBSCRIBER);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpenAsync, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtSelectionObjectGet, Return value: %s, should"
			" be SA_AIS_OK\n",
			get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }

	evtError = saEvtDispatch(evtHandle, dispatchFlag);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtDispatch, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }

	evtError = saEvtEventAllocate(channelHandleGlobal,&evtEventHandle);
        if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtEventAllocate, Return value: %s, should be"
		       " SA_AIS_OK\n",get_error_string(evtError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}

        evtError = saEvtFinalize(evtHandle);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtFinalize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}

	 evtError = saEvtEventAttributesSet(evtEventHandle,
						 &evtPatternArray,
						 priority,
						 retentionTime,
						 &publisherName);
	if (evtError != SA_AIS_ERR_BAD_HANDLE){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saEvtEventAttributesSet, Return value: %s, should "
		       " be SA_AIS_ERR_BAD_HANDLE\n",
		       get_error_string(evtError));
		ret = SAF_TEST_FAIL;
	}

	//clean up the unlinked channel
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
	if (evtError != SA_AIS_OK){
		return ret;
	}
	evtError = saEvtChannelOpen(evtHandle,&channelName,
                                SA_EVT_CHANNEL_PUBLISHER,
				SA_TIME_MAX,
				&channelHandle);
        if (evtError != SA_AIS_OK){
                goto final_1;
        }
	
final_2 :
	saEvtChannelClose(channelHandle);
	saEvtChannelUnlink(evtHandle, &channelName);
	
final_1 :
	saEvtFinalize(evtHandle);
	return ret;
}
