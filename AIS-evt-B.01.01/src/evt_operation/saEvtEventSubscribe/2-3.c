/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Wang, Xuelian <xuelian.wang@intel.com>
 *      Yu, Ling <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtEventSubscribe
 * Description:
 *   Call the api with valid filters in which the filtertype is set to 
 *   SA_EVT_EXACT_FILTER.
 * Line:        P51-1:P51-5
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

SaEvtEventHandleT eventHandleGlobal;
static void evt_deliver_callback (SaEvtSubscriptionIdT subscriptionId,
        SaEvtEventHandleT eventHandle,
	SaSizeT eventDataSize)
{
	eventHandleGlobal = eventHandle;
        return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saEvtEventSubscribe";
        SaEvtHandleT    evtHandle;
	SaAisErrorT	evtError;
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};
 
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	
	fd_set readSet;
	int selectRet;
	SaSelectionObjectT 	selObject;
	SaEvtChannelHandleT	channelHandle;
	int ret = SAF_TEST_PASS;
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = NULL,
                .saEvtEventDeliverCallback = evt_deliver_callback
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	int i;
	char evtdata[] = "evtdata";
	SaEvtEventHandleT evtEventHandle;
	char filter_pattern[6][20] ={"xyz","aBc","YX","abcd","xy","abcd"};
	char evt_pattern[6][20]={"xyz","aBc","YX","abcda","Xy","bc"};
	
	SaEvtEventPatternT evtPattern = {
				.allocatedSize = 20,
				.patternSize = 20,
				.pattern =  NULL
	};
	SaEvtEventPatternT filterPattern = {
				.allocatedSize = 20,
				.patternSize = 20,
				.pattern =  NULL
	};
	SaEvtEventPatternArrayT	evtPatternArray = {
			.allocatedNumber = 1,
			.patternsNumber = 1,
			.patterns = &evtPattern
	};
	
	SaEvtEventPatternArrayT	evtPatternArrayGet = {
			.allocatedNumber = 0,
			.patternsNumber = 0,
			.patterns = NULL
	};

	SaEvtEventPriorityT	priority = SA_EVT_HIGHEST_PRIORITY;
	SaTimeT		retentionTime = 0;
	SaTimeT		publishTime;
	SaNameT		publisherName = {
				.value = "publish",
				.length = strlen("publish")
	};
	SaEvtEventIdT	eventId;
	struct timeval tv = {
			.tv_sec = 5,
			.tv_usec = 0
	};	
	SaEvtEventFilterT evtFilter = {
				.filterType = SA_EVT_SUFFIX_FILTER,
				.filter = filterPattern
	};
	
	SaEvtEventFilterArrayT	filter = {
				.filtersNumber = 1,
				.filters = &evtFilter
	};
	
	SaEvtSubscriptionIdT 	subscriptionId = 1;
	
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	for(i = 0; i < 6; i++){
		evtError = saEvtChannelOpen(evtHandle,&channelName,
					SA_EVT_CHANNEL_CREATE|
					SA_EVT_CHANNEL_PUBLISHER|
					SA_EVT_CHANNEL_SUBSCRIBER,
					SA_TIME_MAX,
					&channelHandle);
		if (evtError != SA_AIS_OK){
			printf("  Does not conform the expected behaviors!\n");
               		printf("  saEvtChannelOpen, Return value: %s, should be"
				" SA_AIS_OK\n",get_error_string(evtError));
                	ret = SAF_TEST_UNRESOLVED;
			goto final_1;
		}
	
		evtError = saEvtEventAllocate(channelHandle,&evtEventHandle);
		if (evtError != SA_AIS_OK){
        	        printf("  Does not conform the expected behaviors!\n");
                	printf("  saEvtEventAllocate, Return value: %s, should"
				" be SA_AIS_OK\n",get_error_string(evtError));
                	ret = SAF_TEST_UNRESOLVED;
			goto final_2;
        	}
		evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
		if (evtError != SA_AIS_OK){
        	        printf("  Does not conform the expected behaviors!\n");
                	printf("  saEvtSelectionObjectGet, Return value: %s, "
				"should be SA_AIS_OK\n",
				get_error_string(evtError));
                	ret = SAF_TEST_UNRESOLVED;
			goto final_3;
        	}
		
		printf("Test filter %s, pattern %s...\n",
			filter_pattern[i],evt_pattern[i]);
 
		evtPattern.pattern = evt_pattern[i];
		evtPattern.patternSize = strlen(evt_pattern[i]);
		evtFilter.filter.pattern = filter_pattern[i];
		evtFilter.filter.patternSize = strlen(filter_pattern[i]);
		evtError = saEvtEventSubscribe(channelHandle,&filter,
						++subscriptionId);
		if (evtError != SA_AIS_OK){
                	printf("  Does not conform the expected behaviors!\n");
                	printf("  saEvtEventSubscribe, Return value: %s, "
				"should be SA_AIS_OK\n",
				get_error_string(evtError));
                	ret = SAF_TEST_UNRESOLVED;
			goto final_3;
        	}
		evtError = saEvtEventAttributesSet(evtEventHandle,
					&evtPatternArray,
					priority,
					retentionTime,
					&publisherName);
		if (evtError != SA_AIS_OK){
                	printf("  Does not conform the expected behaviors!\n");
                	printf("  saEvtEventAttributesSet, Return value: %s, "
			"should be SA_AIS_OK\n",get_error_string(evtError));
                	ret = SAF_TEST_UNRESOLVED;
			goto final_4;
        	}
		evtError = saEvtEventPublish(evtEventHandle, evtdata,
				strlen(evtdata),&eventId);
		if (evtError != SA_AIS_OK){
                	printf("  Does not conform the expected behaviors!\n");
                	printf("  saEvtEventPublish, Return value: %s, "
			"should be SA_AIS_OK\n",get_error_string(evtError));
                	ret = SAF_TEST_UNRESOLVED;
			goto final_4;
        	}
		
		FD_ZERO(&readSet);
        	FD_SET(selObject, &readSet);
        	selectRet = select(selObject + 1, &readSet, NULL, NULL, &tv);
		//test the matched situations.
		if (i < 3){
			if(selectRet <= 0 ){
        	       		printf("  select error! no event is received!"
					"\nFilter is %s, Event Pattern is %s\n",
					filter_pattern[i],evt_pattern[i]);
               			ret = SAF_TEST_FAIL;
				goto final_4;
        		}
			evtError = saEvtDispatch(evtHandle, dispatchFlag);
        	        if (evtError != SA_AIS_OK){
				printf("  Does not conform the expected "
					"behaviors!\nsaEvtDispatch, Return "
					"value: %s, should be SA_AIS_OK\n",
					get_error_string(evtError));
				ret = SAF_TEST_UNRESOLVED;
				goto final_4;
			}
		
			evtError = saEvtEventAttributesGet(eventHandleGlobal,
					&evtPatternArrayGet,
					&priority,
					&retentionTime,
					&publisherName,
					&publishTime,
					&eventId);
			if (evtError != SA_AIS_OK){
				printf("  Does not conform the expected"
					" behaviors!\n");
               			printf("  %s, Return value: %s, should be"
					" SA_AIS_OK\n",
					apiName, get_error_string(evtError));
               			ret = SAF_TEST_UNRESOLVED;
				saEvtEventFree(eventHandleGlobal);
				goto final_4;
			}
			if (evtPatternArrayGet.patternsNumber != 1 ){
				printf(" patternArray is not the same as "
					"initialize value\n");
				printf(" patternsNumber should be 1,Returned "
					"value :%lld\n",
					evtPatternArrayGet.patternsNumber);
				ret = SAF_TEST_FAIL;
				saEvtEventFree(eventHandleGlobal);
				goto final_4;
			}

			if (strncmp(evtPatternArrayGet.patterns->pattern,
	     			evtPatternArray.patterns->pattern,
				evtPatternArray.patterns->patternSize) != 0){
				printf("patternArray is not the same as the "
					"fit pattern, initial=%s, get=%s\n",
					evtPatternArray.patterns->pattern,
					evtPatternArrayGet.patterns->pattern);
				ret = SAF_TEST_FAIL;
				saEvtEventFree(eventHandleGlobal);
				goto final_4;
			}
			saEvtEventFree(eventHandleGlobal);
		}
		//test the unmatched situations.
		else{
			if(selectRet > 0 ){
        	       		printf("  select error! Event is received "
					"mistakenly !\nFilter is %s, Event "
					"Pattern is %s\n",
					filter_pattern[i],evt_pattern[i]);
               			ret = SAF_TEST_UNRESOLVED;
				goto final_4;
			}
		}
final_4:		
		saEvtEventUnsubscribe(channelHandle,subscriptionId);
final_3 :
		saEvtEventFree(evtEventHandle);
final_2 :
		saEvtChannelClose(channelHandle);
		saEvtChannelUnlink(evtHandle, &channelName);
	}
final_1 :
	saEvtFinalize(evtHandle);
	return ret;
}
