/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtEventSubscribe
 * Description:
 *   Call the api with null filters and other valid params.
 *   Return = SA_AIS_ERR_INVALID_PARAM
 * Line:        P51-1:P51-5
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

static void evt_deliver_callback (SaEvtSubscriptionIdT subscriptionId,
        SaEvtEventHandleT eventHandle,
	SaSizeT eventDataSize)
{
	saEvtEventFree(eventHandle);
        return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saEvtEventSubscribe";
	SaAisErrorT     expectedReturn= SA_AIS_ERR_INVALID_PARAM;
	SaAisErrorT	evtError;
	SaEvtHandleT    evtHandle;
	
	SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = NULL,
                .saEvtEventDeliverCallback = evt_deliver_callback
        };
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT		channelName = {
			.value = "channel",
			.length = strlen("channel")
	};
 	
	SaEvtChannelHandleT	channelHandle;
	SaEvtSubscriptionIdT 	subscriptionId = 2;
	
	int ret = SAF_TEST_PASS;
	
	evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtChannelOpen(evtHandle,&channelName,
				SA_EVT_CHANNEL_CREATE|
				SA_EVT_CHANNEL_SUBSCRIBER,
				SA_TIME_MAX,
				&channelHandle);
	if (evtError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpen, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	evtError = saEvtEventSubscribe(channelHandle,NULL,subscriptionId);
	if (evtError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(evtError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
	}
	
	saEvtChannelClose(channelHandle);	
	saEvtChannelUnlink(evtHandle, &channelName);
final_1 :
	saEvtFinalize(evtHandle);
	return ret;
}
