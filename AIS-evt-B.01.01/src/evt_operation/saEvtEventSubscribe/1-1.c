/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtEventSubscribe
 * Description:
 *   Call the api with invaid channelhandle which is not obtained 
 *   previously by the invocatin of one of the saEvtChannelOpen or
 *   saEvtChannelOpenCallback.
 *   Return = SA_AIS_ERR_BAD_HANDLE
 * Line:        P50-40:P50-43
 */

#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saEvtEventSubscribe";
	SaAisErrorT     expectedReturn= SA_AIS_ERR_BAD_HANDLE;
	SaAisErrorT	evtError;
	int ret = SAF_TEST_PASS;
	
	SaEvtChannelHandleT	channelHandle;
	char pattern[20] = "pattern";
	SaEvtEventPatternT evtPattern = {
				.allocatedSize = 20,
				.patternSize = 20,
				.pattern =  pattern
	};
	SaEvtEventFilterT evtFilter = {
				.filterType = SA_EVT_PASS_ALL_FILTER,
				.filter = evtPattern
	};
	
	SaEvtEventFilterArrayT	filter = { 
				.filtersNumber = 1,
				.filters = &evtFilter
	};
	
	SaEvtSubscriptionIdT 	subscriptionId = 2;
	
	evtError = saEvtEventSubscribe(channelHandle,&filter,subscriptionId);
	if (evtError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(evtError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
	}
	
	return ret;
}
