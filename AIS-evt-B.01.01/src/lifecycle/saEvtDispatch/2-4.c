/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtDispatch
 * Description:
 *   Dispatch is called with an dispatch flag other than "SA_DISPATCH_ONE, 
 *   SA_DISPATCH_ALL or SA_DISPATCH_BLOCKING".
 *   Return = SA_AIS_ERR_INVALID_PARAM.
 * Line:        P27-38:P27-42
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

int callbackFlag = 0;

static void evt_channel_open_callback (SaInvocationT invocation,
        SaEvtChannelHandleT evtHandle,
        SaAisErrorT error)
{
	callbackFlag ++;
	saEvtChannelClose(evtHandle);
        return;
}

static void evt_deliver_callback (SaEvtSubscriptionIdT subscriptionId,
        SaEvtEventHandleT eventHandle,
	SaSizeT eventDataSize)
{
        return;
}
int main(int argc, char *argv[])
{
	char apiName[] = "saEvtDispatch";
	SaAisErrorT     expectedReturn= SA_AIS_ERR_INVALID_PARAM;
        SaEvtHandleT   evtHandle;
        SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = evt_channel_open_callback,
                .saEvtEventDeliverCallback = evt_deliver_callback
        };
	SaAisErrorT	evtError;
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaDispatchFlagsT dispatchFlag = 0xff;
	
	fd_set readSet;
	int selectRet;
	SaNameT channelName ={
		.value = "channelName",
		.length = strlen("channelName") 
	};
	SaSelectionObjectT 	selObject;
        int ret = SAF_TEST_PASS;
	

        evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" saEvtSelectionObjectGet\n",
			get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
	evtError = saEvtChannelOpenAsync(evtHandle,1,
					&channelName,
					SA_EVT_CHANNEL_CREATE|
					SA_EVT_CHANNEL_PUBLISHER);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpenAsync, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                saEvtDispatch(evtHandle,dispatchFlag);
		goto final_2;
        }

	evtError = saEvtDispatch(evtHandle, dispatchFlag);
	if (evtError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(evtError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
	}
	
final_2:
	saEvtChannelUnlink(evtHandle, &channelName);
final_1 :
	saEvtFinalize(evtHandle);
	return ret;
}

