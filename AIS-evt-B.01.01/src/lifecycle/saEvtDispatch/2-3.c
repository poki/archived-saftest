/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtDispatch
 * Description:
 *   Dispatch correctly, Set dispatch Flag to SA_DISPATCH_BLOCKING. The 
 *   function completes successfully.
 *   Return = SA_AIS_OK
 * Line:        P27-38:P27-42
 */

#include <stdio.h>
#include <sys/time.h>
#include <sys/select.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

int callbackFlag = 0;
SaAisErrorT dispatchReturn;
sem_t sem_start_dispatch;
sem_t sem_end_dispatch;

static void evt_channel_open_callback (SaInvocationT invocation,
        SaEvtChannelHandleT evtHandle,
        SaAisErrorT error)
{
	callbackFlag = 1;
	saEvtChannelClose(evtHandle);
        return;
}

static void evt_deliver_callback (SaEvtSubscriptionIdT subscriptionId,
        SaEvtEventHandleT eventHandle,
	SaSizeT eventDataSize)
{
        return;
}

static void *dispatch_thread(void *arg)
{
	callbackFlag = 0;
	sem_post(&sem_start_dispatch);
	dispatchReturn = saEvtDispatch(* ((SaEvtHandleT *) arg),
					SA_DISPATCH_BLOCKING);
	sem_post(&sem_end_dispatch);
	return NULL;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saEvtDispatch";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaEvtHandleT   evtHandle;
        SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = evt_channel_open_callback,
                .saEvtEventDeliverCallback = evt_deliver_callback
        };
	SaAisErrorT	evtError;
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT channelName ={
		.value = "channelName",
		.length = strlen("channelName") 
	};
	SaSelectionObjectT 	selObject;
        int ret = SAF_TEST_PASS;
	
	pthread_t thread_id;
	int pthreadError;

        evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	sem_init(&sem_start_dispatch, 0, 0);
	sem_init(&sem_end_dispatch,0,0);
	pthreadError = pthread_create(&thread_id,
					NULL, 
					dispatch_thread, 
					(void*)&evtHandle);
	if (pthreadError){
		printf("	invoke \"pthread_create\" has error,"
				" return %s\n", strerror(pthreadError));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" saEvtSelectionObjectGet\n",
			get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
        }
	
	evtError = saEvtChannelOpenAsync(evtHandle,1,
					&channelName,
					SA_EVT_CHANNEL_CREATE);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpenAsync, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
        }
	
	/*
	 * wait for the callback to have been called
	 */
	while (!callbackFlag) 
		sleep(1);
	
	evtError = saEvtChannelUnlink(evtHandle, &channelName);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelUnlink, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
        }
	
final :
	evtError = saEvtFinalize(evtHandle);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtFinalize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
        }
	
	if (ret != SAF_TEST_PASS)
		return ret;

	sem_wait(&sem_end_dispatch);
	
	if ( callbackFlag <= 0){
		printf("	invoke %s has error, pending callbacks are "
				"not invoked\n", apiName);
		return SAF_TEST_FAIL;
	}
	
	if ( dispatchReturn != expectedReturn ){
		printf(" Does not conform the expected behaviors!\n");
                printf(" %s, Return value :%s, should be %s\n", 
			apiName,
			get_error_string(dispatchReturn),
			get_error_string(expectedReturn));
		return SAF_TEST_FAIL;
	}
	return ret;
}


