/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtSelectionObjectGet
 * Description:   
 *   Call saEvtSelectionObjectGet related to the evtHandle , After finalizing
 *   we call select to detect whether the selectionObject is invaild .the 
 *   returned value should be -1.
 *   Return = SA_AIS_OK
 * Line:        P26-35:P26-38
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/time.h>
#include <string.h>
#include <unistd.h>
#include "saEvt.h"
#include "saf_test.h"

static void evt_channel_open_callback (SaInvocationT invocation,
        SaEvtChannelHandleT evtHandle,
        SaAisErrorT error)
{
	saEvtChannelClose(evtHandle);
        return;
}

static void evt_deliver_callback (SaEvtSubscriptionIdT subscriptionId,
        SaEvtEventHandleT eventHandle,
	SaSizeT eventDataSize)
{
        return;
}
int main(int argc, char *argv[])
{
	char apiName[] = "saEvtSelectionObjectGet";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaEvtHandleT   evtHandle;
        SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = evt_channel_open_callback,
                .saEvtEventDeliverCallback = evt_deliver_callback
        };
	SaAisErrorT	evtError;
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ONE;
	
	fd_set readSet;
	int selectRet;
	SaNameT channelName ={
		.value = "channelName",
		.length = strlen("channelName") 
	};
	SaSelectionObjectT 	selObject;
        int ret = SAF_TEST_PASS;
	

        evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	evtError = saEvtSelectionObjectGet(evtHandle, &selObject);
	if (evtError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(evtError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
		goto final_1;
	}
	
	evtError = saEvtChannelOpenAsync(evtHandle,1,
					&channelName,
					SA_EVT_CHANNEL_CREATE);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpenAsync, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
        }
	
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_FAIL;
                goto final_2;
        }
	
	saEvtDispatch(evtHandle, dispatchFlag);


        evtError = saEvtChannelUnlink(evtHandle,&channelName);
        if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelUnlink, Return value: %s, should be"
                        " SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
                saEvtFinalize(evtHandle);
                return ret;
        }

	evtError = saEvtFinalize(evtHandle);
	if (evtError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(evtError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
        }
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if (selectRet > 0){
                printf("  select error!\n");
                ret = SAF_TEST_FAIL;
        }
	return ret;
	
final_2:
	saEvtChannelUnlink(evtHandle, &channelName);
final_1 :
	saEvtFinalize(evtHandle);
	return ret;
}
