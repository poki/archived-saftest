/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtInitialize
 * Description:   
 *  	Initialize the event service with a version code. Suppose the 
 *   	returned major version number is  equal to the required 
 *   	majorversion number
 *   	Return = SA_AIS_OK
 * Line:        P24-36:P25-4
 */

#include <stdio.h>
#include <stdlib.h>
#include "saEvt.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saEvtInitialize";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaEvtHandleT   evtHandle;
        SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = NULL,
                .saEvtEventDeliverCallback = NULL
        };
	SaAisErrorT	evtError;
	SaUint8T 	majorVersion = AIS_B_VERSION_MAJOR;
	SaUint8T	minorVersion = AIS_B_VERSION_MINOR;
	
        SaVersionT      evtVersion = {
                .majorVersion = majorVersion,
                .minorVersion = minorVersion,
                .releaseCode = AIS_B_RELEASE_CODE
        };

        int ret = SAF_TEST_PASS;
	
        evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != expectedReturn){
                printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(evtError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
        }
	else {
		if (evtVersion.majorVersion != majorVersion){
                        printf("  Does not conform the expected behaviors!\n");
                        printf("  %s's major version code:%d,should be %d\n",
                                apiName,
                                evtVersion.majorVersion,
                                majorVersion);
                        ret = SAF_TEST_FAIL;
			goto final;
                }
                                                                                                               
                if (evtVersion.releaseCode != AIS_B_RELEASE_CODE){
                        printf("  Does not conform the expected behaviors!\n");
                        printf("  %s's release code:%d,should be %d\n",
                                apiName,
                                evtVersion.releaseCode,
                                AIS_B_RELEASE_CODE);
                        ret = SAF_TEST_FAIL;
			goto final;
                }
                                                                                                               
                if (evtVersion.minorVersion != AIS_B_VERSION_MINOR){
                        printf("  Does not conform the expected behaviors!\n");
                        printf("  %s's minor version code:%d,should be %d\n",
                                apiName,
                                evtVersion.minorVersion,
				AIS_B_VERSION_MINOR);
			ret = SAF_TEST_FAIL;
		}
final :
		saEvtFinalize(evtHandle);
	}
	return ret;
}



