/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtInitialize
 * Description:   
 *  	Initialize the event service with correct parameter such as 
 *   	normal handle,verion and callback, and callback is not NULL.
 *   	Return = SA_AIS_OK
 * Line:        P24-19:P24-24
 */

#include <stdio.h>
#include <stdlib.h>
#include "saEvt.h"
#include "saf_test.h"

static void evt_channel_open_callback (SaInvocationT invocation,
        SaEvtChannelHandleT evtHandle,
        SaAisErrorT error)
{
        return;
}

static void evt_deliver_callback (SaEvtSubscriptionIdT subscriptionId,
        SaEvtEventHandleT eventHandle,
	SaSizeT eventDataSize)
{
        return;
}


int main(int argc, char *argv[])
{
	char apiName[] = "saEvtInitialize";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaEvtHandleT   evtHandle;
        SaEvtCallbacksT evtCallback = {
                .saEvtChannelOpenCallback  = evt_channel_open_callback,
                .saEvtEventDeliverCallback = evt_deliver_callback
        };
	SaAisErrorT	evtError;
        SaVersionT      evtVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

        int ret = SAF_TEST_PASS;

        evtError = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
        if (evtError != expectedReturn){
                printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(evtError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
        }
	else
		saEvtFinalize(evtHandle);

        return ret;
}

