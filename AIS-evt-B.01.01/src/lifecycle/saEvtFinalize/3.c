/*
 * Copyright (c) 2005, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *        Wang, AnLi <an.li.wang@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saEvtFinalize
 * Description:   
 *  After finalizing, we call event channels associated with the evtHandle. 
 *  Return = SA_AIS_ERR_BAD_HANDLE
 * Line:        P29-4:P29-6
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "saAis.h"
#include "saEvt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE SA_TIME_MAX

static void 
callback_channel_open(SaInvocationT invocation,
		      SaEvtChannelHandleT channelHandle,
		      SaAisErrorT error)

{
	return;
}

static void 
callback_event_deliver(SaEvtSubscriptionIdT sub_id,
			SaEvtEventHandleT event_handle,
			SaSizeT eventDataSize)

{
	return; 
}

int main(int argc, char *argv[])
{
	SaAisErrorT	error;
	SaEvtHandleT	evtHandle;

	SaEvtCallbacksT evtCallback = {
		.saEvtChannelOpenCallback
		= (SaEvtChannelOpenCallbackT)callback_channel_open,
		.saEvtEventDeliverCallback
		= (SaEvtEventDeliverCallbackT)callback_event_deliver
	};

	SaVersionT	evtVersion = {
		.majorVersion = AIS_B_VERSION_MAJOR,
		.minorVersion = AIS_B_VERSION_MINOR,
		.releaseCode = AIS_B_RELEASE_CODE 
	};
	SaNameT 	channelName = {
		.length = strlen("channelName"),
		.value = "channelName"
	};
	
	SaTimeT	saTimeout = TIMEOUT_VALUE;
	SaEvtChannelOpenFlagsT channelOpenFlags = 
					SA_EVT_CHANNEL_CREATE;
	SaEvtChannelHandleT channelHandle;
	

	int ret = SAF_TEST_PASS;

	error = saEvtInitialize(&evtHandle, &evtCallback, &evtVersion);
	if (error != SA_AIS_OK) {
		printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	error = saEvtChannelOpen(evtHandle,
					&channelName,
					channelOpenFlags,
					saTimeout,
					&channelHandle
					);
	if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelOpen, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		saEvtFinalize(evtHandle);
		return ret;
        }
	
	error = saEvtFinalize(evtHandle);
      if (error != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtFinalize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(error));
		ret = SAF_TEST_UNRESOLVED;
		return ret;
        }

	error = saEvtChannelClose(channelHandle);
        if (error != SA_AIS_ERR_BAD_HANDLE) {
                printf("  Does not conform the expected behaviors!\n");
                printf("  saEvtChannelClose, Return value: %s,"
			" should be SA_AIS_ERR_BAD_HANDLE\n",
			 get_error_string(error));
                ret = SAF_TEST_FAIL;
        }

	return ret;
}
