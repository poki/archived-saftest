/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Yu, Ling <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptCheckpointSynchronizeAsync
 * Description:   
 *                The saCkptIntialize has been invoked without the 
 *		saCkptCheckpointSynchronizeCallback(), invoke
 *		saCkptCheckpointSynchronize with normal params, 
 *		This api return value should be SA_AIS_ERR_INIT.
 * Line:        P61-17:P61-19
 */

#include <stdio.h>
#include <string.h>

#include "saCkpt.h"
#include "saf_test.h"

#define INVOCATION_NUM 10

static void ckpt_open_callback (SaInvocationT invocation,
                                SaCkptCheckpointHandleT checkpoint_handle,
                                SaAisErrorT error)
{
        return ;
}

int main(int argc, char *argv[])
{
        SaAisErrorT     error;
        SaCkptHandleT    ckptHandle;
        SaCkptCallbacksT ckptCallback = {
                .saCkptCheckpointOpenCallback        = ckpt_open_callback,
                .saCkptCheckpointSynchronizeCallback = NULL
        };
        SaVersionT      version = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
        int ret = SAF_TEST_UNKNOWN;

        char name_open[] = "checkpoint";
        SaNameT ckpt_name;
        SaCkptCheckpointCreationAttributesT ckpt_create_attri;
        SaCkptCheckpointHandleT checkpoint_handle;

        char initData[] = "section data";
        int initDataSize = strlen(initData);
        SaCkptSectionIdT section_id = {
                                .id = "section",
                                .idLen = strlen("section")
        };
        SaCkptSectionCreationAttributesT section_attr = {
                                .sectionId = &section_id,
                                .expirationTime = SA_TIME_END
        };

        SaInvocationT invocation = INVOCATION_NUM;

        error = saCkptInitialize(&ckptHandle, &ckptCallback, &version);
        if (error != SA_AIS_OK){
                printf("  saCkptInitialize, Return value: %s, should be "
                                "SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto out;
        }

        ckpt_name.length = sizeof(name_open) ;
        memcpy (ckpt_name.value, name_open, ckpt_name.length);

        ckpt_create_attri.creationFlags = SA_CKPT_WR_ACTIVE_REPLICA;
        ckpt_create_attri.retentionDuration = SA_TIME_END;
        ckpt_create_attri.checkpointSize = 1000;
        ckpt_create_attri.maxSectionSize = 100;
        ckpt_create_attri.maxSections = 10;
        ckpt_create_attri.maxSectionIdSize = 10;


        error = saCkptCheckpointOpen (  ckptHandle,
                                        &ckpt_name,
                                        &ckpt_create_attri ,
                                        SA_CKPT_CHECKPOINT_CREATE
                                        |SA_CKPT_CHECKPOINT_WRITE,
                                        SA_TIME_MAX,
                                        &checkpoint_handle);
        if ( error != SA_AIS_OK){
                printf("  can't open checkpoint, return %s \n",
                                       get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final_2;
        }

        error = saCkptSectionCreate(checkpoint_handle,
                                &section_attr,
                                initData,
                                initDataSize);
        if ( error != SA_AIS_OK){
                printf("  create section, Return value: %s",
                                get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final_1;
        }

        error = saCkptCheckpointSynchronizeAsync(checkpoint_handle,
                                                invocation);
        if ( error != SA_AIS_ERR_INIT ){
                printf("  saCkptCheckpointSynchronizeAsync, Return value: %s",
                                get_error_string(error));
                ret = SAF_TEST_FAIL;
                goto final_1;
        }
        ret = SAF_TEST_PASS;

final_1:
        saCkptCheckpointClose(checkpoint_handle);
        saCkptCheckpointUnlink(ckptHandle, &ckpt_name);
final_2:
        saCkptFinalize(ckptHandle);
out:
        return ret;
}

