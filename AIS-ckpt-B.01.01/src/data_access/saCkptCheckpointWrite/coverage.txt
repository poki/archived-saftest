This file defines the coverage for the saCkptCheckpointWrite conformance testing.

Assertion       Status
1		YES
2-1		YES
2-2		YES
3-1		YES
3-2		YES
4		YES
5-1		YES
5-2		YES
6		YES
7-1		YES
7-2		YES
8		NO
9		NO
10		NO
11		YES
12		YES
13		NO
14		NO
15		NO
16-1		YES
16-2		YES
17		YES
18		YES
19		NO
20		NO
21-1		YES
21-2		YES
22		YES

