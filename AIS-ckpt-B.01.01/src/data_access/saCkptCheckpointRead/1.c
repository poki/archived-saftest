/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptCheckpointRead
 * Description:
 *		Invoke saCkptCheckpointRead with valid checkpointHandle
 *		and other valid params.
 *		Returned value should be SA_AIS_OK.
 * Line:        P58-23:P58-23
 */

#include <stdio.h>
#include <string.h>

#include "saCkpt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE 	SA_TIME_MAX
#define CHECKPOINT_SIZE	10

int main(int argc, char *argv[])
{
	char	apiName[] = "saCkptCheckpointRead";
	SaAisErrorT     expectedReturn=  SA_AIS_OK;
        SaCkptHandleT   ckptHandle;
	SaCkptCallbacksT ckptCallback = {
        	.saCkptCheckpointOpenCallback  = NULL,
                .saCkptCheckpointSynchronizeCallback = NULL
	};
	SaAisErrorT	ckptError;
	SaVersionT      ckptVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	checkpointName = {
		.length = strlen("checkpointName"),
		.value 	= "checkpointName"
	};
	SaCkptCheckpointCreationAttributesT   checkpointCreationAttributes = {
		.creationFlags  = SA_CKPT_WR_ALL_REPLICAS,
		.checkpointSize = CHECKPOINT_SIZE * CHECKPOINT_SIZE,
		.retentionDuration = TIMEOUT_VALUE,
		.maxSections	= CHECKPOINT_SIZE,
		.maxSectionSize	= CHECKPOINT_SIZE,
		.maxSectionIdSize = CHECKPOINT_SIZE   
	};
	SaCkptCheckpointOpenFlagsT	checkpointOpenFlags = 
			SA_CKPT_CHECKPOINT_CREATE | SA_CKPT_CHECKPOINT_READ
				| SA_CKPT_CHECKPOINT_WRITE;
	SaTimeT		timeout = TIMEOUT_VALUE;
	SaCkptCheckpointHandleT		ckptCheckpointHandle;
	
	char initData[] = "section";
	int initDataSize = strlen(initData);
	SaCkptSectionIdT sectionIdTmp = {
				.id = "sectionId",
				.idLen = strlen("sectionId")
	};
	SaCkptSectionCreationAttributesT sectionCreateAttributes = {
				.sectionId = &sectionIdTmp, 
				.expirationTime = SA_TIME_MAX		
	};
	
	char dataBuf[10] = ""; 
	SaCkptIOVectorElementT  ckptIoVectorElement = {
				.dataBuffer = dataBuf,
				.dataSize = 10,
				.dataOffset = 0,
				.readSize = 0
	};
	SaUint32T 	errorIndex;
	int ret = SAF_TEST_PASS;
	
	ckptIoVectorElement.sectionId = sectionIdTmp;
	ckptError = saCkptInitialize(&ckptHandle, &ckptCallback, &ckptVersion);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	ckptError = saCkptCheckpointOpen(ckptHandle,
					&checkpointName,
					&checkpointCreationAttributes,
					checkpointOpenFlags,
					timeout,
					&ckptCheckpointHandle);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptCheckpointOpen,Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(ckptError));
		ret = SAF_TEST_UNRESOLVED;
		saCkptFinalize(ckptHandle);	
		return ret;
	}

	ckptError = saCkptSectionCreate(ckptCheckpointHandle,
				&sectionCreateAttributes,
				initData,
				initDataSize);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptSectionCreate, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(ckptError));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	ckptError = saCkptCheckpointRead(ckptCheckpointHandle,
					&ckptIoVectorElement,
					1,
					&errorIndex);
	if (ckptError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
		printf(" %s, Return value:%s, should be %s\n",
			apiName,
			get_error_string(ckptError),
			get_error_string(expectedReturn));
		ret = SAF_TEST_FAIL;
		goto final;
	}
	
	if (strncmp(dataBuf,initData,strlen(initData)) != 0){
		printf(" Reading data is error,Return value is :%s"
		",shoule be %s",dataBuf,initData);
		ret = SAF_TEST_FAIL;
	}
	
final :
	saCkptCheckpointClose(ckptCheckpointHandle);
	saCkptCheckpointUnlink(ckptHandle, &checkpointName);
	saCkptFinalize(ckptHandle);
	return ret;
	
}


