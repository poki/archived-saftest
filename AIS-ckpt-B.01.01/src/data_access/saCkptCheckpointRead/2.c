/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptCheckpointRead
 * Description:
 *		Invoke saCkptCheckpointRead with invalid checkpointHandle
 *		which is not obtained via saCkptCheckpointOpen or
 *		saCkptCheckpointOpenAsync.
 *		Returned value should be SA_AIS_ERR_BAD_HANDLE.
 * Line:        P58-24:P58-26
 */

#include <stdio.h>
#include <string.h>

#include "saCkpt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE 	SA_TIME_MAX
#define CHECKPOINT_SIZE	10

int main(int argc, char *argv[])
{
	char	apiName[] = "saCkptCheckpointRead";
	SaAisErrorT     expectedReturn=  SA_AIS_ERR_BAD_HANDLE;
	SaAisErrorT	ckptError;
	
	SaCkptCheckpointHandleT		ckptCheckpointHandle;
	
	char dataBuf[10] = "";
	SaCkptIOVectorElementT  ckptIoVectorElement = {
				.dataBuffer = dataBuf,
				.dataSize = 10,
				.dataOffset = 0,
				.readSize = 0,
				.sectionId = SA_CKPT_DEFAULT_SECTION_ID
	};
	SaUint32T 	errorIndex;
	int ret = SAF_TEST_PASS;
	
	ckptError = saCkptCheckpointRead(ckptCheckpointHandle,
					&ckptIoVectorElement,
					1,
					&errorIndex);
	if (ckptError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
		printf(" %s, Return value:%s, should be %s\n",
			apiName,
			get_error_string(ckptError),
			get_error_string(expectedReturn));
		ret = SAF_TEST_FAIL;
	}
	
	return ret;
	
}


