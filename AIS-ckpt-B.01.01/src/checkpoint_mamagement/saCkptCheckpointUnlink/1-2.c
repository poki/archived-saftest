/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Yu, Ling <ling.l.yu@intel.com>.
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptCheckpointUnlink
 * Description:
 *    Call saCkptCheckpointUnlink before initialization.
 *    The api invocation returned value should SA_AIS_ERR_BAD_HANDLE.
 * Line:        P37-33:P37-34
 */

#include <stdio.h>
#include <string.h>

#include "saCkpt.h"
#include "saf_test.h"


int main(int argc, char *argv[])
{
        SaAisErrorT     error;
        SaCkptHandleT    ckptHandle;
        int ret = SAF_TEST_UNKNOWN;

        char name_open[] = "checkpoint";
        SaNameT ckpt_name;
        ckpt_name.length = sizeof(name_open) ;
        memcpy (ckpt_name.value, name_open, ckpt_name.length);

        error = saCkptCheckpointUnlink(ckptHandle, &ckpt_name);
        if ( error != SA_AIS_ERR_BAD_HANDLE ){
                printf(" invoke saCkptCheckpointUnlink, return %s "
                "(should be SA_AIS_ERR_BAD_HANDLE)\n", get_error_string(error));
                ret = SAF_TEST_FAIL;
        }
	ret = SAF_TEST_PASS;
        return ret;
}

