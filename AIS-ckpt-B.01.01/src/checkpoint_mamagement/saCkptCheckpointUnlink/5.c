/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Yu, Ling <ling.l.yu@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptCheckpointUnlink
 * Description:
 *    create a checkpoint, open but do not close it, then unlink it.
 *    create another checkpoint with same name and SA_CKPT_CHECKPOINT_CREATE
 *    flag, the result should be SA_AIS_OK.
 *    get checkpoint status throught old checkpoint handle, it still works.  
 * Line:        P38-3:P37-7
 */


#include <stdio.h>
#include <string.h>

#include "saCkpt.h"
#include "saf_test.h"

static void ckpt_open_callback (SaInvocationT invocation,
                                SaCkptCheckpointHandleT checkpoint_handle,
                                SaAisErrorT error)
{
        return ;
}

static void ckpt_sync_callback (SaInvocationT invocation,
                                SaAisErrorT error)
{
        return ;
}

int main(int argc, char *argv[])
{
        SaAisErrorT     error;
        SaCkptHandleT    ckptHandle;
        SaCkptCallbacksT ckptCallback = {
                .saCkptCheckpointOpenCallback        = ckpt_open_callback,
                .saCkptCheckpointSynchronizeCallback = ckpt_sync_callback
        };
        SaVersionT      version = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
        int ret = SAF_TEST_UNKNOWN;

        char name_open[] = "checkpoint";
        SaNameT ckpt_name;
        SaCkptCheckpointCreationAttributesT ckpt_create_attri;
        SaCkptCheckpointHandleT checkpoint_handle,checkpoint_handle_1;

	SaCkptCheckpointDescriptorT ckpt_status;

        error = saCkptInitialize(&ckptHandle, &ckptCallback, &version);
        if (error != SA_AIS_OK){
                printf("  saCkptInitialize, Return value: %s, should be "
                                "SA_AIS_OK\n", get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto out;
        }

        ckpt_name.length = sizeof(name_open) ;
        memcpy (ckpt_name.value, name_open, ckpt_name.length);

        ckpt_create_attri.creationFlags = SA_CKPT_WR_ALL_REPLICAS;
        ckpt_create_attri.retentionDuration = SA_TIME_END;
        ckpt_create_attri.checkpointSize = 1000;
        ckpt_create_attri.maxSectionSize = 100;
        ckpt_create_attri.maxSections = 10;
        ckpt_create_attri.maxSectionIdSize = 10;

        error = saCkptCheckpointOpen (  ckptHandle,
                                        &ckpt_name,
                                        &ckpt_create_attri ,
                                        SA_CKPT_CHECKPOINT_CREATE,
                                        SA_TIME_MAX,
                                        &checkpoint_handle);
        if ( error != SA_AIS_OK){
                printf("  can't open checkpoint 1, return %s \n",
                                       get_error_string(error));
                ret = SAF_TEST_UNRESOLVED;
                goto final;
        }

        error = saCkptCheckpointUnlink(ckptHandle, &ckpt_name);
        if ( error != SA_AIS_OK ){
                printf(" invoke saCkptCheckpointUnlink, return %s "
                "(should be SA_AIS_OK)\n", get_error_string(error));
                ret = SAF_TEST_FAIL;
                goto final;
        }

        //create a new checkpoint with same name
        error = saCkptCheckpointOpen (  ckptHandle,
                                        &ckpt_name,
                                        &ckpt_create_attri,
                                        SA_CKPT_CHECKPOINT_CREATE,
                                        SA_TIME_MAX,
	                                &checkpoint_handle_1);
        if ( error != SA_AIS_OK){
                printf("  can't open checkpoint again, return %s \n",
                                       get_error_string(error));
                ret = SAF_TEST_FAIL;
                goto final;
        }
	//old unclosed checkpoint still can be accessed. 
	error = saCkptCheckpointStatusGet(checkpoint_handle, &ckpt_status);
        if ( error != SA_AIS_OK){
                printf("  open a unlinked checkpoint, return %s",
                                        get_error_string(error));
                ret = SAF_TEST_FAIL;
                goto final;
        }
        ret = SAF_TEST_PASS;

	//clean up
        saCkptCheckpointClose(checkpoint_handle);
        saCkptCheckpointClose(checkpoint_handle_1);
        saCkptCheckpointUnlink(ckptHandle, &ckpt_name);

final:
        saCkptFinalize(ckptHandle);
out:
        return ret;
}

