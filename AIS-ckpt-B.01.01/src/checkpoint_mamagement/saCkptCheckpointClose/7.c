/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptCheckpointClose
 * Description:
 *		Test if it cancels all pending callbacks ,when checkpoint
 *		is closed.
 * Line:        P36-33:P36-37
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>

#include "saCkpt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE 	SA_TIME_MAX
#define CHECKPOINT_SIZE	10

int callback_flag = 0;

static void ckpt_synchronize_callback (SaInvocationT invocation,
        SaAisErrorT error)
{
	callback_flag = 1;
        return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saCkptCheckpointClose";
        SaCkptHandleT   ckptHandle;
        SaCkptCallbacksT ckptCallback = {
        	.saCkptCheckpointOpenCallback  = NULL,
                .saCkptCheckpointSynchronizeCallback = 
			(SaCkptCheckpointSynchronizeCallbackT) 
				ckpt_synchronize_callback
	};
	SaAisErrorT	ckptError;
        SaVersionT      ckptVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	checkpointName = {
		.length = strlen("checkpointName"),
		.value 	= "checkpointName"
	};
	SaCkptCheckpointCreationAttributesT	checkpointCreationAttributes = {
		.creationFlags  = SA_CKPT_WR_ACTIVE_REPLICA_WEAK,
		.checkpointSize = CHECKPOINT_SIZE * CHECKPOINT_SIZE,
		.retentionDuration = TIMEOUT_VALUE,
		.maxSections	= CHECKPOINT_SIZE,
		.maxSectionSize	= CHECKPOINT_SIZE,
		.maxSectionIdSize = CHECKPOINT_SIZE   
	};
	SaCkptCheckpointOpenFlagsT	checkpointOpenFlags = 
				SA_CKPT_CHECKPOINT_CREATE | SA_CKPT_CHECKPOINT_WRITE;
	SaTimeT		timeout = TIMEOUT_VALUE;
	SaCkptCheckpointHandleT		ckptCheckpointHandle;
	
	fd_set readSet;
	int selectRet;
	
	SaSelectionObjectT 	selObject;
        int ret = SAF_TEST_PASS;
	struct timeval tv = {
		.tv_sec = 8,
		.tv_usec = 0	
	};
	
        ckptError = saCkptInitialize(&ckptHandle, &ckptCallback, &ckptVersion);
	if (ckptError != SA_AIS_OK) 
	{
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	ckptError = saCkptSelectionObjectGet(ckptHandle, &selObject);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptSelectionObjectGet, Return value: %s,should"
			" be SA_AIS_OK\n",get_error_string(ckptError) );
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	ckptError = saCkptCheckpointOpen(ckptHandle,
					&checkpointName,
					&checkpointCreationAttributes,
					checkpointOpenFlags,
					timeout,
					&ckptCheckpointHandle);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptCheckpointOpen, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	ckptError = saCkptCheckpointSynchronizeAsync(ckptCheckpointHandle,1);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptCheckpointSynchronizeAsync, Return value: %s,"
			" should be SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}

        ckptError = saCkptCheckpointClose(ckptCheckpointHandle);
        if (ckptError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be SA_AIS_OK\n",
                        apiName,
                        get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
                goto final_2;
        }
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, &tv);
        if (selectRet == -1) {
                printf("  select error!\n");
                ret = SAF_TEST_FAIL;
        }

final_2:
	saCkptCheckpointUnlink(ckptHandle, &checkpointName);
final_1 :	
	saCkptFinalize(ckptHandle);
	return ret;
}

