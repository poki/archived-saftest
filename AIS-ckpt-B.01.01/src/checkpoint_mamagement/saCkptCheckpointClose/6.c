/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptCheckpointClose
 * Description:
 *		Test if all of its open checkpoint are closed ,when the 
 *		process terminates .
 *		6-fork.c is used as its child process
 * Line:        P36-32:P36-32
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include "saCkpt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE 	SA_TIME_MAX
#define CHECKPOINT_SIZE	10

int main(int argc, char *argv[])
{
	char	apiName[] = "saCkptCheckpointClose";
        SaCkptHandleT   ckptHandle;
	SaCkptCallbacksT ckptCallback = {
        	.saCkptCheckpointOpenCallback  = NULL,
                .saCkptCheckpointSynchronizeCallback = NULL
	};
	SaAisErrorT	ckptError;
	SaVersionT      ckptVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	checkpointName = {
		.length = strlen("checkpointName"),
		.value 	= "checkpointName"
	};
	SaCkptCheckpointCreationAttributesT	checkpointCreationAttributes = {
		.creationFlags  = SA_CKPT_WR_ACTIVE_REPLICA,
		.checkpointSize = CHECKPOINT_SIZE * CHECKPOINT_SIZE,
		.retentionDuration = TIMEOUT_VALUE,
		.maxSections	= CHECKPOINT_SIZE,
		.maxSectionSize	= CHECKPOINT_SIZE,
		.maxSectionIdSize = CHECKPOINT_SIZE   
	};
	SaCkptCheckpointOpenFlagsT	checkpointOpenFlags = 
						SA_CKPT_CHECKPOINT_CREATE;
	SaTimeT		timeout = TIMEOUT_VALUE;
	SaCkptCheckpointHandleT		ckptCheckpointHandle;
	
	int ret = SAF_TEST_PASS;
	pid_t pid ;
	int status;

	ckptError = saCkptInitialize(&ckptHandle, &ckptCallback, &ckptVersion);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	ckptError = saCkptCheckpointOpen(ckptHandle,
					&checkpointName,
					&checkpointCreationAttributes,
					checkpointOpenFlags,
					timeout,
					&ckptCheckpointHandle);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptCheckpointOpen, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	ckptError = saCkptCheckpointClose(ckptCheckpointHandle);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be SA_AIS_OK\n",
			apiName,
			get_error_string(ckptError));
		ret = SAF_TEST_FAIL;
		goto final;
	}
	
	pid = fork();
	if (pid < 0){
		printf(" fork is error\n");
		ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	if (pid == 0)
		if (execl("./6-fork.test", NULL) == -1){
			printf("execl is error\n");
			ret = SAF_TEST_UNRESOLVED;
			return ret;
		}

	wait(&status);
	if (WIFEXITED(status) == 0){
		printf("WIFEXITED is error\n");
		ret = SAF_TEST_UNRESOLVED;
	}
	else
		if(ret == SAF_TEST_PASS)
			ret = WEXITSTATUS(status);
	
	if (ret != SAF_TEST_PASS){
		saCkptCheckpointUnlink(ckptHandle, &checkpointName);
		goto final;
	}

	ckptError = saCkptCheckpointUnlink(ckptHandle, &checkpointName);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptCheckpointUnlink, Return value: %s, should be"
			" SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	ckptError = saCkptCheckpointOpen(ckptHandle,
					&checkpointName,
					NULL,
					SA_CKPT_CHECKPOINT_READ,
					timeout,
					&ckptCheckpointHandle);
	if (ckptError != SA_AIS_ERR_NOT_EXIST){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptCheckpointOpen, Return value: %s, should be"
			" SA_AIS_ERR_NOT_EXIST\n", get_error_string(ckptError));
                ret = SAF_TEST_FAIL;
	}

final :
	saCkptFinalize(ckptHandle);
	return ret;

}
