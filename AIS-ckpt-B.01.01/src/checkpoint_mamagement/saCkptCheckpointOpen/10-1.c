/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptCheckpointOpen
 * Description:
 *		Call api to open an existing checkpoint multiple times for 
 *		reading or writing in the same process.
 *		Return value be SA_AIS_OK .  
 * Line:        P32-28:P32-31
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include "saCkpt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE 	SA_TIME_MAX
#define CHECKPOINT_SIZE	10

int main(int argc, char *argv[])
{
	char	apiName[] = "saCkptCheckpointOpen";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaCkptHandleT   ckptHandle;
	SaCkptCallbacksT ckptCallback = {
        	.saCkptCheckpointOpenCallback  = NULL,
                .saCkptCheckpointSynchronizeCallback = NULL
	};
	SaAisErrorT	ckptError;
	SaVersionT      ckptVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	checkpointName = {
		.length = strlen("checkpointName"),
		.value 	= "checkpointName"
	};
	SaCkptCheckpointCreationAttributesT	checkpointCreationAttributes = {
		.creationFlags  = SA_CKPT_WR_ALL_REPLICAS,
		.checkpointSize = CHECKPOINT_SIZE,
		.retentionDuration = TIMEOUT_VALUE,
		.maxSections	= CHECKPOINT_SIZE,
		.maxSectionSize	= CHECKPOINT_SIZE,
		.maxSectionIdSize = CHECKPOINT_SIZE   
	};
	SaCkptCheckpointOpenFlagsT	checkpointOpenFlags = 
						SA_CKPT_CHECKPOINT_CREATE;
	SaTimeT		timeout = TIMEOUT_VALUE;
	SaCkptCheckpointHandleT		ckptCheckpointHandle[4];
	
	int ret = SAF_TEST_PASS;
	int openTimes = 3;
	int i;
	
	ckptError = saCkptInitialize(&ckptHandle, &ckptCallback, &ckptVersion);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	ckptError = saCkptCheckpointOpen(ckptHandle,
					&checkpointName,
					&checkpointCreationAttributes,
					checkpointOpenFlags,
					timeout,
					ckptCheckpointHandle);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be SA_AIS_OK\n",
			apiName, get_error_string(ckptError));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	checkpointOpenFlags = SA_CKPT_CHECKPOINT_WRITE |
				 SA_CKPT_CHECKPOINT_READ;
	for (i = 0;i < openTimes;i++){
		ckptError = saCkptCheckpointOpen(ckptHandle,
					&checkpointName,
					NULL,
					checkpointOpenFlags,
					timeout,
					ckptCheckpointHandle+i+1);
		if (ckptError != expectedReturn){
			printf("  Does not conform the expected behaviors!\n");
                	printf("  %s, Return value: %s, should be %s\n",
				apiName,
				get_error_string(ckptError),
				get_error_string(expectedReturn));
			ret = SAF_TEST_FAIL;
			break;
		}
		saCkptCheckpointClose(ckptCheckpointHandle[i+1]);
	}
	
	saCkptCheckpointClose(ckptCheckpointHandle[0]);
	saCkptCheckpointUnlink(ckptHandle, &checkpointName);

final :	
	saCkptFinalize(ckptHandle);	
	return ret;

}
