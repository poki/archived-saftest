/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptCheckpointOpen
 * Description:
 *		Call api to open an existing checkpoint multiple times for 
 *		reading or writing in different processes.
 *		Return value be SA_AIS_OK .  
 *		10-2-fork.c is used as its child process
 * Line:        P32-28:P32-31
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include "saCkpt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE 	SA_TIME_MAX
#define CHECKPOINT_SIZE	10

int main(int argc, char *argv[])
{
	char	apiName[] = "saCkptCheckpointOpen";
        SaCkptHandleT   ckptHandle;
	SaCkptCallbacksT ckptCallback = {
        	.saCkptCheckpointOpenCallback  = NULL,
                .saCkptCheckpointSynchronizeCallback = NULL
	};
	SaAisErrorT	ckptError;
	SaVersionT      ckptVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        }; 
	SaNameT 	checkpointName = {
		.length = strlen("checkpointName"),
		.value 	= "checkpointName"
	};
	SaCkptCheckpointCreationAttributesT	checkpointCreationAttributes = {
		.creationFlags  = SA_CKPT_WR_ALL_REPLICAS,
		.checkpointSize = CHECKPOINT_SIZE * CHECKPOINT_SIZE,
		.retentionDuration = TIMEOUT_VALUE,
		.maxSections	= CHECKPOINT_SIZE,
		.maxSectionSize	= CHECKPOINT_SIZE,
		.maxSectionIdSize = CHECKPOINT_SIZE   
	};
	SaCkptCheckpointOpenFlagsT	checkpointOpenFlags = 
						SA_CKPT_CHECKPOINT_CREATE;
	SaTimeT		timeout = TIMEOUT_VALUE;
	SaCkptCheckpointHandleT		ckptCheckpointHandle;

	int ret = SAF_TEST_PASS;
	pid_t pid[3];
	int status;
	int callback = 3;

	ckptError = saCkptInitialize(&ckptHandle, &ckptCallback, &ckptVersion);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	ckptError = saCkptCheckpointOpen(ckptHandle,
					&checkpointName,
					&checkpointCreationAttributes,
					checkpointOpenFlags,
					timeout,
					&ckptCheckpointHandle);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be SA_AIS_OK\n",
			apiName, get_error_string(ckptError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}

	do {
		pid[3 - callback] = fork();
		if (pid[3 - callback] < 0){
			printf(" fork is error\n");
			ret = SAF_TEST_UNRESOLVED;
			return ret;
		}

		if (pid[3 - callback] == 0)
			if (execl("./10-2-fork.test", NULL) == -1){
				printf("execl ./10-2-fork.test fails.");
				ret = SAF_TEST_UNRESOLVED;
				return ret;
			}
		callback --;
	} while(callback > 0);

	while(callback < 3) {
		wait(&status);
		if (WIFEXITED(status) == 0){
			printf("WIFEXITED(status) for child process is wrong.");
			ret = SAF_TEST_UNRESOLVED;
		}
		else
			if(ret == SAF_TEST_PASS)
				ret = WEXITSTATUS(status);
		callback ++;
	}

	saCkptCheckpointClose(ckptCheckpointHandle);
	saCkptCheckpointUnlink(ckptHandle, &checkpointName);
final_1:
	saCkptFinalize(ckptHandle);
	return ret;
}


