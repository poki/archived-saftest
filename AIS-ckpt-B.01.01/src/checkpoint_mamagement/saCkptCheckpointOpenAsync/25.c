/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptCheckpointOpenAsync
 * Description:
 *		Test whether the API saCkptCheckpointOpenAsync can return the
 *		value SA_AIS_ERR_NOT_EXIST,when the SA_CKPT_CHECKPOINT_CREATE
 *              is not set.
 * Line:        P34-1:P34-3
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include "saCkpt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE 	SA_TIME_MAX
#define CHECKPOINT_SIZE	10


static void ckpt_open_callback (SaInvocationT invocation,
	SaCkptCheckpointHandleT checkpointHandle,
	SaAisErrorT error)
{
	return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saCkptCheckpointOpenAsync";
	SaAisErrorT     expectedReturn= SA_AIS_ERR_NOT_EXIST;
        SaCkptHandleT   ckptHandle;
        SaCkptCallbacksT ckptCallback = {
		.saCkptCheckpointOpenCallback  =
			(SaCkptCheckpointOpenCallbackT) ckpt_open_callback,
                .saCkptCheckpointSynchronizeCallback = NULL
	};
	SaAisErrorT	ckptError;
        SaVersionT      ckptVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	checkpointName = {
		.length = strlen("checkpointName"),
		.value 	= "checkpointName"
	};
	SaCkptCheckpointOpenFlagsT	checkpointOpenFlags = 
						SA_CKPT_CHECKPOINT_READ;
	
        int ret = SAF_TEST_PASS;
	
        ckptError = saCkptInitialize(&ckptHandle, &ckptCallback, &ckptVersion);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	ckptError = saCkptCheckpointOpenAsync(ckptHandle,
					1,
					&checkpointName, 
					NULL,
					checkpointOpenFlags);
	if (ckptError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n",
			apiName, 
			get_error_string(ckptError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
	}

	saCkptFinalize(ckptHandle);
	return ret;
}

