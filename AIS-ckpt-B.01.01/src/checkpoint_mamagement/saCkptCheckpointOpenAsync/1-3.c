/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptCheckpointOpenAsync
 * Description:
 *                Call api with valid params. 
 *		Return value should be SA_AIS_OK.
 * Line:        P31-31:P31-32
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>

#include "saCkpt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE 	SA_TIME_MAX
#define CHECKPOINT_SIZE	10

int callbackInvocation;

static void ckpt_open_callback (SaInvocationT invocation,
        SaCkptCheckpointHandleT checkpointHandle,
        SaAisErrorT error)
{
	callbackInvocation = invocation;
	saCkptCheckpointClose(checkpointHandle);
        return;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saCkptCheckpointOpenAsync";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaCkptHandleT   ckptHandle;
        SaCkptCallbacksT ckptCallback = {
        	.saCkptCheckpointOpenCallback  = 
			(SaCkptCheckpointOpenCallbackT) ckpt_open_callback,
                .saCkptCheckpointSynchronizeCallback = NULL
	};
	SaAisErrorT	ckptError;
        SaVersionT      ckptVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	checkpointName = {
		.length = strlen("checkpointName"),
		.value 	= "checkpointName"
	};
	SaCkptCheckpointCreationAttributesT	checkpointCreationAttributes = {
		.creationFlags  = SA_CKPT_WR_ALL_REPLICAS,
		.checkpointSize = CHECKPOINT_SIZE * CHECKPOINT_SIZE,
		.retentionDuration = TIMEOUT_VALUE,
		.maxSections	= CHECKPOINT_SIZE,
		.maxSectionSize	= CHECKPOINT_SIZE,
		.maxSectionIdSize = CHECKPOINT_SIZE   
	};
	SaCkptCheckpointOpenFlagsT	checkpointOpenFlags = 
						SA_CKPT_CHECKPOINT_CREATE ;
	SaInvocationT	ckptInvocation = 1 ;
	
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ALL;
	
	fd_set readSet;
	int selectRet;
	
	SaSelectionObjectT 	selObject;
        int ret = SAF_TEST_PASS;
	
	
        ckptError = saCkptInitialize(&ckptHandle, &ckptCallback, &ckptVersion);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}

	ckptError = saCkptSelectionObjectGet(ckptHandle, &selObject);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptSelectionObjectGet, Return value: %s,should"
			" be SA_AIS_OK\n",get_error_string(ckptError) );
                ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	ckptError = saCkptCheckpointOpenAsync(ckptHandle,
					ckptInvocation,
					&checkpointName, 
					&checkpointCreationAttributes,
					checkpointOpenFlags);
	if (ckptError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n",
			apiName, 
			get_error_string(ckptError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
		goto final_1 ;
	}
	
	
	FD_ZERO(&readSet);
        FD_SET(selObject, &readSet);
        selectRet = select(selObject + 1, &readSet, NULL, NULL, NULL);
        if ((selectRet == -1) || (selectRet == 0)){
                printf("  select error!\n");
                ret = SAF_TEST_UNRESOLVED;
                goto final_2;
        }
	
	ckptError = saCkptDispatch(ckptHandle, dispatchFlag);
        if (ckptError != SA_AIS_OK){
                printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptDispatch, Return value: %s, should be"
			" SA_AIS_OK\n",get_error_string(ckptError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;
        }
	if (callbackInvocation != ckptInvocation){
		printf("callback invocation error, callback is %d" 
			"(should be %d)\n", 
			callbackInvocation, 
			(int)ckptInvocation);
                ret = SAF_TEST_FAIL;
	}
	
final_2:
	saCkptCheckpointUnlink(ckptHandle, &checkpointName);
	
final_1:
	saCkptFinalize(ckptHandle);
	return ret;
}


