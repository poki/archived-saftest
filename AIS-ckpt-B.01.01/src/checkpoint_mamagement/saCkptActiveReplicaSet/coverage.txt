This file defines the coverage for the saCkptActiveReplicaSet conformance testing.

Assertion       Status
1-1		YES
1-2		YES
2-1		YES
2-2		YES
2-3		YES
3		YES
4		NO
5		YES
6		NO
7		NO
8		NO
9		YES
10		YES
11		YES
12		YES
13		YES

Total	16
Done	12
