/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptSectionIterationInitalize
 * Description:
 *		Invoke saCkptSectionIterationInitialize with 
 *		SA_CKPT_SECTIONS_GEQ_EXPIRATION_TIME,Test if returned 
 *		sectionIterationHandle match the criteria specified in 
 *		sectionsChosen.
 * Line:        P49-20:P49-23
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <time.h>
#include "saCkpt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE 	SA_TIME_MAX
#define CHECKPOINT_SIZE	10

int main(int argc, char *argv[])
{
	char	apiName[] = "saCkptSectionIterationInitialize";
	SaAisErrorT     expectedReturn=  SA_AIS_OK;
        SaCkptHandleT   ckptHandle;
	SaCkptCallbacksT ckptCallback = {
        	.saCkptCheckpointOpenCallback  = NULL,
                .saCkptCheckpointSynchronizeCallback = NULL
	};
	SaAisErrorT	ckptError;
	SaVersionT      ckptVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	checkpointName = {
		.length = strlen("checkpointName"),
		.value 	= "checkpointName"
	};
	SaCkptCheckpointCreationAttributesT	checkpointCreationAttributes = {
		.creationFlags  = SA_CKPT_WR_ALL_REPLICAS,
		.checkpointSize = CHECKPOINT_SIZE * CHECKPOINT_SIZE,
		.retentionDuration = TIMEOUT_VALUE,
		.maxSections	= CHECKPOINT_SIZE,
		.maxSectionSize	= CHECKPOINT_SIZE,
		.maxSectionIdSize = CHECKPOINT_SIZE   
	};
	SaCkptCheckpointOpenFlagsT	checkpointOpenFlags = 
			SA_CKPT_CHECKPOINT_CREATE | SA_CKPT_CHECKPOINT_WRITE;
	SaTimeT		timeout = TIMEOUT_VALUE;
	SaCkptCheckpointHandleT		ckptCheckpointHandle;
	
	char initData[] = "section";
	int initDataSize = strlen(initData);
	SaCkptSectionIdT sectionIdTmp = {
				.id = "identify",
				.idLen = strlen("identify")
	};
	SaCkptSectionCreationAttributesT sectionCreateAttributes = {
				.sectionId = &sectionIdTmp, 
				.expirationTime = 100000000ULL /* 100 msec */
	};
	
	SaCkptSectionIterationHandleT	sectionIterationHandle;
	SaCkptSectionDescriptorT	sectionDescriptor;
	clockid_t	clk_id = CLOCK_REALTIME;
	struct timespec 	tp;
	int	clkError;
	long long int ckTime;
	int ret = SAF_TEST_PASS;
	
	ckptError = saCkptInitialize(&ckptHandle, &ckptCallback, &ckptVersion);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	ckptError = saCkptCheckpointOpen(ckptHandle,
					&checkpointName,
					&checkpointCreationAttributes,
					checkpointOpenFlags,
					timeout,
					&ckptCheckpointHandle);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptCheckpointOpen,Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(ckptError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}

	clkError = clock_gettime(clk_id,&tp);
	if (clkError != 0){
		printf("  Does not conform the expected behaviors!\n");
		printf("  To get system time is error\n");
		ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	ckTime = tp.tv_sec * 1000000000ULL + tp.tv_nsec;
	sectionCreateAttributes.expirationTime += ckTime;
	
	ckptError = saCkptSectionCreate(ckptCheckpointHandle,
				&sectionCreateAttributes,
				initData,
				initDataSize);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptSectionCreate, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(ckptError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_1;
	}
	
	ckptError = saCkptSectionIterationInitialize(ckptCheckpointHandle,
					SA_CKPT_SECTIONS_GEQ_EXPIRATION_TIME,
					ckTime,
					&sectionIterationHandle);
	if (ckptError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n",
			apiName,
			get_error_string(ckptError),
			get_error_string(expectedReturn));
		ret = SAF_TEST_FAIL;
		goto final_1;
	}
	
	ckptError =saCkptSectionIterationNext(sectionIterationHandle,
						&sectionDescriptor);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptSectionIterationNext, Return value: %s,"
			" should be SA_AIS_OK\n",get_error_string(ckptError));
		ret = SAF_TEST_UNRESOLVED;
		goto final_2;
	}
	
	if (memcmp(sectionDescriptor.sectionId.id,sectionIdTmp.id,
			sectionIdTmp.idLen)!=0){
		printf("iteration is error . the section id is not same\n");
		printf("Returned value is %s,should be %s\n",
			sectionDescriptor.sectionId.id,sectionIdTmp.id);
		ret = SAF_TEST_FAIL;
		goto final_2;
	}
	
	if (sectionDescriptor.expirationTime < ckTime){
		printf("iteration is error, Returned section expirationTime "
			"is %llu, should greater than or equal to %llu",
			sectionDescriptor.expirationTime,
			ckTime);
		ret = SAF_TEST_FAIL;
	}
final_2:
	saCkptSectionIterationFinalize(sectionIterationHandle);
final_1 :
	saCkptCheckpointClose(ckptCheckpointHandle);
	saCkptCheckpointUnlink(ckptHandle, &checkpointName);
	saCkptFinalize(ckptHandle);	
	return ret;
	
}


