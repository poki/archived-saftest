/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao,QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptSectionExpirationTimeSet
 * Description:   
 *		Invoke saCkptSectionExpirationTimeSet with valid expirationTime.
 *		when checkpoint is open. and then test if the checkpoint
 *		will automatically deleted when expirationtime is over.
 * Line:        P47-22:P47-25
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include "saCkpt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE 	SA_TIME_MAX
#define CHECKPOINT_SIZE	10

int main(int argc, char *argv[])
{
	char	apiName[] = "saCkptSectionExpirationTimeSet";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaCkptHandleT   ckptHandle;
	SaCkptCallbacksT ckptCallback = {
        	.saCkptCheckpointOpenCallback  = NULL,
                .saCkptCheckpointSynchronizeCallback = NULL
	};
	SaAisErrorT	ckptError;
	SaVersionT      ckptVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	checkpointName = {
		.length = strlen("checkpointName"),
		.value 	= "checkpointName"
	};
	SaCkptCheckpointCreationAttributesT	checkpointCreationAttributes = {
		.creationFlags  = SA_CKPT_WR_ALL_REPLICAS,
		.checkpointSize = CHECKPOINT_SIZE * CHECKPOINT_SIZE,
		.retentionDuration = TIMEOUT_VALUE,
		.maxSections	= CHECKPOINT_SIZE,
		.maxSectionSize	= CHECKPOINT_SIZE,
		.maxSectionIdSize = CHECKPOINT_SIZE   
	};
	SaCkptCheckpointOpenFlagsT	checkpointOpenFlags = 
			SA_CKPT_CHECKPOINT_CREATE | SA_CKPT_CHECKPOINT_WRITE |
						SA_CKPT_CHECKPOINT_READ;
	SaTimeT		timeout = TIMEOUT_VALUE;
	SaCkptCheckpointHandleT		ckptCheckpointHandle;
	
	char initData[] = "section";
	int initDataSize = strlen(initData);
	SaCkptSectionIdT sectionIdTmp = {
				.id = "identify",
				.idLen = (SaUint16T) strlen("identify")
	};
	SaCkptSectionCreationAttributesT sectionCreateAttributes = {
				.sectionId = &sectionIdTmp, 
				.expirationTime = SA_TIME_MAX				
	};
	
	SaCkptIOVectorElementT	ioVector;
	SaUint32T	ioError;
	char	dataBuf[10];
	clockid_t	clk_id = CLOCK_REALTIME;
	struct timespec 	tp;
	int	clkError;
	int ret = SAF_TEST_PASS;
	
	ckptError = saCkptInitialize(&ckptHandle, &ckptCallback, &ckptVersion);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	ckptError = saCkptCheckpointOpen(ckptHandle,
					&checkpointName,
					&checkpointCreationAttributes,
					checkpointOpenFlags,
					timeout,
					&ckptCheckpointHandle);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptCheckpointOpen,Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(ckptError));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	ckptError = saCkptSectionCreate(ckptCheckpointHandle,
				&sectionCreateAttributes,
				initData,
				initDataSize);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptSectionCreate, Return value: %s, should"
			" be SA_AIS_OK\n",get_error_string(ckptError));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	clkError = clock_gettime(clk_id,&tp);
	if (clkError != 0){
		printf("  Does not conform the expected behaviors!\n");
		printf("  To get system time is error\n");
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	ckptError = saCkptSectionExpirationTimeSet(ckptCheckpointHandle,
						&sectionIdTmp,
						((unsigned long long)tp.tv_sec) * 1000000000ULL
						+ ((unsigned long long)tp.tv_nsec) + 
						SA_TIME_ONE_SECOND);
	if (ckptError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n",
			apiName,
			get_error_string(ckptError),
			get_error_string(expectedReturn));
		ret = SAF_TEST_FAIL;
		goto final;
	}
	
	sleep(2);
	ioVector.sectionId = sectionIdTmp;
	ioVector.dataBuffer = dataBuf;
	ioVector.dataSize = 10 ;
	ioVector.dataOffset = 0;
	ioVector.readSize = 0;
	ckptError = saCkptCheckpointRead(ckptCheckpointHandle,
					&ioVector,
					1,
					&ioError);
	if (ckptError != SA_AIS_ERR_NOT_EXIST){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptCheckpointRead,Return value: %s, should"
			" be SA_AIS_ERR_NOT_EXIST\n",
			get_error_string(ckptError));
		ret = SAF_TEST_FAIL;
	}

final :
	saCkptCheckpointClose(ckptCheckpointHandle);
	saCkptCheckpointUnlink(ckptHandle, &checkpointName);
	saCkptFinalize(ckptHandle);	
	return ret;
	
}


