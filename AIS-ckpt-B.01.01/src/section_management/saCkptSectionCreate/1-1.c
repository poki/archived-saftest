/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptSectionCreate
 * Description:
 *		Invoke saCkptSectionCreate with invalid checkpointHandle,
 *		which is not obtained via the saCkptCheckpointOpen() or
 *		saCkptCheckpointOpenCallback() functions,
 *		Returned value should be SA_AIS_ERR_BAD_HANDLE.
 * Line:        P43-34:P43-38
 */

#include <stdio.h>
#include <string.h>
#include "saCkpt.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char	apiName[] = "saCkptSectionCreate";
	SaAisErrorT     expectedReturn= SA_AIS_ERR_BAD_HANDLE;
        SaCkptCheckpointHandleT		ckptCheckpointHandle;
	SaAisErrorT	ckptError;

	int ret = SAF_TEST_PASS;
	
	char initData[] = "section";
	int initDataSize = strlen(initData);
	SaCkptSectionIdT sectionIdTmp = {
				.id = "identify",
				.idLen = (SaUint16T) strlen("identify")
	};
	SaCkptSectionCreationAttributesT sectionCreateAttributes = {
				.sectionId = &sectionIdTmp, 
				.expirationTime = SA_TIME_MAX				
	};

	ckptError = saCkptSectionCreate(ckptCheckpointHandle,
				&sectionCreateAttributes,
				initData,
				initDataSize);
	if (ckptError != expectedReturn){
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n",
			apiName,
			get_error_string(ckptError),
			get_error_string(expectedReturn));
		ret = SAF_TEST_FAIL;
	}
	
	return ret;
}
