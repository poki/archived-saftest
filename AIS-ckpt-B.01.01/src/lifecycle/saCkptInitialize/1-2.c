/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptInitialize
 * Description:   
 *   Initialize the checkpoint service with a NULL ckpthandle.
 *   Return value should be SA_AIS_ERR_INVALID_PARAM
 * Line:        P25-16:P25-18
 */

#include <stdio.h>
#include <stdlib.h>
#include "saCkpt.h"
#include "saf_test.h"

int main(int argc, char *argv[])
{
	char apiName[] = "saCkptInitialize";
	SaAisErrorT     expectedReturn= SA_AIS_ERR_INVALID_PARAM;
        SaCkptCallbacksT ckptCallback = {
                .saCkptCheckpointOpenCallback  = NULL,
                .saCkptCheckpointSynchronizeCallback = NULL
        };
	SaAisErrorT	ckptError;
        SaVersionT      ckptVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };

        int ret = SAF_TEST_PASS;

        ckptError = saCkptInitialize(NULL, &ckptCallback, &ckptVersion);
        if (ckptError != expectedReturn){
                printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be %s\n", 
			apiName,
			get_error_string(ckptError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
        }

        return ret;
}

