/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptFinalize
 * Description:
 *  Call saCkptCheckpointOpen() to get checkpointHandle associated with the 
 *  ckptHandle, After finalizing , Call saCkptCheckpointClose() .
 *  Return value should be SA_AIS_ERR_BAD_HANDLE.
 * Line:        P29-40:P30-1
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <unistd.h>

#include "saCkpt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE 	SA_TIME_MAX
#define CHECKPOINT_SIZE	10
 
int main(int argc, char *argv[])
{
	char apiName[] = "saCkptFinalize";
	SaAisErrorT     expectedReturn= SA_AIS_ERR_BAD_HANDLE;
        SaCkptHandleT   ckptHandle;
        SaCkptCallbacksT ckptCallback = {
        	.saCkptCheckpointOpenCallback  = NULL,
                .saCkptCheckpointSynchronizeCallback = NULL
	};
	SaAisErrorT	ckptError;
        SaVersionT      ckptVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	checkpointName = {
		.length = strlen("checkpointName"),
		.value 	= "checkpointName"
	};
	SaTimeT		saTimeout = TIMEOUT_VALUE;
	SaCkptCheckpointCreationAttributesT	checkpointCreationAttributes = {
		.creationFlags  = SA_CKPT_WR_ALL_REPLICAS,
		.checkpointSize = CHECKPOINT_SIZE*CHECKPOINT_SIZE,
		.retentionDuration = TIMEOUT_VALUE,
		.maxSections	= CHECKPOINT_SIZE,
		.maxSectionSize	= CHECKPOINT_SIZE,
		.maxSectionIdSize = CHECKPOINT_SIZE   
	};
	SaCkptCheckpointOpenFlagsT	checkpointOpenFlags = 
						SA_CKPT_CHECKPOINT_CREATE;
	SaCkptCheckpointHandleT 	checkpointHandle;
		
        int ret = SAF_TEST_PASS;
	
        ckptError = saCkptInitialize(&ckptHandle, &ckptCallback, &ckptVersion);
	if (ckptError != SA_AIS_OK) 
	{
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	ckptError = saCkptCheckpointOpen(ckptHandle, 
					&checkpointName, 
					&checkpointCreationAttributes,
					checkpointOpenFlags,
					saTimeout,
					&checkpointHandle);
	if (ckptError != SA_AIS_OK) 
	{
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptCheckpointOpen, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		saCkptFinalize(ckptHandle);
		goto final;
	}
	
	ckptError = saCkptFinalize(ckptHandle);
	if (ckptError != SA_AIS_OK)
	{
		printf("  Does not conform the expected behaviors!\n");
                printf("  %s, Return value: %s, should be "
			"SA_AIS_OK\n",
			apiName,
			get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	ckptError = saCkptCheckpointClose(checkpointHandle);
	if (ckptError != expectedReturn)
	{
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptCheckpointClose, Return value: %s, should be "
			"%s\n",
			get_error_string(ckptError),
			get_error_string(expectedReturn));
                ret = SAF_TEST_FAIL;
	}
final :	
        saCkptInitialize(&ckptHandle, &ckptCallback, &ckptVersion);
	saCkptCheckpointUnlink(ckptHandle, &checkpointName);
	saCkptFinalize(ckptHandle);

	return ret;
}


