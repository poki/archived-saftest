/*
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Bao, QingFeng <QingFeng.Bao@intel.com>
 *
 * Spec:        AIS-B.01.01
 * Function:    saCkptDispatch
 * Description:
 *   Dispatch correctly, Set dispatch Flag to SA_DISPATCH_BLOCKING. The 
 *   function completes successfully.
 *   Return value should be SA_AIS_OK
 * Line:        P28-35:P28-39
 */

#include <stdio.h>
#include <sys/types.h>
#include <sys/select.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include "saCkpt.h"
#include "saf_test.h"

#define TIMEOUT_VALUE 	SA_TIME_MAX
#define CHECKPOINT_SIZE	10
 
int callbackFlag = -1;
SaAisErrorT dispatchReturn;
sem_t sem_start_dispatch;
sem_t sem_end_dispatch;
sem_t sem_finish_close;

static void ckpt_open_callback (SaInvocationT invocation,
        SaCkptCheckpointHandleT checkpointHandle,
        SaAisErrorT error)
{
	callbackFlag = 1;
	saCkptCheckpointClose(checkpointHandle);
	sem_post(&sem_finish_close);
        return;
}

static void *dispatch_thread(void *arg)
{
	callbackFlag = 0;
	sem_post(&sem_start_dispatch);
	dispatchReturn = saCkptDispatch(* ((SaCkptHandleT *) arg),
					SA_DISPATCH_BLOCKING);
	sem_post(&sem_end_dispatch);
	return NULL;
}

int main(int argc, char *argv[])
{
	char apiName[] = "saCkptDispatch";
	SaAisErrorT     expectedReturn= SA_AIS_OK;
        SaCkptHandleT   ckptHandle;
        SaCkptCallbacksT ckptCallback = {
        	.saCkptCheckpointOpenCallback  = 
			(SaCkptCheckpointOpenCallbackT) ckpt_open_callback,
                .saCkptCheckpointSynchronizeCallback = NULL
	};
	SaAisErrorT	ckptError;
        SaVersionT      ckptVersion = {
                .majorVersion = AIS_B_VERSION_MAJOR,
                .minorVersion = AIS_B_VERSION_MINOR,
                .releaseCode = AIS_B_RELEASE_CODE
        };
	
	SaNameT 	checkpointName = {
		.length = strlen("checkpointName"),
		.value 	= "checkpointName"
	};
	SaInvocationT	ckptInvocation = 100;
	SaCkptCheckpointCreationAttributesT	checkpointCreationAttributes = {
		.creationFlags  = SA_CKPT_WR_ALL_REPLICAS,
		.checkpointSize = CHECKPOINT_SIZE * CHECKPOINT_SIZE,
		.retentionDuration = TIMEOUT_VALUE,
		.maxSections	= CHECKPOINT_SIZE,
		.maxSectionSize	= CHECKPOINT_SIZE,
		.maxSectionIdSize = CHECKPOINT_SIZE   
	};
	SaCkptCheckpointOpenFlagsT	checkpointOpenFlags = 
						SA_CKPT_CHECKPOINT_CREATE;
	
	int ret = SAF_TEST_PASS;
	pthread_t thread_id;
	int pthreadError;
	
        ckptError = saCkptInitialize(&ckptHandle, &ckptCallback, &ckptVersion);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptInitialize, Return value: %s, should be "
			"SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		return ret;
	}
	
	sem_init(&sem_start_dispatch, 0, 0);
	sem_init(&sem_end_dispatch,0,0);
	sem_init(&sem_finish_close,0,0);
	pthreadError = pthread_create(&thread_id,
					NULL, 
					dispatch_thread, 
					(void*)&ckptHandle);
	if (pthreadError){
		printf("	invoke \"pthread_create\" has error,"
				" return %s\n", strerror(pthreadError));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	
	ckptError = saCkptCheckpointOpenAsync(ckptHandle, 
					ckptInvocation,
					&checkpointName, 
					&checkpointCreationAttributes,
					checkpointOpenFlags);
	if (ckptError != SA_AIS_OK){
		printf("  Does not conform the expected behaviors!\n");
                printf("  saCkptCheckpointOpenAsync, Return value: %s,"
			" should be SA_AIS_OK\n", 
			get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	sem_wait(&sem_finish_close);	
	ckptError = saCkptCheckpointUnlink(ckptHandle, &checkpointName);
        if( ckptError != SA_AIS_OK ){
                printf(" Does not conform the expected behaviors!\n");
                printf(" saCkptCheckpointUnlink, Return value :%s, "
			"should be SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
        }
	
final :
	if(ret != SAF_TEST_UNRESOLVED)
		sem_wait(&sem_start_dispatch);

        ckptError = saCkptFinalize(ckptHandle);
        if( ckptError != SA_AIS_OK ){
                printf(" Does not conform the expected behaviors!\n");
                printf(" saCkptFinalize, Return value :%s, "
			"should be SA_AIS_OK\n", get_error_string(ckptError));
                ret = SAF_TEST_UNRESOLVED;
        }

	if (ret != SAF_TEST_PASS)
		return ret;

	sem_wait(&sem_end_dispatch);
	
	if ( callbackFlag <= 0){
		printf("	invoke %s has error, pending callbacks are "
				"not invoked\n", apiName);
		return SAF_TEST_FAIL;
	}
	
	if ( dispatchReturn != expectedReturn ){
		printf(" Does not conform the expected behaviors!\n");
                printf(" %s, Return value :%s, should be %s\n", 
			apiName,
			get_error_string(dispatchReturn),
			get_error_string(expectedReturn));
		return SAF_TEST_FAIL;
	}
	return ret;
}




