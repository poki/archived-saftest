/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 * 	Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>


static void getnode_callback(SaInvocationT invocation,
				SaClmClusterNodeT *clusterNode,
				SaErrorT error)
{
        if(error != SA_OK){
		printf("GetNode Callback failed [%d]\n", error);
		exit(1);
	}
	return;
}

int main(int argc, char *argv[])
{
	SaErrorT 	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT	clmcallback = {
		.saClmClusterNodeGetCallback
		= 	(SaClmClusterNodeGetCallbackT)getnode_callback,
		.saClmClusterTrackCallback	= NULL
	};
	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = 'A'
	};
	SaDispatchFlagsT dispatchFlag = SA_DISPATCH_ONE;
	SaSelectionObjectT fd;
	SaInvocationT invocation;
	SaClmNodeIdT nodeId = 0;
	SaClmClusterNodeT clusterNode;
	fd_set rset;

	int select_ret;
	int ret = SAF_TEST_PASS;

	error = saClmInitialize(&clmhandle, &clmcallback, &version);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmInitialize, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saClmSelectionObjectGet(&clmhandle, &fd);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmSelectionObjectGet, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
	
	error = saClmClusterNodeGetAsync(&clmhandle, invocation, nodeId, &clusterNode);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmClusterNodeGetAsync, Return value: %s, should be SA_OK", get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	FD_ZERO(&rset);
	FD_SET(fd, &rset);
	select_ret = select(fd + 1, &rset, NULL, NULL, NULL);
	if ((select_ret == -1) || (select_ret == 0)){
		printf("  select error!\n");
		ret = SAF_TEST_UNKNOWN;
		goto final;
	}
		
	error = saClmDispatch(&clmhandle, dispatchFlag);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmDispatch, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		ret = SAF_TEST_FAIL;
	}
	
final:	error = saClmFinalize(&clmhandle);
	if(error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmFinalize, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
	}
out:
	return ret;
}
