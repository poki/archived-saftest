/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

static int exit_select = 0;

static void
dump_nodeinfo(SaClmClusterNodeT *cn)
{
	printf("Dump information from SaClmClusterNodeGet\n");
	printf("\n");
	printf("nodeId = %ld\n", (long)cn->nodeId);
	printf("nodeAddress = %s\n"
	,	cn->nodeAddress.length > 0
	?	(char *)cn->nodeAddress.value: "N/A");
	printf("nodeName = %s\n"
	,	cn->nodeName.length > 0
	?	(char *)cn->nodeName.value: "N/A");
	printf("clusterName = %s\n"
	,	cn->clusterName.length > 0
	?	(char *)cn->nodeName.value: "N/A");
	printf("member = %d\n", cn->member);
	printf("bootTimeStamp = %lld\n", cn->bootTimestamp);
	printf("\n");
}
static void getnode_callback(SaInvocationT invocation,
				SaClmClusterNodeT *clusterNode,
				SaErrorT error)
{
	if(error != SA_OK){
		printf("GetNode Callback failed [%d]\n", error);
		exit(1);
	}
	fprintf(stderr, "Invocation [%d]\n", invocation);
	dump_nodeinfo(clusterNode);
	exit_select = 1;
}

int main(int argc, char *argv[])
{
	SaErrorT 	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT	clmcallback = {
		.saClmClusterNodeGetCallback 
		=	(SaClmClusterNodeGetCallbackT)getnode_callback,
		.saClmClusterTrackCallback	= NULL
	};
	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = 'A'
	};
	SaSelectionObjectT selectionObject;
	SaClmClusterNodeT clusterNode;
	SaClmNodeIdT nodeId = 0;

	int ret = SAF_TEST_PASS;

	error = saClmInitialize(&clmhandle, &clmcallback, &version);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmInitialize, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saClmSelectionObjectGet(&clmhandle, &selectionObject);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmInitialize, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saClmClusterNodeGetAsync(&clmhandle, 1234, nodeId, &clusterNode);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmClusterNodeGet, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	for (;;){
		fd_set rset;
		FD_ZERO(&rset);
		FD_SET(selectionObject, &rset);

		if (select(selectionObject + 1, &rset, NULL, NULL, NULL) == -1) {
			if (errno == EINTR)
				continue;
			else{
				printf("select error\n");
				ret = SAF_TEST_UNRESOLVED;
				break;
			}
			if(exit_select){
				break;
			}
		}
		error = saClmDispatch(&clmhandle, SA_DISPATCH_ALL);
		if (error != SA_OK){
			printf("saClmDispath, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
			ret = SAF_TEST_UNRESOLVED;
			break;
		}
	}

	error = saClmFinalize(&clmhandle);
	if(error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmFinalize, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
	}
out:
	return ret;
}
