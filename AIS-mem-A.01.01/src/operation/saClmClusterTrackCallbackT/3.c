/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>

int callback_called = 0;
int ret = SAF_TEST_PASS;
SaClmHandleT    clmhandle;
SaClmClusterNotificationT nbuf[100];
SaUint32T numberItem = 100;

static void 
track_callback(SaClmClusterNotificationT *notificationBuffer, SaUint32T numberOfItems,
		SaUint32T numberOfMembers,SaUint64T viewNumber, SaErrorT error)
{
	if (error != SA_OK){
		printf("saClmClusterTrackCallback, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		callback_called = -1;
		exit(1);
	}
	
	callback_called = 1;
}

static void
track_start(int sig)
{
	int error;
        SaUint8T flag = SA_TRACK_CHANGES_ONLY;

        signal(SIGUSR1, &track_start);
        if ((error = saClmClusterTrackStart(&clmhandle, flag, nbuf, numberItem)) != SA_OK) {
		printf("saClmClusterTrackStart, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
        }
	return;
}

static void
track_stop(int sig)
{
        int error;

	signal(SIGUSR2, &track_start);
	if ((error = saClmClusterTrackStop(&clmhandle)) != SA_OK)
		printf("saClmClusterTrackStop, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
	return;
}



int main(int argc, char *argv[])
{
	SaErrorT 	error;
	SaClmCallbacksT	clmcallback = {
		.saClmClusterNodeGetCallback	= NULL,
		.saClmClusterTrackCallback
		=	(SaClmClusterTrackCallbackT)track_callback
	};
	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = RELEASE_CODE
	};
	SaSelectionObjectT st;

	error = saClmInitialize(&clmhandle, &clmcallback, &version);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmInitialize, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saClmSelectionObjectGet(&clmhandle, &st);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmSelectionObjectGet, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}

	track_start(SIGUSR1);
	signal(SIGUSR2, &track_stop);

	fd_set rset;
	FD_ZERO(&rset);
	FD_SET(st, &rset);

	if (select(st + 1, &rset, NULL, NULL, NULL) == -1){
		perror("select error");
		ret = SAF_TEST_UNKNOWN;
		goto final;
	}

	if ((error = saClmDispatch(&clmhandle, SA_DISPATCH_ALL)) != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmDispatch, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto final;
	}
			
	switch (callback_called){
		case 1:
			break;	
		case -1:
			ret = SAF_TEST_FAIL;
			break;
		default:
			printf("saClmClusterTrackCallback has not been called\n");
			ret = SAF_TEST_FAIL;
	}

final:
	error = saClmFinalize(&clmhandle);
	if(error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmFinalize, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
	}
out:
	return ret;
}
