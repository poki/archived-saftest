/*
 * Copyright (c) 2004, Intel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 *
 */

#include "im_spec.h"
#include "saf_test.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

static void
getnode_callback(SaInvocationT invocation, SaClmClusterNodeT *clusterNode
,       SaErrorT error)
{
        if (error != SA_OK) {
                fprintf(stderr, "Get Node Callback failed [%d]\n", error);
                exit(1);
        }
}

int main(int argc, char *argv[])
{
	SaErrorT 	error;
	SaClmHandleT	clmhandle;
	SaClmCallbacksT	clmcallback = {
		.saClmClusterNodeGetCallback
		=	(SaClmClusterNodeGetCallbackT)getnode_callback,
		.saClmClusterTrackCallback
		=	NULL
	};
	SaVersionT	version = {
		.major = VERSION_MAJOR,
		.minor = VERSION_MINOR,
		.releaseCode = 'A'
	};
	SaInvocationT invocation;
	SaClmClusterNodeT clusterNode;
	SaClmNodeIdT nodeId = 1;

	int ret = SAF_TEST_PASS;

	error = saClmInitialize(&clmhandle, &clmcallback, &version);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmInitialize, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saClmFinalize(&clmhandle);
	if (error != SA_OK){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmFinalize, Return value: %s, should be SA_OK\n", get_aisAerror_string(error));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	error = saClmClusterNodeGetAsync(&clmhandle, invocation, nodeId, &clusterNode);
	if (error != SA_ERR_INIT && error != SA_ERR_BAD_HANDLE){
		printf("  Does not conform the expected behaviors!\n");
		printf("  saClmClusterNodeGetAsync, Return value: %s, should be SA_ERR_INIT or SA_ERR_BAD_HANDLE\n"
		,	 get_aisAerror_string(error));
		ret = SAF_TEST_FAIL;
	}
out:
	return ret;
}
