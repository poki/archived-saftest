/*
 * Copyright (c) 2004, Intel Corporation.
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Mi,Jun  <jun.mi@intel.com>
 */


#define VERSION_MAJOR 0x01
#define VERSION_MINOR 0x01
#define RELEASE_CODE 'A'

static inline const char * get_aisAerror_string(SaErrorT error)
{
	switch(error) {
		case SA_OK:
			return "SA_OK";
		case SA_ERR_LIBRARY:
			return "SA_ERR_LIBRARY";
		case SA_ERR_VERSION:
			return "SA_ERR_VERSION";
		case SA_ERR_INIT:
			return "SA_ERR_INIT";
		case SA_ERR_TIMEOUT:
			return "SA_ERR_TIMEOUT";
		case SA_ERR_TRY_AGAIN:
			return "SA_ERR_TRY_AGAIN";
		case SA_ERR_INVALID_PARAM:
			return "SA_ERR_INVALID_PARAM";
		case SA_ERR_NO_MEMORY:
			return "SA_ERR_NO_MEMORY";
		case SA_ERR_BAD_HANDLE:
			return "SA_ERR_BAD_HANDLE";
		case SA_ERR_BUSY:
			return "SA_ERR_BUSY";
		case SA_ERR_ACCESS:
			return "SA_ERR_ACCESS";
		case SA_ERR_NOT_EXIST:
			return "SA_ERR_NOT_EXIST";
		case SA_ERR_NAME_TOO_LONG:
			return "SA_ERR_NAME_TOO_LONG";
		case SA_ERR_EXIST:
			return "SA_ERR_EXIST";
		case SA_ERR_NO_SPACE:
			return "SA_ERR_NO_SPACE";
		case SA_ERR_INTERRUPT:
			return "SA_ERR_INTERRUPT";
		case SA_ERR_SYSTEM:
			return "SA_ERR_SYSTEM";
		case SA_ERR_NAME_NOT_FOUND:
			return "SA_ERR_NAME_NOT_FOUND";
		case SA_ERR_NO_RESOURCES:
			return "SA_ERR_NO_RESOURCES";
		case SA_ERR_NOT_SUPPORTED:
			return "SA_ERR_NOT_SUPPORT";
		case SA_ERR_BAD_OPERATION:
			return "SA_ERR_BAD_OPEARATION";
		case SA_ERR_FAILED_OPERATION:
			return "SA_ERR_FAILED_OPERATION";
		case SA_ERR_MESSAGE_ERROR:
			return "SA_ERR_MESSAGE_ERROR";
		case SA_ERR_NO_MESSAGE:
			return "SA_ERR_NO_MESSAGE";
		case SA_ERR_QUEUE_FULL:
			return "SA_ERR_QUEUE_FULL";
		case SA_ERR_QUEUE_NOT_AVAILABLE:
			return "SA_ERR_QUEUE_NOT_AVAILABLE";
		case SA_ERR_BAD_CHECKPOINT:
			return "SA_ERR_BAD_CHECKPOINT";
		case SA_ERR_BAD_FLAGS:
			return "SA_ERR_BAD_FLAGS";
		default:
			return "(invalid error code)";
        }
}
