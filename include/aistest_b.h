/*
 * Copyright (c) 2004, Intel Corporation.
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *      Yu,Ling  <ling.l.yu@intel.com>
 */


#define AIS_B_VERSION_MAJOR 0x01
#define AIS_B_VERSION_MINOR 0x01
#define AIS_B_RELEASE_CODE 'B'

inline static const char * get_error_string(SaAisErrorT error)
{
	switch(error) {
		case SA_AIS_OK:
			return "SA_AIS_OK";
		case SA_AIS_ERR_LIBRARY:
			return "SA_AIS_ERR_LIBRARY";
		case SA_AIS_ERR_VERSION:
			return "SA_AIS_ERR_VERSION";
		case SA_AIS_ERR_INIT:
			return "SA_AIS_ERR_INIT";
		case SA_AIS_ERR_TIMEOUT:
			return "SA_AIS_ERR_TIMEOUT";
		case SA_AIS_ERR_TRY_AGAIN:
			return "SA_AIS_ERR_TRY_AGAIN";
		case SA_AIS_ERR_INVALID_PARAM:
			return "SA_AIS_ERR_INVALID_PARAM";
		case SA_AIS_ERR_NO_MEMORY:
			return "SA_AIS_ERR_NO_MEMORY";
		case SA_AIS_ERR_BAD_HANDLE:
			return "SA_AIS_ERR_BAD_HANDLE";
		case SA_AIS_ERR_BUSY:
			return "SA_AIS_ERR_BUSY";
		case SA_AIS_ERR_ACCESS:
			return "SA_AIS_ERR_ACCESS";
		case SA_AIS_ERR_NOT_EXIST:
			return "SA_AIS_ERR_NOT_EXIST";
		case SA_AIS_ERR_NAME_TOO_LONG:
			return "SA_AIS_ERR_NAME_TOO_LONG";
		case SA_AIS_ERR_EXIST:
			return "SA_AIS_ERR_EXIST";
		case SA_AIS_ERR_NO_SPACE:
			return "SA_AIS_ERR_NO_SPACE";
		case SA_AIS_ERR_INTERRUPT:
			return "SA_AIS_ERR_INTERRUPT";
		case SA_AIS_ERR_NAME_NOT_FOUND:
			return "SA_AIS_ERR_NAME_NOT_FOUND";
		case SA_AIS_ERR_NO_RESOURCES:
			return "SA_AIS_ERR_NO_RESOURCES";
		case SA_AIS_ERR_NOT_SUPPORTED:
			return "SA_AIS_ERR_NOT_SUPPORT";
		case SA_AIS_ERR_BAD_OPERATION:
			return "SA_AIS_ERR_BAD_OPEARATION";
		case SA_AIS_ERR_FAILED_OPERATION:
			return "SA_AIS_ERR_FAILED_OPERATION";
		case SA_AIS_ERR_MESSAGE_ERROR:
			return "SA_AIS_ERR_MESSAGE_ERROR";
		case SA_AIS_ERR_QUEUE_FULL:
			return "SA_AIS_ERR_QUEUE_FULL";
		case SA_AIS_ERR_QUEUE_NOT_AVAILABLE:
			return "SA_AIS_ERR_QUEUE_NOT_AVAILABLE";
		case SA_AIS_ERR_BAD_FLAGS:
			return "SA_AIS_ERR_BAD_FLAGS";
		case SA_AIS_ERR_TOO_BIG:
			return "SA_AIS_ERR_TOO_BIG";
		case SA_AIS_ERR_NO_SECTIONS:
			return "SA_AIS_ERR_NO_SECTIONS";
		default:
			return "(invalid error code)";
        }
}
