# Copyright (c) 2004, Intel Corporation.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307 USA.
#
# Authors:
#       Mi,Jun  <jun.mi@intel.com>
#

TIMEOUT_CMD     = t0
SUB_TARGETS :=$(shell find . -type d -maxdepth 1 -name "[A-Z][A-Z][A-Z]-*" -print)
.PHONY: $(SUB_TARGETS)

$(SUB_TARGETS):$(TIMEOUT_CMD)
	for subdir in $(SUB_TARGETS) ; do \
		(cd $$subdir && $(MAKE)) \
	done

$(TIMEOUT_CMD):$(TIMEOUT_CMD).c
	@echo Building timeout helper files; \
	$(CC) -O2 -o $@ $< ; \


clean:
	rm -f $(TIMEOUT_CMD)
	for subdir in $(SUB_TARGETS) ; do \
		(cd $$subdir && $(MAKE) clean) \
	done
