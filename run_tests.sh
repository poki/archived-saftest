#!/bin/sh
#
# Copyright (c) 2004 by Intel Corp.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms and conditions of the GNU General Public License,
# version 2, as published by the Free Software Foundation.
#
# This program is distributed in the hope it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place - Suite 330, Boston, MA 02111-1307 USA.
#
# Authors:
#       Mi,Jun  <jun.mi@intel.com>
#	Yu,Ling <ling.l.yu@intel.com>
#

topdir=.
run_tests="all"
run_log=./log/run_log
error_log=./log/error_log

RESULT_TXT=result.txt
RESULT_XML=result.xml

usage()
{
	cat <<EOF
Usage: $0 [all|SPEC]
Run the SAF-Test conformance test cases for SAF SPECs.
All log information can be found at ./log directory.

  all           All test cases will be run, including:
                - AIS-A.01.01 conformance test cases
                - AIS-B.01.01 conformance test cases
                - HPI-A.01.01 conformance test cases
                - HPI-B.01.01 conformance test cases
  SPEC          Run the test cases for specific SAF Specification.
                SPEC can be:
        AIS-A.01.01             for whole SAF AIS A.01.01
        AIS-amf-A.01.01         for SAF AIS A.01.01 Availability Management
                                Framework
        AIS-ckpt-A.01.01        for SAF AIS A.01.01 Checkpoint Service
        AIS-evt-A.01.01         for SAF AIS A.01.01 Event Service
        AIS-mem-A.01.01         for SAF AIS A.01.01 Cluster Membership Service
        AIS-mes-A.01.01         for SAF AIS A.01.01 Message Service
        AIS-lock-A.01.01        for SAF AIS A.01.01 Lock Service
        AIS-B.01.01             for whole SAF AIS B.01.01
        AIS-amf-B.01.01         for SAF AIS B.01.01 Availability Management
                                Framework
        AIS-ckpt-B.01.01        for SAF AIS B.01.01 Checkpoint Service
        AIS-evt-B.01.01         for SAF AIS B.01.01 Event Service
        AIS-clm-B.01.01         for SAF AIS B.01.01 Custer Membership Service
        AIS-msg-B.01.01         for SAF AIS B.01.01 Message Service
        AIS-lock-B.01.01         for SAF AIS B.01.01 Lock Service
        HPI-A.01.01             for whole SAF HIP A.01.01
        HPI-B.01.01             for whole SAF HIP B.01.01

EOF
}

run_testsuites()
{
        echo "run tests in $topdir/$1"
	if [ -d $topdir/$1 ]; then
		run_prog="./run_tests.sh all"
		cd $topdir/$1/
		$run_prog 2>&1
		cd -
		cat $topdir/$1/log/run_log >> "$run_log-$1"
		cat $topdir/$1/log/error_log >> "$error_log-$1"
	else
		echo "	Can't find $topdir/$1"
	fi
}

reset_log()
{
if [ -d log ]; then
        `rm -f $run_log`
        `rm -f $error_log`
else
        `mkdir ./log`
fi
}

case "$1" in
   "")
        usage
        ;;
   "--help")
        usage
        ;;
   "all")
        reset_log
        run_testsuites "AIS-amf-A.01.01"
        run_testsuites "AIS-ckpt-A.01.01"
        run_testsuites "AIS-evt-A.01.01"
        run_testsuites "AIS-mem-A.01.01"
        run_testsuites "AIS-mes-A.01.01"
        run_testsuites "AIS-lock-A.01.01"
        run_testsuites "AIS-amf-B.01.01"
        run_testsuites "AIS-ckpt-B.01.01"
        run_testsuites "AIS-evt-B.01.01"
        run_testsuites "AIS-clm-B.01.01"
        run_testsuites "AIS-msg-B.01.01"
        run_testsuites "AIS-lock-B.01.01"
        run_testsuites "HPI-A.01.01"
        run_testsuites "HPI-B.01.01"
        ;;
   "AIS-A.01.01")
        reset_log
        run_testsuites "AIS-amf-A.01.01"
        run_testsuites "AIS-ckpt-A.01.01"
        run_testsuites "AIS-evt-A.01.01"
        run_testsuites "AIS-mem-A.01.01"
        run_testsuites "AIS-mes-A.01.01"
        run_testsuites "AIS-lock-A.01.01"
        ;;
   "AIS-B.01.01")
        reset_log
	run_testsuites "AIS-amf-B.01.01"
        run_testsuites "AIS-ckpt-B.01.01"
        run_testsuites "AIS-evt-B.01.01"
        run_testsuites "AIS-clm-B.01.01"
        run_testsuites "AIS-msg-B.01.01"
        run_testsuites "AIS-lock-B.01.01"
        ;;
   *)
        reset_log
        run_testsuites $1
        ;;
esac

