/*
 * Copyright (c) 2003-2005, Intel Corporation
 * 
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <saf_test.h>

int process_resource(SaHpiSessionIdT session_id, SaHpiRptEntryT rpt_entry, callback2_t func)
{
	SaHpiResourceIdT	resource_id;
	SaHpiHsIndicatorStateT  state_new, state_old;
	SaErrorT		val;
	int 			ret = SAF_TEST_NOTSUPPORT;
	

	if (rpt_entry.ResourceCapabilities & 
				SAHPI_CAPABILITY_MANAGED_HOTSWAP) {
		ret = SAF_TEST_PASS;
		resource_id = rpt_entry.ResourceId;
	
		val = saHpiHotSwapIndicatorStateGet(session_id, resource_id, 
			&state_old);
		if (val != SA_OK) {
			printf("  Function \"saHpiHotSwapIndicatorStateGet\" works abnormally!\n");
			printf("  Cannot get the old indicator state!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}

		val = saHpiHotSwapIndicatorStateSet(session_id, resource_id,
				SAHPI_HS_INDICATOR_ON);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot change indicator state!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}

		val = saHpiHotSwapIndicatorStateGet(session_id, resource_id, 
				&state_new);
		if (val != SA_OK) {
			printf("  Function \"saHpiHotSwapIndicatorStateGet\" works abnormally!\n");
			printf("  Cannot get the new power state!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out1;
		}

		if (state_new != SAHPI_HS_INDICATOR_ON) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  The state set is invalid!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		}

out1:
		val = saHpiHotSwapIndicatorStateSet(session_id, resource_id,
				state_old);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot change indicator state!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		}
	}

out:
	return ret;
}


/**********************************************************
*
*   hotswap_indicator_state_process test -- Call saHpiHotSwapIndicatorStateSet
*	   Set the state of the hot swap indicator associated with the specified resource. State = SAHPI_HS_INDICATOR_ON.
*
*   Expected return:     saHpiHotSwapIndicatorStateGet() returns SA_OK. saHpiHotSwapIndicztorStateSet() returns SA_OK.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT        val;
	int             ret = SAF_TEST_PASS;
	
	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}
		
	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, process_resource, NULL,
			NULL);
	if (ret == SAF_TEST_NOTSUPPORT) {
		printf("  No resource supporting hotswap in SAHPI_DEFAULT_DOMAIN_ID.\n");
	}
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}

