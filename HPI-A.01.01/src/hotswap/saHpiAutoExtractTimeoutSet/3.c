/*
 * Copyright (c) 2003-2005, Intel Corporation
 * 
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <saf_test.h>

int process_resource(SaHpiSessionIdT session_id, SaHpiRptEntryT rpt_entry, callback2_t func)
{
	SaHpiResourceIdT	resource_id;
	SaHpiTimeoutT   	timeout_new, timeout_old;
	SaErrorT		val;
	int 			ret = SAF_TEST_NOTSUPPORT;
	

	if (rpt_entry.ResourceCapabilities & 
				SAHPI_CAPABILITY_MANAGED_HOTSWAP) {
		ret = SAF_TEST_PASS;
		resource_id = rpt_entry.ResourceId;

		val = saHpiAutoExtractTimeoutGet(session_id, resource_id, 
				&timeout_old);
		if (val != SA_OK) {
			printf("  Funcition \"saHpiAutoExtractTimeoutGet\" works abnormally!\n");
			printf("  Cannot get get old insert timeout info!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}

		val = saHpiAutoExtractTimeoutSet(session_id, resource_id,
				SAHPI_TIMEOUT_IMMEDIATE);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Set new timeout value failed!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}

		val = saHpiAutoExtractTimeoutGet(session_id, resource_id, 
				&timeout_new);
		if (val != SA_OK) {
			printf("  Funcition \"saHpiAutoExtractTimeoutGet\" works abnormally!\n");
			printf("  Cannot get old insert timeout info!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out1;
		}

		if (timeout_new != SAHPI_TIMEOUT_IMMEDIATE) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Set timeout value is invalid!\n");
			ret = SAF_TEST_FAIL;
		}

out1:
		val = saHpiAutoExtractTimeoutSet(session_id,resource_id, 
				timeout_old);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Set old timeout value failed!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		}
	}
out:
	return ret;
}


/**********************************************************
*
*   auto_extract_timeout test -- Call saHpiAutoExtractTimeoutSet
*	   Configure a timeout for how long to wait before the default auto-extraction policy is invoked. Timeout = SAHPI_TIMEOUT_IMMEDIATE.
*
*   Expected return:     saHpiAutoExtractTimeoutSet() returns SA_OK. The autonomous handling is proceeded immediately.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT        val;
	int             ret = SAF_TEST_PASS;
	
	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}
		
	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, process_resource, NULL,
			NULL);
	if (ret == SAF_TEST_NOTSUPPORT) {
		printf("  No resource supporting hotswap in SAHPI_DEFAULT_DOMAIN_ID.\n");		
	}
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
