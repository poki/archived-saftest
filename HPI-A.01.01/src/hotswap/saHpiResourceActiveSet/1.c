/*
 * Copyright (c) 2003-2005, Intel Corporation
 * 
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <unistd.h>
#include <saf_test.h>

int process_resource(SaHpiSessionIdT session_id, SaHpiRptEntryT rpt_entry, callback2_t func)
{
	SaHpiResourceIdT	resource_id;
	SaHpiHsStateT   	state, state_new;
	SaErrorT		val;
	int 			ret = SAF_TEST_NOTSUPPORT;
	
	if (rpt_entry.ResourceCapabilities & 
				SAHPI_CAPABILITY_MANAGED_HOTSWAP) {
		resource_id = rpt_entry.ResourceId;

		val = saHpiHotSwapStateGet(session_id, resource_id, &state);
		if (val != SA_OK) {
			printf("  Function \"saHpiHotSwapStateGet\" works abnormally!\n");
			printf("  Cannot get the hotswap state!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}

		if (state == SAHPI_HS_STATE_ACTIVE_HEALTHY 
		||  state == SAHPI_HS_STATE_ACTIVE_UNHEALTHY) {		
			val = saHpiHotSwapActionRequest(session_id, resource_id,
					SAHPI_HS_ACTION_EXTRACTION);
			if (val != SA_OK) {
				printf("  Function \"saHpiHotSwapActionRequest\" works abnormally!\n");
				printf("  Cannot change hotswap state!\n");
				printf("  Return value: %s\n", get_hpiAerror_string(val));
				ret = SAF_TEST_FAIL;
				goto out;
			}	
			sleep(1);
			val = saHpiHotSwapStateGet(session_id, resource_id, 
					&state_new);
			if (val != SA_OK) {
				printf("  Function \"saHpiHotSwapStateGet\" works abnormally!\n");
				printf("  Cannot get the hotswap state!\n");
				printf("  Return value: %s\n", get_hpiAerror_string(val));
				ret = SAF_TEST_FAIL;
				goto out;
			}
	
			if (state_new != SAHPI_HS_STATE_INACTIVE) {
				printf("The hotswap state is not changed!\n");
				goto out;
			}
		}
		else if (state != SAHPI_HS_STATE_INACTIVE)
			goto out;	/*We don't care the pending state */

		ret = SAF_TEST_PASS;
	
		val = saHpiHotSwapControlRequest(session_id, resource_id);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Function \"saHpiHotSwapControlRequest\" failed!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out1;
		}

		val = saHpiHotSwapActionRequest(session_id, resource_id,
				SAHPI_HS_ACTION_INSERTION);
		if (val != SA_OK) {
			printf("  Function \"saHpiHotSwapActionRequest\" works abnormally!\n");
			printf("  Cannot change hotswap state!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out1;
		}

		val = saHpiResourceActiveSet(session_id, resource_id);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Function \"saHpiResourceActiveSet\" failed\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		}

		val = saHpiHotSwapActionRequest(session_id, resource_id,
				SAHPI_HS_ACTION_EXTRACTION);
		if (val != SA_OK) {
			printf("  Function \"saHpiHotSwapActionRequest\" works abnormally!\n");
			printf("  Cannot change hotswap state!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		}

	out1:
		if (state == SAHPI_HS_STATE_ACTIVE_HEALTHY ||
		    state == SAHPI_HS_STATE_ACTIVE_UNHEALTHY) {		
			val = saHpiHotSwapActionRequest(session_id, resource_id,
					SAHPI_HS_ACTION_INSERTION);
			if (val != SA_OK) {
				printf("  Function \"saHpiHotSwapActionRequest\" works abnormally!\n");
				printf("  Cannot change hotswap state!\n");
				printf("  Return value: %s\n", get_hpiAerror_string(val));
				ret = SAF_TEST_FAIL;
				goto out;
			}
		}
	}
out:
	return ret;
}


/**********************************************************
*
*   resource_active test -- Call saHpiResourceActiveSet
*	   Signal that the resource should now transition into ACTIVE/HEALTHY or ACTIVE/UNHEALTHY state.
*
*   Expected return:     saHpiHotSwapControlRequest() returns SA_OK. saHpiResourceActiveSet() returns SA_OK.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT        val;
	int             ret = SAF_TEST_PASS;
	
	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}
		
	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, process_resource, NULL,
			NULL);
	if (ret == SAF_TEST_NOTSUPPORT) {
		printf("  No resource supporting hotswap in SAHPI_DEFAULT_DOMAIN_ID.\n");		
	}
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
