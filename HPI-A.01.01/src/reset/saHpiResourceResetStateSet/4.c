/*
 * (C) Copyright IBM Corp. 2004
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <unistd.h>
#include <saf_test.h>

int process_resource(SaHpiSessionIdT session_id, SaHpiRptEntryT rpt_entry, callback2_t func)
{
	SaHpiResourceIdT	resource_id = rpt_entry.ResourceId;
	SaHpiResetActionT       action;
	SaErrorT		val;
	int                     ret = SAF_TEST_PASS;
	int                     i, retry;
	int                     isresetable;

	/* Hack - just test entity paths with blades in the path - 
           things like power, switches, etc are problematic for an automated test 
           - need to think of a better way.
           this test might be better with negitive logic (e.g. list all the things
           one cannot reset in an automated test */
	
	isresetable = 0;
	for (i=0; i < SAHPI_MAX_ENTITY_PATH &&
	     rpt_entry.ResourceEntity.Entry[i].EntityType != SAHPI_ENT_ROOT; i++) {
		if (rpt_entry.ResourceEntity.Entry[i].EntityType == SAHPI_ENT_SBC_BLADE) {
			isresetable = 1;
			break;
		}
	}

	if (!isresetable) {
		ret = SAF_TEST_NOTSUPPORT;
		goto out;
	}

/*	printf("  Resource=%s\n", rpt_entry.ResourceTag.Data); */

	val = saHpiResourceResetStateSet(session_id, resource_id,
			SAHPI_COLD_RESET);
	if (val == SA_ERR_HPI_INVALID_CMD) //The resource has no reset control
		goto out;
	if (val != SA_OK) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Cannot set the reset state!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
		goto out;
	}

	/* Actually after retry limit exceeded test should be in UNKNOWN state */
	retry = 0;
 RETRY:
	val = saHpiResourceResetStateGet(session_id, resource_id, &action);
	if (val == SA_ERR_HPI_BUSY && retry < 5) {
		sleep(2);
		retry++;
		goto RETRY;
	}
	if (val != SA_OK) {
		printf("  Function \"saHpiResourceResetStateGet\" works abnormally!\n");
		printf("  Cannot get the reset state!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
		goto out;
	}
	
	if (action != SAHPI_RESET_DEASSERT) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  The reset action we set is invalid!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
	}

out:
	return ret;
}


/**********************************************************
*
*   resource_reset_state_process test -- Call saHpiResourceResetStateSet
*	   Perform a 'Cold Reset' on the entity, leaving reset de-asserted. ResetAction = SAHPI_COLD_RESET.
*
*   Expected return:     saHpiResourceResetStateGet() returns SA_OK, or SA_ERR_HPI_INVALID_CMD if resource has no reset control. saHpiResourceResetStateSet() returns SA_OK, or SA_ERR_HPI_INVALID_CMD if resource has no reset control.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT	val;
	int 		ret = SAF_TEST_PASS;

	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, process_resource, NULL,
			NULL);
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
