/*
 * (C) Copyright IBM Corp. 2004
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *     Kevin Gao <kevin.gao@intel.com>
 *     Carl McAdams <carlmc@us.ibm.com>
 */

#include <stdio.h>
#include <saf_test.h>

int process_domain_eventlog(SaHpiSessionIdT session_id)
{
	SaHpiTimeT      	time, RestoreTime;
	SaErrorT		val;
	int 			ret = SAF_TEST_PASS;

	time = SAHPI_TIME_MAX_RELATIVE;
        // save off the time to restore later
	val = saHpiEventLogTimeGet(session_id, SAHPI_DOMAIN_CONTROLLER_ID, 
			&RestoreTime);
	if (val != SA_OK) 
        {
		ret = SAF_TEST_NOTSUPPORT;
	}
        else
        {
        	val = saHpiEventLogTimeSet(session_id, SAHPI_DOMAIN_CONTROLLER_ID, 
        			time);
        	if (val != SA_OK) {
        		printf("  Does not conform the expected behaviors!(Domain)\n");
        		printf("  Return value: %s\n", get_hpiAerror_string(val));
        		ret = SAF_TEST_FAIL;
        	}
                // Restore the time.
                val = saHpiEventLogTimeSet(session_id, 
                                           SAHPI_DOMAIN_CONTROLLER_ID, 
                                           RestoreTime);                
        }

	return ret;
}

int process_resource(SaHpiSessionIdT session_id, SaHpiRptEntryT rpt_entry, callback2_t func)
{
	SaHpiResourceIdT	resource_id = rpt_entry.ResourceId;
	SaHpiTimeT      	time, RestoreTime;
	SaErrorT		val;
	int 			ret = SAF_TEST_NOTSUPPORT;

	time = SAHPI_TIME_MAX_RELATIVE;
        // save off the time to restore later
        val = saHpiEventLogTimeGet(session_id, resource_id, &RestoreTime);
        if (val != SA_OK)
        {
                ret = SAF_TEST_NOTSUPPORT;
        }
        else
        {
        	if (rpt_entry.ResourceCapabilities & SAHPI_CAPABILITY_SEL) {
        		val = saHpiEventLogTimeSet(session_id, resource_id, time);
        		if (val != SA_OK) {
        			printf("  Does not conform the expected behaviors!(Resource)\n");
        			printf("  Return value: %s\n", get_hpiAerror_string(val));
        			ret = SAF_TEST_FAIL;
        		} else {
                                ret = SAF_TEST_PASS;
                        }
                        //Restore Time
                        val = saHpiEventLogTimeSet(session_id, 
                                                   resource_id, 
                                                   RestoreTime);
                }
	}

	return ret;
}


/**********************************************************
*
*   event_log_time test -- Call saHpiEventLogTimeSet
*	   Set event log's time clock. Time &lt;= SAHPI_TIME_MAX_RELATIVE.
*
*   Expected return:     saHpiEventLogTimeSet() returns SA_OK.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT	val;
	int 		ret = SAF_TEST_PASS;

	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_NOTSUPPORT;
		goto out;
	}

	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, process_resource, NULL,
			process_domain_eventlog);
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
