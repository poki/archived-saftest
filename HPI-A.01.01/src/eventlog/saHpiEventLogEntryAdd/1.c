/*
 * (C) Copyright IBM Corp. 2004
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <string.h>
#include <saf_test.h>

#define	TEST_STR  "Event log test str"

int process_domain_eventlog(SaHpiSessionIdT session_id)
{
	SaHpiSelEntryIdT	prev_entry_id;
	SaHpiSelEntryIdT	next_entry_id;
	SaHpiSelEntryT		entry_get, entry_add;
	SaHpiRdrT		rdr;
	SaHpiRptEntryT 		rpt_entry;
	SaHpiBoolT              enable_old;
	SaErrorT		val;
	int 			ret = SAF_TEST_PASS;

	/* Disable event log state, to ensure entry is newest eventlog entry */
	val = saHpiEventLogStateGet(session_id, SAHPI_DOMAIN_CONTROLLER_ID, &enable_old);
	if (val != SA_OK) {
		printf("  Function \"saHpiEventLogStateGet\" works abnormally!\n");
		printf("  Cannot get the event log state info!(Domain)\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	val = saHpiEventLogStateSet(session_id, SAHPI_DOMAIN_CONTROLLER_ID, SAHPI_FALSE);
	if (val != SA_OK) {
		if (val == SA_ERR_HPI_INVALID_REQUEST) {
			ret = SAF_TEST_NOTSUPPORT;
			goto out;
		}
		else {
			printf("  Function \"saHpiEventLogStateSet\" works abnormally!\n");
			printf("  Set event log state to TRUE failed!(Domain)\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_UNRESOLVED;
			goto out1;
		}
	}

	entry_add.Event.Source = SAHPI_UNSPECIFIED_RESOURCE_ID;
	entry_add.Event.EventType = SAHPI_ET_USER;
	entry_add.Event.Timestamp = SAHPI_TIME_UNSPECIFIED;
	entry_add.Event.Severity = SAHPI_OK;
	memcpy(entry_add.Event.EventDataUnion.UserEvent.UserEventData,
			TEST_STR, sizeof(TEST_STR));

	val = saHpiEventLogEntryAdd(session_id, SAHPI_DOMAIN_CONTROLLER_ID, &entry_add);
	if (val != SA_OK) {
		if (val == SA_ERR_HPI_INVALID_REQUEST) {
			ret = SAF_TEST_NOTSUPPORT;
			goto out;
		}
		else {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Add entry the the system event log failed!(Domain)\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out1;
		}
	}

	val = saHpiEventLogEntryGet(session_id, 
			SAHPI_DOMAIN_CONTROLLER_ID,
			SAHPI_NEWEST_ENTRY, &prev_entry_id,
			&next_entry_id,	&entry_get, &rdr, &rpt_entry);
	if (val != SA_OK) {
		printf("  Function \"saHpiEventLogEntryGet\" works abnormally\n");
		printf("  Cannot retrieve the specified event log entry!(Domain)\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
		goto out1;
	}
	
	if (memcmp(&entry_add.Event, &entry_get.Event, sizeof(entry_add.Event))) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Add event log entry function is invalid!(Domain)\n");
		ret = SAF_TEST_FAIL;
	}
	else {
		ret = SAF_TEST_PASS;
	}
out1:
	val = saHpiEventLogStateSet(session_id, SAHPI_DOMAIN_CONTROLLER_ID, enable_old);
	if (val != SA_OK) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Set old event log state value failed!(Domain)\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:
	return ret;
}

int process_resource(SaHpiSessionIdT session_id, SaHpiRptEntryT rpt_entry, callback2_t func)
{
	SaHpiResourceIdT	resource_id = rpt_entry.ResourceId;
	SaHpiSelEntryIdT	prev_entry_id;
	SaHpiSelEntryIdT	next_entry_id;
	SaHpiSelEntryT		entry_get, entry_add;
	SaHpiRdrT		rdr;
	SaHpiRptEntryT 		rpt_entry1;
	SaHpiBoolT		enable_old;
	SaErrorT		val;
	int 			ret = SAF_TEST_NOTSUPPORT;

	if (rpt_entry.ResourceCapabilities & SAHPI_CAPABILITY_SEL) {
		ret = SAF_TEST_PASS;
		/* Disable event log state, to ensure entry is newest eventlog entry */
		val = saHpiEventLogStateGet(session_id, resource_id, &enable_old);
		if (val != SA_OK) {
			printf("  Function \"saHpiEventLogStateGet\" works abnormally!\n");
			printf("  Cannot get the event log state info!(Resource)\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_UNRESOLVED;
			goto out;
		}

		val = saHpiEventLogStateSet(session_id, resource_id, SAHPI_FALSE);
		if (val != SA_OK) {
			if (val == SA_ERR_HPI_INVALID_REQUEST) {
				ret = SAF_TEST_NOTSUPPORT;
				goto out;
			}
			else {
				printf("  Does not conform the expected behaviors!\n");
				printf("  Set event log state to TRUE failed!(Resource)\n");
				printf("  Return value: %s\n", get_hpiAerror_string(val));
				ret = SAF_TEST_UNRESOLVED;
				goto out1;
			}
		}
		
		entry_add.Timestamp = SAHPI_TIME_UNSPECIFIED;
		entry_add.Event.Source = SAHPI_UNSPECIFIED_RESOURCE_ID;
		entry_add.Event.EventType = SAHPI_ET_USER;
		entry_add.Event.Timestamp = SAHPI_TIME_UNSPECIFIED;
		entry_add.Event.Severity = SAHPI_OK;
		memcpy(entry_add.Event.EventDataUnion.UserEvent.UserEventData,
				TEST_STR, sizeof(TEST_STR));

		val = saHpiEventLogEntryAdd(session_id, resource_id, 
				&entry_add);
		if (val != SA_OK) {
			if (val == SA_ERR_HPI_INVALID_REQUEST) {
				ret = SAF_TEST_NOTSUPPORT;
				goto out;
			}
			else {
				printf("  Does not conform the expected behaviors!\n");
				printf("  Add entry the the system event log failed!(Resource)\n");
				printf("  Return value: %s\n", get_hpiAerror_string(val));
				ret = SAF_TEST_FAIL;
				goto out1;
			}
		}

		val = saHpiEventLogEntryGet(session_id, resource_id,
				SAHPI_NEWEST_ENTRY, &prev_entry_id, 
				&next_entry_id,	&entry_get, &rdr, &rpt_entry1);
		if (val != SA_OK) {
			printf("  Function \"saHpiEventLogEntryGet\" works abnormally\n");
			printf("  Cannot retrieve the specified event log entry!(Resource)\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out1;
		}

		if (memcmp(&entry_add.Event, &entry_get.Event, 
					sizeof(entry_add.Event))) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Add event log entry function is invalid!(Resource)\n");
			ret = SAF_TEST_FAIL;
		}
		else {
			ret = SAF_TEST_PASS;
		}

out1:
		val = saHpiEventLogStateSet(session_id, resource_id, enable_old);
		if (val != SA_OK) {
			if (val == SA_ERR_HPI_INVALID_REQUEST) {
				ret = SAF_TEST_NOTSUPPORT;
			}
			else {
				printf("  Does not conform the expected behaviors!\n");
				printf("  Set old event log state value failed!(Resource)\n");
				printf("  Return value: %s\n", get_hpiAerror_string(val));
			}
		}	
	}

out:
	return ret;
}


/**********************************************************
*
*   event_log_process test -- Call saHpiEventLogEntryAdd
*	   Add entries to the system event log.
*
*   Expected return:     saHpiEventLogEntryAdd() returns SA_OK. The entry will be added.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT	val;
	int 		ret = SAF_TEST_PASS;

	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, process_resource, NULL,
			process_domain_eventlog);

	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;
}
