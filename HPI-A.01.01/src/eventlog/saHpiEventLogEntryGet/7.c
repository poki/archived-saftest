/*
 * (C) Copyright IBM Corp. 2004
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Carl McAdams <carlmc@us.ibm.com>
 *      Xiaowei Yang <xiaowei.yang@intel.com>
 */

#include <stdio.h>
#include <saf_test.h>

/**********************************************************
*
*   event_log_process test -- Call saHpiEventLogEntryGet
*                               while passing in a NULL
*                               pointer for NextLogEntry
*   Expected return:  call returns with an error
*
*   Main Function
*      takes no arguments
*      
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main(int argc, char** argv )
{
        SaHpiVersionT           ver;
        SaHpiSessionIdT         session;
        SaErrorT                status;
        int                     retval = SAF_TEST_PASS;
        SaHpiSelEntryIdT        PrevLogEntry;
        SaHpiSelEntryT          LogEntry;
        SaHpiEntryIdT           RptEntry, RptNextEntry;
        SaHpiRptEntryT          Report;
        SaHpiBoolT              SelCapableFound = SAHPI_FALSE;

        
        //  Initialize
        //  
        status = saHpiInitialize(&ver);
        if (status != SA_OK)
        {
                printf(" Function \"saHpiInitialize\" works abnormally!\n");
                printf(" Cannot initialize HPI!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
                retval = SAF_TEST_UNRESOLVED;
        }
        
        //
        //  Open the session
        //
        status = saHpiSessionOpen(SAHPI_DEFAULT_DOMAIN_ID, &session, NULL);
        
        if (status != SA_OK)
        {
                printf(" Function \"saHpiSessionOpen\" works abnormally!\n");
                printf(" Does not conform to expected behavior!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
                retval = SAF_TEST_UNRESOLVED;
        }
       
        //
        // Discover Resources 
        //
        status = saHpiResourcesDiscover(session);
        if (status != SA_OK)
        {
                printf(" Function \"saHpiResourcesDiscover\" works abnormally!\n");
                printf(" cannot generate a report!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
                retval = SAF_TEST_UNRESOLVED;
        }

        //
        // Find a Resource Which supports the Event logs
        //
        RptEntry = SAHPI_FIRST_ENTRY;
        while ((status == SA_OK) && (RptEntry != SAHPI_LAST_ENTRY))
        {
                status = saHpiRptEntryGet(session,
                                          RptEntry,
                                          &RptNextEntry,
                                          &Report);
                if (status != SA_OK)
                {
                        printf(" Function \"saHpiRptEntryGet\" works abnormally!\n");
                        printf(" cannot retrieve a report entry!\n");
                        printf(" Return value: %s\n", get_hpiAerror_string(status));
                        retval = SAF_TEST_UNRESOLVED;
                }
                RptEntry = RptNextEntry;
                if (Report.ResourceCapabilities & SAHPI_CAPABILITY_SEL)
                {
                        RptEntry = SAHPI_LAST_ENTRY;
                        SelCapableFound = SAHPI_TRUE;
                }

        }
        if ((status  == SA_OK) && (SelCapableFound == SAHPI_FALSE))
        {
                printf(" Could not find a resouce which supports event logs \n");
                retval = SAF_TEST_FAIL;
        }
       
        if (retval == SAF_TEST_PASS)
        {
                //
                //  Call saHpiEventLogEntryGet passing in a NULL pointer for NextLogEntry
                //
                status = saHpiEventLogEntryGet(session, 
                                               Report.ResourceId, 
                                               SAHPI_FIRST_ENTRY, 
                                               NULL, 
                                               &PrevLogEntry,
                                               &LogEntry,
                                               NULL, 
                                               NULL);
                if (status == SA_OK)
                {
                        printf(" Function \"saHpiEventLogEntryGet\" works abnormally!\n");
                        printf(" Succeeding in geting info with a bad NextLogEntry pointer!\n");
                        printf(" Return value: %s\n", get_hpiAerror_string(status));
                        retval = SAF_TEST_FAIL; 
                }
        }        
      
        //
        // Close the session
        //
        status = saHpiSessionClose(session);
        if (status != SA_OK)
        {
                printf(" Function \"saHpiFinalize\" works abnormally!\n");
                printf(" Cannot close out the HPI session!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
        }
        
        
        status = saHpiFinalize();
        if (status != SA_OK)
        {
                printf(" Function \"saHpiFinalize\" works abnormally!\n");
                printf(" Cannot clean up HPI!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
        }

        return(retval);
}
