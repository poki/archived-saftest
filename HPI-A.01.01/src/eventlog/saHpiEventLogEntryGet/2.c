/*
 * Copyright (c) 2003-2005, Intel Corporation
 * 
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <saf_test.h>

int process_domain_eventlog(SaHpiSessionIdT session_id)
{
	SaHpiSelEntryIdT	prev_entry_id;
	SaHpiSelEntryIdT	next_entry_id;
	SaHpiSelEntryIdT	current_entry_id;
	SaHpiSelEntryT		eventlog_entry;
	SaHpiRdrT		rdr;
	SaHpiRptEntryT 		rpt_entry1;
	SaErrorT		val;
	int 			ret = SAF_TEST_PASS;

	val = saHpiEventLogEntryGet(session_id,	SAHPI_DOMAIN_CONTROLLER_ID, 
		       SAHPI_NEWEST_ENTRY, &prev_entry_id, &next_entry_id, 
		       &eventlog_entry, &rdr, &rpt_entry1);
	if (val == SA_ERR_HPI_INVALID)
		goto out;
	if (val != SA_OK) {
                // Test to see if the return means that there is an empty log
                if (val == SA_ERR_HPI_NOT_PRESENT)
                {
                        goto out;
                }
		printf("  Does not conform the expected behaviors!\n");
		printf("  Retrieve the newest event log entry failed!(Domain)\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
		goto out;
	}

	while (prev_entry_id != SAHPI_NO_MORE_ENTRIES) {
		current_entry_id = prev_entry_id;
		val = saHpiEventLogEntryGet(session_id, 
			SAHPI_DOMAIN_CONTROLLER_ID, current_entry_id, 
			&prev_entry_id, &next_entry_id, &eventlog_entry, 
			&rdr, &rpt_entry1);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Retrieve the prev event log entry failed!(Domain)\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}
	}

out:
	return ret;
}

int process_resource(SaHpiSessionIdT session_id, SaHpiRptEntryT rpt_entry, callback2_t func)
{
	SaHpiResourceIdT	resource_id = rpt_entry.ResourceId;
	SaHpiSelEntryIdT	prev_entry_id;
	SaHpiSelEntryIdT	next_entry_id;
	SaHpiSelEntryIdT	current_entry_id;
	SaHpiSelEntryT		eventlog_entry;
	SaHpiRdrT		rdr;
	SaHpiRptEntryT 		rpt_entry1;
	SaErrorT		val;
	int 			ret = SAF_TEST_PASS;

	if (rpt_entry.ResourceCapabilities & SAHPI_CAPABILITY_SEL) {
		val = saHpiEventLogEntryGet(session_id, resource_id,
				SAHPI_NEWEST_ENTRY, &prev_entry_id, 
				&next_entry_id, &eventlog_entry, 
				&rdr, &rpt_entry1);
		if (val == SA_ERR_HPI_INVALID)
			goto out;
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Retrieve the newest event log entry failed!(Resource)\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}

		while (prev_entry_id != SAHPI_NO_MORE_ENTRIES) {
			current_entry_id = prev_entry_id;
			val = saHpiEventLogEntryGet(session_id, resource_id,
					current_entry_id, &prev_entry_id, 
					&next_entry_id, &eventlog_entry, 
					&rdr, &rpt_entry1);
			if (val != SA_OK) {
				printf("  Does not conform the expected behaviors!\n");
				printf("  Retrieve the prev event log entry failed!(Resource)\n");
				printf("  Return value: %s\n", get_hpiAerror_string(val));
				ret = SAF_TEST_FAIL;
				goto out;
			}
		}
	}

out:
	return ret;
}


/**********************************************************
*
*   event_log_process test -- Call saHpiEventLogEntryGet
*	   Retrieve an entire list of entries going backrward in the log. EntryId: First: SAHPI_NEWEST_ENTRY Then: returned PreEntryID Until: PreEntryID returned is SAHPI_NO_MORE_ENTRIES.
*
*   Expected return:     saHpiEventLogEntryGet() returns SA_OK, and entire list of entries is tracked backward.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT	val;
	int 		ret = SAF_TEST_PASS;

	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, process_resource, NULL,
			process_domain_eventlog);
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
