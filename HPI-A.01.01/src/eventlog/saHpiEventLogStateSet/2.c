/*
 * (C) Copyright IBM Corp. 2004
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <saf_test.h>

int process_domain_eventlog(SaHpiSessionIdT session_id)
{
	SaHpiBoolT     	enable_new, enable_old;
	SaErrorT	val;
	int 		ret = SAF_TEST_NOTSUPPORT;

	val = saHpiEventLogStateGet(session_id, SAHPI_DOMAIN_CONTROLLER_ID, &enable_old);
	if (val != SA_OK) {
		printf("  Function \"saHpiEventLogStateGet\" works abnormally!\n");
		printf("  Cannot get the event log state info!(Domain)\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
		goto out;
	}

	val = saHpiEventLogStateSet(session_id, SAHPI_DOMAIN_CONTROLLER_ID, SAHPI_FALSE);
	if (val != SA_OK) {
		if (val == SA_ERR_HPI_INVALID_REQUEST) {
			ret = SAF_TEST_NOTSUPPORT;
		}
		else {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Set event log state to TRUE failed!(Domain)\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		}
		goto out;
	}

	val = saHpiEventLogStateGet(session_id, SAHPI_DOMAIN_CONTROLLER_ID,
			&enable_new);
	if (val != SA_OK) {
		printf("  Function \"saHpiEventLogStateGet\" works abnormally!\n");
		printf("  Cannot get the event log state info!(Domain)\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
		goto out1;
	}

	if (enable_new != SAHPI_FALSE) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Set event log state function is invalid!(Domain)\n");
		ret = SAF_TEST_FAIL;
	}
	else {
		ret = SAF_TEST_PASS;
	}
out1:
	val = saHpiEventLogStateSet(session_id, SAHPI_DOMAIN_CONTROLLER_ID, enable_old);
	if (val != SA_OK) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Set old event log state value failed!(Domain)\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
	}

out:
	return ret;
}

int process_resource(SaHpiSessionIdT session_id, SaHpiRptEntryT rpt_entry, callback2_t func)
{
	SaHpiResourceIdT	resource_id = rpt_entry.ResourceId;
	SaHpiBoolT      	enable_new, enable_old;
	SaErrorT		val;
	int 			ret = SAF_TEST_NOTSUPPORT;

	if (rpt_entry.ResourceCapabilities & SAHPI_CAPABILITY_SEL) {
		val = saHpiEventLogStateGet(session_id, resource_id, &enable_old);
		if (val != SA_OK) {
			printf("  Function \"saHpiEventLogStateGet\" works abnormally!\n");
			printf("  Cannot get the event log state info!(Resource)\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}

		val = saHpiEventLogStateSet(session_id, resource_id, SAHPI_FALSE);
		if (val != SA_OK) {
			if (val == SA_ERR_HPI_INVALID_REQUEST) {
				ret = SAF_TEST_NOTSUPPORT;
			}
			else {
				printf("  Does not conform the expected behaviors!\n");
				printf("  Set event log state to TRUE failed!(Resource)\n");
				printf("  Return value: %s\n", get_hpiAerror_string(val));
				ret = SAF_TEST_FAIL;
			}
			goto out;
		}
	
		val = saHpiEventLogStateGet(session_id, resource_id, &enable_new);
		if (val != SA_OK) {
			printf("  Function \"saHpiEventLogStateGet\" works abnormally!\n");
			printf("  Cannot get the event log state info!(Resource)\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out1;
		}

		if (enable_new != SAHPI_FALSE) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Set event log state function is invalid!(Resource)\n");
			ret = SAF_TEST_FAIL;
		}
		else {
			ret = SAF_TEST_PASS;
		}
out1:
		val = saHpiEventLogStateSet(session_id, resource_id, enable_old);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Set old event log state value failed!(Resource)\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		}	
	}

out:
	return ret;
}


/**********************************************************
*
*   event_log_process test -- Call saHpiEventLogStateSet
*	   Pass in an invalid Session ID.
*
*   Expected return:     saHpiEventLogStateSet() returns SA_ERR_HPI_INVALID_SESSION.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT	val;
	int 		ret = SAF_TEST_PASS;

	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_NOTSUPPORT;
		goto out;
	}

	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, process_resource, NULL,
			process_domain_eventlog);
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
