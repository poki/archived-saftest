/*
 * Copyright (c) 2003-2005, Intel Corporation
 * 
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <saf_test.h>

int process_domain_eventlog(SaHpiSessionIdT session_id)
{
	SaErrorT		val;
	int 			ret = SAF_TEST_PASS;

/* We can not insure whether the the function delete the event log entry
 * or only delete the information of event log entry.	*/	
	val = saHpiEventLogEntryDelete(session_id, SAHPI_DOMAIN_CONTROLLER_ID,
			SAHPI_NEWEST_ENTRY);
	if (val != SA_OK && val != SA_ERR_HPI_INVALID_CMD) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Delete entry the the system event log failed!(Domain)\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
	}

	return ret;
}

int process_resource(SaHpiSessionIdT session_id, SaHpiRptEntryT rpt_entry, callback2_t func)
{
	SaHpiResourceIdT	resource_id = rpt_entry.ResourceId;
	SaErrorT		val;
	int 			ret = SAF_TEST_NOTSUPPORT;

/* We can not insure whether the the function delete the event log entry
 * or only delete the information of event log entry.	*/
	if (rpt_entry.ResourceCapabilities & SAHPI_CAPABILITY_SEL) {
		val = saHpiEventLogEntryDelete(session_id, resource_id,
				SAHPI_NEWEST_ENTRY);
		if (val != SA_OK && val != SA_ERR_HPI_INVALID_CMD) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Delete entry the the system event log failed!(Resource)\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		} else {
			ret = SAF_TEST_PASS;
		}
	}

	return ret;
}


/**********************************************************
*
*   event_log_process test -- Call saHpiEventLogEntryDelete
*	   Delete an event log entry. EntryId = SAHPI_NEWEST_ENTRY
*
*   Expected return:     saHpiEventLogEntryDelete() returns SA_OK, and the entry specified is deleted. (If log does not support, it will returns SA_ERR_INVALID_CMD).
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT	val;
	int 		ret = SAF_TEST_PASS;

	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_NOTSUPPORT;
		goto out;
	}

	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, process_resource, NULL,
			process_domain_eventlog);
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
