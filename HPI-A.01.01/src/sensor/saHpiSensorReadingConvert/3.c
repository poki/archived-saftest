/*
 * Copyright (c) 2003-2005, Intel Corporation
 * 
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <string.h>
#include <saf_test.h>

#define CONVERT_DATA	2

int do_sensor(SaHpiSessionIdT session_id, SaHpiResourceIdT resource_id, SaHpiRdrT rdr)
{
	SaHpiSensorReadingT	reading_input;
	SaHpiSensorReadingT	converted_reading;
	SaHpiSensorNumT         num;
	SaErrorT        	val;
	int             	ret = SAF_TEST_UNKNOWN;

	if (rdr.RdrType == SAHPI_SENSOR_RDR) {
		ret = SAF_TEST_PASS;
		num = rdr.RdrTypeUnion.SensorRec.Num;
		memset(&reading_input, 0, sizeof(reading_input));
		reading_input.ValuesPresent = SAHPI_SRF_RAW;
		reading_input.Raw = CONVERT_DATA;
		val = saHpiSensorReadingConvert(session_id, resource_id, num,
				&reading_input, &converted_reading);

/* If the sensor does not support raw reading, SA_ERR_HPI_INVALID_DATA
 *  * is returned, but it still conform the API spec. */
		if (val == SA_ERR_HPI_INVALID_DATA) {                        
			goto out;
                }
                        
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Convert the reading value failed!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}

		if ((converted_reading.ValuesPresent & SAHPI_SRF_INTERPRETED) == NULL) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Invalid reading value after convert!\n");
			ret = SAF_TEST_FAIL;
		}	
	} else {
                ret = SAF_TEST_NOTSUPPORT;
        }

out:
	return ret;
}


/**********************************************************
*
*   sensor_reading_convert test -- Call saHpiSensorReadingConvert
*	   Convert between raw and interpreted sensor values. ReadingInput neither contains raw value nor contains interpreted value.
*
*   Expected return:     saHpiSensorReadingConvert() returns SA_ERR_HPI_INVALID_PARAMS.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT        val;
	int             ret = SAF_TEST_PASS;
	
	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
		goto out;
	}
		
	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, &do_resource, &do_sensor, NULL);
                        
	if (ret == SAF_TEST_NOTSUPPORT)
		printf("  No Sensor in SAHPI_DEFAULT_DOMAIN_ID or sensor does not suport raw readings.\n");
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
	}

out:	
	return ret;	
}
