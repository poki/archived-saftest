/*
 * Copyright (c) 2003-2005, Intel Corporation
 * 
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <string.h>
#include <saf_test.h>

#define THRESHOLDS_TEST_DATA 	2

int enables_cmp(SaHpiSensorEvtEnablesT enables,
                SaHpiSensorEvtEnablesT enables_new)
{
	if (enables.SensorStatus == enables_new.SensorStatus &&
            enables.AssertEvents == enables_new.AssertEvents &&
            enables.DeassertEvents == enables_new.DeassertEvents)
		return 0;
	return -1;	
}

int do_sensor(SaHpiSessionIdT session_id, SaHpiResourceIdT resource_id,
              SaHpiRdrT rdr)
{
	SaHpiSensorEvtEnablesT	enables, enables_old, enables_new;
	SaErrorT        	val;
	SaHpiSensorNumT         num;
	SaHpiEventStateT        event;
	int             	ret = SAF_TEST_NOTSUPPORT;

	/* Need to skip sensors with no events */
	if (rdr.RdrType == SAHPI_SENSOR_RDR &&
	    rdr.RdrTypeUnion.SensorRec.EventCtrl != SAHPI_SEC_NO_EVENTS) {
		ret = SAF_TEST_PASS;
		num = rdr.RdrTypeUnion.SensorRec.Num;
		event = rdr.RdrTypeUnion.SensorRec.Events;

		val = saHpiSensorEventEnablesGet(session_id, resource_id, num,
                                                 &enables_old);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot get the old event status message!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}

		memset(&enables, 0, sizeof(enables));
		enables.SensorStatus = SAHPI_SENSTAT_EVENTS_ENABLED;
		enables.AssertEvents = event;
		enables.DeassertEvents = event;
		
		val = saHpiSensorEventEnablesSet(session_id, resource_id, num,
                                                 &enables);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot set the specified event status messages!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}
		
		val = saHpiSensorEventEnablesGet(session_id, resource_id, num,
                                                 &enables_new);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot get the new event status messages!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out1;
		}

		if (memcmp(&enables, &enables_new, sizeof(enables))) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  The new event status message is invalid!\n");
			ret = SAF_TEST_FAIL;
		}
		
out1:
		val = saHpiSensorEventEnablesSet(session_id, resource_id, num,
                                                 &enables_old);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot set the old event status message!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}
	}

out:
	return ret;
}


/**********************************************************
*
*   sensor_event_enable test -- Call saHpiSensorEventEnableSet
*	   Pass in an invalid SessionID.
*
*   Expected return:     saHpiSensorEventEnableSet() returns SA_ERR_HPI_INVALID_SESSION.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT        val;
	int             ret = SAF_TEST_PASS;
	
	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}
		
	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, &do_resource, &do_sensor,
                             NULL);
	if (ret == SAF_TEST_NOTSUPPORT) {
		printf("  No Sensor with events in SAHPI_DEFAULT_DOMAIN_ID.\n");
	}

	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
