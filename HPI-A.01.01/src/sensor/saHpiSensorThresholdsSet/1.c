/*
 * (C) Copyright IBM Corp. 2004
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <string.h>
#include <saf_test.h>

#define THRESHOLDS_TEST_DATA 	2

int get_reading(SaHpiSensorRangeT *range, SaHpiSensorReadingT *reading)
{
	SaHpiSensorRangeFlagsT flags = range->Flags;

	if (flags & SAHPI_SRF_MIN)
		memcpy(reading, &range->Min, sizeof(range->Min));
	else if (flags & SAHPI_SRF_MAX)
		memcpy(reading, &range->Max, sizeof(range->Max));
	else if (flags & SAHPI_SRF_NORMAL_MIN)
		memcpy(reading, &range->NormalMin, sizeof(range->NormalMin));
	else if (flags & SAHPI_SRF_NORMAL_MAX)
		memcpy(reading, &range->NormalMax, sizeof(range->NormalMax));
	else if (flags & SAHPI_SRF_NOMINAL)
		memcpy(reading, &range->Nominal, sizeof(range->Nominal));
	else
		return -1;

	return 0;
}

void set_data(SaHpiSensorReadingT *reading, SaHpiSensorReadingT *reading_old,
	      SaHpiSensorRangeT *range)
{
	SaHpiSensorInterpretedTypeT     type;
	int ret;
	
	ret = get_reading(range, reading);
	if (ret >= 0)
		return;

	type = reading_old->Interpreted.Type;
	reading->ValuesPresent = SAHPI_SRF_INTERPRETED;
	reading->Interpreted.Type = type;

	switch(type) {
	case SAHPI_SENSOR_INTERPRETED_TYPE_UINT8:
		reading->Interpreted.Value.SensorUint8 = THRESHOLDS_TEST_DATA;
		break;

	case SAHPI_SENSOR_INTERPRETED_TYPE_UINT16:
		reading->Interpreted.Value.SensorUint16 = THRESHOLDS_TEST_DATA;
		break;

	case SAHPI_SENSOR_INTERPRETED_TYPE_UINT32:
		reading->Interpreted.Value.SensorUint32 = THRESHOLDS_TEST_DATA;
		break;

	case SAHPI_SENSOR_INTERPRETED_TYPE_INT8:
		reading->Interpreted.Value.SensorInt8 = THRESHOLDS_TEST_DATA;
		break;

	case SAHPI_SENSOR_INTERPRETED_TYPE_INT16:
		reading->Interpreted.Value.SensorInt16 = THRESHOLDS_TEST_DATA;
		break;

	case SAHPI_SENSOR_INTERPRETED_TYPE_INT32:
		reading->Interpreted.Value.SensorInt32 = THRESHOLDS_TEST_DATA;
		break;

	case SAHPI_SENSOR_INTERPRETED_TYPE_FLOAT32:
		reading->Interpreted.Value.SensorFloat32 = THRESHOLDS_TEST_DATA;
		break;

	case SAHPI_SENSOR_INTERPRETED_TYPE_BUFFER:
		memset(reading->Interpreted.Value.SensorBuffer, 0, 
			sizeof(reading->Interpreted.Value.SensorBuffer));
		reading->Interpreted.Value.SensorBuffer[0] =
			THRESHOLDS_TEST_DATA;
		break;
	}
}

void value_init(SaHpiSensorThresholdsT	*thresholds,
		SaHpiSensorThresholdsT	*thresholds_old,
		SaHpiSensorThdDefnT   	*defn,
		SaHpiSensorRangeT	*range)
{
	SaHpiSensorThdMaskT 	read_thold = defn->ReadThold;
	SaHpiSensorThdMaskT 	write_thold = defn->WriteThold;

	if (read_thold & SAHPI_STM_LOW_CRIT && 
	    write_thold & SAHPI_STM_LOW_CRIT) 
		set_data(&thresholds->LowCritical,&thresholds_old->LowCritical,
			 range);

	if (read_thold & SAHPI_STM_LOW_MAJOR &&
	    write_thold & SAHPI_STM_LOW_MAJOR) 
		set_data(&thresholds->LowMajor, &thresholds_old->LowMajor,
			 range);

	if (read_thold & SAHPI_STM_LOW_MINOR && 
	    write_thold & SAHPI_STM_LOW_MINOR) 
		set_data(&thresholds->LowMinor, &thresholds_old->LowMinor,
			 range);

	if (read_thold & SAHPI_STM_UP_CRIT &&
	    write_thold & SAHPI_STM_UP_CRIT) 
		set_data(&thresholds->UpCritical, &thresholds_old->UpCritical,
			 range);
	
	if (read_thold & SAHPI_STM_UP_MAJOR &&
	    write_thold & SAHPI_STM_UP_MAJOR) 
		set_data(&thresholds->UpMajor, &thresholds_old->UpMajor,
			 range);
	
	if (read_thold & SAHPI_STM_UP_MINOR &&
	    write_thold & SAHPI_STM_UP_MINOR) 
		set_data(&thresholds->UpMinor, &thresholds_old->UpMinor, range);
	
	if (read_thold & SAHPI_STM_UP_HYSTERESIS &&
	    write_thold & SAHPI_STM_UP_HYSTERESIS) 
		set_data(&thresholds->PosThdHysteresis,
			 &thresholds_old->PosThdHysteresis, range);

	if (read_thold & SAHPI_STM_LOW_HYSTERESIS &&
	    write_thold & SAHPI_STM_LOW_HYSTERESIS) 
		set_data(&thresholds->NegThdHysteresis,
			 &thresholds_old->NegThdHysteresis, range);
}

int do_sensor(SaHpiSessionIdT session_id, SaHpiResourceIdT resource_id, SaHpiRdrT rdr)
{
	SaHpiSensorThresholdsT	thresholds, thresholds_old;
	SaHpiSensorThdDefnT	defn;
	SaHpiSensorRangeT	range;
	SaHpiSensorNumT		num;
	SaErrorT        	val;
	int             	ret = SAF_TEST_NOTSUPPORT;

	if (rdr.RdrType == SAHPI_SENSOR_RDR) {
		num = rdr.RdrTypeUnion.SensorRec.Num;
		defn = rdr.RdrTypeUnion.SensorRec.ThresholdDefn;
		range = rdr.RdrTypeUnion.SensorRec.DataFormat.Range;

		if (defn.IsThreshold == SAHPI_FALSE)
			goto out;

		if (!defn.ReadThold || !defn.WriteThold)
			goto out;
		
		ret = SAF_TEST_PASS;
		val = saHpiSensorThresholdsGet(session_id, resource_id,
					       num, &thresholds_old);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot get the old thresholds!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}

		memset(&thresholds, 0, sizeof(thresholds));
		value_init(&thresholds, &thresholds_old, &defn, &range);
		val = saHpiSensorThresholdsSet(session_id, resource_id,
					       num, &thresholds);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot set the specified thresholds!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		}			

		val = saHpiSensorThresholdsSet(session_id, resource_id,
					       num, &thresholds_old);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot set the old thresholds!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		}
	}

out:
	return ret;
}


/**********************************************************
*
*   sensor_thresholds test -- Call saHpiSensorThresholdsSet
*	   Pass in an invalid SessionID.
*
*   Expected return:     saHpiSensorThresholdsSet() returns SA_ERR_HPI_INVALID_SESSION.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT        val;
	int             ret = SAF_TEST_PASS;
	
	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}
		
	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, &do_resource, &do_sensor,
			     NULL);
	if (ret == SAF_TEST_NOTSUPPORT) {
		printf("  No Sensor in SAHPI_DEFAULT_DOMAIN_ID.\n");
	}
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
