/*
 * (C) Copyright IBM Corp. 2004
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <string.h>
#include <saf_test.h>

#define SAHPI_SRF_UNPRESENT	0	
#define THRESHOLDS_TEST_DATA 	2

void value_init(SaHpiSensorThresholdsT *thresholds, SaHpiSensorThdDefnT defn)
{
	SaHpiSensorThdMaskT read_thold = defn.ReadThold;
	SaHpiSensorThdMaskT write_thold = defn.WriteThold;

	if (read_thold & SAHPI_STM_LOW_CRIT && 
			write_thold & SAHPI_STM_LOW_CRIT) {
		thresholds->LowCritical.ValuesPresent = SAHPI_SRF_UNPRESENT;
		thresholds->LowCritical.Interpreted.Type = 
			SAHPI_SENSOR_INTERPRETED_TYPE_UINT32;
		thresholds->LowCritical.Interpreted.Value.SensorUint32 =
			THRESHOLDS_TEST_DATA;
	}

	if (read_thold & SAHPI_STM_LOW_MAJOR &&
			write_thold & SAHPI_STM_LOW_MAJOR) {
		thresholds->LowMajor.ValuesPresent = SAHPI_SRF_UNPRESENT;
		thresholds->LowMajor.Interpreted.Type = 
			SAHPI_SENSOR_INTERPRETED_TYPE_UINT32;
		thresholds->LowMajor.Interpreted.Value.SensorUint32 =
			THRESHOLDS_TEST_DATA;
	}

	if (read_thold & SAHPI_STM_LOW_MINOR && 
			write_thold & SAHPI_STM_LOW_MINOR) {
		thresholds->LowMinor.ValuesPresent = SAHPI_SRF_UNPRESENT;
		thresholds->LowMinor.Interpreted.Type = 
			SAHPI_SENSOR_INTERPRETED_TYPE_UINT32;
		thresholds->LowMinor.Interpreted.Value.SensorUint32 =
			THRESHOLDS_TEST_DATA;
	}

	if (read_thold & SAHPI_STM_UP_CRIT &&
			write_thold & SAHPI_STM_UP_CRIT) {
		thresholds->UpCritical.ValuesPresent = SAHPI_SRF_UNPRESENT;
		thresholds->UpCritical.Interpreted.Type = 
			SAHPI_SENSOR_INTERPRETED_TYPE_UINT32;
		thresholds->UpCritical.Interpreted.Value.SensorUint32 =
			THRESHOLDS_TEST_DATA;
	}
	
	if (read_thold & SAHPI_STM_UP_MAJOR &&
			write_thold & SAHPI_STM_UP_MAJOR) {
		thresholds->UpMajor.ValuesPresent = SAHPI_SRF_UNPRESENT;
		thresholds->UpMajor.Interpreted.Type = 
			SAHPI_SENSOR_INTERPRETED_TYPE_UINT32;
		thresholds->UpMajor.Interpreted.Value.SensorUint32 =
			THRESHOLDS_TEST_DATA;
	}
	
	if (read_thold & SAHPI_STM_UP_MINOR &&
			write_thold & SAHPI_STM_UP_MINOR) {
		thresholds->UpMinor.ValuesPresent = SAHPI_SRF_UNPRESENT;
		thresholds->UpMinor.Interpreted.Type = 
			SAHPI_SENSOR_INTERPRETED_TYPE_UINT32;
		thresholds->UpMinor.Interpreted.Value.SensorUint32 =
			THRESHOLDS_TEST_DATA;
	}
	
	if (read_thold & SAHPI_STM_UP_HYSTERESIS &&
			write_thold & SAHPI_STM_UP_HYSTERESIS) {
		thresholds->PosThdHysteresis.ValuesPresent = 
			SAHPI_SRF_UNPRESENT;
		thresholds->PosThdHysteresis.Interpreted.Type = 
			SAHPI_SENSOR_INTERPRETED_TYPE_UINT32;
		thresholds->PosThdHysteresis.Interpreted.Value.SensorUint32 = 
			THRESHOLDS_TEST_DATA;
	}

	if (read_thold & SAHPI_STM_LOW_HYSTERESIS &&
			write_thold & SAHPI_STM_LOW_HYSTERESIS) {
		thresholds->NegThdHysteresis.ValuesPresent =
			SAHPI_SRF_UNPRESENT;
		thresholds->NegThdHysteresis.Interpreted.Type = 
			SAHPI_SENSOR_INTERPRETED_TYPE_UINT32;
		thresholds->NegThdHysteresis.Interpreted.Value.SensorUint32 = 
			THRESHOLDS_TEST_DATA;
	}
}

int do_sensor(SaHpiSessionIdT session_id, SaHpiResourceIdT resource_id, SaHpiRdrT rdr)
{
	SaHpiSensorThresholdsT	thresholds, thresholds_old, thresholds_new;
	SaHpiSensorNumT 	num;
	SaHpiSensorThdDefnT     defn;
	SaErrorT        	val;
	int             	ret = SAF_TEST_NOTSUPPORT;

	if (rdr.RdrType == SAHPI_SENSOR_RDR) {
		num = rdr.RdrTypeUnion.SensorRec.Num;
		defn = rdr.RdrTypeUnion.SensorRec.ThresholdDefn;

		if (defn.IsThreshold == SAHPI_FALSE)
			goto out;
		if (!defn.ReadThold || !defn.WriteThold)
			goto out;

		ret = SAF_TEST_PASS;
		val = saHpiSensorThresholdsGet(session_id, resource_id,	num, 
				&thresholds_old);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot get the old thresholds!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}

		memset(&thresholds, 0, sizeof(thresholds));
		value_init(&thresholds,
				rdr.RdrTypeUnion.SensorRec.ThresholdDefn);
		val = saHpiSensorThresholdsSet(session_id, resource_id,
				num, &thresholds);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot set the specified thresholds!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}
	
		val = saHpiSensorThresholdsGet(session_id, resource_id, num, 
				&thresholds_new);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot get the new thresholds!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out1;
		}
	
		if (memcmp(&thresholds_new, &thresholds_old, 
					sizeof(thresholds))) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  The new thresholds is invalid!\n");
			ret = SAF_TEST_FAIL;
		}
	
out1:	
		val = saHpiSensorThresholdsSet(session_id, resource_id,
				num, &thresholds_old);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot set the old thresholds!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		}
	}

out:
	return ret;
}


/**********************************************************
*
*   sensor_thresholds test -- Call saHpiSensorThresholdsSet
*	   Pass in an invalid ResourceID.
*
*   Expected return:     saHpiSensorThresholdsSet() returns an error.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT        val;
	int             ret = SAF_TEST_PASS;
	
	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}
		
	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, &do_resource, &do_sensor,
			NULL);
	if (ret == SAF_TEST_NOTSUPPORT) {
		printf("  No Sensor in SAHPI_DEFAULT_DOMAIN_ID.\n");
	}
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
