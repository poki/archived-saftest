/*
 * (C) Copyright IBM Corp. 2004
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Carl McAdams <carlmc@us.ibm.com>
 *      Xiaowei Yang <xiaowei.yang@intel.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <saf_test.h>


//definitions
#define HPI_TEST_DATA_SIZE 0x100

/**********************************************************
*
*   entity_inventory_data test -- Call saHpiEntityInventoryDataWrite
*                                with passing data which is invalid
*
*   Expected return:  call returns with an error
*
*   Main Function
*      takes no arguments
*      
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main(int argc, char** argv )
{
        SaHpiVersionT           ver;
        SaHpiSessionIdT         session;
        SaErrorT                status;
        int                     retval = SAF_TEST_PASS;
        SaHpiEntryIdT           RptEntry, RptNextEntry;
        SaHpiEntryIdT           RdrEntry, RdrNextEntry;
        SaHpiRdrT               DataRecord;
        SaHpiRptEntryT          Report;
        SaHpiBoolT              EntityInventoryRecordFound = SAHPI_FALSE;
        SaHpiInventoryDataT     *InventData;
        SaHpiEirIdT             EirId = 0;
        void*                   allocation;

        //  Initialize
        //  
        // create an allocation for the InventData Structure
        //
        allocation = malloc(HPI_TEST_DATA_SIZE + 16);
        // set proper alignment in the allocation
        InventData = (SaHpiInventoryDataT*)(((SaHpiUint32T)allocation + 
                                             0x10) & 0xFFFFFFF0);

        status = saHpiInitialize(&ver);
        if (status != SA_OK)
        {
                printf(" Function \"saHpiInitialize\" works abnormally!\n");
                printf(" Cannot initialize HPI!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
                retval = SAF_TEST_UNRESOLVED;
        }
        
        //
        //  Open the session
        //
        status = saHpiSessionOpen(SAHPI_DEFAULT_DOMAIN_ID, &session, NULL);
        
        if (status != SA_OK)
        {
                printf(" Function \"saHpiSessionOpen\" works abnormally!\n");
                printf(" Does not conform to expected behavior!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
                retval = SAF_TEST_UNRESOLVED;
        }
       
        //
        // Discover Resources 
        //
        status = saHpiResourcesDiscover(session);
        if (status != SA_OK)
        {
                printf(" Function \"saHpiResourcesDiscover\" works abnormally!\n");
                printf(" cannot generate a report!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
                retval = SAF_TEST_UNRESOLVED;
        }

        //
        // Find an inventory data record
        //
        RptNextEntry = SAHPI_FIRST_ENTRY;
        while ((status == SA_OK) && 
               (RptNextEntry != SAHPI_LAST_ENTRY) && 
               (retval == SAF_TEST_PASS))
        {
                RptEntry = RptNextEntry;
                status = saHpiRptEntryGet(session,
                                          RptEntry,
                                          &RptNextEntry,
                                          &Report);
                if (status != SA_OK)
                {
                        printf(" Function \"saHpiRptEntryGet\" works abnormally!\n");
                        printf(" cannot retrieve a report entry!\n");
                        printf(" Return value: %s\n", get_hpiAerror_string(status));
                        retval = SAF_TEST_UNRESOLVED;
                }
                RptEntry = RptNextEntry;

                RdrNextEntry = SAHPI_FIRST_ENTRY;
                while ((RdrNextEntry != SAHPI_LAST_ENTRY) && 
                       (EntityInventoryRecordFound == SAHPI_FALSE) &&
                       (retval == SAF_TEST_PASS))
                {
                        RdrEntry = RdrNextEntry;
                        status = saHpiRdrGet(session,
                                             Report.ResourceId,
                                             RdrEntry,
                                             &RdrNextEntry,
                                             &DataRecord);
                        if (status != SA_OK)
                        {
                                printf(" Function \"saHpiRdrGet\" works abnormally!\n");
                                printf(" cannot retrieve a record!\n");
                                printf(" Return value: %s\n", get_hpiAerror_string(status));
                                retval = SAF_TEST_UNRESOLVED;
                                break;
                        }
                        // test to see if Record is an inventory record
                        if (DataRecord.RdrType == SAHPI_INVENTORY_RDR)
                        {
                                EirId = DataRecord.RdrTypeUnion.CtrlRec.Num;
                                EntityInventoryRecordFound = SAHPI_TRUE;
                        }
                }
        }

        if (EntityInventoryRecordFound == SAHPI_FALSE)
        {
                retval = SAF_TEST_NOTSUPPORT;
        }

        if (retval == SAF_TEST_PASS)
        {
                //
                //  Call saHpiEntityInventoryDataWrite passing with passing data which is invalid
                //
                InventData->Validity = SAHPI_INVENT_DATA_INVALID;
                status = saHpiEntityInventoryDataWrite(session,
                                                       Report.ResourceId,
                                                       EirId,
                                                       InventData);
                if (status == SA_OK)
                {
                        printf(" Function \"saHpiEntityInventoryDataWrite\" works abnormally!\n");
                        printf(" Succeeding in geting info with with passing data which is invalid!\n");
                        printf(" Return value: %s\n", get_hpiAerror_string(status));
                        retval = SAF_TEST_FAIL; 
                }
        }        
      
        //
        // Close the session
        //
        status = saHpiSessionClose(session);
        if ((status != SA_OK) && (retval == SAF_TEST_PASS))
        {
                printf(" Function \"saHpiFinalize\" works abnormally!\n");
                printf(" Cannot close out the HPI session!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
        }
        
        
        status = saHpiFinalize();
        if ((status != SA_OK) && (retval == SAF_TEST_PASS))
        {
                printf(" Function \"saHpiFinalize\" works abnormally!\n");
                printf(" Cannot clean up HPI!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
        }

        free(allocation);
        return(retval);
}
