/*
 * Copyright (c) 2003-2005, Intel Corporation
 * 
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *     Kevin Gao <kevin.gao@intel.com>
 */

/* temporarily defined here. */ 
#define SA_ERR_INVENT_DATA_TRUNCATED    (SaErrorT)(SA_HPI_ERR_BASE - 1000)

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <saf_test.h>

#define INVENTORY_TEST_DATA	0xff

int do_inventory(SaHpiSessionIdT session_id, SaHpiResourceIdT resource_id, SaHpiRdrT rdr)
{
	SaHpiUint32T		buffer_size;
	SaHpiInventoryDataT     *data_read;
	SaHpiUint32T		actual_size;	
	SaHpiEirIdT		num;
	SaErrorT       		val;
	int            		ret = SAF_TEST_NOTSUPPORT;

	if (rdr.RdrType == SAHPI_INVENTORY_RDR) {
		ret = SAF_TEST_PASS;
		num = rdr.RdrTypeUnion.InventoryRec.EirId;

		//data_read = NULL;
		data_read = (SaHpiInventoryDataT *) malloc(buffer_size);
		buffer_size = 0;
		val = saHpiEntityInventoryDataRead(session_id, resource_id,
				num, buffer_size, data_read, &actual_size);
		if (val != SA_ERR_INVENT_DATA_TRUNCATED) {
			printf("  Function \"saHpiEntityInventoryDataRead\" works abnormally!\n");
			printf("  Cannot check the actual size of the inventory data!");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}
			
		buffer_size = actual_size - 4;
		//data_read = (SaHpiInventoryDataT *) malloc(buffer_size);
		if (data_read == NULL) {
			printf("  Function \"malloc\" works abnormally!\n");
			printf("  memory allocing failed!\n");
			ret = SAF_TEST_FAIL;
			goto out;
		}
		
		val = saHpiEntityInventoryDataRead(session_id, resource_id, 
				num, buffer_size, data_read, &actual_size);
		if (val != SA_ERR_INVENT_DATA_TRUNCATED) {
			printf(" Does not conform the expected behaviors!\n"); 
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		}
			
		free(data_read);
	}
out:
	return ret;
}


/**********************************************************
*
*   entity_inventory_data test -- Call saHpiEntityInventoryDataRead
*	   Return the inventory data for a particular entity associated with a resource. BufferSize is not large enough.
*
*   Expected return:     saHpiEntityInventoryDataRead() returns SA_ERR_INVENT_DATA_TRUNCATED.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT        val;
	int             ret = SAF_TEST_PASS;
	
	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}
		
	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, &do_resource, 
			&do_inventory, NULL);
	if (ret == SAF_TEST_NOTSUPPORT) {
		printf("  No Inventory data in SAHPI_DEFAULT_DOMAIN_ID.\n");
	}
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
