/*
 * Copyright (c) 2003-2005, Intel Corporation
 * 
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <string.h>
#include <saf_test.h>

#define TEST_STR	"Test Tag Components"
#define STR_LEN		20

int tag_cmp(SaHpiTextBufferT *tag, SaHpiTextBufferT *tag_new)
{
	if (tag->DataType == tag_new->DataType &&
	    tag->Language == tag_new->Language &&
	    tag->DataLength == tag_new->DataLength &&
	    !memcmp(tag->Data, tag_new->Data, tag->DataLength))
		return 0;
	else
		return -1;
}

int process_resource(SaHpiSessionIdT session_id, SaHpiRptEntryT rpt_entry, callback2_t funcm)
{
	SaHpiResourceIdT	resource_id;
	SaHpiTextBufferT        tag, tag_old;
	SaErrorT		val;
	int 			ret = SAF_TEST_PASS;
	
	resource_id = rpt_entry.ResourceId;
	tag_old = rpt_entry.ResourceTag;
	
	memset(&tag, 0, sizeof(tag));
	tag.DataType = SAHPI_TL_TYPE_BINARY;
	tag.Language = SAHPI_LANG_ENGLISH;
	tag.DataLength = STR_LEN;
	memcpy(tag.Data, TEST_STR, sizeof(tag.Data));
	val = saHpiResourceTagSet(session_id, resource_id, &tag);
	if (val != SA_OK) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Set new tag failed!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		val = SAF_TEST_FAIL;
		goto out;
	}
	
	val = saHpiRptEntryGetByResourceId(session_id, rpt_entry.ResourceId, 
			&rpt_entry);
	if (val != SA_OK) {
		printf("  Function \"saHpiRptEntryGetByResourceId\" works abnoramally!\n");
		printf("  Cannot get RPT by its resource ID!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
		goto out1;
	}

	if (tag_cmp(&rpt_entry.ResourceTag, &tag)) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Set tag of RPT function is invalid!\n");
		ret = SAF_TEST_FAIL;
	}

out1:
	val = saHpiResourceTagSet(session_id, resource_id, &tag_old);
	if (val != SA_OK) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Set old tag failed!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
	}

out:
	return ret;
}


/**********************************************************
*
*   rpt_entry_process test -- Call saHpiResourceTagSet
*	   Set resource tag for a particular resource.
*
*   Expected return:     saHpiResourceTagSet() returns SA_OK, and ResourceTag is set to the Entry.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT	val;
	int 		ret = SAF_TEST_PASS;

	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, process_resource, NULL,
			NULL);
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
