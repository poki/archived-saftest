/*
 * Copyright (c) 2003-2005, Intel Corporation
 * 
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <saf_test.h>


/**********************************************************
*
*   self_resourceID_get test -- Call saHpiResourceIdGet
*	   Get the resource ID of the resource associated with the entity upon which the caller is running.
*
*   Expected return:     saHpiResourceIdGet() returns SA_OK/SA_ERR_HPI_NOT_PRESENT/SA_ERR_HPI_UNKNOWN based on hardware environment.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 		version;
	SaHpiSessionIdT		session_id;
	SaHpiResourceIdT	resource_id;
	SaErrorT		val;
	int			ret = SAF_TEST_PASS;

	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}
	
	val = saHpiSessionOpen(SAHPI_DEFAULT_DOMAIN_ID, &session_id, NULL);
	if (val != SA_OK) {
		printf("  Function \"saHpiSessionOpen\" works abnormally!\n");	
		printf("  Cannot open the session!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out1;
	}


	saHpiResourcesDiscover(session_id);
        if (val != SA_OK) {
		printf("  Function \"saHpiResourcesDiscover\" works abnormally!\n");
                printf("Can not regenerate the RPT!\n");
                printf("  Return value: %s\n", get_hpiAerror_string(val));
                ret = SAF_TEST_UNRESOLVED;
                goto out2;
        }

	val = saHpiResourceIdGet(session_id, &resource_id);
	if (val != SA_OK && val != SA_ERR_HPI_NOT_PRESENT &&
			val != SA_ERR_HPI_UNKNOWN) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
	}
	
out2:
	val = saHpiSessionClose(session_id);
	if (val != SA_OK) {
		printf("  Function \"saHpiSessionClose\" works abnormally!\n");
		printf("  Cannot close the session\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out1:
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
