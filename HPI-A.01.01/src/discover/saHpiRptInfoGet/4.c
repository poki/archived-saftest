/*
 * (C) Copyright IBM Corp. 2004
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Carl McAdams <carlmc@us.ibm.com>
 *      Xiaowei Yang <xiaowei.yang@intel.com>
 */

#include <stdio.h>
#include <saf_test.h>

// definitions
        // number of seconds from 1/1/1970 to 1/1/2000
#define SECONDS_TO_YEAR_TWO_K 946080000
/**********************************************************
*
*   rpt_entry_process test -- Call saHpiRptInfoGet and validate
*                              the returned data by testing the
*                              date to see if it is passed 
*                              year 2000
*
*   Expected return:  call returns successfull and the timestamp
*                      in the returned data is greater than 
*                      year 2000
*
*   Main Function
*      takes no arguments
*      
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main(int argc, char** argv )
{
        SaHpiVersionT           ver;
        SaHpiSessionIdT         session;
        SaErrorT                status;
        int                     retval = SAF_TEST_PASS;
        SaHpiRptInfoT           ReportInfo;
        
        //  Initialize
        //  
        status = saHpiInitialize(&ver);
        if (status != SA_OK)
        {
                printf(" Function \"saHpiInitialize\" works abnormally!\n");
                printf(" Cannot initialize HPI!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
                retval = SAF_TEST_UNRESOLVED;
        }
        
        //
        //  Open the session
        //
        status = saHpiSessionOpen(SAHPI_DEFAULT_DOMAIN_ID, &session, NULL);
        
        if (status != SA_OK)
        {
                printf(" Function \"saHpiSessionOpen\" works abnormally!\n");
                printf(" Does not conform to expected behavior!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
                retval = SAF_TEST_UNRESOLVED;
        }
        //
        //  Discover Resouces
        //
        status = saHpiResourcesDiscover(session);
        if (status != SA_OK)
        {
                printf(" Function \"saHpiResourcesDiscover\" works abnormally!\n");
                printf(" cannot generate a report!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
                retval = SAF_TEST_UNRESOLVED;
        }
       
        if (retval == SAF_TEST_PASS)
        {
                //
                //  Call saHpiRptInfoGet 
                //
                status = saHpiRptInfoGet(session, &ReportInfo);
                if (status != SA_OK)
                {
                        printf(" Function \"saHpiRptInfoGet\" works abnormally!\n");
                        printf(" Failed to get report information!\n");
                        printf(" Return value: %s\n", get_hpiAerror_string(status));
                        retval = SAF_TEST_FAIL; 
                }
                //
                //  This test will test the data returned by the ReportInfo
                //  The date in the returned data is a 64 unsigned int representing
                //  the number of nanoseconds transpired since 0:00:00 1/1/1970
                //  this test will compare the time to make sure that it returns
                //  a time after year 2000
                //
                // Hopefully this compiler can handle the 64 bit comparison
                else if (ReportInfo.UpdateTimestamp/1000000000 < SECONDS_TO_YEAR_TWO_K)
                {
                        printf(" Function \"saHpiRptInfoGet\" works abnormally!\n");
                        printf(" Update Time Stamp is before the year 2000\n");
                        retval = SAF_TEST_FAIL;
                }
        }        
      
        //
        // Close the session
        //
        status = saHpiSessionClose(session);
        if (status != SA_OK)
        {
                printf(" Function \"saHpiFinalize\" works abnormally!\n");
                printf(" Cannot close out the HPI session!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
        }
        
        
        status = saHpiFinalize();
        if (status != SA_OK)
        {
                printf(" Function \"saHpiFinalize\" works abnormally!\n");
                printf(" Cannot clean up HPI!\n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
        }

        return(retval);
}
