/*
 * Copyright (c) 2003-2005, Intel Corporation
 * 
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Authors:
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <saf_test.h>

int process_resource(SaHpiSessionIdT session_id, SaHpiRptEntryT rpt_entry, callback2_t func)
{
	SaHpiResourceIdT	resource_id;
	SaHpiSeverityT		severity_old, severity_new;	
	SaErrorT		val;
	int 			ret = SAF_TEST_PASS;
	
	resource_id = rpt_entry.ResourceId;
	severity_old = rpt_entry.ResourceSeverity;
	severity_new = SAHPI_CRITICAL;
	if (severity_old == SAHPI_CRITICAL)
		severity_new = SAHPI_MAJOR;
	
	val = saHpiResourceSeveritySet(session_id, resource_id, severity_new);
	if (val != SA_OK) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Set new severity failed!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}
	
	val = saHpiRptEntryGetByResourceId(session_id, rpt_entry.ResourceId, 
			&rpt_entry);
	if (val != SA_OK) {
		printf("  Function \"saHpiRptEntryGetByResourceId\" works abnoramally!\n");
		printf("  Cannot get RPT by its resource ID!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out1;
	}

	if (rpt_entry.ResourceSeverity != severity_new) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Invalid severity level of RPT!\n");
		ret = SAF_TEST_FAIL;
	}

out1:
	val = saHpiResourceSeveritySet(session_id, resource_id, severity_old);
	if (val != SA_OK) {
		printf("  Does not conform the expected behaviors!\n");
		printf("  Set old severity failed!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_FAIL;
	}

out:
	return ret;
}


/**********************************************************
*
*   rpt_entry_process test -- Call saHpiResourceSeveritySet
*	   Set severity level of event issued when the resource unexpectedly becomes unavailable to the HPI.
*
*   Expected return:     saHpiResourceSeveritySet() returns SA_OK, and Severity is set to the Entry.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT	val;
	int 		ret = SAF_TEST_PASS;

	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}

	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, process_resource, NULL,
			NULL);
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
