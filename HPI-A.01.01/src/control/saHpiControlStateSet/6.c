/*
 * (C) Copyright IBM Corp. 2004
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <saf_test.h>

#define CONTROL_TEST_DATA 	2
#define CONTROL_TEST_STR	"Test Control component"
#define CONTROL_TEST_LEN	23
#define OEM_CONTROL_BODY_LENGTH	1

void value_init(SaHpiCtrlStateT *state, SaHpiCtrlTypeT type)
{
	state->Type = type;
	switch(type) {
		case SAHPI_CTRL_TYPE_DIGITAL:
			state->StateUnion.Digital = SAHPI_CTRL_STATE_OFF;
			break;
		case SAHPI_CTRL_TYPE_DISCRETE:
			state->StateUnion.Discrete = CONTROL_TEST_DATA;
			break;
		case SAHPI_CTRL_TYPE_ANALOG:
			state->StateUnion.Analog = CONTROL_TEST_DATA;
			break;
		case SAHPI_CTRL_TYPE_STREAM:
			state->StateUnion.Stream.Repeat = SAHPI_TRUE;
			memcpy(&(state->StateUnion.Stream.Stream), 
				CONTROL_TEST_STR, 
				sizeof(state->StateUnion.Stream.Stream));
			state->StateUnion.Stream.StreamLength = 
				CONTROL_TEST_LEN;
			break;
		case SAHPI_CTRL_TYPE_TEXT:
			state->StateUnion.Text.Line = SAHPI_TLN_ALL_LINES;
			state->StateUnion.Text.Text.DataType = 
				SAHPI_TL_TYPE_BINARY;
			state->StateUnion.Text.Text.Language =
				SAHPI_LANG_ENGLISH;
			state->StateUnion.Text.Text.DataLength = 
				CONTROL_TEST_LEN;
			memcpy(&(state->StateUnion.Text.Text.Data),
				CONTROL_TEST_STR, 
				sizeof(state->StateUnion.Text.Text.Data));
			break;
		case SAHPI_CTRL_TYPE_OEM:
			state->StateUnion.Oem.MId = CONTROL_TEST_DATA;
			state->StateUnion.Oem.BodyLength = 
				OEM_CONTROL_BODY_LENGTH;
			state->StateUnion.Oem.Body[0] = CONTROL_TEST_DATA;
			break;
	}
}

int cs_cmp(SaHpiCtrlStateT *ctrl_state, SaHpiCtrlStateT *ctrl_state_new)
{
	SaHpiCtrlStateUnionT *s_union = &ctrl_state->StateUnion;
	SaHpiCtrlStateUnionT *s_union_new = &ctrl_state_new->StateUnion;
	
	if (ctrl_state->Type != ctrl_state_new->Type)
		return -1;

	switch (ctrl_state->Type) {
	case SAHPI_CTRL_TYPE_STREAM:
		if (s_union->Stream.Repeat == s_union_new->Stream.Repeat &&
				
		    s_union->Stream.StreamLength == 
		    s_union_new->Stream.StreamLength &&
		    !memcmp(s_union->Stream.Stream, s_union_new->Stream.Stream,
			    s_union->Stream.StreamLength))
			return 0;
		else
			return -1;

	case SAHPI_CTRL_TYPE_TEXT:
		if (s_union->Text.Line == s_union_new->Text.Line &&
		    s_union->Text.Text.DataType ==
		    s_union_new->Text.Text.DataType &&
		    s_union->Text.Text.Language ==
		    s_union_new->Text.Text.Language &&
		    s_union->Text.Text.DataLength ==
		    s_union_new->Text.Text.DataLength &&
		    memcmp(s_union->Text.Text.Data, s_union_new->Text.Text.Data,
			   s_union->Text.Text.DataLength))
			return 0;
		else
			return -1;

	case SAHPI_CTRL_TYPE_OEM:
		if (s_union->Oem.MId == s_union_new->Oem.MId &&
		    s_union->Oem.BodyLength == s_union_new->Oem.BodyLength &&
		    memcmp(s_union->Oem.Body, s_union_new->Oem.Body,
			   s_union->Oem.BodyLength))
			return 0;
		else
			return -1;

	default:
		memcmp(ctrl_state, ctrl_state_new, sizeof(*ctrl_state));
	}
	
	return 0;
}

int do_ctrl(SaHpiSessionIdT session_id, SaHpiResourceIdT resource_id, SaHpiRdrT rdr)
{
	SaHpiCtrlStateT	ctrl_state, ctrl_state_new, ctrl_state_old;
	SaErrorT       	val;
	SaHpiCtrlNumT	num;
	int            	ret = SAF_TEST_NOTSUPPORT;
	int             retry;

	if (rdr.RdrType == SAHPI_CTRL_RDR) {
		ret = SAF_TEST_PASS;
		num = rdr.RdrTypeUnion.CtrlRec.Num; 

		val = saHpiControlStateGet(session_id, resource_id, num, 
				&ctrl_state_old);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot get the old control state!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}

		memset(&ctrl_state, 0, sizeof(ctrl_state));
		value_init(&ctrl_state, ctrl_state_old.Type);

		retry = 0;
 RETRY:			
		val = saHpiControlStateSet(session_id, resource_id, num, 
				&ctrl_state);
		
		if (val == SA_ERR_HPI_BUSY && retry < 5) {
			sleep(2);
			retry++;
			goto RETRY;
		}

		/* Some systems let you read a value but not to set it */
		if (val != SA_OK && val != SA_ERR_HPI_INVALID_CMD) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot set the specified control state!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			int i;
			for (i = 0; i < SAHPI_MAX_ENTITY_PATH; i++) {
				printf("{%d,%d}",rdr.Entity.Entry[i].EntityType,rdr.Entity.Entry[i].EntityInstance);
			}
			printf("\n");
			ret = SAF_TEST_FAIL;
			goto out;
		}

		retry = 0;
	RETRY_GET:		
		val = saHpiControlStateGet(session_id, resource_id, num, 
				&ctrl_state_new);

		if (val == SA_ERR_HPI_BUSY && retry < 5) {
			sleep(2);
			retry++;
			goto RETRY_GET;
		}

		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot get the new control state!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out1;
		}

		if (cs_cmp(&ctrl_state, &ctrl_state_new)) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  The new control state is invalid!\n");
			ret = SAF_TEST_FAIL;
		}
			
out1:
		retry = 0;
	RETRY_SET_ORIG:
		val = saHpiControlStateSet(session_id, resource_id, num, 
				&ctrl_state_old);

		if (val == SA_ERR_HPI_BUSY && retry < 5) {
			sleep(2);
			retry++;
			goto RETRY_SET_ORIG;
		}

		/* Some systems let you read a value but not to set it */
		if (val != SA_OK && val != SA_ERR_HPI_INVALID_CMD) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot set the old control state!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;
		}
	}

out:
	return ret;
}


/**********************************************************
*
*   control_state test -- Call saHpiControlStateSet
*	   Set the state of the specified control object.
*
*   Expected return:     saHpiControlStateSet() returns SA_OK. saHpiControlStateGet() returns SA_OK.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT        val;
	int             ret = SAF_TEST_PASS;
	
	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}
		
	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, &do_resource, &do_ctrl,
			NULL);
	if (ret == SAF_TEST_NOTSUPPORT) {
		printf("  No Control in SAHPI_DEFAULT_DOMAIN_ID.\n");
	}
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
