/*
 * (C) Copyright IBM Corp. 2004
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *      Carl McAdams <carlmc@us.ibm.com>
 *      Xiaowei Yang <xiaowei.yang@intel.com>
 */

#include <stdio.h>
#include <saf_test.h>
/**********************************************************
*
*   session_open test -- Opening a new session without intializing
*
*   Expected return:  call returns with SA_ERR_HPI_UNINITIALIZED
*
*   Main Function
*      takes no arguments
*      
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main(int argc, char** argv )
{
        SaHpiSessionIdT         session;
        SaErrorT                status;
        int                     retval = SAF_TEST_PASS;
        
        //
        // Initialize HPI
        //          
        
        status = saHpiSessionOpen(SAHPI_DEFAULT_DOMAIN_ID, &session, NULL);
        
        if (status != SA_ERR_HPI_UNINITIALIZED)
        {
                printf(" Function \"saHpiSessionOpen\" works abnormally!\n");
                printf(" Function works does not error out properly!\n");
                printf(" Expected SA_ERR_HPI_UNINITIALIZED -- \n");
                printf(" Return value: %s\n", get_hpiAerror_string(status));
                retval = SAF_TEST_FAIL;
        }

        // If by chance the call did work, then clean up
        if (status == SA_OK)        
        {
                //Clean up
                saHpiSessionClose(session);
        }
        return(retval);

}
