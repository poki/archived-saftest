/*
 * (C) Copyright IBM Corp. 2004
 * Copyright (c) 2005, Intel Corporation
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place - Suite 330, Boston, MA 02111-1307 USA.
 *
 * Author(s):
 *     Kevin Gao <kevin.gao@intel.com>
 */

#include <stdio.h>
#include <string.h>
#include <saf_test.h>

static SaHpiWatchdogT wd = {
	.Log			= SAHPI_FALSE,
	.Running		= SAHPI_TRUE,
	.TimerUse		= SAHPI_WTU_SMS_OS,
	.TimerAction		= SAHPI_WA_RESET,
	.PretimerInterrupt	= SAHPI_WPI_NONE,
	.PreTimeoutInterval	= 0,
	.TimerUseExpFlags	= SAHPI_WATCHDOG_EXP_SMS_OS,
	.InitialCount		= 900000, //900 seconds
	.PresentCount		= 0
};

int do_watchdogtimer(SaHpiSessionIdT session_id, SaHpiResourceIdT resource_id, SaHpiRdrT rdr)
{
	SaHpiWatchdogT wd_old;
	SaHpiWatchdogNumT wd_num;
	SaErrorT val;
	int ret = SAF_TEST_NOTSUPPORT;

	if (rdr.RdrType == SAHPI_WATCHDOG_RDR) {
		ret = SAF_TEST_PASS;
		wd_num = rdr.RdrTypeUnion.WatchdogRec.WatchdogNum;

		val = saHpiWatchdogTimerGet(session_id, resource_id, wd_num, 
				&wd_old);
		if (val != SA_OK) {
			printf("  saHpiWatchdogTimerGet failed!\n");
			printf("  Cannot get the old watchdog timer!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;				
		}
		
		val = saHpiWatchdogTimerSet(session_id, resource_id, wd_num, 
				&wd);
		if (val != SA_OK) {
			printf("  saHpiWatchdogTimerSet failed!\n");
			printf("  Cannot set the config info for the watchdog timer!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out1;				
		}

		val = saHpiWatchdogTimerReset(session_id, resource_id, wd_num);
		if (val != SA_OK) {
			printf("  Does not conform the expected behaviors!\n");
			printf("  Cannot reset the new watchdog timer!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
		}

out1:
		val = saHpiWatchdogTimerSet(session_id ,resource_id ,wd_num, 
				&wd_old);
		if (val != SA_OK) {
			printf("  saHpiWatchdogTimerSet failed!\n");
			printf("  Cannot set the old config info for the watchdog timer!\n");
			printf("  Return value: %s\n", get_hpiAerror_string(val));
			ret = SAF_TEST_FAIL;
			goto out;				
		}
	}

out:
	return ret;
}


/**********************************************************
*
*   watchdog_timer test -- Call saHpiWatchdogTimerSet
*	   Pass in an invalid SessionID.
*
*   Expected return:     saHpiWatchdogTimerSet() returns SA_ERR_HPI_INVALID_SESSION.
*
*   Main Function
*      takes no arguments
*
*       returns: SAF_TEST_PASS when successfull
*                SAF_TEST_FAIL when an unexpected error occurs
*************************************************************/

int main()
{
	SaHpiVersionT 	version;
	SaErrorT        val;
	int             ret = SAF_TEST_PASS;
	
	val = saHpiInitialize(&version);
	if (val != SA_OK) {
		printf("  Function \"saHpiInitialize\" works abnormally!\n");
		printf("  Cannot initialize HPI!\n");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
		ret = SAF_TEST_UNRESOLVED;
		goto out;
	}
		
	ret = process_domain(SAHPI_DEFAULT_DOMAIN_ID, &do_resource, 
			&do_watchdogtimer, NULL);

        if (ret == SAF_TEST_NOTSUPPORT) {
		printf("  No Watchdog timer in SAHPI_DEFAULT_DOMAIN_ID.\n");		
	}
	
	val = saHpiFinalize();
	if (val != SA_OK) {
		printf("  Function \"saHpiFinalize\" works abnormally!\n");
		printf("  Cannot cleanup HPI");
		printf("  Return value: %s\n", get_hpiAerror_string(val));
	}

out:	
	return ret;	
}
